
[ok]	Opdater manual + omd�b til FFEM_manual.pdf



BUGS: 
(Disse bugs st�r ogs� som kommentarer l�ngere nede... )
[ok]	Knapper til sigmax og sigmay. 
[ok]	Sort box n�r en distrib. underst�tning flyttes, og der er sp�ndinger til stede.
	... Samme problem: man kan ikke tegne nyt dom�ne.....
[ok*]	Det fungerer ikke at skravere de fordelte underst�tninger, da der vises nogle sp�ndinger indenunder. (*: Der skraveres ikke l�ngere men fyldes ud med farve)
[ok]	Texturet fader til sort i toppen, n�r l�rredet er bemalet i toppen.
[ok*]	Texturet fader mod bl�t omkring en fordelt underst�tning. (*: Nu fader det bare mod blot p� indersiden af den fordelte underst�tning...)
[ok*]	Flytbare underst�tninger flytter sig ikke altid med. (*: Fungerer stadigt med gc.timedShowDisp, hvilket sutter)

Sm� bugs:
[ok*]	Hatch bliver mindre n�r elementmeshet er st�rre. (*: Der skraveres ikke l�ngere men fyldes ud med farve)





1. PRIORITET:

[ok]	Multi touch
[ok]	Linjeunderst�tning
[ok*]	About + vejledning (web) (*: kedelig grafik i About og vejledning er blot pdf i dropboxlink. Vejledning er uddateret.)
[ok]	Plot-interpolering (farver) 
[ok]	Hovedsp�ndinger



2. PRIORITET:

[ok]	Ikoner til Hovedsp�ndinger
[ok]	Ikon til orthogonal flytning
[LG]	Fix clear-funktionen... (LG = ligegyldigt)
[ok]	Fixed supports er default
[ok]	Underst�tningsrotation - nedad!
[ok]	Multi-slet
[ok]	Fix bug: underst�tningerne bev�ger sig ikke altid med...



ANDET - ikke et must:

[Nej]	Bl�d rand
