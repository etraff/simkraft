using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NuTracking : MonoBehaviour
{
	private static NuTracking staticInstance = null;
	
	public static NuTracking Instance
	{
		get 
		{
			if (staticInstance == null)
			{
				staticInstance = FindObjectOfType(typeof(NuTracking)) as NuTracking;
			}
			
			if (staticInstance == null)
			{
				GameObject obj = new GameObject("_NuTracking_Manager");
				DontDestroyOnLoad(obj);
				staticInstance = obj.AddComponent<NuTracking>();
				staticInstance.Initialize();
				Debug.Log("_NuTracking_Manager created");
			}
			
			return staticInstance;
		}
	}
	
	public void Initialize ()
	{
	}
	
	
	void OnApplicationQuit()
	{
		staticInstance = null;
	}
	
	private bool isStarted = false;
	
	public void Startup(string GAAccountCode, string hostDomain, string gameShortName)
	{
		Startup(GAAccountCode, hostDomain, gameShortName, true, true, false);
	}
	
	public void Startup(string GAAccountCode, string hostDomain, string gameShortName, bool wantObfuscateGlobalIP, bool wantMonitorEditor, bool wantLog)
	{
		NuTrackingHelpers.GameShortName = gameShortName;
		NuTrackingHelpers.GAAccountCode = GAAccountCode;
		NuTrackingHelpers.GAHostDomain = hostDomain;
		NuTrackingHelpers.WantObfuscateGlobalIP = wantObfuscateGlobalIP;
		NuTrackingHelpers.WantMonitorEditor = wantMonitorEditor;
		NuTrackingHelpers.WantLog = wantLog;
		
		NuTrackingHelpers.GameStartupTimeInUTC = DateTime.UtcNow;
		
		NuTrackingHelpers.Log (String.Format ("User-Agent is {0}, PlayerID is {1}", NuTrackingHelpers.ConstructUserAgentString (), NuTrackingHelpers.GetVisitorIdInCookie ()));
		
		isStarted = true;
		
		// detect new day start, must be called before constructing cookies
		bool isAnotherDay = false;
		if (!NuTrackingHelpers.HasPreviousVisitTimeStampInCookie ())
		{
			isAnotherDay = true;
		}
		else
		{
			int currentTimeStamp = NuTrackingHelpers.GetCurrentTimeStamp ();
			int previousTimeStamp = NuTrackingHelpers.GetPreviousVisitTimeStampInCookie ();
			NuTrackingHelpers.Log (string.Format ("currentTimeStamp = {0}, previousTimeStamp={1}", currentTimeStamp, previousTimeStamp));
			isAnotherDay = !NuTrackingHelpers.InSameDay (currentTimeStamp, previousTimeStamp);
		}

		// visitCount / previouStimeStamp will be updated
		NuTrackingHelpers.ConstructCookies ();
		NuTrackingHelpers.Log (string.Format ("__utma={0}, __utmz={1}", NuTrackingHelpers.UtmaCookie, NuTrackingHelpers.UtmzCookie));
		
#if !UNITY_WEBPLAYER
		gaService = new NuGAService ();
#endif
		
		// global tracking		
		AddPageView ("Every Access", NuTrackingHelpers.PageRoot + "/everyaccess");
		AddPageView ("Every Access", NuTrackingHelpers.PageRoot + "/" + NuTrackingHelpers.PrimaryPageName + "/everyaccess");
		if (isAnotherDay)
		{
			AddPageView ("Daily Access", NuTrackingHelpers.PageRoot + "/dailyaccess");
			AddPageView ("Daily Access", NuTrackingHelpers.PageRoot + "/" + NuTrackingHelpers.PrimaryPageName + "/dailyaccess");
		}
		
#if !UNITY_WEBPLAYER
		StartCoroutine (NuTrackingHelpers.RetrieveGlobalIP ());
#endif
		StartCoroutine(FlushTrackingQuestsAsync());
	}
	
	private IEnumerator FlushTrackingQuestsAsync()
	{
		while (true)
		{
#if !UNITY_WEBPLAYER
			while (!NuTrackingHelpers.IsGlobalIPRetrieved())
			{
				yield return new WaitForSeconds(2.0f);
			}
#endif
			if (requestQueue.Count == 0)
			{
				yield return new WaitForSeconds(2.0f);
			}
			else
			{
				foreach (TrackingRequest request in requestQueue)
				{
					if (request.gaEvent == null && request.gaTransaction == null)
					{
#if UNITY_WEBPLAYER
						NuTrackingHelpers.WebPlayerTrack("pageTracker._trackPageview", new object[]{request.pageUrl});
#else
						string url = gaService.GetPageViewURL(request.pageTitle, request.pageUrl);
						NuGARequest.IssueRequest(this, new List<string>() { url });
#endif
					}
					else if (request.gaEvent != null)
					{
#if UNITY_WEBPLAYER
						NuTrackingHelpers.WebPlayerTrack("pageTracker._trackEvent", new object[]{request.gaEvent.Object, request.gaEvent.Action, request.gaEvent.Label, request.gaEvent.Value});
#else
						string urlEvent = gaService.GetEventURL(request.gaEvent, request.pageTitle, request.pageUrl);
						NuGARequest.IssueRequest(this, new List<string>() {urlEvent});
#endif
					}
					else if (request.gaTransaction != null)
					{
#if UNITY_WEBPLAYER
						string id = NuTrackingHelpers.GetRandomIdString();
						NuTrackingHelpers.WebPlayerTrack("pageTracker._addTrans", new object[]{
							id,
							NuTrackingHelpers.GameShortName,
							(request.gaTransaction.ProductUnitPrice*request.gaTransaction.ProductQuantity).ToString("#.##"),
							"0.00",
							"0.00",
							"CityNA",
							"StateNA",
							"CountryNA"
						});
						NuTrackingHelpers.WebPlayerTrack("pageTracker._addItem", new object[]{
							id,
							request.gaTransaction.ProductCode,
							request.gaTransaction.ProductName,
							"none",
							request.gaTransaction.ProductUnitPrice.ToString("#.##"),
							request.gaTransaction.ProductQuantity.ToString()
						});
						NuTrackingHelpers.WebPlayerTrack("pageTracker._trackTrans", new object[] {});
#else
						string transId;
						string url1 = gaService.GetTransactionURL(request.gaTransaction, out transId);
						string url2 = gaService.GetItemURL(request.gaTransaction, transId);
						NuGARequest.IssueRequest(this, new List<string>() {url1, url2});
#endif
					}
					else
					{
						NuTrackingHelpers.Assert(false);
					}
				}
				requestQueue.Clear();
			}
		}
	}
	
	public void Update()
	{
	}
	
	public void AddGenericEvent(string _object, string _action, string _label = null, int? _value = null)
	{
		AddGenericEvent("All Event Page", NuTrackingHelpers.PageRoot + "/event/all", _object, _action, _label, _value);
		AddGenericEvent(NuTrackingHelpers.PrimaryPageName + " Event Page", NuTrackingHelpers.PageRoot + String.Format("/event/{0}/{1}", NuTrackingHelpers.PrimaryPageName, NuTrackingHelpers.SecondaryPageName), _object, _action, _label, _value);
	}
	
	public void AddGenericEvent(string _pageTitle, string _pageUrl, string _object, string _action, string _label = null, int? _value = null)
	{
		if (!isStarted)
		{
			Debug.LogError("NuTracking not started");
			return;
		}
#if UNITY_EDITOR
		if (!NuTrackingHelpers.WantMonitorEditor) return;
#endif
		NuGAEvent localEvent = new NuGAEvent(_object, _action, _label, _value);
		TrackingRequest request = new TrackingRequest();
		request.gaEvent = localEvent;
		request.gaTransaction = null;
		request.pageTitle = _pageTitle;
		request.pageUrl = _pageUrl;		
		requestQueue.Add(request);
	}
	
	public void AddGenericTransaction(string _productSku, string _productName, float _price, int _quantity)
	{
		AddGenericTransaction("All Transaction Page", NuTrackingHelpers.PageRoot + "/transaction/all", _productSku, _productName, _price, _quantity);
		AddGenericTransaction(NuTrackingHelpers.PrimaryPageName + " Transaction Page", NuTrackingHelpers.PageRoot + String.Format("/transaction/{0}/{1}", NuTrackingHelpers.PrimaryPageName, NuTrackingHelpers.SecondaryPageName), _productSku, _productName, _price, _quantity);
	}
	
	public void AddGenericTransaction(string _pageTitle, string _pageUrl, string _productSku, string _productName, float _price, int _quantity)
	{
		if (!isStarted)
		{
			Debug.LogError("NuTracking not started");
			return;
		}
#if UNITY_EDITOR
		if (!NuTrackingHelpers.WantMonitorEditor) return;
#endif
		NuGATransaction localTransaction = new NuGATransaction(_productSku, _productName, _price, _quantity);
		TrackingRequest request = new TrackingRequest();
		request.gaEvent = null;
		request.gaTransaction = localTransaction;
		request.pageTitle = _pageTitle;
		request.pageUrl = _pageUrl;		
		requestQueue.Add(request);
	}
	
	public void AddPageView(string _pageTitle, string _pageUrl)
	{
#if UNITY_EDITOR
		if (!NuTrackingHelpers.WantMonitorEditor) return;
#endif
		TrackingRequest request = new TrackingRequest();
		request.gaEvent = null;
		request.gaTransaction = null;
		request.pageTitle = _pageTitle;
		request.pageUrl = _pageUrl;		
		requestQueue.Add(request);
	}
	
	public void RecordPlatformInformation()
	{
		string platform = NuTrackingHelpers.GetPlatformName();
		AddGenericEvent("Platform Information", NuTrackingHelpers.PageRoot + "/info/platform", "Platform", platform);
		AddGenericEvent("Platform Information", NuTrackingHelpers.PageRoot + "/info/platform", "Model", SystemInfo.deviceModel);
		AddGenericEvent("Platform Information", NuTrackingHelpers.PageRoot + "/info/platform", "SystemName", SystemInfo.operatingSystem);
		
		RecordIPhoneInformation();
	}
	
	public void RecordIPhoneInformation()
	{
#if UNITY_IPHONE
		AddGenericEvent("iPhone Information", NuTrackingHelpers.PageRoot + "/info/iphone", "Generation", UnityEngine.iOS.Device.generation.ToString());
#endif
	}
	
#if !UNITY_WEBPLAYER
	private NuGAService gaService;
#endif
	
	private class TrackingRequest
	{
		public string pageTitle;
		public string pageUrl;
		public NuGAEvent gaEvent;
		public NuGATransaction gaTransaction;
	}
	private List<TrackingRequest> requestQueue = new List<TrackingRequest>();
}
