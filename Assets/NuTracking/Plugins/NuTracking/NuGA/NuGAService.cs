using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

// reference: Here's some code to make a Google Analytics non-Javascript call
// http://www.mattromaine.com/projects/code/AnalyticsCode.html
// http://code.google.com/apis/analytics/docs/tracking/gaTrackingTroubleshooting.html

public class NuGAService 
{
	public NuGAService()
	{
	}
	
	private const string ReferralSource = "(direct)";
	private const string Medium = "(none)";
	private const string Campaign = "(direct)";

	private string BuildUtmcCookieString()  
	{
		string utma = NuTrackingHelpers.UtmaCookie;
		string utmz = NuTrackingHelpers.UtmzCookie;

		string utmcc = Uri.EscapeDataString(String.Format("__utma={0};+__utmz={1};",
		                                                  utma,
		                                                  utmz
		                                    	));
		return utmcc;
	}
	
	public void AddPageViewParameters(List<KeyValuePair<string, string>> result, string _pageTitle, string _pageUrl)
	{
		string pageTitle = (_pageTitle != null ? Uri.EscapeDataString(_pageTitle): "");
		
		result.Add(new KeyValuePair<string,string>("utmcs", "UTF-8"));								
		result.Add(new KeyValuePair<string,string>("utmsr", "-"));									
		result.Add(new KeyValuePair<string,string>("utmsc", "-"));									
		result.Add(new KeyValuePair<string,string>("utmul", "-"));									
		result.Add(new KeyValuePair<string,string>("utmje", "0"));										
		result.Add(new KeyValuePair<string,string>("utmfl", "-"));										
		result.Add(new KeyValuePair<string,string>("utmdt", pageTitle));								
		result.Add(new KeyValuePair<string,string>("utmhid", NuTrackingHelpers.GenerateHash(pageTitle)));	
		result.Add(new KeyValuePair<string,string>("utmp", _pageUrl));									
	}
	
	public void AddEventParameters(List<KeyValuePair<string, string>> result, NuGAEvent _event, string _pageTitle, string _pageUrl )
	{
		AddPageViewParameters(result, _pageTitle, _pageUrl);
		
		string eventString = _event.ToString();
		
		result.Add(new KeyValuePair<string, string>("utme", Uri.EscapeDataString(eventString)));
		result.Add(new KeyValuePair<string, string>("gaq","1"));
		result.Add(new KeyValuePair<string, string>("utmt", "event"));
	}
	
	public string AddTransactionParameters(List<KeyValuePair<string, string>> result, NuGATransaction _transaction)
	{
		string transId = NuTrackingHelpers.GetRandomIdString();
		result.Add(new KeyValuePair<string, string>("utmtci", "CityNA"));
		result.Add(new KeyValuePair<string, string>("utmtrg", "StateNA"));
		result.Add(new KeyValuePair<string, string>("utmtco", "CountryNA"));
		result.Add(new KeyValuePair<string, string>("utmtst", Uri.EscapeDataString(NuTrackingHelpers.GameShortName)));
		result.Add(new KeyValuePair<string, string>("utmtid", transId));
		result.Add(new KeyValuePair<string, string>("utmtsp", "0.00"));
		result.Add(new KeyValuePair<string, string>("utmtto", (_transaction.ProductUnitPrice * _transaction.ProductQuantity).ToString("#.##")));
		result.Add(new KeyValuePair<string, string>("utmttx", "0.00"));
		result.Add(new KeyValuePair<string, string>("utmt", "tran"));
		return transId;
	}
	
	public void AddItemParameters(List<KeyValuePair<string, string>> result, NuGATransaction _transaction, string _transId)
	{
		result.Add(new KeyValuePair<string, string>("utmipc", Uri.EscapeDataString(_transaction.ProductCode)));
		result.Add(new KeyValuePair<string, string>("utmipn", Uri.EscapeDataString(_transaction.ProductName)));
		result.Add(new KeyValuePair<string, string>("utmipr", _transaction.ProductUnitPrice.ToString("#.##")));
		result.Add(new KeyValuePair<string, string>("utmiqt", _transaction.ProductQuantity.ToString()));
		result.Add(new KeyValuePair<string, string>("utmiva", "none"));
		result.Add(new KeyValuePair<string, string>("utmtid", _transId));
		result.Add(new KeyValuePair<string, string>("utmt", "item"));
	}
	
	public List<KeyValuePair<string, string>> ConstructCommonParameters()
	{
		List<KeyValuePair<string, string>> paramList = new List<KeyValuePair<string, string>>
		{
			new KeyValuePair<string,string>("utmwv", "4.6.5"),									
			new KeyValuePair<string,string>("utmn", NuTrackingHelpers.GetRandomIdString()),	
			new KeyValuePair<string,string>("utmhn", NuTrackingHelpers.GAHostDomain),				
			new KeyValuePair<string,string>("utmr", "-"),										
			new KeyValuePair<string,string>("utmac", NuTrackingHelpers.GAAccountCode),		
			new KeyValuePair<string,string>("utmcc", BuildUtmcCookieString())					
		};

		string ipAddr = NuTrackingHelpers.GetGlobalIP();
		paramList.Add(new KeyValuePair<string, string>("utmip", ipAddr));
		
		return paramList;
	}
	
	public string GenerateURL(List<KeyValuePair<string, string>> _paramList)
	{
		StringBuilder GaParams = new StringBuilder();
		foreach (KeyValuePair<string, string> pair in _paramList)
		{
			GaParams.Append(String.Format("{0}={1}&", pair.Key, pair.Value));
		}
		GaParams.Remove(GaParams.Length-1, 1);

		string paramsFinal = GaParams.ToString();

#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER
		string urlFinal = string.Format("https://ssl.google-analytics.com/__utm.gif?{0}",
							 paramsFinal);
#else
		string urlFinal = string.Format("http://www.google-analytics.com/__utm.gif?{0}",
							 paramsFinal);
#endif							 
		
		return urlFinal;
	}
	
	public string GetPageViewURL(string _pageTitle, string _pageUrl)
	{
		List<KeyValuePair<string, string>> paramList = ConstructCommonParameters();
		AddPageViewParameters(paramList, _pageTitle, _pageUrl);
		string url = GenerateURL(paramList);
		return url;
	}
	
	public string GetEventURL(NuGAEvent _event, string _pageTitle, string _pageUrl)
	{
		List<KeyValuePair<string, string>> paramList = ConstructCommonParameters();
		AddEventParameters(paramList, _event, _pageTitle, _pageUrl);
		string url = GenerateURL(paramList);
		return url;
	}
	
	public string GetTransactionURL(NuGATransaction _transaction, out string _transId)
	{
		List<KeyValuePair<string, string>> paramList = ConstructCommonParameters();
		_transId = AddTransactionParameters(paramList, _transaction);
		string url = GenerateURL(paramList);
		return url;
	}
	
	public string GetItemURL(NuGATransaction _transaction, string _transId)
	{
		List<KeyValuePair<string, string>> paramList = ConstructCommonParameters();
		AddItemParameters(paramList, _transaction, _transId);
		string url = GenerateURL(paramList);
		return url;
	}
	
	public void Validate()
	{
	}
}
