using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HTTP;

public class NuGARequest
{
	public static void IssueRequest(MonoBehaviour behaviour, List<string> _urlList)
	{
		behaviour.StartCoroutine(SendAsyncWebRequest(_urlList));
	}
	
	private static IEnumerator SendAsyncWebRequest(List<string> _urlList)
	{
		NuTrackingHelpers.Log(string.Format("Issue Request List has {0} items", _urlList.Count));
		
		foreach(string url in _urlList)
		{
			NuTrackingHelpers.Log (string.Format ("Issue Request \"{0}\"", url));
			var r = new Request("get", url);
			r.SetHeader("User-Agent", NuTrackingHelpers.ConstructUserAgentString());
			r.Send();
			while (!r.isDone)
			{
				yield return new WaitForSeconds(0.5f);
			}
			if (r.response == null || r.response.status != 200)
			{
				NuTrackingHelpers.Log(string.Format("SendTrackingData error, response = {0}", r.response == null ? "null" : r.response.status.ToString()));
			}
		}
	}
}
