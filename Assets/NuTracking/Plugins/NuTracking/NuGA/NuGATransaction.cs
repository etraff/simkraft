using UnityEngine;
using System;
using System.Collections;


public class NuGATransaction
{
	public string ProductCode { get; set; }
	public string ProductName { get; set; }
	public float ProductUnitPrice { get; set; }
	public int ProductQuantity { get; set; }
	
	public NuGATransaction(string _productCode, string _productName, float _productUnitPrice, int _productQuantity = 1)
	{
		ProductCode = _productCode;
		ProductName = _productName;
		ProductUnitPrice = _productUnitPrice;
		ProductQuantity = _productQuantity;
	}
}
