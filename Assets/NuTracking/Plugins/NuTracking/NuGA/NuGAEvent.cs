using UnityEngine;
using System.Collections;

// http://code.google.com/apis/analytics/docs/tracking/gaTrackingTroubleshooting.html
//   Google Analytics uses the value of the utme parameter to track events in the form of 5(object*action*label)(value):
//     5 is a constant
//     object and action are required
//     label and value are options

public class NuGAEvent 
{
	public string Object { get; set; }
	public string Action { get; set; }
	public string Label { get; set; }
	public int? Value { get; set; }

	public NuGAEvent(string _object, string _action, string _label, int? _value)
	{
		Object = _object;
		Action = _action;
		Label = _label;
		Value = _value;
		Validate();
	}

	public void Validate()
	{
		NuTrackingHelpers.Assert(!string.IsNullOrEmpty(Object), "Invalid Parameter");
		NuTrackingHelpers.Assert(!string.IsNullOrEmpty(Action), "Invalid Parameter");
	}
	
	public override string ToString()
	{
		return string.Format("5({0}*{1}*{2})({3})",
		                     Object,
		                     Action,
		                     Label ?? "",
		                     Value.HasValue ? Value.Value.ToString() : "");
	}
}
