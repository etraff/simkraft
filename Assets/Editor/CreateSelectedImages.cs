using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CreateSelectedImages : Editor {

	[MenuItem ("Tools/Create Selected Images")]
	public static void DoCreateSelectedImages() {
		string[] files = Directory.GetFiles(Application.dataPath+"/textures/ingame/Resources");
		foreach (var file in files){
			if (file.EndsWith(".png") && !file.EndsWith("_selected.png")){
				try{
					string assetFile = file.Substring(file.IndexOf("/Assets/")+1);
					TextureImporter ti =
						(TextureImporter) TextureImporter.GetAtPath(assetFile);
					bool wasReadable = ti.isReadable;
					ti.isReadable = true;

					AssetDatabase.ImportAsset(assetFile);
					Texture2D texture2D = AssetDatabase.LoadAssetAtPath(assetFile, typeof(Texture2D)) as Texture2D;
					string outputFile = file.Substring(0,file.Length-4) + "_selected.png";
#if UNITY_EDITOR										
					Debug.Log(outputFile+" assetFile "+assetFile+" texture2D "+(texture2D==null));
#endif					
					texture2D = ModifyTexture(texture2D);
					byte[] res = texture2D.EncodeToPNG(); 
					FileStream outFile = File.Create(outputFile);

					outFile.Write(res,0,res.Length);

					ti.isReadable = wasReadable;
					AssetDatabase.ImportAsset(assetFile);
				} catch (System.Exception e){
#if UNITY_EDITOR										
					Debug.Log(e);
#endif					
				}
			}

		}
		//
	}

	static Texture2D ModifyTexture(Texture2D texture){
		Texture2D res = new Texture2D(texture.width, texture.height,TextureFormat.ARGB32, true, true);

		Color[] colors = texture.GetPixels();
		float height = texture.height;
		for (int i=0;i<colors.Length;i++){
			int row = i/texture.width;
			float fraction = row/height;
			if ((colors[i].r >= 0.9 && colors[i].g >= 0.9 && colors[i].b >= 0.9) || colors[i].a < 0.1f){
				Color color2 = new Color(242/255.0f,214/255.0f,20/255.0f,colors[i].a);
				Color color1 = new Color(233/255.0f,126/255.0f,12/255.0f,colors[i].a);
				colors[i] = Color.Lerp(color1, color2, fraction);
			} else {
				colors[i] = colors[i];
			}
		}
		res.SetPixels(colors);
		res.Apply();
		return res;
	}
}
