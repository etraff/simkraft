//
//  FEA_data.h
//  FEA_BLIB
//
//  Created by FAM on 04/03/2017.
//  Copyright © 2017 FAM. All rights reserved.
//

#ifndef FEA_data_h
#define FEA_data_h


class FEA_data {
    
private:
    int nex, ney , ne, nx, ny ,ndof, nbdof;
    bool * elementDenseties;
    int * ReductionKey;
    int * ExpansionKey;
    int * bdof;
    
    int reduced_ndof;
    bool First_Vib = true;
    
public:
    oocholmod::SparseMatrix * K;
    oocholmod::DenseMatrix  * M;
    oocholmod::DenseMatrix  * D;
    oocholmod::DenseMatrix  * D_old;
    
    // Constructor, for updating size of system.
    FEA_data(const int n_elemx, const int n_elemy, const int rho[], const int nbdof, const int boundeddof[]);
    
    // Destructor, for tearing the Datastructure down.
    ~FEA_data();
    
    // Method for copying stiffnessmatrix as output
    void GetStiff(oocholmod::SparseMatrix * K);
    
    // Method for enforcing boundary conditions on the input displacements
    void EnforceBC_on_Displ(int& ndispls, int ddof[], float dlength[]);
    
    void EnforceBC_on_Matrix(oocholmod::SparseMatrix * K);

    // Methods for returning stuff
    int Getndof(){ return ndof; }
    int Getnex(){ return nex; }
    int Getney(){ return ney; }
    int Getnnode(){ return nx*ny; }
    int Get_reduceddof(){ return reduced_ndof; }
    int* GetRedKey(){ return ReductionKey; }
    int* GetExpKey(){ return ExpansionKey; }
    bool* GetDenseties(){ return elementDenseties; }
    void GetStress(const int plotChoice, float * stress, float& maxstress, float& minstress, const float u[]);
    void UpdateD(oocholmod::DenseMatrix * D_new){
        delete D_old;
        D_old = D;
        D = D_new;
    };
};


#endif /* FEA_data_h */



