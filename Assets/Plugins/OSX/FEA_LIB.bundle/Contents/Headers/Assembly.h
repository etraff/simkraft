//
//  Assembly.h
//  FEA_BLIB
//
//  Created by FAM on 27/02/2017.
//  Copyright © 2017 FAM. All rights reserved.
//

#ifndef Assembly_h
#define Assembly_h

//#include "mainhead.h"

// Generate reductions from denseties
void GenerateReductionKey(const int nex, const int ney, const bool elementDenseties[], int& reduced_ndof, int ReductionKey[]);

// Generate expansions from reductions
void GenerateExpansionKey(const int ndof, const int reduced_ndof, const int ReductionKey[], int ExpansionKey[]);

// Generate element stiffness matrix
void AssembleElementStiff(float * KE, const int ney);

// Generate global stiffness matrix
void AssembleStiff(oocholmod::SparseMatrix * K, const int nex, const int ney, const int reduced_ndof, const bool elementTopology[], const int ReductionKey[]);

// Generate global mass matrix
void AssembleMass(oocholmod::DenseMatrix * M, const int nex, const int ney, const int reduced_ndof, const bool elementTopology[], const int ReductionKey[]);

//Take stiff matrix, enforce bc on it
void EnforceBC_Matrix(oocholmod::SparseMatrix * K, const int reduced_ndof, const int n_fixed_dof, const int ReductionKey[], const int fixeddof[]);

// Enforce BC on displs
void Enforce_BC_on_Displs(const int nbdof, const int fixeddof[], int& ndispls, int displdof[], float displlength[]);

// Generate the right hand side
oocholmod::SparseMatrix Generate_RHS(const int reduced_ndof, const int ndispls, const int displdof[], const float displlength[], const int ReductionKey[]);

void EnforceDispl_Matrix(oocholmod::SparseMatrix& K,const int ndispls, const int displdof[], const float displlength[], const int ReductionKey[]);

void GetStressVect(const int nex, const int ney, const bool elementDenseties[], const float u[], float nodalstress[], float& maxstress, float& minstress);

void GetStressVect(const int plotChoice, const int nex,const int ney,const bool elementDenseties[], const float u[], float nodalstress[], float& maxstress, float& minstress);

void GetBmat(float * B);

void GetCmat(float young,float * B);

void ApplyBigFingerDispls(oocholmod::SparseMatrix * K, int nex, int ney, bool elementDenseties[], int ReductionKey[], int ndispls, int displdofs[]);

#endif /* Assembly_h */
