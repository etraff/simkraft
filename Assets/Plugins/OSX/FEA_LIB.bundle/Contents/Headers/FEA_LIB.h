//
//  FEA_LIB.h
//  FEA_BLIB
//
//  Created by FAM on 04/03/2017.
//  Copyright © 2017 FAM. All rights reserved.
//

#ifndef FEA_LIB_h
#define FEA_LIB_h

extern "C" void  _DeleteFEA();
extern "C" void  _InitFEA(int nex, int ney, int elementDenseties[],int nbdof, int bdof[]);
extern "C" void  _GetSolution(const int plotChoice, int ndispls, int displDof[], float displLength[], float U[], int reduced_dof, float nodestress[], float maxminstress[]);
extern "C" void  _GetVib(int isFirst, const int plotChoice, float U[], float nodestress[]);

#endif /* FEA_LIB_h */
