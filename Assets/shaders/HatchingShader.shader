// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/HatchingShader" {

    Properties
    {
   	 	_Color ("Main Color", Color) = (1,1,1,1)
        _MainTex ("Base", 2D) = "white" {}
        _Resolution ("Resolution x", Float) = 10.0
    }

    SubShader
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha 
        Pass
        {
            CGPROGRAM
                #include "UnityCG.cginc"
                #pragma vertex vert
                #pragma fragment frag

                struct v2f
                {
                    half4 pos : SV_POSITION;
                    half4 projPos :  TEXCOORD0;
                };

				fixed4 _Color;
                sampler2D _MainTex;
                float _Resolution;
                half4 _MainTex_ST;
                

                v2f vert(appdata_full v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.projPos = ComputeScreenPos (o.pos);
                    
                    return o;
                }
                
                fixed4 frag(v2f i) : COLOR
                {
                	i.projPos /= i.projPos.w;
                	i.projPos *= _Resolution;
                	float trans = sin(i.projPos.x*_ScreenParams.x + i.projPos.y*_ScreenParams.y); 
                    return fixed4(_Color.xyz,trans); 
                }
            ENDCG
        }
    }
    Fallback "Unlit/Texture"
}