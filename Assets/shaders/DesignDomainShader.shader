// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DesignDomainShader" {

    Properties
    {
        _MainTex ("Base", 2D) = "white" {}
        _StressTex ("Stress", 2D) = "black" {}
        _MainColor("MainColor", Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }

        Pass
        { 
        	Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
                #include "UnityCG.cginc"
                #pragma vertex vert
                #pragma fragment frag

                struct v2f
                {
                    fixed4 pos : SV_POSITION;
                    fixed2 uv[2] : TEXCOORD0;
                };

                sampler2D _MainTex;
                sampler2D _StressTex;
                fixed4 _MainTex_ST;
                fixed4 _MainColor;

                v2f vert(appdata_full v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv[0] = TRANSFORM_TEX(v.texcoord, _MainTex);
                    o.uv[1] = TRANSFORM_TEX(v.texcoord1, _MainTex);
 
                    return o;
                }
                
                fixed4 frag(v2f i) : COLOR
                {
                	fixed4 c = tex2D(_MainTex, i.uv[0] );
                	fixed4 stress = tex2D(_StressTex, i.uv[1] );
                	if (c[3] < 0.5){
                		discard;
                	}
                	return fixed4( stress[0], stress[1], stress[2], c[3])*_MainColor; 
                }
            ENDCG
        }
    }
    Fallback "Unlit/Texture"
}