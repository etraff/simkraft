// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/CountDownTimer" {

    Properties
    {
    	_Color ("Main Color", Color) = (1,1,1,0)
        _TimeColor ("Time Color", Color) = (1,1,1,0)
        _TimeFrac ("TimeFrac", float) = 0.5
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
                #include "UnityCG.cginc"
                #pragma vertex vert
                #pragma fragment frag

                struct v2f
                {
                    fixed4 pos : SV_POSITION;
                    fixed2 uv[2] : TEXCOORD0;
                };

                fixed4 _Color;
                fixed4 _TimeColor;
                half _TimeFrac;
                fixed4 _MainTex_ST;

                v2f vert(appdata_full v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv[0] = v.texcoord; 
                    return o;
                }
                
                

                fixed4 frag(v2f i) : COLOR
                {
                	fixed4 res;
                	if (i.uv[0].y < _TimeFrac){
                		res = _TimeColor;
                	} else {
                		res = _Color;
                	}
                	return res; 
                }
            ENDCG
        }
    }
    Fallback "Unlit/Texture"
}