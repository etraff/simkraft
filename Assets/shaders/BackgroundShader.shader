// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/BackgroundShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_textureSize ("Texture Size", Float) = 256.0
		_noiseTex ("Noise (RGB)", 2D) = "white" {}
		_noiseTextureSize ("Noise Texture Size", Float) = 256.0
	}
	
	SubShader {
		Tags { "Queue"="Background" "RenderType"="Background" }
		Cull Off ZWrite Off Fog { Mode Off }

		Pass {	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _textureSize;
			sampler2D _noiseTex;
			float _noiseTextureSize;
	
			struct Input {
				float4 vertex : POSITION;
			};
			
			struct v2f {
				float4 vertex : POSITION;
				float2 calculatedScreenPos : TEXCOORD0;
			}; 
	 
			float2 ConvertToScreenPos( float4 normalizedDeviceCoordinates )
			{
				return 0.5*(normalizedDeviceCoordinates.xy+1.0) * _ScreenParams.xy;
			}
			  
			v2f vert (Input v)
			{
				// convert to normalized device coordinates (to avoid perpective correct 
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertex.xyz /= o.vertex.w;
				o.vertex.w = 1.0;
				o.calculatedScreenPos = ConvertToScreenPos(o.vertex);
				return o;
			}
	
			fixed4 frag (v2f i) : COLOR
			{
				fixed4 noise = tex2D(_noiseTex, i.calculatedScreenPos.xy/_noiseTextureSize);
				fixed4 col = tex2D(_MainTex, i.calculatedScreenPos.xy/_textureSize);
				fixed4 res = col+fixed4(noise.xyz*noise.w, 0);
					;
				return res;
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
