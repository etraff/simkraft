// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/UnlitGridShader" {

    Properties
    {
        _MainTex ("Base", 2D) = "white" {}
        _NumberOfLinesX ("Number of lines x", Float) = 10.0
        _NumberOfLinesY ("Number of lines y", Float) = 10.0
        _BorderOpacity ("Border opacity", Float) = 1.0
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
                #include "UnityCG.cginc"
                #pragma vertex vert
                #pragma fragment frag

                struct v2f
                {
                    fixed4 pos : SV_POSITION;
                    fixed2 uv[2] : TEXCOORD0;
                };

                sampler2D _MainTex;
                float _NumberOfLinesX;
                float _NumberOfLinesY;
                float _BorderOpacity;
                fixed4 _MainTex_ST;

                v2f vert(appdata_full v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv[0] = TRANSFORM_TEX(v.texcoord, _MainTex);
 
                    return o;
                }
                
                float edgeDetection(fixed2 uv) {
                	uv = fmod(uv*float2(_NumberOfLinesX,_NumberOfLinesY), 1.0);
                	float2 distanceToEdge = (abs(uv-float2(0.5,0.5)))*float2(2.0,2.0);
                	return clamp((1-max(distanceToEdge.x,distanceToEdge.y))*10.0,0,1);
                }

                fixed4 frag(v2f i) : COLOR
                {
                	fixed4 c = tex2D(_MainTex, i.uv[0] );
                	fixed4 borderColor = fixed4(0.5,0.5,0.5,1);
                	fixed edgeDetectionValue = edgeDetection(i.uv[0]);
                	fixed4 grid = lerp(borderColor,c,edgeDetectionValue);
                    return lerp(c, grid, _BorderOpacity); 
                }
            ENDCG
        }
    }
    Fallback "Unlit/Texture"
}