// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/UnlitColorShader" {

    Properties
    {
    	 _Color ("Main Color", Color) = (1,1,1,0)
        _MainTex ("Base", 2D) = "white" {}
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
                #include "UnityCG.cginc"
                #pragma vertex vert
                #pragma fragment frag

                struct v2f
                {
                    fixed4 pos : SV_POSITION;
                    fixed2 uv[2] : TEXCOORD0;
                };

                sampler2D _MainTex;
                fixed4 _Color;
                fixed4 _MainTex_ST;

                v2f vert(appdata_full v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv[0] = TRANSFORM_TEX(v.texcoord, _MainTex);
 
                    return o;
                }
                
                

                fixed4 frag(v2f i) : COLOR
                {
                	fixed4 c = tex2D(_MainTex, i.uv[0] );
                	return c * _Color; 
                }
            ENDCG
        }
    }
    Fallback "Unlit/Texture"
}