﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Lumos
struct Lumos_t1046626606;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Action
struct Action_t1264377477;
// System.Delegate
struct Delegate_t1188392813;
// LumosCredentials
struct LumosCredentials_t1718792749;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// Lumos/<SendQueuedData>c__Iterator0
struct U3CSendQueuedDataU3Ec__Iterator0_t3572231512;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// LumosAnalytics
struct LumosAnalytics_t2745971597;
// LumosAnalyticsSetup
struct LumosAnalyticsSetup_t3600549041;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// System.Type
struct Type_t;
// LumosDiagnostics
struct LumosDiagnostics_t279239158;
// UnityEngine.Application/LogCallback
struct LogCallback_t3588208630;
// LumosDiagnosticsSetup
struct LumosDiagnosticsSetup_t110199435;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Dictionary_2_t2650618762;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct ValueCollection_t71695784;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t1848589470;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct List_1_t42469909;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct IEnumerable_1_t1845215352;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// LumosFeedbackGUI
struct LumosFeedbackGUI_t1268982120;
// LumosFeedbackGUI/CloseHandler
struct CloseHandler_t719506259;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t3146511083;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t811797299;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IList
struct IList_t2094931216;
// LumosLocation/<Record>c__AnonStorey0
struct U3CRecordU3Ec__AnonStorey0_t811208082;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String>
struct Dictionary_2_t1123770287;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.Object>
struct Dictionary_2_t2356425762;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// LumosPlayer
struct LumosPlayer_t370097434;
// LumosPlayer/<RequestPlayerId>c__AnonStorey0
struct U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854;
// LumosRequest
struct LumosRequest_t457072260;
// LumosRequest/<SendCoroutine>c__Iterator0
struct U3CSendCoroutineU3Ec__Iterator0_t597473103;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t1952596188;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1432317219;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// LumosRequest/<LoadImageCoroutine>c__Iterator1
struct U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Exception
struct Exception_t;
// System.Security.Cryptography.MD5CryptoServiceProvider
struct MD5CryptoServiceProvider_t3005586042;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1694351041;
// System.Collections.Generic.HashSet`1/Link<System.String>[]
struct LinkU5BU5D_t3969674095;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>[]
struct Dictionary_2U5BU5D_t2798332550;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.DictionaryEntry>
struct Transform_1_t1648744266;
// UnityEngine.LogType[]
struct LogTypeU5BU5D_t772852639;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.LogType>
struct IEqualityComparer_1_t2181097452;
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t2997890289;
// UnityEngine.ILogger
struct ILogger_t2607134790;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Text.DecoderFallback
struct DecoderFallback_t3123823036;
// System.Text.EncoderFallback
struct EncoderFallback_t1188251036;
// System.Reflection.Assembly
struct Assembly_t;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.Void
struct Void_t1185182177;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Mono.Security.Cryptography.BlockProcessor
struct BlockProcessor_t1851031225;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302;
// UnityEngine.GUISettings
struct GUISettings_t1774757634;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3742157810;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1143955295;

extern RuntimeClass* Lumos_t1046626606_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_t1264377477_il2cpp_TypeInfo_var;
extern const uint32_t Lumos_add_OnReady_m627233499_MetadataUsageId;
extern const uint32_t Lumos_remove_OnReady_m4202809949_MetadataUsageId;
extern const uint32_t Lumos_add_OnTimerFinish_m2405319448_MetadataUsageId;
extern const uint32_t Lumos_remove_OnTimerFinish_m2760070207_MetadataUsageId;
extern const uint32_t Lumos_get_credentials_m1762176871_MetadataUsageId;
extern const uint32_t Lumos_set_credentials_m2691661372_MetadataUsageId;
extern const uint32_t Lumos_get_debug_m1677971330_MetadataUsageId;
extern const uint32_t Lumos_set_debug_m4002013879_MetadataUsageId;
extern const uint32_t Lumos_get_playerId_m505766044_MetadataUsageId;
extern const uint32_t Lumos_set_playerId_m314876524_MetadataUsageId;
extern const uint32_t Lumos_get_timerInterval_m619285440_MetadataUsageId;
extern const uint32_t Lumos_set_timerInterval_m1080298005_MetadataUsageId;
extern const uint32_t Lumos_get_timerPaused_m281469672_MetadataUsageId;
extern const uint32_t Lumos_set_timerPaused_m3894062333_MetadataUsageId;
extern const uint32_t Lumos_get_runInEditor_m2527088602_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral10572647;
extern String_t* _stringLiteral1389463571;
extern const uint32_t Lumos_Awake_m3796621327_MetadataUsageId;
extern RuntimeClass* Action_1_t269755560_il2cpp_TypeInfo_var;
extern RuntimeClass* LumosPlayer_t370097434_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Lumos_U3CStartU3Em__0_m2632898143_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m3735703580_RuntimeMethod_var;
extern const uint32_t Lumos_Start_m2536240861_MetadataUsageId;
extern String_t* _stringLiteral1305370915;
extern const uint32_t Lumos_RunRoutine_m1158721104_MetadataUsageId;
extern String_t* _stringLiteral2228809126;
extern String_t* _stringLiteral1377196673;
extern const uint32_t Lumos_Remove_m2435435980_MetadataUsageId;
extern RuntimeClass* U3CSendQueuedDataU3Ec__Iterator0_t3572231512_il2cpp_TypeInfo_var;
extern const uint32_t Lumos_SendQueuedData_m1193834825_MetadataUsageId;
extern RuntimeClass* Action_1_t3252573759_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Debug_Log_m4051431634_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m118522912_RuntimeMethod_var;
extern const uint32_t Lumos_Log_m2411948670_MetadataUsageId;
extern const RuntimeMethod* Debug_LogWarning_m3752629331_RuntimeMethod_var;
extern const uint32_t Lumos_LogWarning_m3534960519_MetadataUsageId;
extern const RuntimeMethod* Debug_LogError_m2850623458_RuntimeMethod_var;
extern const uint32_t Lumos_LogError_m3535111913_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m2461023210_RuntimeMethod_var;
extern const uint32_t Lumos_LogMessage_m2367926511_MetadataUsageId;
extern const uint32_t Lumos__cctor_m1354239268_MetadataUsageId;
extern const uint32_t Lumos_U3CStartU3Em__0_m2632898143_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendQueuedDataU3Ec__Iterator0_MoveNext_m783816231_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CSendQueuedDataU3Ec__Iterator0_Reset_m4035466761_RuntimeMethod_var;
extern const uint32_t U3CSendQueuedDataU3Ec__Iterator0_Reset_m4035466761_MetadataUsageId;
extern RuntimeClass* LumosAnalytics_t2745971597_il2cpp_TypeInfo_var;
extern const uint32_t LumosAnalytics_get_levelsAsCategories_m3551605565_MetadataUsageId;
extern const uint32_t LumosAnalytics_get_baseUrl_m394505943_MetadataUsageId;
extern const uint32_t LumosAnalytics_set_baseUrl_m3622155395_MetadataUsageId;
extern const RuntimeMethod* LumosLocation_Record_m3373362066_RuntimeMethod_var;
extern const RuntimeMethod* LumosEvents_Send_m923369243_RuntimeMethod_var;
extern String_t* _stringLiteral4130627310;
extern const uint32_t LumosAnalytics_Awake_m3671350359_MetadataUsageId;
extern String_t* _stringLiteral4020123101;
extern const uint32_t LumosAnalytics_OnLevelWasLoaded_m301866328_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisLumosAnalytics_t2745971597_m3266012221_RuntimeMethod_var;
extern String_t* _stringLiteral41802212;
extern String_t* _stringLiteral3786590064;
extern String_t* _stringLiteral3765604456;
extern const uint32_t LumosAnalytics_IsInitialized_m3408112055_MetadataUsageId;
extern RuntimeClass* LumosEvents_t3060038184_il2cpp_TypeInfo_var;
extern const uint32_t LumosAnalytics_RecordEvent_m4291932900_MetadataUsageId;
extern const RuntimeMethod* Nullable_1__ctor_m2775392932_RuntimeMethod_var;
extern const uint32_t LumosAnalytics_RecordEvent_m606101563_MetadataUsageId;
extern const uint32_t LumosAnalytics_RecordEvent_m3453334534_MetadataUsageId;
extern const uint32_t LumosAnalytics_RecordEvent_m2084622174_MetadataUsageId;
extern const uint32_t LumosAnalytics_RecordEvent_m1464728650_MetadataUsageId;
extern const uint32_t LumosAnalytics_RecordEvent_m4009380798_MetadataUsageId;
extern const uint32_t LumosAnalytics_RecordEvent_m2764324973_MetadataUsageId;
extern const uint32_t LumosAnalytics_RecordEvent_m454352664_MetadataUsageId;
extern String_t* _stringLiteral2098737819;
extern const uint32_t LumosAnalytics__cctor_m1705858773_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisLumosAnalytics_t2745971597_m4089171744_RuntimeMethod_var;
extern String_t* _stringLiteral2107940303;
extern const uint32_t LumosAnalyticsSetup_Setup_m1888243477_MetadataUsageId;
extern const uint32_t LumosCredentials__ctor_m1196037201_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern const uint32_t LumosCredentials_get_gameID_m318748771_MetadataUsageId;
extern const RuntimeType* LumosCredentials_t1718792749_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* LumosCredentials_t1718792749_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral470592798;
extern const uint32_t LumosCredentials_Load_m1109527268_MetadataUsageId;
extern RuntimeClass* LumosDiagnostics_t279239158_il2cpp_TypeInfo_var;
extern const uint32_t LumosDiagnostics_get_recordDebugLogs_m2269547550_MetadataUsageId;
extern const uint32_t LumosDiagnostics_get_recordDebugWarnings_m2284271601_MetadataUsageId;
extern const uint32_t LumosDiagnostics_get_recordDebugErrors_m1784451440_MetadataUsageId;
extern const uint32_t LumosDiagnostics_get_baseUrl_m476843529_MetadataUsageId;
extern const uint32_t LumosDiagnostics_set_baseUrl_m3227158108_MetadataUsageId;
extern RuntimeClass* LogCallback_t3588208630_il2cpp_TypeInfo_var;
extern const RuntimeMethod* LumosSpecs_Record_m3944775057_RuntimeMethod_var;
extern const RuntimeMethod* LumosLogs_Send_m4102305593_RuntimeMethod_var;
extern const RuntimeMethod* LumosLogs_Record_m1639051678_RuntimeMethod_var;
extern const uint32_t LumosDiagnostics_Awake_m307644535_MetadataUsageId;
extern RuntimeClass* LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var;
extern const uint32_t LumosDiagnostics_OnGUI_m2919980339_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisLumosDiagnostics_t279239158_m51467272_RuntimeMethod_var;
extern String_t* _stringLiteral3557641152;
extern const uint32_t LumosDiagnostics_IsInitialized_m1820736181_MetadataUsageId;
extern String_t* _stringLiteral2374478329;
extern const uint32_t LumosDiagnostics__cctor_m4121200263_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisLumosDiagnostics_t279239158_m1556835766_RuntimeMethod_var;
extern String_t* _stringLiteral1950643850;
extern const uint32_t LumosDiagnosticsSetup_Setup_m2086867239_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2865362463_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern const RuntimeMethod* HashSet_1_Contains_m183505512_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Add_m2640748363_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3962145734_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m825500632_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_m2191841112_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m1008004203_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m2329160973_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m761006959_RuntimeMethod_var;
extern String_t* _stringLiteral2203338223;
extern String_t* _stringLiteral1948332219;
extern String_t* _stringLiteral3452614550;
extern String_t* _stringLiteral379197113;
extern String_t* _stringLiteral1318188887;
extern String_t* _stringLiteral105392964;
extern String_t* _stringLiteral1095525057;
extern String_t* _stringLiteral3493618073;
extern const uint32_t LumosEvents_Record_m3157052489_MetadataUsageId;
extern RuntimeClass* List_1_t42469909_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_get_Count_m3447859782_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Values_m2758554421_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1152355348_RuntimeMethod_var;
extern const RuntimeMethod* LumosEvents_U3CSendU3Em__0_m2551348430_RuntimeMethod_var;
extern const RuntimeMethod* LumosEvents_U3CSendU3Em__1_m2551281941_RuntimeMethod_var;
extern String_t* _stringLiteral3438004084;
extern const uint32_t LumosEvents_Send_m923369243_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t2650618762_il2cpp_TypeInfo_var;
extern RuntimeClass* HashSet_1_t412400163_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m199014843_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1__ctor_m3976624165_RuntimeMethod_var;
extern const uint32_t LumosEvents__cctor_m1980163439_MetadataUsageId;
extern RuntimeClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern const RuntimeMethod* HashSet_1_GetEnumerator_m4010819012_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2985855221_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2302661619_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m823536440_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m41144274_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Clear_m3290545120_RuntimeMethod_var;
extern const uint32_t LumosEvents_U3CSendU3Em__0_m2551348430_MetadataUsageId;
extern String_t* _stringLiteral295665451;
extern const uint32_t LumosEvents_U3CSendU3Em__1_m2551281941_MetadataUsageId;
extern String_t* _stringLiteral3253941996;
extern String_t* _stringLiteral3497109170;
extern String_t* _stringLiteral2215313024;
extern const uint32_t LumosFeedback_Record_m4263107104_MetadataUsageId;
extern RuntimeClass* LumosFeedback_t607479745_il2cpp_TypeInfo_var;
extern const RuntimeMethod* LumosFeedback_U3CSendU3Em__0_m2627546650_RuntimeMethod_var;
extern const RuntimeMethod* LumosFeedback_U3CSendU3Em__1_m1768186989_RuntimeMethod_var;
extern String_t* _stringLiteral1917745513;
extern const uint32_t LumosFeedback_Send_m64505330_MetadataUsageId;
extern String_t* _stringLiteral2128908254;
extern const uint32_t LumosFeedback_U3CSendU3Em__0_m2627546650_MetadataUsageId;
extern String_t* _stringLiteral3260327796;
extern const uint32_t LumosFeedback_U3CSendU3Em__1_m1768186989_MetadataUsageId;
extern RuntimeClass* CloseHandler_t719506259_il2cpp_TypeInfo_var;
extern const uint32_t LumosFeedbackGUI_add_windowClosed_m2691644395_MetadataUsageId;
extern const uint32_t LumosFeedbackGUI_remove_windowClosed_m1213351066_MetadataUsageId;
extern const uint32_t LumosFeedbackGUI_get_skin_m3102013148_MetadataUsageId;
extern const uint32_t LumosFeedbackGUI_set_skin_m517710851_MetadataUsageId;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern RuntimeClass* WindowFunction_t3146511083_il2cpp_TypeInfo_var;
extern RuntimeClass* GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var;
extern const RuntimeMethod* LumosFeedbackGUI_DisplayWindow_m1570154048_RuntimeMethod_var;
extern String_t* _stringLiteral2701337995;
extern const uint32_t LumosFeedbackGUI_OnGUI_m2428985537_MetadataUsageId;
extern String_t* _stringLiteral159732273;
extern String_t* _stringLiteral3243520198;
extern String_t* _stringLiteral2906987907;
extern String_t* _stringLiteral1985564044;
extern String_t* _stringLiteral835031361;
extern String_t* _stringLiteral3457136609;
extern const uint32_t LumosFeedbackGUI_DisplayWindow_m1570154048_MetadataUsageId;
extern const uint32_t LumosFeedbackGUI_ShowDialog_m3299159908_MetadataUsageId;
extern const uint32_t LumosFeedbackGUI_HideDialog_m330235545_MetadataUsageId;
extern String_t* _stringLiteral2827162806;
extern const uint32_t LumosFeedbackGUI__cctor_m67856517_MetadataUsageId;
extern RuntimeClass* LumosJson_t1197320855_il2cpp_TypeInfo_var;
extern const uint32_t LumosJson_Deserialize_m2451457175_MetadataUsageId;
extern RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
extern const uint32_t LumosJson_Serialize_m1033319704_MetadataUsageId;
extern const uint32_t LumosJson_LastDecodeSuccessful_m4240989907_MetadataUsageId;
extern const uint32_t LumosJson_GetLastErrorIndex_m1793719774_MetadataUsageId;
extern const uint32_t LumosJson_GetLastErrorSnippet_m1997652777_MetadataUsageId;
extern const uint32_t LumosJson_ParseObject_m1401908411_MetadataUsageId;
extern RuntimeClass* List_1_t257213610_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2321703786_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3338814081_RuntimeMethod_var;
extern const uint32_t LumosJson_ParseArray_m812927360_MetadataUsageId;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const uint32_t LumosJson_ParseValue_m571621602_MetadataUsageId;
extern RuntimeClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1986677933;
extern const uint32_t LumosJson_ParseString_m278549945_MetadataUsageId;
extern RuntimeClass* Int64_t3736567304_il2cpp_TypeInfo_var;
extern RuntimeClass* Double_t594665363_il2cpp_TypeInfo_var;
extern const uint32_t LumosJson_ParseNumber_m3864485428_MetadataUsageId;
extern String_t* _stringLiteral2206812729;
extern const uint32_t LumosJson_GetLastIndexOfNumber_m999849873_MetadataUsageId;
extern String_t* _stringLiteral1596281288;
extern const uint32_t LumosJson_EatWhitespace_m2987301487_MetadataUsageId;
extern const uint32_t LumosJson_LookAhead_m2682965121_MetadataUsageId;
extern const uint32_t LumosJson_NextToken_m857433679_MetadataUsageId;
extern RuntimeClass* IDictionary_t1363984059_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_t1941168011_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t LumosJson_SerializeObject_m2207719924_MetadataUsageId;
extern const uint32_t LumosJson_SerializeArray_m369273678_MetadataUsageId;
extern RuntimeClass* IList_t2094931216_il2cpp_TypeInfo_var;
extern RuntimeClass* Char_t3634460470_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1202628576;
extern String_t* _stringLiteral4002445229;
extern String_t* _stringLiteral3875954633;
extern const uint32_t LumosJson_SerializeValue_m1474558115_MetadataUsageId;
extern String_t* _stringLiteral3450386420;
extern String_t* _stringLiteral3458119668;
extern String_t* _stringLiteral3454580724;
extern String_t* _stringLiteral3454318580;
extern String_t* _stringLiteral3454842868;
extern String_t* _stringLiteral3455629300;
extern String_t* _stringLiteral3455498228;
extern String_t* _stringLiteral3455432692;
extern const uint32_t LumosJson_SerializeString_m1899413942_MetadataUsageId;
extern const uint32_t LumosJson__cctor_m1132831327_MetadataUsageId;
extern RuntimeClass* U3CRecordU3Ec__AnonStorey0_t811208082_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* SystemLanguage_t949212163_il2cpp_TypeInfo_var;
extern RuntimeClass* LumosLocation_t2816019121_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CRecordU3Ec__AnonStorey0_U3CU3Em__0_m4184800487_RuntimeMethod_var;
extern const RuntimeMethod* LumosLocation_U3CRecordU3Em__0_m71196215_RuntimeMethod_var;
extern String_t* _stringLiteral3050654888;
extern String_t* _stringLiteral3452614641;
extern String_t* _stringLiteral1541660959;
extern String_t* _stringLiteral2113053237;
extern String_t* _stringLiteral3467912857;
extern String_t* _stringLiteral2143290860;
extern const uint32_t LumosLocation_Record_m3373362066_MetadataUsageId;
extern String_t* _stringLiteral2939991487;
extern const uint32_t LumosLocation_U3CRecordU3Em__0_m71196215_MetadataUsageId;
extern String_t* _stringLiteral2970753464;
extern const uint32_t U3CRecordU3Ec__AnonStorey0_U3CU3Em__0_m4184800487_MetadataUsageId;
extern RuntimeClass* LumosLogs_t761625864_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2598014212_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2973281003_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3556324971_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2026665411_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m2887862096_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m24148094_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m845009296_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m3179620279_RuntimeMethod_var;
extern String_t* _stringLiteral4051234214;
extern String_t* _stringLiteral847691140;
extern String_t* _stringLiteral2122998792;
extern String_t* _stringLiteral3243520166;
extern String_t* _stringLiteral1232840130;
extern String_t* _stringLiteral1609214314;
extern const uint32_t LumosLogs_Record_m1639051678_MetadataUsageId;
extern const RuntimeMethod* LumosLogs_U3CSendU3Em__0_m3334083460_RuntimeMethod_var;
extern const RuntimeMethod* LumosLogs_U3CSendU3Em__1_m754180927_RuntimeMethod_var;
extern String_t* _stringLiteral3955078645;
extern const uint32_t LumosLogs_Send_m4102305593_MetadataUsageId;
extern const RuntimeMethod* List_1_AddRange_m1670619200_RuntimeMethod_var;
extern const uint32_t LumosLogs_AddMessagesToIgnore_m880380652_MetadataUsageId;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t1123770287_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3365230958_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m474478774_RuntimeMethod_var;
extern String_t* _stringLiteral3718524431;
extern String_t* _stringLiteral2595384961;
extern String_t* _stringLiteral95342995;
extern String_t* _stringLiteral2618865335;
extern String_t* _stringLiteral79347;
extern String_t* _stringLiteral1461069931;
extern const uint32_t LumosLogs__cctor_m991303528_MetadataUsageId;
extern const uint32_t LumosLogs_U3CSendU3Em__0_m3334083460_MetadataUsageId;
extern const uint32_t LumosLogs_U3CSendU3Em__1_m754180927_MetadataUsageId;
extern const uint32_t LumosPlayer_get_baseUrl_m2504180808_MetadataUsageId;
extern const uint32_t LumosPlayer_set_baseUrl_m1864604462_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m1933767679_RuntimeMethod_var;
extern String_t* _stringLiteral1887700128;
extern String_t* _stringLiteral3464815593;
extern const uint32_t LumosPlayer_Init_m3905959549_MetadataUsageId;
extern RuntimeClass* U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CRequestPlayerIdU3Ec__AnonStorey0_U3CU3Em__0_m2106923745_RuntimeMethod_var;
extern String_t* _stringLiteral381117126;
extern const uint32_t LumosPlayer_RequestPlayerId_m1064872458_MetadataUsageId;
extern const RuntimeMethod* LumosPlayer_U3CPingU3Em__0_m3675009066_RuntimeMethod_var;
extern const RuntimeMethod* LumosPlayer_U3CPingU3Em__1_m3675104263_RuntimeMethod_var;
extern String_t* _stringLiteral740565913;
extern String_t* _stringLiteral3984296161;
extern String_t* _stringLiteral2670441722;
extern const uint32_t LumosPlayer_Ping_m3221862318_MetadataUsageId;
extern String_t* _stringLiteral3018646317;
extern const uint32_t LumosPlayer__cctor_m1608523625_MetadataUsageId;
extern String_t* _stringLiteral2466698272;
extern const uint32_t U3CRequestPlayerIdU3Ec__AnonStorey0_U3CU3Em__0_m2106923745_MetadataUsageId;
extern const uint32_t LumosRequest_Send_m812021116_MetadataUsageId;
extern const uint32_t LumosRequest_Send_m1124333148_MetadataUsageId;
extern const uint32_t LumosRequest_Send_m3116052310_MetadataUsageId;
extern const uint32_t LumosRequest_Send_m1223465123_MetadataUsageId;
extern const uint32_t LumosRequest_Send_m1094114333_MetadataUsageId;
extern const uint32_t LumosRequest_Send_m552353156_MetadataUsageId;
extern RuntimeClass* U3CSendCoroutineU3Ec__Iterator0_t597473103_il2cpp_TypeInfo_var;
extern const uint32_t LumosRequest_SendCoroutine_m2726591602_MetadataUsageId;
extern RuntimeClass* Encoding_t1523322056_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3455956949;
extern const uint32_t LumosRequest_SerializePostData_m3510791873_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var;
extern RuntimeClass* HMACSHA1_t1952596188_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral39705060;
extern const uint32_t LumosRequest_GenerateAuthorizationHeader_m1225326569_MetadataUsageId;
extern const uint32_t LumosRequest_LoadImage_m640554234_MetadataUsageId;
extern RuntimeClass* U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463_il2cpp_TypeInfo_var;
extern const uint32_t LumosRequest_LoadImageCoroutine_m281388845_MetadataUsageId;
extern RuntimeClass* WWW_t3688466362_il2cpp_TypeInfo_var;
extern RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CLoadImageCoroutineU3Ec__Iterator1_MoveNext_m1188241163_RuntimeMethod_var;
extern String_t* _stringLiteral2183713642;
extern const uint32_t U3CLoadImageCoroutineU3Ec__Iterator1_MoveNext_m1188241163_MetadataUsageId;
extern const RuntimeMethod* U3CLoadImageCoroutineU3Ec__Iterator1_Reset_m836804549_RuntimeMethod_var;
extern const uint32_t U3CLoadImageCoroutineU3Ec__Iterator1_Reset_m836804549_MetadataUsageId;
extern const uint32_t U3CSendCoroutineU3Ec__Iterator0_MoveNext_m592653404_MetadataUsageId;
extern const RuntimeMethod* U3CSendCoroutineU3Ec__Iterator0_Reset_m3763864985_RuntimeMethod_var;
extern const uint32_t U3CSendCoroutineU3Ec__Iterator0_Reset_m3763864985_MetadataUsageId;
extern RuntimeClass* MD5CryptoServiceProvider_t3005586042_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3451434968;
extern const uint32_t LumosUtil_MD5Hash_m2689163647_MetadataUsageId;
extern const uint32_t LumosUtil_AddToDictionaryIfNonempty_m1393722542_MetadataUsageId;

struct GUILayoutOptionU5BU5D_t2510215842;
struct CharU5BU5D_t3528271667;
struct ObjectU5BU5D_t2843939325;
struct StringU5BU5D_t1281789340;
struct ByteU5BU5D_t4116647657;


#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef LUMOSLOCATION_T2816019121_H
#define LUMOSLOCATION_T2816019121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosLocation
struct  LumosLocation_t2816019121  : public RuntimeObject
{
public:

public:
};

struct LumosLocation_t2816019121_StaticFields
{
public:
	// System.Action`1<System.Object> LumosLocation::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LumosLocation_t2816019121_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSLOCATION_T2816019121_H
#ifndef LUMOSEVENTS_T3060038184_H
#define LUMOSEVENTS_T3060038184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosEvents
struct  LumosEvents_t3060038184  : public RuntimeObject
{
public:

public:
};

struct LumosEvents_t3060038184_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> LumosEvents::events
	Dictionary_2_t2650618762 * ___events_0;
	// System.Collections.Generic.HashSet`1<System.String> LumosEvents::unsentUniqueEvents
	HashSet_1_t412400163 * ___unsentUniqueEvents_1;
	// System.Action`1<System.Object> LumosEvents::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<System.Object> LumosEvents::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_events_0() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___events_0)); }
	inline Dictionary_2_t2650618762 * get_events_0() const { return ___events_0; }
	inline Dictionary_2_t2650618762 ** get_address_of_events_0() { return &___events_0; }
	inline void set_events_0(Dictionary_2_t2650618762 * value)
	{
		___events_0 = value;
		Il2CppCodeGenWriteBarrier((&___events_0), value);
	}

	inline static int32_t get_offset_of_unsentUniqueEvents_1() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___unsentUniqueEvents_1)); }
	inline HashSet_1_t412400163 * get_unsentUniqueEvents_1() const { return ___unsentUniqueEvents_1; }
	inline HashSet_1_t412400163 ** get_address_of_unsentUniqueEvents_1() { return &___unsentUniqueEvents_1; }
	inline void set_unsentUniqueEvents_1(HashSet_1_t412400163 * value)
	{
		___unsentUniqueEvents_1 = value;
		Il2CppCodeGenWriteBarrier((&___unsentUniqueEvents_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSEVENTS_T3060038184_H
#ifndef LUMOSANALYTICSSETUP_T3600549041_H
#define LUMOSANALYTICSSETUP_T3600549041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosAnalyticsSetup
struct  LumosAnalyticsSetup_t3600549041  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSANALYTICSSETUP_T3600549041_H
#ifndef U3CRECORDU3EC__ANONSTOREY0_T811208082_H
#define U3CRECORDU3EC__ANONSTOREY0_T811208082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosLocation/<Record>c__AnonStorey0
struct  U3CRecordU3Ec__AnonStorey0_t811208082  : public RuntimeObject
{
public:
	// System.String LumosLocation/<Record>c__AnonStorey0::prefsKey
	String_t* ___prefsKey_0;

public:
	inline static int32_t get_offset_of_prefsKey_0() { return static_cast<int32_t>(offsetof(U3CRecordU3Ec__AnonStorey0_t811208082, ___prefsKey_0)); }
	inline String_t* get_prefsKey_0() const { return ___prefsKey_0; }
	inline String_t** get_address_of_prefsKey_0() { return &___prefsKey_0; }
	inline void set_prefsKey_0(String_t* value)
	{
		___prefsKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefsKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDU3EC__ANONSTOREY0_T811208082_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectU5BU5D_t2843939325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ___EmptyArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef LUMOSSPECS_T2754611293_H
#define LUMOSSPECS_T2754611293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosSpecs
struct  LumosSpecs_t2754611293  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSSPECS_T2754611293_H
#ifndef LUMOSLOGS_T761625864_H
#define LUMOSLOGS_T761625864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosLogs
struct  LumosLogs_t761625864  : public RuntimeObject
{
public:

public:
};

struct LumosLogs_t761625864_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> LumosLogs::toIgnore
	List_1_t3319525431 * ___toIgnore_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> LumosLogs::logs
	Dictionary_2_t2650618762 * ___logs_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String> LumosLogs::typeLabels
	Dictionary_2_t1123770287 * ___typeLabels_2;
	// System.Action`1<System.Object> LumosLogs::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_3;
	// System.Action`1<System.Object> LumosLogs::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_4;

public:
	inline static int32_t get_offset_of_toIgnore_0() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___toIgnore_0)); }
	inline List_1_t3319525431 * get_toIgnore_0() const { return ___toIgnore_0; }
	inline List_1_t3319525431 ** get_address_of_toIgnore_0() { return &___toIgnore_0; }
	inline void set_toIgnore_0(List_1_t3319525431 * value)
	{
		___toIgnore_0 = value;
		Il2CppCodeGenWriteBarrier((&___toIgnore_0), value);
	}

	inline static int32_t get_offset_of_logs_1() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___logs_1)); }
	inline Dictionary_2_t2650618762 * get_logs_1() const { return ___logs_1; }
	inline Dictionary_2_t2650618762 ** get_address_of_logs_1() { return &___logs_1; }
	inline void set_logs_1(Dictionary_2_t2650618762 * value)
	{
		___logs_1 = value;
		Il2CppCodeGenWriteBarrier((&___logs_1), value);
	}

	inline static int32_t get_offset_of_typeLabels_2() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___typeLabels_2)); }
	inline Dictionary_2_t1123770287 * get_typeLabels_2() const { return ___typeLabels_2; }
	inline Dictionary_2_t1123770287 ** get_address_of_typeLabels_2() { return &___typeLabels_2; }
	inline void set_typeLabels_2(Dictionary_2_t1123770287 * value)
	{
		___typeLabels_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeLabels_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSLOGS_T761625864_H
#ifndef LUMOSDIAGNOSTICSSETUP_T110199435_H
#define LUMOSDIAGNOSTICSSETUP_T110199435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosDiagnosticsSetup
struct  LumosDiagnosticsSetup_t110199435  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSDIAGNOSTICSSETUP_T110199435_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2865362463_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1694351041 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1694351041 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1694351041 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1694351041 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef HASHSET_1_T412400163_H
#define HASHSET_1_T412400163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<System.String>
struct  HashSet_1_t412400163  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.HashSet`1/Link<T>[] System.Collections.Generic.HashSet`1::links
	LinkU5BU5D_t3969674095* ___links_5;
	// T[] System.Collections.Generic.HashSet`1::slots
	StringU5BU5D_t1281789340* ___slots_6;
	// System.Int32 System.Collections.Generic.HashSet`1::touched
	int32_t ___touched_7;
	// System.Int32 System.Collections.Generic.HashSet`1::empty_slot
	int32_t ___empty_slot_8;
	// System.Int32 System.Collections.Generic.HashSet`1::count
	int32_t ___count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::threshold
	int32_t ___threshold_10;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::comparer
	RuntimeObject* ___comparer_11;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::si
	SerializationInfo_t950877179 * ___si_12;
	// System.Int32 System.Collections.Generic.HashSet`1::generation
	int32_t ___generation_13;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_links_5() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___links_5)); }
	inline LinkU5BU5D_t3969674095* get_links_5() const { return ___links_5; }
	inline LinkU5BU5D_t3969674095** get_address_of_links_5() { return &___links_5; }
	inline void set_links_5(LinkU5BU5D_t3969674095* value)
	{
		___links_5 = value;
		Il2CppCodeGenWriteBarrier((&___links_5), value);
	}

	inline static int32_t get_offset_of_slots_6() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___slots_6)); }
	inline StringU5BU5D_t1281789340* get_slots_6() const { return ___slots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_slots_6() { return &___slots_6; }
	inline void set_slots_6(StringU5BU5D_t1281789340* value)
	{
		___slots_6 = value;
		Il2CppCodeGenWriteBarrier((&___slots_6), value);
	}

	inline static int32_t get_offset_of_touched_7() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___touched_7)); }
	inline int32_t get_touched_7() const { return ___touched_7; }
	inline int32_t* get_address_of_touched_7() { return &___touched_7; }
	inline void set_touched_7(int32_t value)
	{
		___touched_7 = value;
	}

	inline static int32_t get_offset_of_empty_slot_8() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___empty_slot_8)); }
	inline int32_t get_empty_slot_8() const { return ___empty_slot_8; }
	inline int32_t* get_address_of_empty_slot_8() { return &___empty_slot_8; }
	inline void set_empty_slot_8(int32_t value)
	{
		___empty_slot_8 = value;
	}

	inline static int32_t get_offset_of_count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___count_9)); }
	inline int32_t get_count_9() const { return ___count_9; }
	inline int32_t* get_address_of_count_9() { return &___count_9; }
	inline void set_count_9(int32_t value)
	{
		___count_9 = value;
	}

	inline static int32_t get_offset_of_threshold_10() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___threshold_10)); }
	inline int32_t get_threshold_10() const { return ___threshold_10; }
	inline int32_t* get_address_of_threshold_10() { return &___threshold_10; }
	inline void set_threshold_10(int32_t value)
	{
		___threshold_10 = value;
	}

	inline static int32_t get_offset_of_comparer_11() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___comparer_11)); }
	inline RuntimeObject* get_comparer_11() const { return ___comparer_11; }
	inline RuntimeObject** get_address_of_comparer_11() { return &___comparer_11; }
	inline void set_comparer_11(RuntimeObject* value)
	{
		___comparer_11 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_11), value);
	}

	inline static int32_t get_offset_of_si_12() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___si_12)); }
	inline SerializationInfo_t950877179 * get_si_12() const { return ___si_12; }
	inline SerializationInfo_t950877179 ** get_address_of_si_12() { return &___si_12; }
	inline void set_si_12(SerializationInfo_t950877179 * value)
	{
		___si_12 = value;
		Il2CppCodeGenWriteBarrier((&___si_12), value);
	}

	inline static int32_t get_offset_of_generation_13() { return static_cast<int32_t>(offsetof(HashSet_1_t412400163, ___generation_13)); }
	inline int32_t get_generation_13() const { return ___generation_13; }
	inline int32_t* get_address_of_generation_13() { return &___generation_13; }
	inline void set_generation_13(int32_t value)
	{
		___generation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T412400163_H
#ifndef DICTIONARY_2_T2650618762_H
#define DICTIONARY_2_T2650618762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct  Dictionary_2_t2650618762  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	Dictionary_2U5BU5D_t2798332550* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___valueSlots_7)); }
	inline Dictionary_2U5BU5D_t2798332550* get_valueSlots_7() const { return ___valueSlots_7; }
	inline Dictionary_2U5BU5D_t2798332550** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(Dictionary_2U5BU5D_t2798332550* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2650618762_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1648744266 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2650618762_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1648744266 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1648744266 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1648744266 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2650618762_H
#ifndef LIST_1_T42469909_H
#define LIST_1_T42469909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct  List_1_t42469909  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Dictionary_2U5BU5D_t2798332550* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t42469909, ____items_1)); }
	inline Dictionary_2U5BU5D_t2798332550* get__items_1() const { return ____items_1; }
	inline Dictionary_2U5BU5D_t2798332550** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Dictionary_2U5BU5D_t2798332550* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t42469909, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t42469909, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t42469909_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Dictionary_2U5BU5D_t2798332550* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t42469909_StaticFields, ___EmptyArray_4)); }
	inline Dictionary_2U5BU5D_t2798332550* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Dictionary_2U5BU5D_t2798332550** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Dictionary_2U5BU5D_t2798332550* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T42469909_H
#ifndef VALUECOLLECTION_T71695784_H
#define VALUECOLLECTION_T71695784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct  ValueCollection_t71695784  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t2650618762 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t71695784, ___dictionary_0)); }
	inline Dictionary_2_t2650618762 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2650618762 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2650618762 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T71695784_H
#ifndef LUMOSJSON_T1197320855_H
#define LUMOSJSON_T1197320855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosJson
struct  LumosJson_t1197320855  : public RuntimeObject
{
public:

public:
};

struct LumosJson_t1197320855_StaticFields
{
public:
	// System.Int32 LumosJson::lastErrorIndex
	int32_t ___lastErrorIndex_1;
	// System.String LumosJson::lastDecode
	String_t* ___lastDecode_2;

public:
	inline static int32_t get_offset_of_lastErrorIndex_1() { return static_cast<int32_t>(offsetof(LumosJson_t1197320855_StaticFields, ___lastErrorIndex_1)); }
	inline int32_t get_lastErrorIndex_1() const { return ___lastErrorIndex_1; }
	inline int32_t* get_address_of_lastErrorIndex_1() { return &___lastErrorIndex_1; }
	inline void set_lastErrorIndex_1(int32_t value)
	{
		___lastErrorIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastDecode_2() { return static_cast<int32_t>(offsetof(LumosJson_t1197320855_StaticFields, ___lastDecode_2)); }
	inline String_t* get_lastDecode_2() const { return ___lastDecode_2; }
	inline String_t** get_address_of_lastDecode_2() { return &___lastDecode_2; }
	inline void set_lastDecode_2(String_t* value)
	{
		___lastDecode_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastDecode_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSJSON_T1197320855_H
#ifndef LUMOSPLAYER_T370097434_H
#define LUMOSPLAYER_T370097434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosPlayer
struct  LumosPlayer_t370097434  : public RuntimeObject
{
public:

public:
};

struct LumosPlayer_t370097434_StaticFields
{
public:
	// System.String LumosPlayer::_baseUrl
	String_t* ____baseUrl_0;
	// System.Action`1<System.Object> LumosPlayer::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_1;
	// System.Action`1<System.Object> LumosPlayer::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of__baseUrl_0() { return static_cast<int32_t>(offsetof(LumosPlayer_t370097434_StaticFields, ____baseUrl_0)); }
	inline String_t* get__baseUrl_0() const { return ____baseUrl_0; }
	inline String_t** get_address_of__baseUrl_0() { return &____baseUrl_0; }
	inline void set__baseUrl_0(String_t* value)
	{
		____baseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(LumosPlayer_t370097434_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(LumosPlayer_t370097434_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSPLAYER_T370097434_H
#ifndef U3CREQUESTPLAYERIDU3EC__ANONSTOREY0_T2149773854_H
#define U3CREQUESTPLAYERIDU3EC__ANONSTOREY0_T2149773854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosPlayer/<RequestPlayerId>c__AnonStorey0
struct  U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> LumosPlayer/<RequestPlayerId>c__AnonStorey0::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREQUESTPLAYERIDU3EC__ANONSTOREY0_T2149773854_H
#ifndef DICTIONARY_2_T1123770287_H
#define DICTIONARY_2_T1123770287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String>
struct  Dictionary_2_t1123770287  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	LogTypeU5BU5D_t772852639* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1281789340* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___keySlots_6)); }
	inline LogTypeU5BU5D_t772852639* get_keySlots_6() const { return ___keySlots_6; }
	inline LogTypeU5BU5D_t772852639** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(LogTypeU5BU5D_t772852639* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___valueSlots_7)); }
	inline StringU5BU5D_t1281789340* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1281789340** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1281789340* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1123770287_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t2997890289 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1123770287_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t2997890289 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t2997890289 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t2997890289 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1123770287_H
#ifndef DEBUG_T3317548046_H
#define DEBUG_T3317548046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Debug
struct  Debug_t3317548046  : public RuntimeObject
{
public:

public:
};

struct Debug_t3317548046_StaticFields
{
public:
	// UnityEngine.ILogger UnityEngine.Debug::s_Logger
	RuntimeObject* ___s_Logger_0;

public:
	inline static int32_t get_offset_of_s_Logger_0() { return static_cast<int32_t>(offsetof(Debug_t3317548046_StaticFields, ___s_Logger_0)); }
	inline RuntimeObject* get_s_Logger_0() const { return ___s_Logger_0; }
	inline RuntimeObject** get_address_of_s_Logger_0() { return &___s_Logger_0; }
	inline void set_s_Logger_0(RuntimeObject* value)
	{
		___s_Logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Logger_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUG_T3317548046_H
#ifndef CUSTOMYIELDINSTRUCTION_T1895667560_H
#define CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1895667560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1895667560_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LUMOSUTIL_T3964483066_H
#define LUMOSUTIL_T3964483066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosUtil
struct  LumosUtil_t3964483066  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSUTIL_T3964483066_H
#ifndef LUMOSREQUEST_T457072260_H
#define LUMOSREQUEST_T457072260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosRequest
struct  LumosRequest_t457072260  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSREQUEST_T457072260_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef LUMOSFEEDBACK_T607479745_H
#define LUMOSFEEDBACK_T607479745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosFeedback
struct  LumosFeedback_t607479745  : public RuntimeObject
{
public:

public:
};

struct LumosFeedback_t607479745_StaticFields
{
public:
	// System.Action`1<System.Object> LumosFeedback::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`1<System.Object> LumosFeedback::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LumosFeedback_t607479745_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LumosFeedback_t607479745_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSFEEDBACK_T607479745_H
#ifndef U3CSENDQUEUEDDATAU3EC__ITERATOR0_T3572231512_H
#define U3CSENDQUEUEDDATAU3EC__ITERATOR0_T3572231512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lumos/<SendQueuedData>c__Iterator0
struct  U3CSendQueuedDataU3Ec__Iterator0_t3572231512  : public RuntimeObject
{
public:
	// System.Object Lumos/<SendQueuedData>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean Lumos/<SendQueuedData>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 Lumos/<SendQueuedData>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CSendQueuedDataU3Ec__Iterator0_t3572231512, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CSendQueuedDataU3Ec__Iterator0_t3572231512, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CSendQueuedDataU3Ec__Iterator0_t3572231512, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDQUEUEDDATAU3EC__ITERATOR0_T3572231512_H
#ifndef U3CSENDCOROUTINEU3EC__ITERATOR0_T597473103_H
#define U3CSENDCOROUTINEU3EC__ITERATOR0_T597473103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosRequest/<SendCoroutine>c__Iterator0
struct  U3CSendCoroutineU3Ec__Iterator0_t597473103  : public RuntimeObject
{
public:
	// System.Action`1<System.Object> LumosRequest/<SendCoroutine>c__Iterator0::errorCallback
	Action_1_t3252573759 * ___errorCallback_0;
	// System.Object LumosRequest/<SendCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean LumosRequest/<SendCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 LumosRequest/<SendCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_errorCallback_0() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___errorCallback_0)); }
	inline Action_1_t3252573759 * get_errorCallback_0() const { return ___errorCallback_0; }
	inline Action_1_t3252573759 ** get_address_of_errorCallback_0() { return &___errorCallback_0; }
	inline void set_errorCallback_0(Action_1_t3252573759 * value)
	{
		___errorCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___errorCallback_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDCOROUTINEU3EC__ITERATOR0_T597473103_H
#ifndef HASHALGORITHM_T1432317219_H
#define HASHALGORITHM_T1432317219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t1432317219  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t4116647657* ___HashValue_0;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::disposed
	bool ___disposed_3;

public:
	inline static int32_t get_offset_of_HashValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashValue_0)); }
	inline ByteU5BU5D_t4116647657* get_HashValue_0() const { return ___HashValue_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_HashValue_0() { return &___HashValue_0; }
	inline void set_HashValue_0(ByteU5BU5D_t4116647657* value)
	{
		___HashValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_0), value);
	}

	inline static int32_t get_offset_of_HashSizeValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashSizeValue_1)); }
	inline int32_t get_HashSizeValue_1() const { return ___HashSizeValue_1; }
	inline int32_t* get_address_of_HashSizeValue_1() { return &___HashSizeValue_1; }
	inline void set_HashSizeValue_1(int32_t value)
	{
		___HashSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T1432317219_H
#ifndef ENCODING_T1523322056_H
#define ENCODING_T1523322056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t1523322056  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::codePage
	int32_t ___codePage_0;
	// System.Int32 System.Text.Encoding::windows_code_page
	int32_t ___windows_code_page_1;
	// System.Boolean System.Text.Encoding::is_readonly
	bool ___is_readonly_2;
	// System.Text.DecoderFallback System.Text.Encoding::decoder_fallback
	DecoderFallback_t3123823036 * ___decoder_fallback_3;
	// System.Text.EncoderFallback System.Text.Encoding::encoder_fallback
	EncoderFallback_t1188251036 * ___encoder_fallback_4;
	// System.String System.Text.Encoding::body_name
	String_t* ___body_name_8;
	// System.String System.Text.Encoding::encoding_name
	String_t* ___encoding_name_9;
	// System.String System.Text.Encoding::header_name
	String_t* ___header_name_10;
	// System.Boolean System.Text.Encoding::is_mail_news_display
	bool ___is_mail_news_display_11;
	// System.Boolean System.Text.Encoding::is_mail_news_save
	bool ___is_mail_news_save_12;
	// System.Boolean System.Text.Encoding::is_browser_save
	bool ___is_browser_save_13;
	// System.Boolean System.Text.Encoding::is_browser_display
	bool ___is_browser_display_14;
	// System.String System.Text.Encoding::web_name
	String_t* ___web_name_15;

public:
	inline static int32_t get_offset_of_codePage_0() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___codePage_0)); }
	inline int32_t get_codePage_0() const { return ___codePage_0; }
	inline int32_t* get_address_of_codePage_0() { return &___codePage_0; }
	inline void set_codePage_0(int32_t value)
	{
		___codePage_0 = value;
	}

	inline static int32_t get_offset_of_windows_code_page_1() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___windows_code_page_1)); }
	inline int32_t get_windows_code_page_1() const { return ___windows_code_page_1; }
	inline int32_t* get_address_of_windows_code_page_1() { return &___windows_code_page_1; }
	inline void set_windows_code_page_1(int32_t value)
	{
		___windows_code_page_1 = value;
	}

	inline static int32_t get_offset_of_is_readonly_2() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_readonly_2)); }
	inline bool get_is_readonly_2() const { return ___is_readonly_2; }
	inline bool* get_address_of_is_readonly_2() { return &___is_readonly_2; }
	inline void set_is_readonly_2(bool value)
	{
		___is_readonly_2 = value;
	}

	inline static int32_t get_offset_of_decoder_fallback_3() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___decoder_fallback_3)); }
	inline DecoderFallback_t3123823036 * get_decoder_fallback_3() const { return ___decoder_fallback_3; }
	inline DecoderFallback_t3123823036 ** get_address_of_decoder_fallback_3() { return &___decoder_fallback_3; }
	inline void set_decoder_fallback_3(DecoderFallback_t3123823036 * value)
	{
		___decoder_fallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_fallback_3), value);
	}

	inline static int32_t get_offset_of_encoder_fallback_4() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoder_fallback_4)); }
	inline EncoderFallback_t1188251036 * get_encoder_fallback_4() const { return ___encoder_fallback_4; }
	inline EncoderFallback_t1188251036 ** get_address_of_encoder_fallback_4() { return &___encoder_fallback_4; }
	inline void set_encoder_fallback_4(EncoderFallback_t1188251036 * value)
	{
		___encoder_fallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_fallback_4), value);
	}

	inline static int32_t get_offset_of_body_name_8() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___body_name_8)); }
	inline String_t* get_body_name_8() const { return ___body_name_8; }
	inline String_t** get_address_of_body_name_8() { return &___body_name_8; }
	inline void set_body_name_8(String_t* value)
	{
		___body_name_8 = value;
		Il2CppCodeGenWriteBarrier((&___body_name_8), value);
	}

	inline static int32_t get_offset_of_encoding_name_9() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___encoding_name_9)); }
	inline String_t* get_encoding_name_9() const { return ___encoding_name_9; }
	inline String_t** get_address_of_encoding_name_9() { return &___encoding_name_9; }
	inline void set_encoding_name_9(String_t* value)
	{
		___encoding_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_name_9), value);
	}

	inline static int32_t get_offset_of_header_name_10() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___header_name_10)); }
	inline String_t* get_header_name_10() const { return ___header_name_10; }
	inline String_t** get_address_of_header_name_10() { return &___header_name_10; }
	inline void set_header_name_10(String_t* value)
	{
		___header_name_10 = value;
		Il2CppCodeGenWriteBarrier((&___header_name_10), value);
	}

	inline static int32_t get_offset_of_is_mail_news_display_11() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_mail_news_display_11)); }
	inline bool get_is_mail_news_display_11() const { return ___is_mail_news_display_11; }
	inline bool* get_address_of_is_mail_news_display_11() { return &___is_mail_news_display_11; }
	inline void set_is_mail_news_display_11(bool value)
	{
		___is_mail_news_display_11 = value;
	}

	inline static int32_t get_offset_of_is_mail_news_save_12() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_mail_news_save_12)); }
	inline bool get_is_mail_news_save_12() const { return ___is_mail_news_save_12; }
	inline bool* get_address_of_is_mail_news_save_12() { return &___is_mail_news_save_12; }
	inline void set_is_mail_news_save_12(bool value)
	{
		___is_mail_news_save_12 = value;
	}

	inline static int32_t get_offset_of_is_browser_save_13() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_browser_save_13)); }
	inline bool get_is_browser_save_13() const { return ___is_browser_save_13; }
	inline bool* get_address_of_is_browser_save_13() { return &___is_browser_save_13; }
	inline void set_is_browser_save_13(bool value)
	{
		___is_browser_save_13 = value;
	}

	inline static int32_t get_offset_of_is_browser_display_14() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___is_browser_display_14)); }
	inline bool get_is_browser_display_14() const { return ___is_browser_display_14; }
	inline bool* get_address_of_is_browser_display_14() { return &___is_browser_display_14; }
	inline void set_is_browser_display_14(bool value)
	{
		___is_browser_display_14 = value;
	}

	inline static int32_t get_offset_of_web_name_15() { return static_cast<int32_t>(offsetof(Encoding_t1523322056, ___web_name_15)); }
	inline String_t* get_web_name_15() const { return ___web_name_15; }
	inline String_t** get_address_of_web_name_15() { return &___web_name_15; }
	inline void set_web_name_15(String_t* value)
	{
		___web_name_15 = value;
		Il2CppCodeGenWriteBarrier((&___web_name_15), value);
	}
};

struct Encoding_t1523322056_StaticFields
{
public:
	// System.Reflection.Assembly System.Text.Encoding::i18nAssembly
	Assembly_t * ___i18nAssembly_5;
	// System.Boolean System.Text.Encoding::i18nDisabled
	bool ___i18nDisabled_6;
	// System.Object[] System.Text.Encoding::encodings
	ObjectU5BU5D_t2843939325* ___encodings_7;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t1523322056 * ___asciiEncoding_16;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianEncoding
	Encoding_t1523322056 * ___bigEndianEncoding_17;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t1523322056 * ___defaultEncoding_18;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t1523322056 * ___utf7Encoding_19;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithMarkers
	Encoding_t1523322056 * ___utf8EncodingWithMarkers_20;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingWithoutMarkers
	Encoding_t1523322056 * ___utf8EncodingWithoutMarkers_21;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t1523322056 * ___unicodeEncoding_22;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::isoLatin1Encoding
	Encoding_t1523322056 * ___isoLatin1Encoding_23;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8EncodingUnsafe
	Encoding_t1523322056 * ___utf8EncodingUnsafe_24;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t1523322056 * ___utf32Encoding_25;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUTF32Encoding
	Encoding_t1523322056 * ___bigEndianUTF32Encoding_26;
	// System.Object System.Text.Encoding::lockobj
	RuntimeObject * ___lockobj_27;

public:
	inline static int32_t get_offset_of_i18nAssembly_5() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___i18nAssembly_5)); }
	inline Assembly_t * get_i18nAssembly_5() const { return ___i18nAssembly_5; }
	inline Assembly_t ** get_address_of_i18nAssembly_5() { return &___i18nAssembly_5; }
	inline void set_i18nAssembly_5(Assembly_t * value)
	{
		___i18nAssembly_5 = value;
		Il2CppCodeGenWriteBarrier((&___i18nAssembly_5), value);
	}

	inline static int32_t get_offset_of_i18nDisabled_6() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___i18nDisabled_6)); }
	inline bool get_i18nDisabled_6() const { return ___i18nDisabled_6; }
	inline bool* get_address_of_i18nDisabled_6() { return &___i18nDisabled_6; }
	inline void set_i18nDisabled_6(bool value)
	{
		___i18nDisabled_6 = value;
	}

	inline static int32_t get_offset_of_encodings_7() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___encodings_7)); }
	inline ObjectU5BU5D_t2843939325* get_encodings_7() const { return ___encodings_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_encodings_7() { return &___encodings_7; }
	inline void set_encodings_7(ObjectU5BU5D_t2843939325* value)
	{
		___encodings_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_7), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_16() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___asciiEncoding_16)); }
	inline Encoding_t1523322056 * get_asciiEncoding_16() const { return ___asciiEncoding_16; }
	inline Encoding_t1523322056 ** get_address_of_asciiEncoding_16() { return &___asciiEncoding_16; }
	inline void set_asciiEncoding_16(Encoding_t1523322056 * value)
	{
		___asciiEncoding_16 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_16), value);
	}

	inline static int32_t get_offset_of_bigEndianEncoding_17() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianEncoding_17)); }
	inline Encoding_t1523322056 * get_bigEndianEncoding_17() const { return ___bigEndianEncoding_17; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianEncoding_17() { return &___bigEndianEncoding_17; }
	inline void set_bigEndianEncoding_17(Encoding_t1523322056 * value)
	{
		___bigEndianEncoding_17 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianEncoding_17), value);
	}

	inline static int32_t get_offset_of_defaultEncoding_18() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___defaultEncoding_18)); }
	inline Encoding_t1523322056 * get_defaultEncoding_18() const { return ___defaultEncoding_18; }
	inline Encoding_t1523322056 ** get_address_of_defaultEncoding_18() { return &___defaultEncoding_18; }
	inline void set_defaultEncoding_18(Encoding_t1523322056 * value)
	{
		___defaultEncoding_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_18), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_19() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf7Encoding_19)); }
	inline Encoding_t1523322056 * get_utf7Encoding_19() const { return ___utf7Encoding_19; }
	inline Encoding_t1523322056 ** get_address_of_utf7Encoding_19() { return &___utf7Encoding_19; }
	inline void set_utf7Encoding_19(Encoding_t1523322056 * value)
	{
		___utf7Encoding_19 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_19), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithMarkers_20() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8EncodingWithMarkers_20)); }
	inline Encoding_t1523322056 * get_utf8EncodingWithMarkers_20() const { return ___utf8EncodingWithMarkers_20; }
	inline Encoding_t1523322056 ** get_address_of_utf8EncodingWithMarkers_20() { return &___utf8EncodingWithMarkers_20; }
	inline void set_utf8EncodingWithMarkers_20(Encoding_t1523322056 * value)
	{
		___utf8EncodingWithMarkers_20 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithMarkers_20), value);
	}

	inline static int32_t get_offset_of_utf8EncodingWithoutMarkers_21() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8EncodingWithoutMarkers_21)); }
	inline Encoding_t1523322056 * get_utf8EncodingWithoutMarkers_21() const { return ___utf8EncodingWithoutMarkers_21; }
	inline Encoding_t1523322056 ** get_address_of_utf8EncodingWithoutMarkers_21() { return &___utf8EncodingWithoutMarkers_21; }
	inline void set_utf8EncodingWithoutMarkers_21(Encoding_t1523322056 * value)
	{
		___utf8EncodingWithoutMarkers_21 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingWithoutMarkers_21), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_22() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___unicodeEncoding_22)); }
	inline Encoding_t1523322056 * get_unicodeEncoding_22() const { return ___unicodeEncoding_22; }
	inline Encoding_t1523322056 ** get_address_of_unicodeEncoding_22() { return &___unicodeEncoding_22; }
	inline void set_unicodeEncoding_22(Encoding_t1523322056 * value)
	{
		___unicodeEncoding_22 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_22), value);
	}

	inline static int32_t get_offset_of_isoLatin1Encoding_23() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___isoLatin1Encoding_23)); }
	inline Encoding_t1523322056 * get_isoLatin1Encoding_23() const { return ___isoLatin1Encoding_23; }
	inline Encoding_t1523322056 ** get_address_of_isoLatin1Encoding_23() { return &___isoLatin1Encoding_23; }
	inline void set_isoLatin1Encoding_23(Encoding_t1523322056 * value)
	{
		___isoLatin1Encoding_23 = value;
		Il2CppCodeGenWriteBarrier((&___isoLatin1Encoding_23), value);
	}

	inline static int32_t get_offset_of_utf8EncodingUnsafe_24() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf8EncodingUnsafe_24)); }
	inline Encoding_t1523322056 * get_utf8EncodingUnsafe_24() const { return ___utf8EncodingUnsafe_24; }
	inline Encoding_t1523322056 ** get_address_of_utf8EncodingUnsafe_24() { return &___utf8EncodingUnsafe_24; }
	inline void set_utf8EncodingUnsafe_24(Encoding_t1523322056 * value)
	{
		___utf8EncodingUnsafe_24 = value;
		Il2CppCodeGenWriteBarrier((&___utf8EncodingUnsafe_24), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_25() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___utf32Encoding_25)); }
	inline Encoding_t1523322056 * get_utf32Encoding_25() const { return ___utf32Encoding_25; }
	inline Encoding_t1523322056 ** get_address_of_utf32Encoding_25() { return &___utf32Encoding_25; }
	inline void set_utf32Encoding_25(Encoding_t1523322056 * value)
	{
		___utf32Encoding_25 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_25), value);
	}

	inline static int32_t get_offset_of_bigEndianUTF32Encoding_26() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___bigEndianUTF32Encoding_26)); }
	inline Encoding_t1523322056 * get_bigEndianUTF32Encoding_26() const { return ___bigEndianUTF32Encoding_26; }
	inline Encoding_t1523322056 ** get_address_of_bigEndianUTF32Encoding_26() { return &___bigEndianUTF32Encoding_26; }
	inline void set_bigEndianUTF32Encoding_26(Encoding_t1523322056 * value)
	{
		___bigEndianUTF32Encoding_26 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUTF32Encoding_26), value);
	}

	inline static int32_t get_offset_of_lockobj_27() { return static_cast<int32_t>(offsetof(Encoding_t1523322056_StaticFields, ___lockobj_27)); }
	inline RuntimeObject * get_lockobj_27() const { return ___lockobj_27; }
	inline RuntimeObject ** get_address_of_lockobj_27() { return &___lockobj_27; }
	inline void set_lockobj_27(RuntimeObject * value)
	{
		___lockobj_27 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T1523322056_H
#ifndef U3CLOADIMAGECOROUTINEU3EC__ITERATOR1_T3113194463_H
#define U3CLOADIMAGECOROUTINEU3EC__ITERATOR1_T3113194463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosRequest/<LoadImageCoroutine>c__Iterator1
struct  U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463  : public RuntimeObject
{
public:
	// System.String LumosRequest/<LoadImageCoroutine>c__Iterator1::imageLocation
	String_t* ___imageLocation_0;
	// UnityEngine.WWW LumosRequest/<LoadImageCoroutine>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// UnityEngine.Texture2D LumosRequest/<LoadImageCoroutine>c__Iterator1::texture
	Texture2D_t3840446185 * ___texture_2;
	// System.Object LumosRequest/<LoadImageCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LumosRequest/<LoadImageCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 LumosRequest/<LoadImageCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_imageLocation_0() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___imageLocation_0)); }
	inline String_t* get_imageLocation_0() const { return ___imageLocation_0; }
	inline String_t** get_address_of_imageLocation_0() { return &___imageLocation_0; }
	inline void set_imageLocation_0(String_t* value)
	{
		___imageLocation_0 = value;
		Il2CppCodeGenWriteBarrier((&___imageLocation_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___texture_2)); }
	inline Texture2D_t3840446185 * get_texture_2() const { return ___texture_2; }
	inline Texture2D_t3840446185 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(Texture2D_t3840446185 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGECOROUTINEU3EC__ITERATOR1_T3113194463_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef MD5_T3177620429_H
#define MD5_T3177620429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.MD5
struct  MD5_t3177620429  : public HashAlgorithm_t1432317219
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5_T3177620429_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T913802012_H
#define ENUMERATOR_T913802012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t913802012 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3319525431 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___l_0)); }
	inline List_1_t3319525431 * get_l_0() const { return ___l_0; }
	inline List_1_t3319525431 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3319525431 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T913802012_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef WWW_T3688466362_H
#define WWW_T3688466362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t3688466362  : public CustomYieldInstruction_t1895667560
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t463507806 * ____uwr_0;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t3688466362, ____uwr_0)); }
	inline UnityWebRequest_t463507806 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t463507806 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t463507806 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T3688466362_H
#ifndef KEYEDHASHALGORITHM_T112861511_H
#define KEYEDHASHALGORITHM_T112861511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t112861511  : public HashAlgorithm_t1432317219
{
public:
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t4116647657* ___KeyValue_4;

public:
	inline static int32_t get_offset_of_KeyValue_4() { return static_cast<int32_t>(offsetof(KeyedHashAlgorithm_t112861511, ___KeyValue_4)); }
	inline ByteU5BU5D_t4116647657* get_KeyValue_4() const { return ___KeyValue_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_KeyValue_4() { return &___KeyValue_4; }
	inline void set_KeyValue_4(ByteU5BU5D_t4116647657* value)
	{
		___KeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDHASHALGORITHM_T112861511_H
#ifndef ENUMERATOR_T3350232909_H
#define ENUMERATOR_T3350232909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct  Enumerator_t3350232909 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator::hashset
	HashSet_1_t1645055638 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_hashset_0() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___hashset_0)); }
	inline HashSet_1_t1645055638 * get_hashset_0() const { return ___hashset_0; }
	inline HashSet_1_t1645055638 ** get_address_of_hashset_0() { return &___hashset_0; }
	inline void set_hashset_0(HashSet_1_t1645055638 * value)
	{
		___hashset_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashset_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3350232909, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3350232909_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUMERATOR_T2117577434_H
#define ENUMERATOR_T2117577434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1/Enumerator<System.String>
struct  Enumerator_t2117577434 
{
public:
	// System.Collections.Generic.HashSet`1<T> System.Collections.Generic.HashSet`1/Enumerator::hashset
	HashSet_1_t412400163 * ___hashset_0;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.HashSet`1/Enumerator::stamp
	int32_t ___stamp_2;
	// T System.Collections.Generic.HashSet`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_hashset_0() { return static_cast<int32_t>(offsetof(Enumerator_t2117577434, ___hashset_0)); }
	inline HashSet_1_t412400163 * get_hashset_0() const { return ___hashset_0; }
	inline HashSet_1_t412400163 ** get_address_of_hashset_0() { return &___hashset_0; }
	inline void set_hashset_0(HashSet_1_t412400163 * value)
	{
		___hashset_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashset_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2117577434, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2117577434, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2117577434, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2117577434_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef MD5CRYPTOSERVICEPROVIDER_T3005586042_H
#define MD5CRYPTOSERVICEPROVIDER_T3005586042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.MD5CryptoServiceProvider
struct  MD5CryptoServiceProvider_t3005586042  : public MD5_t3177620429
{
public:
	// System.UInt32[] System.Security.Cryptography.MD5CryptoServiceProvider::_H
	UInt32U5BU5D_t2770800703* ____H_4;
	// System.UInt32[] System.Security.Cryptography.MD5CryptoServiceProvider::buff
	UInt32U5BU5D_t2770800703* ___buff_5;
	// System.UInt64 System.Security.Cryptography.MD5CryptoServiceProvider::count
	uint64_t ___count_6;
	// System.Byte[] System.Security.Cryptography.MD5CryptoServiceProvider::_ProcessingBuffer
	ByteU5BU5D_t4116647657* ____ProcessingBuffer_7;
	// System.Int32 System.Security.Cryptography.MD5CryptoServiceProvider::_ProcessingBufferCount
	int32_t ____ProcessingBufferCount_8;

public:
	inline static int32_t get_offset_of__H_4() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ____H_4)); }
	inline UInt32U5BU5D_t2770800703* get__H_4() const { return ____H_4; }
	inline UInt32U5BU5D_t2770800703** get_address_of__H_4() { return &____H_4; }
	inline void set__H_4(UInt32U5BU5D_t2770800703* value)
	{
		____H_4 = value;
		Il2CppCodeGenWriteBarrier((&____H_4), value);
	}

	inline static int32_t get_offset_of_buff_5() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ___buff_5)); }
	inline UInt32U5BU5D_t2770800703* get_buff_5() const { return ___buff_5; }
	inline UInt32U5BU5D_t2770800703** get_address_of_buff_5() { return &___buff_5; }
	inline void set_buff_5(UInt32U5BU5D_t2770800703* value)
	{
		___buff_5 = value;
		Il2CppCodeGenWriteBarrier((&___buff_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ___count_6)); }
	inline uint64_t get_count_6() const { return ___count_6; }
	inline uint64_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(uint64_t value)
	{
		___count_6 = value;
	}

	inline static int32_t get_offset_of__ProcessingBuffer_7() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ____ProcessingBuffer_7)); }
	inline ByteU5BU5D_t4116647657* get__ProcessingBuffer_7() const { return ____ProcessingBuffer_7; }
	inline ByteU5BU5D_t4116647657** get_address_of__ProcessingBuffer_7() { return &____ProcessingBuffer_7; }
	inline void set__ProcessingBuffer_7(ByteU5BU5D_t4116647657* value)
	{
		____ProcessingBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((&____ProcessingBuffer_7), value);
	}

	inline static int32_t get_offset_of__ProcessingBufferCount_8() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042, ____ProcessingBufferCount_8)); }
	inline int32_t get__ProcessingBufferCount_8() const { return ____ProcessingBufferCount_8; }
	inline int32_t* get_address_of__ProcessingBufferCount_8() { return &____ProcessingBufferCount_8; }
	inline void set__ProcessingBufferCount_8(int32_t value)
	{
		____ProcessingBufferCount_8 = value;
	}
};

struct MD5CryptoServiceProvider_t3005586042_StaticFields
{
public:
	// System.UInt32[] System.Security.Cryptography.MD5CryptoServiceProvider::K
	UInt32U5BU5D_t2770800703* ___K_9;

public:
	inline static int32_t get_offset_of_K_9() { return static_cast<int32_t>(offsetof(MD5CryptoServiceProvider_t3005586042_StaticFields, ___K_9)); }
	inline UInt32U5BU5D_t2770800703* get_K_9() const { return ___K_9; }
	inline UInt32U5BU5D_t2770800703** get_address_of_K_9() { return &___K_9; }
	inline void set_K_9(UInt32U5BU5D_t2770800703* value)
	{
		___K_9 = value;
		Il2CppCodeGenWriteBarrier((&___K_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5CRYPTOSERVICEPROVIDER_T3005586042_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef NETWORKREACHABILITY_T3450623372_H
#define NETWORKREACHABILITY_T3450623372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NetworkReachability
struct  NetworkReachability_t3450623372 
{
public:
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkReachability_t3450623372, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKREACHABILITY_T3450623372_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef TOKEN_T776369722_H
#define TOKEN_T776369722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosJson/TOKEN
struct  TOKEN_t776369722 
{
public:
	// System.Int32 LumosJson/TOKEN::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TOKEN_t776369722, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T776369722_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef LOGTYPE_T73765434_H
#define LOGTYPE_T73765434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t73765434 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t73765434, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T73765434_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef TYPE_T3858932131_H
#define TYPE_T3858932131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption/Type
struct  Type_t3858932131 
{
public:
	// System.Int32 UnityEngine.GUILayoutOption/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3858932131, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3858932131_H
#ifndef HMAC_T2621101144_H
#define HMAC_T2621101144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HMAC
struct  HMAC_t2621101144  : public KeyedHashAlgorithm_t112861511
{
public:
	// System.Boolean System.Security.Cryptography.HMAC::_disposed
	bool ____disposed_5;
	// System.String System.Security.Cryptography.HMAC::_hashName
	String_t* ____hashName_6;
	// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.HMAC::_algo
	HashAlgorithm_t1432317219 * ____algo_7;
	// Mono.Security.Cryptography.BlockProcessor System.Security.Cryptography.HMAC::_block
	BlockProcessor_t1851031225 * ____block_8;
	// System.Int32 System.Security.Cryptography.HMAC::_blockSizeValue
	int32_t ____blockSizeValue_9;

public:
	inline static int32_t get_offset_of__disposed_5() { return static_cast<int32_t>(offsetof(HMAC_t2621101144, ____disposed_5)); }
	inline bool get__disposed_5() const { return ____disposed_5; }
	inline bool* get_address_of__disposed_5() { return &____disposed_5; }
	inline void set__disposed_5(bool value)
	{
		____disposed_5 = value;
	}

	inline static int32_t get_offset_of__hashName_6() { return static_cast<int32_t>(offsetof(HMAC_t2621101144, ____hashName_6)); }
	inline String_t* get__hashName_6() const { return ____hashName_6; }
	inline String_t** get_address_of__hashName_6() { return &____hashName_6; }
	inline void set__hashName_6(String_t* value)
	{
		____hashName_6 = value;
		Il2CppCodeGenWriteBarrier((&____hashName_6), value);
	}

	inline static int32_t get_offset_of__algo_7() { return static_cast<int32_t>(offsetof(HMAC_t2621101144, ____algo_7)); }
	inline HashAlgorithm_t1432317219 * get__algo_7() const { return ____algo_7; }
	inline HashAlgorithm_t1432317219 ** get_address_of__algo_7() { return &____algo_7; }
	inline void set__algo_7(HashAlgorithm_t1432317219 * value)
	{
		____algo_7 = value;
		Il2CppCodeGenWriteBarrier((&____algo_7), value);
	}

	inline static int32_t get_offset_of__block_8() { return static_cast<int32_t>(offsetof(HMAC_t2621101144, ____block_8)); }
	inline BlockProcessor_t1851031225 * get__block_8() const { return ____block_8; }
	inline BlockProcessor_t1851031225 ** get_address_of__block_8() { return &____block_8; }
	inline void set__block_8(BlockProcessor_t1851031225 * value)
	{
		____block_8 = value;
		Il2CppCodeGenWriteBarrier((&____block_8), value);
	}

	inline static int32_t get_offset_of__blockSizeValue_9() { return static_cast<int32_t>(offsetof(HMAC_t2621101144, ____blockSizeValue_9)); }
	inline int32_t get__blockSizeValue_9() const { return ____blockSizeValue_9; }
	inline int32_t* get_address_of__blockSizeValue_9() { return &____blockSizeValue_9; }
	inline void set__blockSizeValue_9(int32_t value)
	{
		____blockSizeValue_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_T2621101144_H
#ifndef SYSTEMLANGUAGE_T949212163_H
#define SYSTEMLANGUAGE_T949212163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemLanguage
struct  SystemLanguage_t949212163 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SystemLanguage_t949212163, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMLANGUAGE_T949212163_H
#ifndef LUMOSFEEDBACKGUI_T1268982120_H
#define LUMOSFEEDBACKGUI_T1268982120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosFeedbackGUI
struct  LumosFeedbackGUI_t1268982120  : public RuntimeObject
{
public:

public:
};

struct LumosFeedbackGUI_t1268982120_StaticFields
{
public:
	// LumosFeedbackGUI/CloseHandler LumosFeedbackGUI::windowClosed
	CloseHandler_t719506259 * ___windowClosed_0;
	// UnityEngine.GUISkin LumosFeedbackGUI::<skin>k__BackingField
	GUISkin_t1244372282 * ___U3CskinU3Ek__BackingField_1;
	// UnityEngine.Rect LumosFeedbackGUI::windowRect
	Rect_t2360479859  ___windowRect_4;
	// System.Boolean LumosFeedbackGUI::visible
	bool ___visible_5;
	// System.Boolean LumosFeedbackGUI::sentMessage
	bool ___sentMessage_6;
	// System.String LumosFeedbackGUI::email
	String_t* ___email_7;
	// System.String LumosFeedbackGUI::message
	String_t* ___message_8;
	// System.String LumosFeedbackGUI::category
	String_t* ___category_9;
	// UnityEngine.GUI/WindowFunction LumosFeedbackGUI::<>f__mg$cache0
	WindowFunction_t3146511083 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_windowClosed_0() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___windowClosed_0)); }
	inline CloseHandler_t719506259 * get_windowClosed_0() const { return ___windowClosed_0; }
	inline CloseHandler_t719506259 ** get_address_of_windowClosed_0() { return &___windowClosed_0; }
	inline void set_windowClosed_0(CloseHandler_t719506259 * value)
	{
		___windowClosed_0 = value;
		Il2CppCodeGenWriteBarrier((&___windowClosed_0), value);
	}

	inline static int32_t get_offset_of_U3CskinU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___U3CskinU3Ek__BackingField_1)); }
	inline GUISkin_t1244372282 * get_U3CskinU3Ek__BackingField_1() const { return ___U3CskinU3Ek__BackingField_1; }
	inline GUISkin_t1244372282 ** get_address_of_U3CskinU3Ek__BackingField_1() { return &___U3CskinU3Ek__BackingField_1; }
	inline void set_U3CskinU3Ek__BackingField_1(GUISkin_t1244372282 * value)
	{
		___U3CskinU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CskinU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_windowRect_4() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___windowRect_4)); }
	inline Rect_t2360479859  get_windowRect_4() const { return ___windowRect_4; }
	inline Rect_t2360479859 * get_address_of_windowRect_4() { return &___windowRect_4; }
	inline void set_windowRect_4(Rect_t2360479859  value)
	{
		___windowRect_4 = value;
	}

	inline static int32_t get_offset_of_visible_5() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___visible_5)); }
	inline bool get_visible_5() const { return ___visible_5; }
	inline bool* get_address_of_visible_5() { return &___visible_5; }
	inline void set_visible_5(bool value)
	{
		___visible_5 = value;
	}

	inline static int32_t get_offset_of_sentMessage_6() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___sentMessage_6)); }
	inline bool get_sentMessage_6() const { return ___sentMessage_6; }
	inline bool* get_address_of_sentMessage_6() { return &___sentMessage_6; }
	inline void set_sentMessage_6(bool value)
	{
		___sentMessage_6 = value;
	}

	inline static int32_t get_offset_of_email_7() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___email_7)); }
	inline String_t* get_email_7() const { return ___email_7; }
	inline String_t** get_address_of_email_7() { return &___email_7; }
	inline void set_email_7(String_t* value)
	{
		___email_7 = value;
		Il2CppCodeGenWriteBarrier((&___email_7), value);
	}

	inline static int32_t get_offset_of_message_8() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___message_8)); }
	inline String_t* get_message_8() const { return ___message_8; }
	inline String_t** get_address_of_message_8() { return &___message_8; }
	inline void set_message_8(String_t* value)
	{
		___message_8 = value;
		Il2CppCodeGenWriteBarrier((&___message_8), value);
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline WindowFunction_t3146511083 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline WindowFunction_t3146511083 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(WindowFunction_t3146511083 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSFEEDBACKGUI_T1268982120_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef HMACSHA1_T1952596188_H
#define HMACSHA1_T1952596188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HMACSHA1
struct  HMACSHA1_t1952596188  : public HMAC_t2621101144
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMACSHA1_T1952596188_H
#ifndef GUILAYOUTOPTION_T811797299_H
#define GUILAYOUTOPTION_T811797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption
struct  GUILayoutOption_t811797299  : public RuntimeObject
{
public:
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(GUILayoutOption_t811797299, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(GUILayoutOption_t811797299, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTION_T811797299_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t777629997  : public ArgumentException_t132251570
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t777629997, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef GUISKIN_T1244372282_H
#define GUISKIN_T1244372282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t1244372282  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1956802104 * ___m_Font_2;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t3956901511 * ___m_box_3;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t3956901511 * ___m_button_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t3956901511 * ___m_toggle_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t3956901511 * ___m_label_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t3956901511 * ___m_textField_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t3956901511 * ___m_textArea_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t3956901511 * ___m_window_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t3956901511 * ___m_horizontalSlider_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t3956901511 * ___m_horizontalSliderThumb_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t3956901511 * ___m_verticalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t3956901511 * ___m_verticalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t3956901511 * ___m_horizontalScrollbar_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t3956901511 * ___m_horizontalScrollbarThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarLeftButton_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarRightButton_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t3956901511 * ___m_verticalScrollbar_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t3956901511 * ___m_verticalScrollbarThumb_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarUpButton_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarDownButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t3956901511 * ___m_ScrollView_22;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2383250302* ___m_CustomStyles_23;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1774757634 * ___m_Settings_24;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t3742157810 * ___m_Styles_26;

public:
	inline static int32_t get_offset_of_m_Font_2() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Font_2)); }
	inline Font_t1956802104 * get_m_Font_2() const { return ___m_Font_2; }
	inline Font_t1956802104 ** get_address_of_m_Font_2() { return &___m_Font_2; }
	inline void set_m_Font_2(Font_t1956802104 * value)
	{
		___m_Font_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_2), value);
	}

	inline static int32_t get_offset_of_m_box_3() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_box_3)); }
	inline GUIStyle_t3956901511 * get_m_box_3() const { return ___m_box_3; }
	inline GUIStyle_t3956901511 ** get_address_of_m_box_3() { return &___m_box_3; }
	inline void set_m_box_3(GUIStyle_t3956901511 * value)
	{
		___m_box_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_3), value);
	}

	inline static int32_t get_offset_of_m_button_4() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_button_4)); }
	inline GUIStyle_t3956901511 * get_m_button_4() const { return ___m_button_4; }
	inline GUIStyle_t3956901511 ** get_address_of_m_button_4() { return &___m_button_4; }
	inline void set_m_button_4(GUIStyle_t3956901511 * value)
	{
		___m_button_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_4), value);
	}

	inline static int32_t get_offset_of_m_toggle_5() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_toggle_5)); }
	inline GUIStyle_t3956901511 * get_m_toggle_5() const { return ___m_toggle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_m_toggle_5() { return &___m_toggle_5; }
	inline void set_m_toggle_5(GUIStyle_t3956901511 * value)
	{
		___m_toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_5), value);
	}

	inline static int32_t get_offset_of_m_label_6() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_label_6)); }
	inline GUIStyle_t3956901511 * get_m_label_6() const { return ___m_label_6; }
	inline GUIStyle_t3956901511 ** get_address_of_m_label_6() { return &___m_label_6; }
	inline void set_m_label_6(GUIStyle_t3956901511 * value)
	{
		___m_label_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_6), value);
	}

	inline static int32_t get_offset_of_m_textField_7() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textField_7)); }
	inline GUIStyle_t3956901511 * get_m_textField_7() const { return ___m_textField_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textField_7() { return &___m_textField_7; }
	inline void set_m_textField_7(GUIStyle_t3956901511 * value)
	{
		___m_textField_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_7), value);
	}

	inline static int32_t get_offset_of_m_textArea_8() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textArea_8)); }
	inline GUIStyle_t3956901511 * get_m_textArea_8() const { return ___m_textArea_8; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textArea_8() { return &___m_textArea_8; }
	inline void set_m_textArea_8(GUIStyle_t3956901511 * value)
	{
		___m_textArea_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_8), value);
	}

	inline static int32_t get_offset_of_m_window_9() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_window_9)); }
	inline GUIStyle_t3956901511 * get_m_window_9() const { return ___m_window_9; }
	inline GUIStyle_t3956901511 ** get_address_of_m_window_9() { return &___m_window_9; }
	inline void set_m_window_9(GUIStyle_t3956901511 * value)
	{
		___m_window_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_9), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_10() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSlider_10)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSlider_10() const { return ___m_horizontalSlider_10; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSlider_10() { return &___m_horizontalSlider_10; }
	inline void set_m_horizontalSlider_10(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_10), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_11() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSliderThumb_11)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSliderThumb_11() const { return ___m_horizontalSliderThumb_11; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSliderThumb_11() { return &___m_horizontalSliderThumb_11; }
	inline void set_m_horizontalSliderThumb_11(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSliderThumb_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_11), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSlider_12)); }
	inline GUIStyle_t3956901511 * get_m_verticalSlider_12() const { return ___m_verticalSlider_12; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSlider_12() { return &___m_verticalSlider_12; }
	inline void set_m_verticalSlider_12(GUIStyle_t3956901511 * value)
	{
		___m_verticalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSliderThumb_13)); }
	inline GUIStyle_t3956901511 * get_m_verticalSliderThumb_13() const { return ___m_verticalSliderThumb_13; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSliderThumb_13() { return &___m_verticalSliderThumb_13; }
	inline void set_m_verticalSliderThumb_13(GUIStyle_t3956901511 * value)
	{
		___m_verticalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_14() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbar_14)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbar_14() const { return ___m_horizontalScrollbar_14; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbar_14() { return &___m_horizontalScrollbar_14; }
	inline void set_m_horizontalScrollbar_14(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarThumb_15)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarThumb_15() const { return ___m_horizontalScrollbarThumb_15; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarThumb_15() { return &___m_horizontalScrollbarThumb_15; }
	inline void set_m_horizontalScrollbarThumb_15(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_16() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarLeftButton_16)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarLeftButton_16() const { return ___m_horizontalScrollbarLeftButton_16; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarLeftButton_16() { return &___m_horizontalScrollbarLeftButton_16; }
	inline void set_m_horizontalScrollbarLeftButton_16(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarLeftButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_17() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarRightButton_17)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarRightButton_17() const { return ___m_horizontalScrollbarRightButton_17; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarRightButton_17() { return &___m_horizontalScrollbarRightButton_17; }
	inline void set_m_horizontalScrollbarRightButton_17(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarRightButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_17), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_18() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbar_18)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbar_18() const { return ___m_verticalScrollbar_18; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbar_18() { return &___m_verticalScrollbar_18; }
	inline void set_m_verticalScrollbar_18(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbar_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_18), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_19() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarThumb_19)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarThumb_19() const { return ___m_verticalScrollbarThumb_19; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarThumb_19() { return &___m_verticalScrollbarThumb_19; }
	inline void set_m_verticalScrollbarThumb_19(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarThumb_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_20() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarUpButton_20)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarUpButton_20() const { return ___m_verticalScrollbarUpButton_20; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarUpButton_20() { return &___m_verticalScrollbarUpButton_20; }
	inline void set_m_verticalScrollbarUpButton_20(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarUpButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_21() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarDownButton_21)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarDownButton_21() const { return ___m_verticalScrollbarDownButton_21; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarDownButton_21() { return &___m_verticalScrollbarDownButton_21; }
	inline void set_m_verticalScrollbarDownButton_21(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarDownButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_21), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_22() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_ScrollView_22)); }
	inline GUIStyle_t3956901511 * get_m_ScrollView_22() const { return ___m_ScrollView_22; }
	inline GUIStyle_t3956901511 ** get_address_of_m_ScrollView_22() { return &___m_ScrollView_22; }
	inline void set_m_ScrollView_22(GUIStyle_t3956901511 * value)
	{
		___m_ScrollView_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_22), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_23() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_CustomStyles_23)); }
	inline GUIStyleU5BU5D_t2383250302* get_m_CustomStyles_23() const { return ___m_CustomStyles_23; }
	inline GUIStyleU5BU5D_t2383250302** get_address_of_m_CustomStyles_23() { return &___m_CustomStyles_23; }
	inline void set_m_CustomStyles_23(GUIStyleU5BU5D_t2383250302* value)
	{
		___m_CustomStyles_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_23), value);
	}

	inline static int32_t get_offset_of_m_Settings_24() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Settings_24)); }
	inline GUISettings_t1774757634 * get_m_Settings_24() const { return ___m_Settings_24; }
	inline GUISettings_t1774757634 ** get_address_of_m_Settings_24() { return &___m_Settings_24; }
	inline void set_m_Settings_24(GUISettings_t1774757634 * value)
	{
		___m_Settings_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_24), value);
	}

	inline static int32_t get_offset_of_m_Styles_26() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Styles_26)); }
	inline Dictionary_2_t3742157810 * get_m_Styles_26() const { return ___m_Styles_26; }
	inline Dictionary_2_t3742157810 ** get_address_of_m_Styles_26() { return &___m_Styles_26; }
	inline void set_m_Styles_26(Dictionary_2_t3742157810 * value)
	{
		___m_Styles_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_26), value);
	}
};

struct GUISkin_t1244372282_StaticFields
{
public:
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t3956901511 * ___ms_Error_25;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t1143955295 * ___m_SkinChanged_27;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t1244372282 * ___current_28;

public:
	inline static int32_t get_offset_of_ms_Error_25() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___ms_Error_25)); }
	inline GUIStyle_t3956901511 * get_ms_Error_25() const { return ___ms_Error_25; }
	inline GUIStyle_t3956901511 ** get_address_of_ms_Error_25() { return &___ms_Error_25; }
	inline void set_ms_Error_25(GUIStyle_t3956901511 * value)
	{
		___ms_Error_25 = value;
		Il2CppCodeGenWriteBarrier((&___ms_Error_25), value);
	}

	inline static int32_t get_offset_of_m_SkinChanged_27() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___m_SkinChanged_27)); }
	inline SkinChangedDelegate_t1143955295 * get_m_SkinChanged_27() const { return ___m_SkinChanged_27; }
	inline SkinChangedDelegate_t1143955295 ** get_address_of_m_SkinChanged_27() { return &___m_SkinChanged_27; }
	inline void set_m_SkinChanged_27(SkinChangedDelegate_t1143955295 * value)
	{
		___m_SkinChanged_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_27), value);
	}

	inline static int32_t get_offset_of_current_28() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___current_28)); }
	inline GUISkin_t1244372282 * get_current_28() const { return ___current_28; }
	inline GUISkin_t1244372282 ** get_address_of_current_28() { return &___current_28; }
	inline void set_current_28(GUISkin_t1244372282 * value)
	{
		___current_28 = value;
		Il2CppCodeGenWriteBarrier((&___current_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T1244372282_H
#ifndef WINDOWFUNCTION_T3146511083_H
#define WINDOWFUNCTION_T3146511083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUI/WindowFunction
struct  WindowFunction_t3146511083  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWFUNCTION_T3146511083_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef LOGCALLBACK_T3588208630_H
#define LOGCALLBACK_T3588208630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Application/LogCallback
struct  LogCallback_t3588208630  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCALLBACK_T3588208630_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef LUMOSCREDENTIALS_T1718792749_H
#define LUMOSCREDENTIALS_T1718792749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosCredentials
struct  LumosCredentials_t1718792749  : public ScriptableObject_t2528358522
{
public:
	// System.String LumosCredentials::apiKey
	String_t* ___apiKey_2;

public:
	inline static int32_t get_offset_of_apiKey_2() { return static_cast<int32_t>(offsetof(LumosCredentials_t1718792749, ___apiKey_2)); }
	inline String_t* get_apiKey_2() const { return ___apiKey_2; }
	inline String_t** get_address_of_apiKey_2() { return &___apiKey_2; }
	inline void set_apiKey_2(String_t* value)
	{
		___apiKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___apiKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSCREDENTIALS_T1718792749_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ACTION_1_T3252573759_H
#define ACTION_1_T3252573759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Object>
struct  Action_1_t3252573759  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3252573759_H
#ifndef ACTION_1_T269755560_H
#define ACTION_1_T269755560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Boolean>
struct  Action_1_t269755560  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T269755560_H
#ifndef CLOSEHANDLER_T719506259_H
#define CLOSEHANDLER_T719506259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosFeedbackGUI/CloseHandler
struct  CloseHandler_t719506259  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEHANDLER_T719506259_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef LUMOSANALYTICS_T2745971597_H
#define LUMOSANALYTICS_T2745971597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosAnalytics
struct  LumosAnalytics_t2745971597  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LumosAnalytics::useLevelsAsCategories
	bool ___useLevelsAsCategories_2;
	// System.Boolean LumosAnalytics::recordLevelCompletionEvents
	bool ___recordLevelCompletionEvents_3;

public:
	inline static int32_t get_offset_of_useLevelsAsCategories_2() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597, ___useLevelsAsCategories_2)); }
	inline bool get_useLevelsAsCategories_2() const { return ___useLevelsAsCategories_2; }
	inline bool* get_address_of_useLevelsAsCategories_2() { return &___useLevelsAsCategories_2; }
	inline void set_useLevelsAsCategories_2(bool value)
	{
		___useLevelsAsCategories_2 = value;
	}

	inline static int32_t get_offset_of_recordLevelCompletionEvents_3() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597, ___recordLevelCompletionEvents_3)); }
	inline bool get_recordLevelCompletionEvents_3() const { return ___recordLevelCompletionEvents_3; }
	inline bool* get_address_of_recordLevelCompletionEvents_3() { return &___recordLevelCompletionEvents_3; }
	inline void set_recordLevelCompletionEvents_3(bool value)
	{
		___recordLevelCompletionEvents_3 = value;
	}
};

struct LumosAnalytics_t2745971597_StaticFields
{
public:
	// System.String LumosAnalytics::_baseUrl
	String_t* ____baseUrl_4;
	// System.Single LumosAnalytics::levelStartTime
	float ___levelStartTime_5;
	// LumosAnalytics LumosAnalytics::instance
	LumosAnalytics_t2745971597 * ___instance_6;
	// System.Action LumosAnalytics::<>f__mg$cache0
	Action_t1264377477 * ___U3CU3Ef__mgU24cache0_7;
	// System.Action LumosAnalytics::<>f__mg$cache1
	Action_t1264377477 * ___U3CU3Ef__mgU24cache1_8;

public:
	inline static int32_t get_offset_of__baseUrl_4() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ____baseUrl_4)); }
	inline String_t* get__baseUrl_4() const { return ____baseUrl_4; }
	inline String_t** get_address_of__baseUrl_4() { return &____baseUrl_4; }
	inline void set__baseUrl_4(String_t* value)
	{
		____baseUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_4), value);
	}

	inline static int32_t get_offset_of_levelStartTime_5() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___levelStartTime_5)); }
	inline float get_levelStartTime_5() const { return ___levelStartTime_5; }
	inline float* get_address_of_levelStartTime_5() { return &___levelStartTime_5; }
	inline void set_levelStartTime_5(float value)
	{
		___levelStartTime_5 = value;
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___instance_6)); }
	inline LumosAnalytics_t2745971597 * get_instance_6() const { return ___instance_6; }
	inline LumosAnalytics_t2745971597 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(LumosAnalytics_t2745971597 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSANALYTICS_T2745971597_H
#ifndef LUMOS_T1046626606_H
#define LUMOS_T1046626606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lumos
struct  Lumos_t1046626606  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Lumos::runWhileInEditor
	bool ___runWhileInEditor_2;

public:
	inline static int32_t get_offset_of_runWhileInEditor_2() { return static_cast<int32_t>(offsetof(Lumos_t1046626606, ___runWhileInEditor_2)); }
	inline bool get_runWhileInEditor_2() const { return ___runWhileInEditor_2; }
	inline bool* get_address_of_runWhileInEditor_2() { return &___runWhileInEditor_2; }
	inline void set_runWhileInEditor_2(bool value)
	{
		___runWhileInEditor_2 = value;
	}
};

struct Lumos_t1046626606_StaticFields
{
public:
	// System.Action Lumos::OnReady
	Action_t1264377477 * ___OnReady_3;
	// System.Action Lumos::OnTimerFinish
	Action_t1264377477 * ___OnTimerFinish_4;
	// System.Single Lumos::_timerInterval
	float ____timerInterval_5;
	// LumosCredentials Lumos::<credentials>k__BackingField
	LumosCredentials_t1718792749 * ___U3CcredentialsU3Ek__BackingField_7;
	// System.Boolean Lumos::<debug>k__BackingField
	bool ___U3CdebugU3Ek__BackingField_8;
	// System.String Lumos::<playerId>k__BackingField
	String_t* ___U3CplayerIdU3Ek__BackingField_9;
	// System.Boolean Lumos::<timerPaused>k__BackingField
	bool ___U3CtimerPausedU3Ek__BackingField_10;
	// Lumos Lumos::instance
	Lumos_t1046626606 * ___instance_11;
	// System.Action`1<System.Boolean> Lumos::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_12;
	// System.Action`1<System.Object> Lumos::<>f__mg$cache0
	Action_1_t3252573759 * ___U3CU3Ef__mgU24cache0_13;
	// System.Action`1<System.Object> Lumos::<>f__mg$cache1
	Action_1_t3252573759 * ___U3CU3Ef__mgU24cache1_14;
	// System.Action`1<System.Object> Lumos::<>f__mg$cache2
	Action_1_t3252573759 * ___U3CU3Ef__mgU24cache2_15;

public:
	inline static int32_t get_offset_of_OnReady_3() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___OnReady_3)); }
	inline Action_t1264377477 * get_OnReady_3() const { return ___OnReady_3; }
	inline Action_t1264377477 ** get_address_of_OnReady_3() { return &___OnReady_3; }
	inline void set_OnReady_3(Action_t1264377477 * value)
	{
		___OnReady_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnReady_3), value);
	}

	inline static int32_t get_offset_of_OnTimerFinish_4() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___OnTimerFinish_4)); }
	inline Action_t1264377477 * get_OnTimerFinish_4() const { return ___OnTimerFinish_4; }
	inline Action_t1264377477 ** get_address_of_OnTimerFinish_4() { return &___OnTimerFinish_4; }
	inline void set_OnTimerFinish_4(Action_t1264377477 * value)
	{
		___OnTimerFinish_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnTimerFinish_4), value);
	}

	inline static int32_t get_offset_of__timerInterval_5() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ____timerInterval_5)); }
	inline float get__timerInterval_5() const { return ____timerInterval_5; }
	inline float* get_address_of__timerInterval_5() { return &____timerInterval_5; }
	inline void set__timerInterval_5(float value)
	{
		____timerInterval_5 = value;
	}

	inline static int32_t get_offset_of_U3CcredentialsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CcredentialsU3Ek__BackingField_7)); }
	inline LumosCredentials_t1718792749 * get_U3CcredentialsU3Ek__BackingField_7() const { return ___U3CcredentialsU3Ek__BackingField_7; }
	inline LumosCredentials_t1718792749 ** get_address_of_U3CcredentialsU3Ek__BackingField_7() { return &___U3CcredentialsU3Ek__BackingField_7; }
	inline void set_U3CcredentialsU3Ek__BackingField_7(LumosCredentials_t1718792749 * value)
	{
		___U3CcredentialsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcredentialsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdebugU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CdebugU3Ek__BackingField_8)); }
	inline bool get_U3CdebugU3Ek__BackingField_8() const { return ___U3CdebugU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CdebugU3Ek__BackingField_8() { return &___U3CdebugU3Ek__BackingField_8; }
	inline void set_U3CdebugU3Ek__BackingField_8(bool value)
	{
		___U3CdebugU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CplayerIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CplayerIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CplayerIdU3Ek__BackingField_9() const { return ___U3CplayerIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CplayerIdU3Ek__BackingField_9() { return &___U3CplayerIdU3Ek__BackingField_9; }
	inline void set_U3CplayerIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CplayerIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplayerIdU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CtimerPausedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CtimerPausedU3Ek__BackingField_10)); }
	inline bool get_U3CtimerPausedU3Ek__BackingField_10() const { return ___U3CtimerPausedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CtimerPausedU3Ek__BackingField_10() { return &___U3CtimerPausedU3Ek__BackingField_10; }
	inline void set_U3CtimerPausedU3Ek__BackingField_10(bool value)
	{
		___U3CtimerPausedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___instance_11)); }
	inline Lumos_t1046626606 * get_instance_11() const { return ___instance_11; }
	inline Lumos_t1046626606 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(Lumos_t1046626606 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_14() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__mgU24cache1_14)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__mgU24cache1_14() const { return ___U3CU3Ef__mgU24cache1_14; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__mgU24cache1_14() { return &___U3CU3Ef__mgU24cache1_14; }
	inline void set_U3CU3Ef__mgU24cache1_14(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__mgU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_15() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__mgU24cache2_15)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__mgU24cache2_15() const { return ___U3CU3Ef__mgU24cache2_15; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__mgU24cache2_15() { return &___U3CU3Ef__mgU24cache2_15; }
	inline void set_U3CU3Ef__mgU24cache2_15(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__mgU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOS_T1046626606_H
#ifndef LUMOSDIAGNOSTICS_T279239158_H
#define LUMOSDIAGNOSTICS_T279239158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosDiagnostics
struct  LumosDiagnostics_t279239158  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LumosDiagnostics::recordLogs
	bool ___recordLogs_2;
	// System.Boolean LumosDiagnostics::recordWarnings
	bool ___recordWarnings_3;
	// System.Boolean LumosDiagnostics::recordErrors
	bool ___recordErrors_4;

public:
	inline static int32_t get_offset_of_recordLogs_2() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158, ___recordLogs_2)); }
	inline bool get_recordLogs_2() const { return ___recordLogs_2; }
	inline bool* get_address_of_recordLogs_2() { return &___recordLogs_2; }
	inline void set_recordLogs_2(bool value)
	{
		___recordLogs_2 = value;
	}

	inline static int32_t get_offset_of_recordWarnings_3() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158, ___recordWarnings_3)); }
	inline bool get_recordWarnings_3() const { return ___recordWarnings_3; }
	inline bool* get_address_of_recordWarnings_3() { return &___recordWarnings_3; }
	inline void set_recordWarnings_3(bool value)
	{
		___recordWarnings_3 = value;
	}

	inline static int32_t get_offset_of_recordErrors_4() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158, ___recordErrors_4)); }
	inline bool get_recordErrors_4() const { return ___recordErrors_4; }
	inline bool* get_address_of_recordErrors_4() { return &___recordErrors_4; }
	inline void set_recordErrors_4(bool value)
	{
		___recordErrors_4 = value;
	}
};

struct LumosDiagnostics_t279239158_StaticFields
{
public:
	// System.String LumosDiagnostics::_baseUrl
	String_t* ____baseUrl_5;
	// LumosDiagnostics LumosDiagnostics::instance
	LumosDiagnostics_t279239158 * ___instance_6;
	// System.Action LumosDiagnostics::<>f__mg$cache0
	Action_t1264377477 * ___U3CU3Ef__mgU24cache0_7;
	// System.Action LumosDiagnostics::<>f__mg$cache1
	Action_t1264377477 * ___U3CU3Ef__mgU24cache1_8;
	// UnityEngine.Application/LogCallback LumosDiagnostics::<>f__mg$cache2
	LogCallback_t3588208630 * ___U3CU3Ef__mgU24cache2_9;

public:
	inline static int32_t get_offset_of__baseUrl_5() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ____baseUrl_5)); }
	inline String_t* get__baseUrl_5() const { return ____baseUrl_5; }
	inline String_t** get_address_of__baseUrl_5() { return &____baseUrl_5; }
	inline void set__baseUrl_5(String_t* value)
	{
		____baseUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___instance_6)); }
	inline LumosDiagnostics_t279239158 * get_instance_6() const { return ___instance_6; }
	inline LumosDiagnostics_t279239158 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(LumosDiagnostics_t279239158 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline LogCallback_t3588208630 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline LogCallback_t3588208630 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(LogCallback_t3588208630 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSDIAGNOSTICS_T279239158_H
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUILayoutOption_t811797299 * m_Items[1];

public:
	inline GUILayoutOption_t811797299 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUILayoutOption_t811797299 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t811797299 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GUILayoutOption_t811797299 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUILayoutOption_t811797299 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUILayoutOption_t811797299 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3735703580_gshared (Action_1_t269755560 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m2461023210_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Single>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m2775392932_gshared (Nullable_1_t3119828856 * __this, float p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(!0)
extern "C"  bool HashSet_1_Contains_m3173358704_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C"  bool HashSet_1_Add_m1971460364_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2191841112_gshared (Nullable_1_t3119828856 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Single>::get_Value()
extern "C"  float Nullable_1_get_Value_m1008004203_gshared (Nullable_1_t3119828856 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m3474379962_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3919933788_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t1848589470 * Dictionary_2_get_Values_m2492087945_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1__ctor_m1328752868_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C"  void HashSet_1__ctor_m4231804131_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Collections.Generic.HashSet`1/Enumerator<!0> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t3350232909  HashSet_1_GetEnumerator_m3346268098_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m4213278602_gshared (Enumerator_t3350232909 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3714175425_gshared (Enumerator_t3350232909 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1204547613_gshared (Enumerator_t3350232909 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1938428402_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C"  void HashSet_1_Clear_m507835370_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m2643635200_gshared (Dictionary_2_t2356425762 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m2278349286_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m2714930061_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m3709462088_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1232595126_gshared (Dictionary_2_t2356425762 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m3808121907_gshared (Dictionary_2_t2356425762 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
extern "C"  void Action_1_Invoke_m1933767679_gshared (Action_1_t269755560 * __this, bool p0, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::Log(System.Object)
extern "C"  void Lumos_Log_m2411948670 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// LumosCredentials LumosCredentials::Load()
extern "C"  LumosCredentials_t1718792749 * LumosCredentials_Load_m1109527268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::set_credentials(LumosCredentials)
extern "C"  void Lumos_set_credentials_m2691661372 (RuntimeObject * __this /* static, unused */, LumosCredentials_t1718792749 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// LumosCredentials Lumos::get_credentials()
extern "C"  LumosCredentials_t1718792749 * Lumos_get_credentials_m1762176871 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m857789090 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::set_timerInterval(System.Single)
extern "C"  void Lumos_set_timerInterval_m1080298005 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m3735703580(__this, p0, p1, method) ((  void (*) (Action_1_t269755560 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m3735703580_gshared)(__this, p0, p1, method)
// System.Void LumosPlayer::Init(System.Action`1<System.Boolean>)
extern "C"  void LumosPlayer_Init_m3905959549 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___callback0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::LogError(System.Object)
extern "C"  void Lumos_LogError_m3535111913 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos/<SendQueuedData>c__Iterator0::.ctor()
extern "C"  void U3CSendQueuedDataU3Ec__Iterator0__ctor_m4007735274 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m118522912(__this, p0, p1, method) ((  void (*) (Action_1_t3252573759 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method)
// System.Void Lumos::LogMessage(System.Action`1<System.Object>,System.Object)
extern "C"  void Lumos_LogMessage_m2367926511 (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___logger0, RuntimeObject * ___message1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Lumos::get_debug()
extern "C"  bool Lumos_get_debug_m1677971330 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Object>::Invoke(!0)
#define Action_1_Invoke_m2461023210(__this, p0, method) ((  void (*) (Action_1_t3252573759 *, RuntimeObject *, const RuntimeMethod*))Action_1_Invoke_m2461023210_gshared)(__this, p0, method)
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m937035532 (Action_t1264377477 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Lumos::SendQueuedData()
extern "C"  RuntimeObject* Lumos_SendQueuedData_m1193834825 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine Lumos::RunRoutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * Lumos_RunRoutine_m1158721104 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Lumos::get_timerInterval()
extern "C"  float Lumos_get_timerInterval_m619285440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Lumos::get_timerPaused()
extern "C"  bool Lumos_get_timerPaused_m281469672 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2994342681 (Action_t1264377477 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::add_OnReady(System.Action)
extern "C"  void Lumos_add_OnReady_m627233499 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::add_OnTimerFinish(System.Action)
extern "C"  void Lumos_add_OnTimerFinish_m2405319448 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosAnalytics::RecordEvent(System.String,System.Boolean)
extern "C"  void LumosAnalytics_RecordEvent_m3453334534 (RuntimeObject * __this /* static, unused */, String_t* ___eventID0, bool ___repeatable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosAnalytics::RecordEvent(System.String,System.Single,System.Boolean)
extern "C"  void LumosAnalytics_RecordEvent_m2084622174 (RuntimeObject * __this /* static, unused */, String_t* ___eventID0, float ___val1, bool ___repeatable2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<LumosAnalytics>()
#define GameObject_GetComponent_TisLumosAnalytics_t2745971597_m3266012221(__this, method) ((  LumosAnalytics_t2745971597 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void LumosEvents::Record(System.String,System.String,System.Nullable`1<System.Single>,System.Boolean)
extern "C"  void LumosEvents_Record_m3157052489 (RuntimeObject * __this /* static, unused */, String_t* ___category0, String_t* ___eventID1, Nullable_1_t3119828856  ___val2, bool ___repeatable3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Nullable`1<System.Single>::.ctor(!0)
#define Nullable_1__ctor_m2775392932(__this, p0, method) ((  void (*) (Nullable_1_t3119828856 *, float, const RuntimeMethod*))Nullable_1__ctor_m2775392932_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<LumosAnalytics>()
#define GameObject_AddComponent_TisLumosAnalytics_t2745971597_m4089171744(__this, method) ((  LumosAnalytics_t2745971597 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t631007953 * Resources_Load_m3480190876 (RuntimeObject * __this /* static, unused */, String_t* p0, Type_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m144650965 (LogCallback_t3588208630 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::RegisterLogCallback(UnityEngine.Application/LogCallback)
extern "C"  void Application_RegisterLogCallback_m484975236 (RuntimeObject * __this /* static, unused */, LogCallback_t3588208630 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosFeedbackGUI::OnGUI()
extern "C"  void LumosFeedbackGUI_OnGUI_m2428985537 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<LumosDiagnostics>()
#define GameObject_GetComponent_TisLumosDiagnostics_t279239158_m51467272(__this, method) ((  LumosDiagnostics_t279239158 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<LumosDiagnostics>()
#define GameObject_AddComponent_TisLumosDiagnostics_t279239158_m1556835766(__this, method) ((  LumosDiagnostics_t279239158 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Boolean LumosAnalytics::IsInitialized()
extern "C"  bool LumosAnalytics_IsInitialized_m3408112055 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::LogWarning(System.Object)
extern "C"  void Lumos_LogWarning_m3534960519 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosAnalytics::get_levelsAsCategories()
extern "C"  bool LumosAnalytics_get_levelsAsCategories_m3551605565 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::get_loadedLevelName()
extern "C"  String_t* Application_get_loadedLevelName_m1849536804 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C"  bool PlayerPrefs_HasKey_m2794939670 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Contains(!0)
#define HashSet_1_Contains_m183505512(__this, p0, method) ((  bool (*) (HashSet_1_t412400163 *, String_t*, const RuntimeMethod*))HashSet_1_Contains_m3173358704_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.String>::Add(!0)
#define HashSet_1_Add_m2640748363(__this, p0, method) ((  bool (*) (HashSet_1_t412400163 *, String_t*, const RuntimeMethod*))HashSet_1_Add_m1971460364_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
#define Dictionary_2__ctor_m3962145734(__this, method) ((  void (*) (Dictionary_2_t2865362463 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1)
#define Dictionary_2_Add_m825500632(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method)
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
#define Nullable_1_get_HasValue_m2191841112(__this, method) ((  bool (*) (Nullable_1_t3119828856 *, const RuntimeMethod*))Nullable_1_get_HasValue_m2191841112_gshared)(__this, method)
// !0 System.Nullable`1<System.Single>::get_Value()
#define Nullable_1_get_Value_m1008004203(__this, method) ((  float (*) (Nullable_1_t3119828856 *, const RuntimeMethod*))Nullable_1_get_Value_m1008004203_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m2329160973(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m761006959(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2650618762 *, String_t*, Dictionary_2_t2865362463 *, const RuntimeMethod*))Dictionary_2_set_Item_m3474379962_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Count()
#define Dictionary_2_get_Count_m3447859782(__this, method) ((  int32_t (*) (Dictionary_2_t2650618762 *, const RuntimeMethod*))Dictionary_2_get_Count_m3919933788_gshared)(__this, method)
// System.String LumosAnalytics::get_baseUrl()
extern "C"  String_t* LumosAnalytics_get_baseUrl_m394505943 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Values()
#define Dictionary_2_get_Values_m2758554421(__this, method) ((  ValueCollection_t71695784 * (*) (Dictionary_2_t2650618762 *, const RuntimeMethod*))Dictionary_2_get_Values_m2492087945_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m1152355348(__this, p0, method) ((  void (*) (List_1_t42469909 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1328752868_gshared)(__this, p0, method)
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Object,System.Action`1<System.Object>,System.Action`1<System.Object>)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m552353156 (RuntimeObject * __this /* static, unused */, String_t* ___url0, RuntimeObject * ___parameters1, Action_1_t3252573759 * ___successCallback2, Action_1_t3252573759 * ___errorCallback3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor()
#define Dictionary_2__ctor_m199014843(__this, method) ((  void (*) (Dictionary_2_t2650618762 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::.ctor()
#define HashSet_1__ctor_m3976624165(__this, method) ((  void (*) (HashSet_1_t412400163 *, const RuntimeMethod*))HashSet_1__ctor_m4231804131_gshared)(__this, method)
// System.DateTime System.DateTime::get_Now()
extern "C"  DateTime_t3738529785  DateTime_get_Now_m1277138875 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTime::ToString()
extern "C"  String_t* DateTime_ToString_m884486936 (DateTime_t3738529785 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1/Enumerator<!0> System.Collections.Generic.HashSet`1<System.String>::GetEnumerator()
#define HashSet_1_GetEnumerator_m4010819012(__this, method) ((  Enumerator_t2117577434  (*) (HashSet_1_t412400163 *, const RuntimeMethod*))HashSet_1_GetEnumerator_m3346268098_gshared)(__this, method)
// !0 System.Collections.Generic.HashSet`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m2985855221(__this, method) ((  String_t* (*) (Enumerator_t2117577434 *, const RuntimeMethod*))Enumerator_get_Current_m4213278602_gshared)(__this, method)
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C"  void PlayerPrefs_SetString_m2101271233 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m2302661619(__this, method) ((  bool (*) (Enumerator_t2117577434 *, const RuntimeMethod*))Enumerator_MoveNext_m3714175425_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m823536440(__this, method) ((  void (*) (Enumerator_t2117577434 *, const RuntimeMethod*))Enumerator_Dispose_m1204547613_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Clear()
#define Dictionary_2_Clear_m41144274(__this, method) ((  void (*) (Dictionary_2_t2650618762 *, const RuntimeMethod*))Dictionary_2_Clear_m1938428402_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.String>::Clear()
#define HashSet_1_Clear_m3290545120(__this, method) ((  void (*) (HashSet_1_t412400163 *, const RuntimeMethod*))HashSet_1_Clear_m507835370_gshared)(__this, method)
// System.String Lumos::get_playerId()
extern "C"  String_t* Lumos_get_playerId_m505766044 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosFeedback::Send(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void LumosFeedback_Send_m64505330 (RuntimeObject * __this /* static, unused */, Dictionary_2_t2865362463 * ___feedback0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String LumosDiagnostics::get_baseUrl()
extern "C"  String_t* LumosDiagnostics_get_baseUrl_m476843529 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin LumosFeedbackGUI::get_skin()
extern "C"  GUISkin_t1244372282 * LumosFeedbackGUI_get_skin_m3102013148 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C"  void GUI_set_skin_m3073574632 (RuntimeObject * __this /* static, unused */, GUISkin_t1244372282 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void WindowFunction__ctor_m2544237635 (WindowFunction_t3146511083 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayout::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t2360479859  GUILayout_Window_m4256324736 (RuntimeObject * __this /* static, unused */, int32_t p0, Rect_t2360479859  p1, WindowFunction_t3146511083 * p2, String_t* p3, GUILayoutOptionU5BU5D_t2510215842* p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::BringWindowToFront(System.Int32)
extern "C"  void GUI_BringWindowToFront_m1364260120 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_BeginHorizontal_m1655989246 (RuntimeObject * __this /* static, unused */, GUILayoutOptionU5BU5D_t2510215842* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandWidth(System.Boolean)
extern "C"  GUILayoutOption_t811797299 * GUILayout_ExpandWidth_m1325694983 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
extern "C"  void GUILayout_Label_m1960000298 (RuntimeObject * __this /* static, unused */, String_t* p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextField(System.String,System.Int32,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextField_m3881040103 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern "C"  void GUILayout_EndHorizontal_m125407884 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextField(System.String,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextField_m1637580227 (RuntimeObject * __this /* static, unused */, String_t* p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinHeight(System.Single)
extern "C"  GUILayoutOption_t811797299 * GUILayout_MinHeight_m98186407 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayout::TextArea(System.String,UnityEngine.GUILayoutOption[])
extern "C"  String_t* GUILayout_TextArea_m2029847618 (RuntimeObject * __this /* static, unused */, String_t* p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayout::FlexibleSpace()
extern "C"  void GUILayout_FlexibleSpace_m1607797253 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern "C"  bool GUILayout_Button_m1340817034 (RuntimeObject * __this /* static, unused */, String_t* p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosFeedbackGUI::HideDialog()
extern "C"  void LumosFeedbackGUI_HideDialog_m330235545 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosFeedback::Record(System.String,System.String,System.String)
extern "C"  void LumosFeedback_Record_m4263107104 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___email1, String_t* ___category2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosFeedbackGUI/CloseHandler::Invoke()
extern "C"  void CloseHandler_Invoke_m3694835316 (CloseHandler_t719506259 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t3528271667* String_ToCharArray_m1492846834 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object LumosJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  RuntimeObject * LumosJson_ParseValue_m571621602 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, bool* ___success2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor(System.Int32)
extern "C"  void StringBuilder__ctor_m2367297767 (StringBuilder_t * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosJson::SerializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool LumosJson_SerializeValue_m1474558115 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// LumosJson/TOKEN LumosJson::NextToken(System.Char[],System.Int32&)
extern "C"  int32_t LumosJson_NextToken_m857433679 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// LumosJson/TOKEN LumosJson::LookAhead(System.Char[],System.Int32)
extern "C"  int32_t LumosJson_LookAhead_m2682965121 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String LumosJson::ParseString(System.Char[],System.Int32&)
extern "C"  String_t* LumosJson_ParseString_m278549945 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
#define List_1__ctor_m2321703786(__this, method) ((  void (*) (List_1_t257213610 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
#define List_1_Add_m3338814081(__this, p0, method) ((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Object LumosJson::ParseNumber(System.Char[],System.Int32&)
extern "C"  RuntimeObject * LumosJson_ParseNumber_m3864485428 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> LumosJson::ParseObject(System.Char[],System.Int32&)
extern "C"  Dictionary_2_t2865362463 * LumosJson_ParseObject_m1401908411 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> LumosJson::ParseArray(System.Char[],System.Int32&)
extern "C"  List_1_t257213610 * LumosJson_ParseArray_m812927360 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.StringBuilder::.ctor()
extern "C"  void StringBuilder__ctor_m3121283359 (StringBuilder_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosJson::EatWhitespace(System.Char[],System.Int32&)
extern "C"  void LumosJson_EatWhitespace_m2987301487 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
extern "C"  StringBuilder_t * StringBuilder_Append_m2383614642 (StringBuilder_t * __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m344457298 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, RuntimeArray * p2, int32_t p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object[])
extern "C"  StringBuilder_t * StringBuilder_AppendFormat_m921870684 (StringBuilder_t * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 LumosJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t LumosJson_GetLastIndexOfNumber_m999849873 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::CreateString(System.Char[])
extern "C"  String_t* String_CreateString_m2818852475 (String_t* __this, CharU5BU5D_t3528271667* ___val0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m363431711 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Int64::Parse(System.String)
extern "C"  int64_t Int64_Parse_m662659148 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Double::Parse(System.String)
extern "C"  double Double_Parse_m4153729520 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosJson::SerializeString(System.String,System.Text.StringBuilder)
extern "C"  void LumosJson_SerializeString_m1899413942 (RuntimeObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
extern "C"  StringBuilder_t * StringBuilder_Append_m1965104174 (StringBuilder_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsArray()
extern "C"  bool Type_get_IsArray_m2591212821 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosJson::SerializeArray(System.Collections.IList,System.Text.StringBuilder)
extern "C"  bool LumosJson_SerializeArray_m369273678 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___anArray0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Char)
extern "C"  String_t* Convert_ToString_m1553911740 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosJson::SerializeObject(System.Collections.IDictionary,System.Text.StringBuilder)
extern "C"  bool LumosJson_SerializeObject_m2207719924 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___anObject0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m1114712797 (Type_t * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Convert::ToDouble(System.Object)
extern "C"  double Convert_ToDouble_m4025515304 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosJson::SerializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void LumosJson_SerializeNumber_m944602447 (RuntimeObject * __this /* static, unused */, double ___number0, StringBuilder_t * ___builder1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Convert::ToInt32(System.Char)
extern "C"  int32_t Convert_ToInt32_m1876369743 (RuntimeObject * __this /* static, unused */, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToString(System.Int32,System.Int32)
extern "C"  String_t* Convert_ToString_m2142825503 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::PadLeft(System.Int32,System.Char)
extern "C"  String_t* String_PadLeft_m1263950263 (String_t* __this, int32_t p0, Il2CppChar p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Double::ToString()
extern "C"  String_t* Double_ToString_m1229922074 (double* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosLocation/<Record>c__AnonStorey0::.ctor()
extern "C"  void U3CRecordU3Ec__AnonStorey0__ctor_m1024376416 (U3CRecordU3Ec__AnonStorey0_t811208082 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String LumosCredentials::get_gameID()
extern "C"  String_t* LumosCredentials_get_gameID_m318748771 (LumosCredentials_t1718792749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m1809518182 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m2163913788 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
extern "C"  int32_t Application_get_systemLanguage_m3110370732 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosDiagnostics::IsInitialized()
extern "C"  bool LumosDiagnostics_IsInitialized_m1820736181 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Lumos::get_runInEditor()
extern "C"  bool Lumos_get_runInEditor_m2527088602 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String)
extern "C"  bool String_StartsWith_m1759067526 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosDiagnostics::get_recordDebugLogs()
extern "C"  bool LumosDiagnostics_get_recordDebugLogs_m2269547550 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosDiagnostics::get_recordDebugWarnings()
extern "C"  bool LumosDiagnostics_get_recordDebugWarnings_m2284271601 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean LumosDiagnostics::get_recordDebugErrors()
extern "C"  bool LumosDiagnostics_get_recordDebugErrors_m1784451440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m2598014212(__this, method) ((  Enumerator_t913802012  (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m2973281003(__this, method) ((  String_t* (*) (Enumerator_t913802012 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m3556324971(__this, method) ((  bool (*) (Enumerator_t913802012 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m2026665411(__this, method) ((  void (*) (Enumerator_t913802012 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String>::get_Item(!0)
#define Dictionary_2_get_Item_m2887862096(__this, p0, method) ((  String_t* (*) (Dictionary_2_t1123770287 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m2643635200_gshared)(__this, p0, method)
// System.String LumosUtil::MD5Hash(System.String[])
extern "C"  String_t* LumosUtil_MD5Hash_m2689163647 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* ___strings0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m24148094(__this, p0, method) ((  bool (*) (Dictionary_2_t2650618762 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m2278349286_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Item(!0)
#define Dictionary_2_get_Item_m845009296(__this, p0, method) ((  Dictionary_2_t2865362463 * (*) (Dictionary_2_t2650618762 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0)
#define Dictionary_2_get_Item_m3179620279(__this, p0, method) ((  RuntimeObject * (*) (Dictionary_2_t2865362463 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method)
// System.Void LumosUtil::AddToDictionaryIfNonempty(System.Collections.IDictionary,System.Object,System.String)
extern "C"  void LumosUtil_AddToDictionaryIfNonempty_m1393722542 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___dict0, RuntimeObject * ___key1, String_t* ___val2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m1670619200(__this, p0, method) ((  void (*) (List_1_t3319525431 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m3709462088_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m706204246(__this, method) ((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m1685793073(__this, p0, method) ((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String>::.ctor()
#define Dictionary_2__ctor_m3365230958(__this, method) ((  void (*) (Dictionary_2_t1123770287 *, const RuntimeMethod*))Dictionary_2__ctor_m1232595126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m474478774(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1123770287 *, int32_t, String_t*, const RuntimeMethod*))Dictionary_2_Add_m3808121907_gshared)(__this, p0, p1, method)
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern "C"  String_t* PlayerPrefs_GetString_m389913383 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Lumos::set_playerId(System.String)
extern "C"  void Lumos_set_playerId_m314876524 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
#define Action_1_Invoke_m1933767679(__this, p0, method) ((  void (*) (Action_1_t269755560 *, bool, const RuntimeMethod*))Action_1_Invoke_m1933767679_gshared)(__this, p0, method)
// System.Void LumosPlayer::Ping()
extern "C"  void LumosPlayer_Ping_m3221862318 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosPlayer::RequestPlayerId(System.Action`1<System.Boolean>)
extern "C"  void LumosPlayer_RequestPlayerId_m1064872458 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___callback0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosPlayer/<RequestPlayerId>c__AnonStorey0::.ctor()
extern "C"  void U3CRequestPlayerIdU3Ec__AnonStorey0__ctor_m112441722 (U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String LumosPlayer::get_baseUrl()
extern "C"  String_t* LumosPlayer_get_baseUrl_m2504180808 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Action`1<System.Object>)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m1124333148 (RuntimeObject * __this /* static, unused */, String_t* ___url0, Action_1_t3252573759 * ___successCallback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LumosRequest::SendCoroutine(System.String,System.Object,System.Action`1<System.Object>,System.Action`1<System.Object>)
extern "C"  RuntimeObject* LumosRequest_SendCoroutine_m2726591602 (RuntimeObject * __this /* static, unused */, String_t* ___url0, RuntimeObject * ___parameters1, Action_1_t3252573759 * ___successCallback2, Action_1_t3252573759 * ___errorCallback3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosRequest/<SendCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__Iterator0__ctor_m701103518 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String LumosJson::Serialize(System.Object)
extern "C"  String_t* LumosJson_Serialize_m1033319704 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___json0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_ASCII()
extern "C"  Encoding_t1523322056 * Encoding_get_ASCII_m3595602635 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern "C"  void HMACSHA1__ctor_m446190279 (HMACSHA1_t1952596188 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.HashAlgorithm::ComputeHash(System.Byte[])
extern "C"  ByteU5BU5D_t4116647657* HashAlgorithm_ComputeHash_m2825542963 (HashAlgorithm_t1432317219 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToBase64String(System.Byte[])
extern "C"  String_t* Convert_ToBase64String_m3839334935 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LumosRequest::LoadImageCoroutine(System.String,UnityEngine.Texture2D)
extern "C"  RuntimeObject* LumosRequest_LoadImageCoroutine_m281388845 (RuntimeObject * __this /* static, unused */, String_t* ___imageLocation0, Texture2D_t3840446185 * ___texture1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LumosRequest/<LoadImageCoroutine>c__Iterator1::.ctor()
extern "C"  void U3CLoadImageCoroutineU3Ec__Iterator1__ctor_m3559792677 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C"  void WWW__ctor_m2915079343 (WWW_t3688466362 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_error()
extern "C"  String_t* WWW_get_error_m3055313367 (WWW_t3688466362 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m1152696503 (Exception_t * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WWW::LoadImageIntoTexture(UnityEngine.Texture2D)
extern "C"  void WWW_LoadImageIntoTexture_m3209745837 (WWW_t3688466362 * __this, Texture2D_t3840446185 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
extern "C"  int32_t Application_get_internetReachability_m432100819 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void DateTime__ctor_m2030998145 (DateTime_t3738529785 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTime::AddSeconds(System.Double)
extern "C"  DateTime_t3738529785  DateTime_AddSeconds_m332574389 (DateTime_t3738529785 * __this, double p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MD5CryptoServiceProvider::.ctor()
extern "C"  void MD5CryptoServiceProvider__ctor_m3271163125 (MD5CryptoServiceProvider_t3005586042 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Byte::ToString(System.String)
extern "C"  String_t* Byte_ToString_m3735479648 (uint8_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::ToLower()
extern "C"  String_t* String_ToLower_m2029374922 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lumos::.ctor()
extern "C"  void Lumos__ctor_m1333223223 (Lumos_t1046626606 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Lumos::add_OnReady(System.Action)
extern "C"  void Lumos_add_OnReady_m627233499 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_add_OnReady_m627233499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1264377477 * V_0 = NULL;
	Action_t1264377477 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnReady_3();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_t1264377477 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_2 = V_1;
		Action_t1264377477 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_t1264377477 * L_5 = V_0;
		Action_t1264377477 * L_6 = InterlockedCompareExchangeImpl<Action_t1264377477 *>((((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_address_of_OnReady_3()), ((Action_t1264377477 *)CastclassSealed((RuntimeObject*)L_4, Action_t1264377477_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_t1264377477 * L_7 = V_0;
		Action_t1264377477 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_t1264377477 *)L_7) == ((RuntimeObject*)(Action_t1264377477 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lumos::remove_OnReady(System.Action)
extern "C"  void Lumos_remove_OnReady_m4202809949 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_remove_OnReady_m4202809949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1264377477 * V_0 = NULL;
	Action_t1264377477 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnReady_3();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_t1264377477 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_2 = V_1;
		Action_t1264377477 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_t1264377477 * L_5 = V_0;
		Action_t1264377477 * L_6 = InterlockedCompareExchangeImpl<Action_t1264377477 *>((((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_address_of_OnReady_3()), ((Action_t1264377477 *)CastclassSealed((RuntimeObject*)L_4, Action_t1264377477_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_t1264377477 * L_7 = V_0;
		Action_t1264377477 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_t1264377477 *)L_7) == ((RuntimeObject*)(Action_t1264377477 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lumos::add_OnTimerFinish(System.Action)
extern "C"  void Lumos_add_OnTimerFinish_m2405319448 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_add_OnTimerFinish_m2405319448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1264377477 * V_0 = NULL;
	Action_t1264377477 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnTimerFinish_4();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_t1264377477 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_2 = V_1;
		Action_t1264377477 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_t1264377477 * L_5 = V_0;
		Action_t1264377477 * L_6 = InterlockedCompareExchangeImpl<Action_t1264377477 *>((((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_address_of_OnTimerFinish_4()), ((Action_t1264377477 *)CastclassSealed((RuntimeObject*)L_4, Action_t1264377477_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_t1264377477 * L_7 = V_0;
		Action_t1264377477 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_t1264377477 *)L_7) == ((RuntimeObject*)(Action_t1264377477 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Lumos::remove_OnTimerFinish(System.Action)
extern "C"  void Lumos_remove_OnTimerFinish_m2760070207 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_remove_OnTimerFinish_m2760070207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1264377477 * V_0 = NULL;
	Action_t1264377477 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnTimerFinish_4();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_t1264377477 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_2 = V_1;
		Action_t1264377477 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Action_t1264377477 * L_5 = V_0;
		Action_t1264377477 * L_6 = InterlockedCompareExchangeImpl<Action_t1264377477 *>((((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_address_of_OnTimerFinish_4()), ((Action_t1264377477 *)CastclassSealed((RuntimeObject*)L_4, Action_t1264377477_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		Action_t1264377477 * L_7 = V_0;
		Action_t1264377477 * L_8 = V_1;
		if ((!(((RuntimeObject*)(Action_t1264377477 *)L_7) == ((RuntimeObject*)(Action_t1264377477 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// LumosCredentials Lumos::get_credentials()
extern "C"  LumosCredentials_t1718792749 * Lumos_get_credentials_m1762176871 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_get_credentials_m1762176871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		LumosCredentials_t1718792749 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CcredentialsU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void Lumos::set_credentials(LumosCredentials)
extern "C"  void Lumos_set_credentials_m2691661372 (RuntimeObject * __this /* static, unused */, LumosCredentials_t1718792749 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_set_credentials_m2691661372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LumosCredentials_t1718792749 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CcredentialsU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Boolean Lumos::get_debug()
extern "C"  bool Lumos_get_debug_m1677971330 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_get_debug_m1677971330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		bool L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CdebugU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Lumos::set_debug(System.Boolean)
extern "C"  void Lumos_set_debug_m4002013879 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_set_debug_m4002013879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CdebugU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.String Lumos::get_playerId()
extern "C"  String_t* Lumos_get_playerId_m505766044 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_get_playerId_m505766044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CplayerIdU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void Lumos::set_playerId(System.String)
extern "C"  void Lumos_set_playerId_m314876524 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_set_playerId_m314876524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CplayerIdU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Single Lumos::get_timerInterval()
extern "C"  float Lumos_get_timerInterval_m619285440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_get_timerInterval_m619285440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		float L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get__timerInterval_5();
		return L_0;
	}
}
// System.Void Lumos::set_timerInterval(System.Single)
extern "C"  void Lumos_set_timerInterval_m1080298005 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_set_timerInterval_m1080298005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set__timerInterval_5(L_0);
		return;
	}
}
// System.Boolean Lumos::get_timerPaused()
extern "C"  bool Lumos_get_timerPaused_m281469672 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_get_timerPaused_m281469672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		bool L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CtimerPausedU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void Lumos::set_timerPaused(System.Boolean)
extern "C"  void Lumos_set_timerPaused_m3894062333 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_set_timerPaused_m3894062333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CtimerPausedU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Boolean Lumos::get_runInEditor()
extern "C"  bool Lumos_get_runInEditor_m2527088602 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_get_runInEditor_m2527088602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		NullCheck(L_0);
		bool L_1 = L_0->get_runWhileInEditor_2();
		return L_1;
	}
}
// System.Void Lumos::Awake()
extern "C"  void Lumos_Awake_m3796621327 (Lumos_t1046626606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_Awake_m3796621327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_Log_m2411948670(NULL /*static, unused*/, _stringLiteral10572647, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0026:
	{
		LumosCredentials_t1718792749 * L_3 = LumosCredentials_Load_m1109527268(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_set_credentials_m2691661372(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		LumosCredentials_t1718792749 * L_4 = Lumos_get_credentials_m1762176871(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		LumosCredentials_t1718792749 * L_6 = Lumos_get_credentials_m1762176871(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = L_6->get_apiKey_2();
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		LumosCredentials_t1718792749 * L_8 = Lumos_get_credentials_m1762176871(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = L_8->get_apiKey_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_11 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007e;
		}
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral1389463571, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return;
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_instance_11(__this);
		bool L_13 = __this->get_runWhileInEditor_2();
		if (!L_13)
		{
			goto IL_00a3;
		}
	}
	{
		bool L_14 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_set_timerInterval_m1080298005(NULL /*static, unused*/, (3.0f), /*hidden argument*/NULL);
	}

IL_00a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Lumos::Start()
extern "C"  void Lumos_Start_m2536240861 (Lumos_t1046626606 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_Start_m2536240861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_12();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		intptr_t L_1 = (intptr_t)Lumos_U3CStartU3Em__0_m2632898143_RuntimeMethod_var;
		Action_1_t269755560 * L_2 = (Action_1_t269755560 *)il2cpp_codegen_object_new(Action_1_t269755560_il2cpp_TypeInfo_var);
		Action_1__ctor_m3735703580(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3735703580_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_12(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t269755560 * L_3 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_12();
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		LumosPlayer_Init_m3905959549(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine Lumos::RunRoutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * Lumos_RunRoutine_m1158721104 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_RunRoutine_m1158721104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogError_m3535111913(NULL /*static, unused*/, _stringLiteral1305370915, /*hidden argument*/NULL);
		return (Coroutine_t3829159415 *)NULL;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_2 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		RuntimeObject* L_3 = ___routine0;
		NullCheck(L_2);
		Coroutine_t3829159415 * L_4 = MonoBehaviour_StartCoroutine_m3411253000(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Lumos::Remove(System.String)
extern "C"  void Lumos_Remove_m2435435980 (RuntimeObject * __this /* static, unused */, String_t* ___reason0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_Remove_m2435435980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_2 = ___reason0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2228809126, L_2, _stringLiteral1377196673, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_4 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Collections.IEnumerator Lumos::SendQueuedData()
extern "C"  RuntimeObject* Lumos_SendQueuedData_m1193834825 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_SendQueuedData_m1193834825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * V_0 = NULL;
	{
		U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * L_0 = (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 *)il2cpp_codegen_object_new(U3CSendQueuedDataU3Ec__Iterator0_t3572231512_il2cpp_TypeInfo_var);
		U3CSendQueuedDataU3Ec__Iterator0__ctor_m4007735274(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Lumos::Log(System.Object)
extern "C"  void Lumos_Log_m2411948670 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_Log_m2411948670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_13();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		intptr_t L_1 = (intptr_t)Debug_Log_m4051431634_RuntimeMethod_var;
		Action_1_t3252573759 * L_2 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_13(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_3 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_13();
		RuntimeObject * L_4 = ___message0;
		Lumos_LogMessage_m2367926511(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Lumos::LogWarning(System.Object)
extern "C"  void Lumos_LogWarning_m3534960519 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_LogWarning_m3534960519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_14();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		intptr_t L_1 = (intptr_t)Debug_LogWarning_m3752629331_RuntimeMethod_var;
		Action_1_t3252573759 * L_2 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_14(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_3 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_14();
		RuntimeObject * L_4 = ___message0;
		Lumos_LogMessage_m2367926511(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Lumos::LogError(System.Object)
extern "C"  void Lumos_LogError_m3535111913 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_LogError_m3535111913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_15();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		intptr_t L_1 = (intptr_t)Debug_LogError_m2850623458_RuntimeMethod_var;
		Action_1_t3252573759 * L_2 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache2_15(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_3 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_15();
		RuntimeObject * L_4 = ___message0;
		Lumos_LogMessage_m2367926511(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Lumos::LogMessage(System.Action`1<System.Object>,System.Object)
extern "C"  void Lumos_LogMessage_m2367926511 (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___logger0, RuntimeObject * ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_LogMessage_m2367926511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_t1046626606 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_instance_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		bool L_2 = Lumos_get_debug_m1677971330(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001b;
		}
	}

IL_001a:
	{
		return;
	}

IL_001b:
	{
		Action_1_t3252573759 * L_3 = ___logger0;
		RuntimeObject * L_4 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2228809126, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Action_1_Invoke_m2461023210(L_3, L_5, /*hidden argument*/Action_1_Invoke_m2461023210_RuntimeMethod_var);
		return;
	}
}
// System.Void Lumos::.cctor()
extern "C"  void Lumos__cctor_m1354239268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos__cctor_m1354239268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->set__timerInterval_5((30.0f));
		return;
	}
}
// System.Void Lumos::<Start>m__0(System.Boolean)
extern "C"  void Lumos_U3CStartU3Em__0_m2632898143 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Lumos_U3CStartU3Em__0_m2632898143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_0 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnReady_3();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_1 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnReady_3();
		NullCheck(L_1);
		Action_Invoke_m937035532(L_1, /*hidden argument*/NULL);
		RuntimeObject* L_2 = Lumos_SendQueuedData_m1193834825(NULL /*static, unused*/, /*hidden argument*/NULL);
		Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Lumos/<SendQueuedData>c__Iterator0::.ctor()
extern "C"  void U3CSendQueuedDataU3Ec__Iterator0__ctor_m4007735274 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Lumos/<SendQueuedData>c__Iterator0::MoveNext()
extern "C"  bool U3CSendQueuedDataU3Ec__Iterator0_MoveNext_m783816231 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendQueuedDataU3Ec__Iterator0_MoveNext_m783816231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_0075;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		float L_2 = Lumos_get_timerInterval_m619285440(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_0(L_3);
		bool L_4 = __this->get_U24disposing_1();
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_2(1);
	}

IL_0040:
	{
		goto IL_0077;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		bool L_5 = Lumos_get_timerPaused_m281469672(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_6 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnTimerFinish_4();
		if (!L_6)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_7 = ((Lumos_t1046626606_StaticFields*)il2cpp_codegen_static_fields_for(Lumos_t1046626606_il2cpp_TypeInfo_var))->get_OnTimerFinish_4();
		NullCheck(L_7);
		Action_Invoke_m937035532(L_7, /*hidden argument*/NULL);
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		RuntimeObject* L_8 = Lumos_SendQueuedData_m1193834825(NULL /*static, unused*/, /*hidden argument*/NULL);
		Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_0075:
	{
		return (bool)0;
	}

IL_0077:
	{
		return (bool)1;
	}
}
// System.Object Lumos/<SendQueuedData>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CSendQueuedDataU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1331796442 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object Lumos/<SendQueuedData>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CSendQueuedDataU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2259990162 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void Lumos/<SendQueuedData>c__Iterator0::Dispose()
extern "C"  void U3CSendQueuedDataU3Ec__Iterator0_Dispose_m2067104152 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_1((bool)1);
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void Lumos/<SendQueuedData>c__Iterator0::Reset()
extern "C"  void U3CSendQueuedDataU3Ec__Iterator0_Reset_m4035466761 (U3CSendQueuedDataU3Ec__Iterator0_t3572231512 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendQueuedDataU3Ec__Iterator0_Reset_m4035466761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CSendQueuedDataU3Ec__Iterator0_Reset_m4035466761_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosAnalytics::.ctor()
extern "C"  void LumosAnalytics__ctor_m4068444788 (LumosAnalytics_t2745971597 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LumosAnalytics::get_levelsAsCategories()
extern "C"  bool LumosAnalytics_get_levelsAsCategories_m3551605565 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_get_levelsAsCategories_m3551605565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		LumosAnalytics_t2745971597 * L_0 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get_instance_6();
		NullCheck(L_0);
		bool L_1 = L_0->get_useLevelsAsCategories_2();
		return L_1;
	}
}
// System.String LumosAnalytics::get_baseUrl()
extern "C"  String_t* LumosAnalytics_get_baseUrl_m394505943 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_get_baseUrl_m394505943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		String_t* L_0 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get__baseUrl_4();
		return L_0;
	}
}
// System.Void LumosAnalytics::set_baseUrl(System.String)
extern "C"  void LumosAnalytics_set_baseUrl_m3622155395 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_set_baseUrl_m3622155395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set__baseUrl_4(L_0);
		return;
	}
}
// System.Void LumosAnalytics::Awake()
extern "C"  void LumosAnalytics_Awake_m3671350359 (LumosAnalytics_t2745971597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_Awake_m3671350359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set_instance_6(__this);
		Action_t1264377477 * L_0 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_7();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		intptr_t L_1 = (intptr_t)LumosLocation_Record_m3373362066_RuntimeMethod_var;
		Action_t1264377477 * L_2 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_7(L_2);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_3 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_add_OnReady_m627233499(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Action_t1264377477 * L_4 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_8();
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		intptr_t L_5 = (intptr_t)LumosEvents_Send_m923369243_RuntimeMethod_var;
		Action_t1264377477 * L_6 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_6, NULL, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_8(L_6);
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_7 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_8();
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_add_OnTimerFinish_m2405319448(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_8 = __this->get_recordLevelCompletionEvents_3();
		if (!L_8)
		{
			goto IL_006a;
		}
	}
	{
		float L_9 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set_levelStartTime_5(L_9);
		LumosAnalytics_RecordEvent_m3453334534(NULL /*static, unused*/, _stringLiteral4130627310, (bool)1, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void LumosAnalytics::OnLevelWasLoaded()
extern "C"  void LumosAnalytics_OnLevelWasLoaded_m301866328 (LumosAnalytics_t2745971597 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_OnLevelWasLoaded_m301866328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_recordLevelCompletionEvents_3();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		LumosAnalytics_RecordEvent_m3453334534(NULL /*static, unused*/, _stringLiteral4130627310, (bool)1, /*hidden argument*/NULL);
		float L_1 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = ((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->get_levelStartTime_5();
		LumosAnalytics_RecordEvent_m2084622174(NULL /*static, unused*/, _stringLiteral4020123101, ((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)), (bool)1, /*hidden argument*/NULL);
		float L_3 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set_levelStartTime_5(L_3);
	}

IL_0036:
	{
		return;
	}
}
// System.Boolean LumosAnalytics::IsInitialized()
extern "C"  bool LumosAnalytics_IsInitialized_m3408112055 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_IsInitialized_m3408112055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral41802212, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3786590064, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0023:
	{
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		LumosAnalytics_t2745971597 * L_4 = GameObject_GetComponent_TisLumosAnalytics_t2745971597_m3266012221(L_3, /*hidden argument*/GameObject_GetComponent_TisLumosAnalytics_t2745971597_m3266012221_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3765604456, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0040:
	{
		return (bool)1;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String)
extern "C"  void LumosAnalytics_RecordEvent_m4291932900 (RuntimeObject * __this /* static, unused */, String_t* ___eventID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m4291932900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3119828856  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___eventID0;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3119828856 ));
		Nullable_1_t3119828856  L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, (String_t*)NULL, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.Single)
extern "C"  void LumosAnalytics_RecordEvent_m606101563 (RuntimeObject * __this /* static, unused */, String_t* ___eventID0, float ___val1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m606101563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___eventID0;
		float L_1 = ___val1;
		Nullable_1_t3119828856  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m2775392932((&L_2), L_1, /*hidden argument*/Nullable_1__ctor_m2775392932_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, (String_t*)NULL, L_0, L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.Boolean)
extern "C"  void LumosAnalytics_RecordEvent_m3453334534 (RuntimeObject * __this /* static, unused */, String_t* ___eventID0, bool ___repeatable1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m3453334534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3119828856  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___eventID0;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3119828856 ));
		Nullable_1_t3119828856  L_1 = V_0;
		bool L_2 = ___repeatable1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, (String_t*)NULL, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.Single,System.Boolean)
extern "C"  void LumosAnalytics_RecordEvent_m2084622174 (RuntimeObject * __this /* static, unused */, String_t* ___eventID0, float ___val1, bool ___repeatable2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m2084622174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___eventID0;
		float L_1 = ___val1;
		Nullable_1_t3119828856  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Nullable_1__ctor_m2775392932((&L_2), L_1, /*hidden argument*/Nullable_1__ctor_m2775392932_RuntimeMethod_var);
		bool L_3 = ___repeatable2;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, (String_t*)NULL, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.String)
extern "C"  void LumosAnalytics_RecordEvent_m1464728650 (RuntimeObject * __this /* static, unused */, String_t* ___category0, String_t* ___eventID1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m1464728650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3119828856  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___category0;
		String_t* L_1 = ___eventID1;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3119828856 ));
		Nullable_1_t3119828856  L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, L_0, L_1, L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.String,System.Single)
extern "C"  void LumosAnalytics_RecordEvent_m4009380798 (RuntimeObject * __this /* static, unused */, String_t* ___category0, String_t* ___eventID1, float ___val2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m4009380798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___category0;
		String_t* L_1 = ___eventID1;
		float L_2 = ___val2;
		Nullable_1_t3119828856  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m2775392932((&L_3), L_2, /*hidden argument*/Nullable_1__ctor_m2775392932_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, L_0, L_1, L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.String,System.Boolean)
extern "C"  void LumosAnalytics_RecordEvent_m2764324973 (RuntimeObject * __this /* static, unused */, String_t* ___category0, String_t* ___eventID1, bool ___repeatable2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m2764324973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3119828856  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___category0;
		String_t* L_1 = ___eventID1;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3119828856 ));
		Nullable_1_t3119828856  L_2 = V_0;
		bool L_3 = ___repeatable2;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::RecordEvent(System.String,System.String,System.Single,System.Boolean)
extern "C"  void LumosAnalytics_RecordEvent_m454352664 (RuntimeObject * __this /* static, unused */, String_t* ___category0, String_t* ___eventID1, float ___val2, bool ___repeatable3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics_RecordEvent_m454352664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___category0;
		String_t* L_1 = ___eventID1;
		float L_2 = ___val2;
		Nullable_1_t3119828856  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m2775392932((&L_3), L_2, /*hidden argument*/Nullable_1__ctor_m2775392932_RuntimeMethod_var);
		bool L_4 = ___repeatable3;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		LumosEvents_Record_m3157052489(NULL /*static, unused*/, L_0, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalytics::.cctor()
extern "C"  void LumosAnalytics__cctor_m1705858773 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalytics__cctor_m1705858773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LumosAnalytics_t2745971597_StaticFields*)il2cpp_codegen_static_fields_for(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var))->set__baseUrl_4(_stringLiteral2098737819);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosAnalyticsSetup::.ctor()
extern "C"  void LumosAnalyticsSetup__ctor_m2322645041 (LumosAnalyticsSetup_t3600549041 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosAnalyticsSetup::Setup()
extern "C"  void LumosAnalyticsSetup_Setup_m1888243477 (LumosAnalyticsSetup_t3600549041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosAnalyticsSetup_Setup_m1888243477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral41802212, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		LumosAnalytics_t2745971597 * L_4 = GameObject_GetComponent_TisLumosAnalytics_t2745971597_m3266012221(L_3, /*hidden argument*/GameObject_GetComponent_TisLumosAnalytics_t2745971597_m3266012221_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1113636619 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_AddComponent_TisLumosAnalytics_t2745971597_m4089171744(L_6, /*hidden argument*/GameObject_AddComponent_TisLumosAnalytics_t2745971597_m4089171744_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2107940303, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosCredentials::.ctor()
extern "C"  void LumosCredentials__ctor_m1196037201 (LumosCredentials_t1718792749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosCredentials__ctor_m1196037201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_apiKey_2(L_0);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String LumosCredentials::get_gameID()
extern "C"  String_t* LumosCredentials_get_gameID_m318748771 (LumosCredentials_t1718792749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosCredentials_get_gameID_m318748771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = __this->get_apiKey_2();
		NullCheck(L_0);
		String_t* L_1 = String_Substring_m1610150815(L_0, 0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0013;
		throw e;
	}

CATCH_0013:
	{ // begin catch(System.ArgumentOutOfRangeException)
		V_0 = (String_t*)NULL;
		goto IL_001b;
	} // end catch (depth: 1)

IL_001b:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// LumosCredentials LumosCredentials::Load()
extern "C"  LumosCredentials_t1718792749 * LumosCredentials_Load_m1109527268 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosCredentials_Load_m1109527268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LumosCredentials_t1718792749 * V_0 = NULL;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (LumosCredentials_t1718792749_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Object_t631007953 * L_2 = Resources_Load_m3480190876(NULL /*static, unused*/, _stringLiteral470592798, L_1, /*hidden argument*/NULL);
		V_0 = ((LumosCredentials_t1718792749 *)IsInstClass((RuntimeObject*)L_2, LumosCredentials_t1718792749_il2cpp_TypeInfo_var));
		LumosCredentials_t1718792749 * L_3 = V_0;
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosDiagnostics::.ctor()
extern "C"  void LumosDiagnostics__ctor_m2989540182 (LumosDiagnostics_t279239158 * __this, const RuntimeMethod* method)
{
	{
		__this->set_recordWarnings_3((bool)1);
		__this->set_recordErrors_4((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LumosDiagnostics::get_recordDebugLogs()
extern "C"  bool LumosDiagnostics_get_recordDebugLogs_m2269547550 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_get_recordDebugLogs_m2269547550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		LumosDiagnostics_t279239158 * L_0 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_instance_6();
		NullCheck(L_0);
		bool L_1 = L_0->get_recordLogs_2();
		return L_1;
	}
}
// System.Boolean LumosDiagnostics::get_recordDebugWarnings()
extern "C"  bool LumosDiagnostics_get_recordDebugWarnings_m2284271601 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_get_recordDebugWarnings_m2284271601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		LumosDiagnostics_t279239158 * L_0 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_instance_6();
		NullCheck(L_0);
		bool L_1 = L_0->get_recordWarnings_3();
		return L_1;
	}
}
// System.Boolean LumosDiagnostics::get_recordDebugErrors()
extern "C"  bool LumosDiagnostics_get_recordDebugErrors_m1784451440 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_get_recordDebugErrors_m1784451440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		LumosDiagnostics_t279239158 * L_0 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_instance_6();
		NullCheck(L_0);
		bool L_1 = L_0->get_recordErrors_4();
		return L_1;
	}
}
// System.String LumosDiagnostics::get_baseUrl()
extern "C"  String_t* LumosDiagnostics_get_baseUrl_m476843529 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_get_baseUrl_m476843529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		String_t* L_0 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get__baseUrl_5();
		return L_0;
	}
}
// System.Void LumosDiagnostics::set_baseUrl(System.String)
extern "C"  void LumosDiagnostics_set_baseUrl_m3227158108 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_set_baseUrl_m3227158108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->set__baseUrl_5(L_0);
		return;
	}
}
// System.Void LumosDiagnostics::Awake()
extern "C"  void LumosDiagnostics_Awake_m307644535 (LumosDiagnostics_t279239158 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_Awake_m307644535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->set_instance_6(__this);
		Action_t1264377477 * L_0 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_7();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		intptr_t L_1 = (intptr_t)LumosSpecs_Record_m3944775057_RuntimeMethod_var;
		Action_t1264377477 * L_2 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_7(L_2);
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_3 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_add_OnReady_m627233499(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Action_t1264377477 * L_4 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_8();
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		intptr_t L_5 = (intptr_t)LumosLogs_Send_m4102305593_RuntimeMethod_var;
		Action_t1264377477 * L_6 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_6, NULL, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_8(L_6);
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_7 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_8();
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_add_OnTimerFinish_m2405319448(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		LogCallback_t3588208630 * L_8 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_9();
		if (L_8)
		{
			goto IL_0062;
		}
	}
	{
		intptr_t L_9 = (intptr_t)LumosLogs_Record_m1639051678_RuntimeMethod_var;
		LogCallback_t3588208630 * L_10 = (LogCallback_t3588208630 *)il2cpp_codegen_object_new(LogCallback_t3588208630_il2cpp_TypeInfo_var);
		LogCallback__ctor_m144650965(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache2_9(L_10);
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		LogCallback_t3588208630 * L_11 = ((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache2_9();
		Application_RegisterLogCallback_m484975236(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosDiagnostics::OnGUI()
extern "C"  void LumosDiagnostics_OnGUI_m2919980339 (LumosDiagnostics_t279239158 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_OnGUI_m2919980339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		LumosFeedbackGUI_OnGUI_m2428985537(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LumosDiagnostics::IsInitialized()
extern "C"  bool LumosDiagnostics_IsInitialized_m1820736181 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics_IsInitialized_m1820736181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral41802212, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3786590064, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0023:
	{
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		LumosDiagnostics_t279239158 * L_4 = GameObject_GetComponent_TisLumosDiagnostics_t279239158_m51467272(L_3, /*hidden argument*/GameObject_GetComponent_TisLumosDiagnostics_t279239158_m51467272_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3557641152, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0040:
	{
		return (bool)1;
	}
}
// System.Void LumosDiagnostics::.cctor()
extern "C"  void LumosDiagnostics__cctor_m4121200263 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnostics__cctor_m4121200263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LumosDiagnostics_t279239158_StaticFields*)il2cpp_codegen_static_fields_for(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var))->set__baseUrl_5(_stringLiteral2374478329);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosDiagnosticsSetup::.ctor()
extern "C"  void LumosDiagnosticsSetup__ctor_m2554986775 (LumosDiagnosticsSetup_t110199435 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosDiagnosticsSetup::Setup()
extern "C"  void LumosDiagnosticsSetup_Setup_m2086867239 (LumosDiagnosticsSetup_t110199435 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosDiagnosticsSetup_Setup_m2086867239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral41802212, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		LumosDiagnostics_t279239158 * L_4 = GameObject_GetComponent_TisLumosDiagnostics_t279239158_m51467272(L_3, /*hidden argument*/GameObject_GetComponent_TisLumosDiagnostics_t279239158_m51467272_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0039;
		}
	}
	{
		GameObject_t1113636619 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_AddComponent_TisLumosDiagnostics_t279239158_m1556835766(L_6, /*hidden argument*/GameObject_AddComponent_TisLumosDiagnostics_t279239158_m1556835766_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1950643850, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosEvents::Record(System.String,System.String,System.Nullable`1<System.Single>,System.Boolean)
extern "C"  void LumosEvents_Record_m3157052489 (RuntimeObject * __this /* static, unused */, String_t* ___category0, String_t* ___eventID1, Nullable_1_t3119828856  ___val2, bool ___repeatable3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosEvents_Record_m3157052489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Dictionary_2_t2865362463 * V_2 = NULL;
	Dictionary_2_t2865362463 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		bool L_0 = LumosAnalytics_IsInitialized_m3408112055(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		String_t* L_1 = ___eventID1;
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = ___eventID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogWarning_m3534960519(NULL /*static, unused*/, _stringLiteral2203338223, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		String_t* L_5 = ___category0;
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_6 = ___category0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005f;
		}
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		bool L_9 = LumosAnalytics_get_levelsAsCategories_m3551605565(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		String_t* L_10 = Application_get_loadedLevelName_m1849536804(NULL /*static, unused*/, /*hidden argument*/NULL);
		___category0 = L_10;
		goto IL_005f;
	}

IL_0058:
	{
		___category0 = _stringLiteral1948332219;
	}

IL_005f:
	{
		String_t* L_11 = ___category0;
		String_t* L_12 = ___eventID1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3755062657(NULL /*static, unused*/, L_11, _stringLiteral3452614550, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		String_t* L_14 = V_0;
		String_t* L_15 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral379197113, L_14, _stringLiteral1318188887, /*hidden argument*/NULL);
		V_1 = L_15;
		bool L_16 = ___repeatable3;
		if (L_16)
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_17 = V_1;
		bool L_18 = PlayerPrefs_HasKey_m2794939670(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_009e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		HashSet_1_t412400163 * L_19 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_unsentUniqueEvents_1();
		String_t* L_20 = V_0;
		NullCheck(L_19);
		bool L_21 = HashSet_1_Contains_m183505512(L_19, L_20, /*hidden argument*/HashSet_1_Contains_m183505512_RuntimeMethod_var);
		if (!L_21)
		{
			goto IL_009f;
		}
	}

IL_009e:
	{
		return;
	}

IL_009f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		HashSet_1_t412400163 * L_22 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_unsentUniqueEvents_1();
		String_t* L_23 = V_0;
		NullCheck(L_22);
		HashSet_1_Add_m2640748363(L_22, L_23, /*hidden argument*/HashSet_1_Add_m2640748363_RuntimeMethod_var);
	}

IL_00ab:
	{
		Dictionary_2_t2865362463 * L_24 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3962145734(L_24, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
		V_3 = L_24;
		Dictionary_2_t2865362463 * L_25 = V_3;
		String_t* L_26 = ___category0;
		NullCheck(L_25);
		Dictionary_2_Add_m825500632(L_25, _stringLiteral105392964, L_26, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_27 = V_3;
		String_t* L_28 = ___eventID1;
		NullCheck(L_27);
		Dictionary_2_Add_m825500632(L_27, _stringLiteral1095525057, L_28, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_29 = V_3;
		V_2 = L_29;
		bool L_30 = Nullable_1_get_HasValue_m2191841112((&___val2), /*hidden argument*/Nullable_1_get_HasValue_m2191841112_RuntimeMethod_var);
		if (!L_30)
		{
			goto IL_00ee;
		}
	}
	{
		Dictionary_2_t2865362463 * L_31 = V_2;
		float L_32 = Nullable_1_get_Value_m1008004203((&___val2), /*hidden argument*/Nullable_1_get_Value_m1008004203_RuntimeMethod_var);
		float L_33 = L_32;
		RuntimeObject * L_34 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		Dictionary_2_set_Item_m2329160973(L_31, _stringLiteral3493618073, L_34, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_35 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_events_0();
		String_t* L_36 = V_0;
		Dictionary_2_t2865362463 * L_37 = V_2;
		NullCheck(L_35);
		Dictionary_2_set_Item_m761006959(L_35, L_36, L_37, /*hidden argument*/Dictionary_2_set_Item_m761006959_RuntimeMethod_var);
		return;
	}
}
// System.Void LumosEvents::Send()
extern "C"  void LumosEvents_Send_m923369243 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosEvents_Send_m923369243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t42469909 * V_1 = NULL;
	List_1_t42469909 * G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	List_1_t42469909 * G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	Action_1_t3252573759 * G_B6_0 = NULL;
	List_1_t42469909 * G_B6_1 = NULL;
	String_t* G_B6_2 = NULL;
	Action_1_t3252573759 * G_B5_0 = NULL;
	List_1_t42469909 * G_B5_1 = NULL;
	String_t* G_B5_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_0 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_events_0();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m3447859782(L_0, /*hidden argument*/Dictionary_2_get_Count_m3447859782_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		String_t* L_2 = LumosAnalytics_get_baseUrl_m394505943(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, L_2, _stringLiteral3438004084, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_4 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_events_0();
		NullCheck(L_4);
		ValueCollection_t71695784 * L_5 = Dictionary_2_get_Values_m2758554421(L_4, /*hidden argument*/Dictionary_2_get_Values_m2758554421_RuntimeMethod_var);
		List_1_t42469909 * L_6 = (List_1_t42469909 *)il2cpp_codegen_object_new(List_1_t42469909_il2cpp_TypeInfo_var);
		List_1__ctor_m1152355348(L_6, L_5, /*hidden argument*/List_1__ctor_m1152355348_RuntimeMethod_var);
		V_1 = L_6;
		String_t* L_7 = V_0;
		List_1_t42469909 * L_8 = V_1;
		Action_1_t3252573759 * L_9 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_2();
		G_B3_0 = L_8;
		G_B3_1 = L_7;
		if (L_9)
		{
			G_B4_0 = L_8;
			G_B4_1 = L_7;
			goto IL_004a;
		}
	}
	{
		intptr_t L_10 = (intptr_t)LumosEvents_U3CSendU3Em__0_m2551348430_RuntimeMethod_var;
		Action_1_t3252573759 * L_11 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_11, NULL, L_10, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_2(L_11);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_12 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_2();
		Action_1_t3252573759 * L_13 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_3();
		G_B5_0 = L_12;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		if (L_13)
		{
			G_B6_0 = L_12;
			G_B6_1 = G_B4_0;
			G_B6_2 = G_B4_1;
			goto IL_0067;
		}
	}
	{
		intptr_t L_14 = (intptr_t)LumosEvents_U3CSendU3Em__1_m2551281941_RuntimeMethod_var;
		Action_1_t3252573759 * L_15 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_15, NULL, L_14, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache1_3(L_15);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0067:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_16 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_3();
		LumosRequest_Send_m552353156(NULL /*static, unused*/, G_B6_2, G_B6_1, G_B6_0, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosEvents::.cctor()
extern "C"  void LumosEvents__cctor_m1980163439 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosEvents__cctor_m1980163439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2650618762 * L_0 = (Dictionary_2_t2650618762 *)il2cpp_codegen_object_new(Dictionary_2_t2650618762_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m199014843(L_0, /*hidden argument*/Dictionary_2__ctor_m199014843_RuntimeMethod_var);
		((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->set_events_0(L_0);
		HashSet_1_t412400163 * L_1 = (HashSet_1_t412400163 *)il2cpp_codegen_object_new(HashSet_1_t412400163_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3976624165(L_1, /*hidden argument*/HashSet_1__ctor_m3976624165_RuntimeMethod_var);
		((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->set_unsentUniqueEvents_1(L_1);
		return;
	}
}
// System.Void LumosEvents::<Send>m__0(System.Object)
extern "C"  void LumosEvents_U3CSendU3Em__0_m2551348430 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___success0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosEvents_U3CSendU3Em__0_m2551348430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_t3738529785  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Enumerator_t2117577434  V_3;
	memset(&V_3, 0, sizeof(V_3));
	String_t* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_0 = DateTime_get_Now_m1277138875(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		String_t* L_1 = DateTime_ToString_m884486936((&V_1), /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		HashSet_1_t412400163 * L_2 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_unsentUniqueEvents_1();
		NullCheck(L_2);
		Enumerator_t2117577434  L_3 = HashSet_1_GetEnumerator_m4010819012(L_2, /*hidden argument*/HashSet_1_GetEnumerator_m4010819012_RuntimeMethod_var);
		V_3 = L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_0024:
		{
			String_t* L_4 = Enumerator_get_Current_m2985855221((&V_3), /*hidden argument*/Enumerator_get_Current_m2985855221_RuntimeMethod_var);
			V_2 = L_4;
			String_t* L_5 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral379197113, L_5, _stringLiteral1318188887, /*hidden argument*/NULL);
			V_4 = L_6;
			String_t* L_7 = V_4;
			String_t* L_8 = V_0;
			PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		}

IL_0046:
		{
			bool L_9 = Enumerator_MoveNext_m2302661619((&V_3), /*hidden argument*/Enumerator_MoveNext_m2302661619_RuntimeMethod_var);
			if (L_9)
			{
				goto IL_0024;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m823536440((&V_3), /*hidden argument*/Enumerator_Dispose_m823536440_RuntimeMethod_var);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosEvents_t3060038184_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_10 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_events_0();
		NullCheck(L_10);
		Dictionary_2_Clear_m41144274(L_10, /*hidden argument*/Dictionary_2_Clear_m41144274_RuntimeMethod_var);
		HashSet_1_t412400163 * L_11 = ((LumosEvents_t3060038184_StaticFields*)il2cpp_codegen_static_fields_for(LumosEvents_t3060038184_il2cpp_TypeInfo_var))->get_unsentUniqueEvents_1();
		NullCheck(L_11);
		HashSet_1_Clear_m3290545120(L_11, /*hidden argument*/HashSet_1_Clear_m3290545120_RuntimeMethod_var);
		return;
	}
}
// System.Void LumosEvents::<Send>m__1(System.Object)
extern "C"  void LumosEvents_U3CSendU3Em__1_m2551281941 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosEvents_U3CSendU3Em__1_m2551281941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogWarning_m3534960519(NULL /*static, unused*/, _stringLiteral295665451, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosFeedback::Record(System.String,System.String,System.String)
extern "C"  void LumosFeedback_Record_m4263107104 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___email1, String_t* ___category2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedback_Record_m4263107104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2865362463 * V_0 = NULL;
	Dictionary_2_t2865362463 * V_1 = NULL;
	{
		String_t* L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		Dictionary_2_t2865362463 * L_4 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3962145734(L_4, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
		V_1 = L_4;
		Dictionary_2_t2865362463 * L_5 = V_1;
		String_t* L_6 = ___message0;
		NullCheck(L_5);
		Dictionary_2_Add_m825500632(L_5, _stringLiteral3253941996, L_6, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_7 = V_1;
		String_t* L_8 = ___category2;
		NullCheck(L_7);
		Dictionary_2_Add_m825500632(L_7, _stringLiteral105392964, L_8, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_9 = V_1;
		String_t* L_10 = ___email1;
		NullCheck(L_9);
		Dictionary_2_Add_m825500632(L_9, _stringLiteral3497109170, L_10, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		String_t* L_12 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_Add_m825500632(L_11, _stringLiteral2215313024, L_12, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_13 = V_1;
		V_0 = L_13;
		Dictionary_2_t2865362463 * L_14 = V_0;
		LumosFeedback_Send_m64505330(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosFeedback::Send(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void LumosFeedback_Send_m64505330 (RuntimeObject * __this /* static, unused */, Dictionary_2_t2865362463 * ___feedback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedback_Send_m64505330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t2865362463 * G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Dictionary_2_t2865362463 * G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Action_1_t3252573759 * G_B4_0 = NULL;
	Dictionary_2_t2865362463 * G_B4_1 = NULL;
	String_t* G_B4_2 = NULL;
	Action_1_t3252573759 * G_B3_0 = NULL;
	Dictionary_2_t2865362463 * G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		String_t* L_0 = LumosDiagnostics_get_baseUrl_m476843529(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, L_0, _stringLiteral1917745513, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		Dictionary_2_t2865362463 * L_3 = ___feedback0;
		Action_1_t3252573759 * L_4 = ((LumosFeedback_t607479745_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedback_t607479745_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_0();
		G_B1_0 = L_3;
		G_B1_1 = L_2;
		if (L_4)
		{
			G_B2_0 = L_3;
			G_B2_1 = L_2;
			goto IL_002a;
		}
	}
	{
		intptr_t L_5 = (intptr_t)LumosFeedback_U3CSendU3Em__0_m2627546650_RuntimeMethod_var;
		Action_1_t3252573759 * L_6 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		((LumosFeedback_t607479745_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedback_t607479745_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_0(L_6);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_002a:
	{
		Action_1_t3252573759 * L_7 = ((LumosFeedback_t607479745_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedback_t607479745_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_0();
		Action_1_t3252573759 * L_8 = ((LumosFeedback_t607479745_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedback_t607479745_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_1();
		G_B3_0 = L_7;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		if (L_8)
		{
			G_B4_0 = L_7;
			G_B4_1 = G_B2_0;
			G_B4_2 = G_B2_1;
			goto IL_0047;
		}
	}
	{
		intptr_t L_9 = (intptr_t)LumosFeedback_U3CSendU3Em__1_m1768186989_RuntimeMethod_var;
		Action_1_t3252573759 * L_10 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		((LumosFeedback_t607479745_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedback_t607479745_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache1_1(L_10);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0047:
	{
		Action_1_t3252573759 * L_11 = ((LumosFeedback_t607479745_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedback_t607479745_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_1();
		LumosRequest_Send_m552353156(NULL /*static, unused*/, G_B4_2, G_B4_1, G_B4_0, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosFeedback::<Send>m__0(System.Object)
extern "C"  void LumosFeedback_U3CSendU3Em__0_m2627546650 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___success0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedback_U3CSendU3Em__0_m2627546650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_Log_m2411948670(NULL /*static, unused*/, _stringLiteral2128908254, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosFeedback::<Send>m__1(System.Object)
extern "C"  void LumosFeedback_U3CSendU3Em__1_m1768186989 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedback_U3CSendU3Em__1_m1768186989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogWarning_m3534960519(NULL /*static, unused*/, _stringLiteral3260327796, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosFeedbackGUI::.ctor()
extern "C"  void LumosFeedbackGUI__ctor_m642821726 (LumosFeedbackGUI_t1268982120 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosFeedbackGUI::add_windowClosed(LumosFeedbackGUI/CloseHandler)
extern "C"  void LumosFeedbackGUI_add_windowClosed_m2691644395 (RuntimeObject * __this /* static, unused */, CloseHandler_t719506259 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_add_windowClosed_m2691644395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CloseHandler_t719506259 * V_0 = NULL;
	CloseHandler_t719506259 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		CloseHandler_t719506259 * L_0 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_windowClosed_0();
		V_0 = L_0;
	}

IL_0006:
	{
		CloseHandler_t719506259 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		CloseHandler_t719506259 * L_2 = V_1;
		CloseHandler_t719506259 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		CloseHandler_t719506259 * L_5 = V_0;
		CloseHandler_t719506259 * L_6 = InterlockedCompareExchangeImpl<CloseHandler_t719506259 *>((((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_address_of_windowClosed_0()), ((CloseHandler_t719506259 *)CastclassSealed((RuntimeObject*)L_4, CloseHandler_t719506259_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		CloseHandler_t719506259 * L_7 = V_0;
		CloseHandler_t719506259 * L_8 = V_1;
		if ((!(((RuntimeObject*)(CloseHandler_t719506259 *)L_7) == ((RuntimeObject*)(CloseHandler_t719506259 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void LumosFeedbackGUI::remove_windowClosed(LumosFeedbackGUI/CloseHandler)
extern "C"  void LumosFeedbackGUI_remove_windowClosed_m1213351066 (RuntimeObject * __this /* static, unused */, CloseHandler_t719506259 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_remove_windowClosed_m1213351066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CloseHandler_t719506259 * V_0 = NULL;
	CloseHandler_t719506259 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		CloseHandler_t719506259 * L_0 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_windowClosed_0();
		V_0 = L_0;
	}

IL_0006:
	{
		CloseHandler_t719506259 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		CloseHandler_t719506259 * L_2 = V_1;
		CloseHandler_t719506259 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		CloseHandler_t719506259 * L_5 = V_0;
		CloseHandler_t719506259 * L_6 = InterlockedCompareExchangeImpl<CloseHandler_t719506259 *>((((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_address_of_windowClosed_0()), ((CloseHandler_t719506259 *)CastclassSealed((RuntimeObject*)L_4, CloseHandler_t719506259_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		CloseHandler_t719506259 * L_7 = V_0;
		CloseHandler_t719506259 * L_8 = V_1;
		if ((!(((RuntimeObject*)(CloseHandler_t719506259 *)L_7) == ((RuntimeObject*)(CloseHandler_t719506259 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// UnityEngine.GUISkin LumosFeedbackGUI::get_skin()
extern "C"  GUISkin_t1244372282 * LumosFeedbackGUI_get_skin_m3102013148 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_get_skin_m3102013148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_0 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_U3CskinU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void LumosFeedbackGUI::set_skin(UnityEngine.GUISkin)
extern "C"  void LumosFeedbackGUI_set_skin_m517710851 (RuntimeObject * __this /* static, unused */, GUISkin_t1244372282 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_set_skin_m517710851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUISkin_t1244372282 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_U3CskinU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void LumosFeedbackGUI::OnGUI()
extern "C"  void LumosFeedbackGUI_OnGUI_m2428985537 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_OnGUI_m2428985537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	int32_t G_B6_1 = 0;
	Rect_t2360479859  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	int32_t G_B5_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		bool L_0 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_visible_5();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = LumosFeedbackGUI_get_skin_m3102013148(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_3 = LumosFeedbackGUI_get_skin_m3102013148(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_skin_m3073574632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_4 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m2614021312((&L_6), (10.0f), (10.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)((int32_t)20)))))), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)((int32_t)20)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_windowRect_4(L_6);
		Rect_t2360479859  L_7 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_windowRect_4();
		WindowFunction_t3146511083 * L_8 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_10();
		G_B5_0 = L_7;
		G_B5_1 = ((int32_t)345992);
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = ((int32_t)345992);
			goto IL_006d;
		}
	}
	{
		intptr_t L_9 = (intptr_t)LumosFeedbackGUI_DisplayWindow_m1570154048_RuntimeMethod_var;
		WindowFunction_t3146511083 * L_10 = (WindowFunction_t3146511083 *)il2cpp_codegen_object_new(WindowFunction_t3146511083_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m2544237635(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_10(L_10);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_006d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		WindowFunction_t3146511083 * L_11 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_10();
		GUILayout_Window_m4256324736(NULL /*static, unused*/, G_B6_1, G_B6_0, L_11, _stringLiteral2701337995, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosFeedbackGUI::DisplayWindow(System.Int32)
extern "C"  void LumosFeedbackGUI_DisplayWindow_m1570154048 (RuntimeObject * __this /* static, unused */, int32_t ___windowId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_DisplayWindow_m1570154048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___windowId0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_BringWindowToFront_m1364260120(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_1 = ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t811797299 * L_2 = GUILayout_ExpandWidth_m1325694983(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_2);
		GUILayout_Label_m1960000298(NULL /*static, unused*/, _stringLiteral159732273, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		String_t* L_3 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_email_7();
		String_t* L_4 = GUILayout_TextField_m3881040103(NULL /*static, unused*/, L_3, ((int32_t)320), ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_email_7(L_4);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_5 = ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t811797299 * L_6 = GUILayout_ExpandWidth_m1325694983(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_6);
		GUILayout_Label_m1960000298(NULL /*static, unused*/, _stringLiteral3243520198, L_5, /*hidden argument*/NULL);
		String_t* L_7 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_category_9();
		String_t* L_8 = GUILayout_TextField_m1637580227(NULL /*static, unused*/, L_7, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_category_9(L_8);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_9 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_sentMessage_6();
		if (L_9)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		String_t* L_10 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_message_8();
		GUILayoutOptionU5BU5D_t2510215842* L_11 = ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t811797299 * L_12 = GUILayout_MinHeight_m98186407(NULL /*static, unused*/, (200.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_12);
		String_t* L_13 = GUILayout_TextArea_m2029847618(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_message_8(L_13);
		goto IL_00e2;
	}

IL_00b8:
	{
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1607797253(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_Label_m1960000298(NULL /*static, unused*/, _stringLiteral2906987907, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1607797253(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m1607797253(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		bool L_14 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_sentMessage_6();
		if (L_14)
		{
			goto IL_0154;
		}
	}
	{
		bool L_15 = GUILayout_Button_m1340817034(NULL /*static, unused*/, _stringLiteral1985564044, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0116;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		LumosFeedbackGUI_HideDialog_m330235545(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0116:
	{
		bool L_16 = GUILayout_Button_m1340817034(NULL /*static, unused*/, _stringLiteral835031361, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_014f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		String_t* L_17 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_message_8();
		String_t* L_18 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_email_7();
		String_t* L_19 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_category_9();
		LumosFeedback_Record_m4263107104(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_message_8(L_20);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_sentMessage_6((bool)1);
	}

IL_014f:
	{
		goto IL_0174;
	}

IL_0154:
	{
		bool L_21 = GUILayout_Button_m1340817034(NULL /*static, unused*/, _stringLiteral3457136609, ((GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0174;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		LumosFeedbackGUI_HideDialog_m330235545(NULL /*static, unused*/, /*hidden argument*/NULL);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_sentMessage_6((bool)0);
	}

IL_0174:
	{
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosFeedbackGUI::ShowDialog()
extern "C"  void LumosFeedbackGUI_ShowDialog_m3299159908 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_ShowDialog_m3299159908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_visible_5((bool)1);
		return;
	}
}
// System.Void LumosFeedbackGUI::HideDialog()
extern "C"  void LumosFeedbackGUI_HideDialog_m330235545 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI_HideDialog_m330235545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		CloseHandler_t719506259 * L_0 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_windowClosed_0();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		CloseHandler_t719506259 * L_1 = ((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->get_windowClosed_0();
		NullCheck(L_1);
		CloseHandler_Invoke_m3694835316(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_visible_5((bool)0);
		return;
	}
}
// System.Void LumosFeedbackGUI::.cctor()
extern "C"  void LumosFeedbackGUI__cctor_m67856517 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosFeedbackGUI__cctor_m67856517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_email_7(L_0);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_message_8(L_1);
		((LumosFeedbackGUI_t1268982120_StaticFields*)il2cpp_codegen_static_fields_for(LumosFeedbackGUI_t1268982120_il2cpp_TypeInfo_var))->set_category_9(_stringLiteral2827162806);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_CloseHandler_t719506259 (CloseHandler_t719506259 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void LumosFeedbackGUI/CloseHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CloseHandler__ctor_m1461940490 (CloseHandler_t719506259 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LumosFeedbackGUI/CloseHandler::Invoke()
extern "C"  void CloseHandler_Invoke_m3694835316 (CloseHandler_t719506259 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CloseHandler_Invoke_m3694835316((CloseHandler_t719506259 *)__this->get_prev_9(), method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult LumosFeedbackGUI/CloseHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* CloseHandler_BeginInvoke_m1526767632 (CloseHandler_t719506259 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void LumosFeedbackGUI/CloseHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CloseHandler_EndInvoke_m2982298130 (CloseHandler_t719506259 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object LumosJson::Deserialize(System.String)
extern "C"  RuntimeObject * LumosJson_Deserialize_m2451457175 (RuntimeObject * __this /* static, unused */, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_Deserialize_m2451457175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t3528271667* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	RuntimeObject * V_3 = NULL;
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->set_lastDecode_2(L_0);
		String_t* L_1 = ___json0;
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_2 = ___json0;
		NullCheck(L_2);
		CharU5BU5D_t3528271667* L_3 = String_ToCharArray_m1492846834(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		V_2 = (bool)1;
		CharU5BU5D_t3528271667* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		RuntimeObject * L_5 = LumosJson_ParseValue_m571621602(NULL /*static, unused*/, L_4, (&V_1), (&V_2), /*hidden argument*/NULL);
		V_3 = L_5;
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->set_lastErrorIndex_1((-1));
		goto IL_0039;
	}

IL_0033:
	{
		int32_t L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->set_lastErrorIndex_1(L_7);
	}

IL_0039:
	{
		RuntimeObject * L_8 = V_3;
		return L_8;
	}

IL_003b:
	{
		return NULL;
	}
}
// System.String LumosJson::Serialize(System.Object)
extern "C"  String_t* LumosJson_Serialize_m1033319704 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_Serialize_m1033319704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	bool V_1 = false;
	String_t* G_B3_0 = NULL;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m2367297767(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		RuntimeObject * L_1 = ___json0;
		StringBuilder_t * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		bool L_3 = LumosJson_SerializeValue_m1474558115(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		StringBuilder_t * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		G_B3_0 = L_6;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Boolean LumosJson::LastDecodeSuccessful()
extern "C"  bool LumosJson_LastDecodeSuccessful_m4240989907 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_LastDecodeSuccessful_m4240989907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastErrorIndex_1();
		return (bool)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Int32 LumosJson::GetLastErrorIndex()
extern "C"  int32_t LumosJson_GetLastErrorIndex_m1793719774 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_GetLastErrorIndex_m1793719774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastErrorIndex_1();
		return L_0;
	}
}
// System.String LumosJson::GetLastErrorSnippet()
extern "C"  String_t* LumosJson_GetLastErrorSnippet_m1997652777 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_GetLastErrorSnippet_m1997652777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastErrorIndex_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_2 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastErrorIndex_1();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)5));
		int32_t L_3 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastErrorIndex_1();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)15)));
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		V_0 = 0;
	}

IL_002b:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		String_t* L_6 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastDecode_2();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		String_t* L_8 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastDecode_2();
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m3847582255(L_8, /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		String_t* L_10 = ((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->get_lastDecode_2();
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		NullCheck(L_10);
		String_t* L_14 = String_Substring_m1610150815(L_10, L_11, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)L_13)), (int32_t)1)), /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> LumosJson::ParseObject(System.Char[],System.Int32&)
extern "C"  Dictionary_2_t2865362463 * LumosJson_ParseObject_m1401908411 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_ParseObject_m1401908411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2865362463 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	bool V_3 = false;
	RuntimeObject * V_4 = NULL;
	{
		Dictionary_2_t2865362463 * L_0 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3962145734(L_0, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
	}

IL_000e:
	{
		CharU5BU5D_t3528271667* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_5 = LumosJson_LookAhead_m2682965121(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_001f;
		}
	}
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}

IL_001f:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_0033;
		}
	}
	{
		CharU5BU5D_t3528271667* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_0033:
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0044;
		}
	}
	{
		CharU5BU5D_t3528271667* L_11 = ___json0;
		int32_t* L_12 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Dictionary_2_t2865362463 * L_13 = V_0;
		return L_13;
	}

IL_0044:
	{
		CharU5BU5D_t3528271667* L_14 = ___json0;
		int32_t* L_15 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		String_t* L_16 = LumosJson_ParseString_m278549945(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		String_t* L_17 = V_2;
		if (L_17)
		{
			goto IL_0054;
		}
	}
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}

IL_0054:
	{
		CharU5BU5D_t3528271667* L_18 = ___json0;
		int32_t* L_19 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_20 = LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) == ((int32_t)5)))
		{
			goto IL_0065;
		}
	}
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}

IL_0065:
	{
		V_3 = (bool)1;
		CharU5BU5D_t3528271667* L_22 = ___json0;
		int32_t* L_23 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		RuntimeObject * L_24 = LumosJson_ParseValue_m571621602(NULL /*static, unused*/, L_22, L_23, (&V_3), /*hidden argument*/NULL);
		V_4 = L_24;
		bool L_25 = V_3;
		if (L_25)
		{
			goto IL_007a;
		}
	}
	{
		return (Dictionary_2_t2865362463 *)NULL;
	}

IL_007a:
	{
		Dictionary_2_t2865362463 * L_26 = V_0;
		String_t* L_27 = V_2;
		RuntimeObject * L_28 = V_4;
		NullCheck(L_26);
		Dictionary_2_set_Item_m2329160973(L_26, L_27, L_28, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
	}

IL_0083:
	{
		goto IL_000e;
	}
}
// System.Collections.Generic.List`1<System.Object> LumosJson::ParseArray(System.Char[],System.Int32&)
extern "C"  List_1_t257213610 * LumosJson_ParseArray_m812927360 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_ParseArray_m812927360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t257213610 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	RuntimeObject * V_3 = NULL;
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)il2cpp_codegen_object_new(List_1_t257213610_il2cpp_TypeInfo_var);
		List_1__ctor_m2321703786(L_0, /*hidden argument*/List_1__ctor_m2321703786_RuntimeMethod_var);
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
	}

IL_000e:
	{
		CharU5BU5D_t3528271667* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_5 = LumosJson_LookAhead_m2682965121(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_001f;
		}
	}
	{
		return (List_1_t257213610 *)NULL;
	}

IL_001f:
	{
		int32_t L_7 = V_1;
		if ((!(((uint32_t)L_7) == ((uint32_t)6))))
		{
			goto IL_0033;
		}
	}
	{
		CharU5BU5D_t3528271667* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_0033:
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_0047;
		}
	}
	{
		CharU5BU5D_t3528271667* L_11 = ___json0;
		int32_t* L_12 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		goto IL_0067;
	}

IL_0047:
	{
		V_2 = (bool)1;
		CharU5BU5D_t3528271667* L_13 = ___json0;
		int32_t* L_14 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		RuntimeObject * L_15 = LumosJson_ParseValue_m571621602(NULL /*static, unused*/, L_13, L_14, (&V_2), /*hidden argument*/NULL);
		V_3 = L_15;
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_005b;
		}
	}
	{
		return (List_1_t257213610 *)NULL;
	}

IL_005b:
	{
		List_1_t257213610 * L_17 = V_0;
		RuntimeObject * L_18 = V_3;
		NullCheck(L_17);
		List_1_Add_m3338814081(L_17, L_18, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
	}

IL_0062:
	{
		goto IL_000e;
	}

IL_0067:
	{
		List_1_t257213610 * L_19 = V_0;
		return L_19;
	}
}
// System.Object LumosJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  RuntimeObject * LumosJson_ParseValue_m571621602 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, bool* ___success2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_ParseValue_m571621602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_2 = LumosJson_LookAhead_m2682965121(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		switch (L_3)
		{
			case 0:
			{
				goto IL_008c;
			}
			case 1:
			{
				goto IL_0054;
			}
			case 2:
			{
				goto IL_0091;
			}
			case 3:
			{
				goto IL_005c;
			}
			case 4:
			{
				goto IL_0091;
			}
			case 5:
			{
				goto IL_0091;
			}
			case 6:
			{
				goto IL_0091;
			}
			case 7:
			{
				goto IL_0044;
			}
			case 8:
			{
				goto IL_004c;
			}
			case 9:
			{
				goto IL_0064;
			}
			case 10:
			{
				goto IL_0073;
			}
			case 11:
			{
				goto IL_0082;
			}
		}
	}
	{
		goto IL_0091;
	}

IL_0044:
	{
		CharU5BU5D_t3528271667* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		String_t* L_6 = LumosJson_ParseString_m278549945(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_004c:
	{
		CharU5BU5D_t3528271667* L_7 = ___json0;
		int32_t* L_8 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		RuntimeObject * L_9 = LumosJson_ParseNumber_m3864485428(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0054:
	{
		CharU5BU5D_t3528271667* L_10 = ___json0;
		int32_t* L_11 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		Dictionary_2_t2865362463 * L_12 = LumosJson_ParseObject_m1401908411(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_005c:
	{
		CharU5BU5D_t3528271667* L_13 = ___json0;
		int32_t* L_14 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		List_1_t257213610 * L_15 = LumosJson_ParseArray_m812927360(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_0064:
	{
		CharU5BU5D_t3528271667* L_16 = ___json0;
		int32_t* L_17 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		bool L_18 = ((bool)1);
		RuntimeObject * L_19 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_18);
		return L_19;
	}

IL_0073:
	{
		CharU5BU5D_t3528271667* L_20 = ___json0;
		int32_t* L_21 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		bool L_22 = ((bool)0);
		RuntimeObject * L_23 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_22);
		return L_23;
	}

IL_0082:
	{
		CharU5BU5D_t3528271667* L_24 = ___json0;
		int32_t* L_25 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		return NULL;
	}

IL_008c:
	{
		goto IL_0091;
	}

IL_0091:
	{
		bool* L_26 = ___success2;
		*((int8_t*)(L_26)) = (int8_t)0;
		return NULL;
	}
}
// System.String LumosJson::ParseString(System.Char[],System.Int32&)
extern "C"  String_t* LumosJson_ParseString_m278549945 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_ParseString_m278549945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	CharU5BU5D_t3528271667* V_5 = NULL;
	{
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_EatWhitespace_m2987301487(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		int32_t* L_5 = ___index1;
		int32_t L_6 = (*((int32_t*)L_5));
		V_2 = L_6;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		int32_t L_7 = V_2;
		NullCheck(L_3);
		int32_t L_8 = L_7;
		uint16_t L_9 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		V_3 = (bool)0;
		goto IL_017e;
	}

IL_0020:
	{
		int32_t* L_10 = ___index1;
		CharU5BU5D_t3528271667* L_11 = ___json0;
		NullCheck(L_11);
		if ((!(((uint32_t)(*((int32_t*)L_10))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length))))))))
		{
			goto IL_002f;
		}
	}
	{
		goto IL_0184;
	}

IL_002f:
	{
		CharU5BU5D_t3528271667* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		int32_t* L_14 = ___index1;
		int32_t L_15 = (*((int32_t*)L_14));
		V_2 = L_15;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		int32_t L_16 = V_2;
		NullCheck(L_12);
		int32_t L_17 = L_16;
		uint16_t L_18 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_1 = L_18;
		Il2CppChar L_19 = V_1;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004a;
		}
	}
	{
		V_3 = (bool)1;
		goto IL_0184;
	}

IL_004a:
	{
		Il2CppChar L_20 = V_1;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0176;
		}
	}
	{
		int32_t* L_21 = ___index1;
		CharU5BU5D_t3528271667* L_22 = ___json0;
		NullCheck(L_22);
		if ((!(((uint32_t)(*((int32_t*)L_21))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length))))))))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0184;
	}

IL_0061:
	{
		CharU5BU5D_t3528271667* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		int32_t* L_25 = ___index1;
		int32_t L_26 = (*((int32_t*)L_25));
		V_2 = L_26;
		*((int32_t*)(L_24)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
		int32_t L_27 = V_2;
		NullCheck(L_23);
		int32_t L_28 = L_27;
		uint16_t L_29 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_1 = L_29;
		Il2CppChar L_30 = V_1;
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0083;
		}
	}
	{
		StringBuilder_t * L_31 = V_0;
		NullCheck(L_31);
		StringBuilder_Append_m2383614642(L_31, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_0083:
	{
		Il2CppChar L_32 = V_1;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0099;
		}
	}
	{
		StringBuilder_t * L_33 = V_0;
		NullCheck(L_33);
		StringBuilder_Append_m2383614642(L_33, ((int32_t)92), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_0099:
	{
		Il2CppChar L_34 = V_1;
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00af;
		}
	}
	{
		StringBuilder_t * L_35 = V_0;
		NullCheck(L_35);
		StringBuilder_Append_m2383614642(L_35, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_00af:
	{
		Il2CppChar L_36 = V_1;
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00c4;
		}
	}
	{
		StringBuilder_t * L_37 = V_0;
		NullCheck(L_37);
		StringBuilder_Append_m2383614642(L_37, 8, /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_00c4:
	{
		Il2CppChar L_38 = V_1;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00da;
		}
	}
	{
		StringBuilder_t * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m2383614642(L_39, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_00da:
	{
		Il2CppChar L_40 = V_1;
		if ((!(((uint32_t)L_40) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_00f0;
		}
	}
	{
		StringBuilder_t * L_41 = V_0;
		NullCheck(L_41);
		StringBuilder_Append_m2383614642(L_41, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_00f0:
	{
		Il2CppChar L_42 = V_1;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0106;
		}
	}
	{
		StringBuilder_t * L_43 = V_0;
		NullCheck(L_43);
		StringBuilder_Append_m2383614642(L_43, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_0106:
	{
		Il2CppChar L_44 = V_1;
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_011c;
		}
	}
	{
		StringBuilder_t * L_45 = V_0;
		NullCheck(L_45);
		StringBuilder_Append_m2383614642(L_45, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0171;
	}

IL_011c:
	{
		Il2CppChar L_46 = V_1;
		if ((!(((uint32_t)L_46) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0171;
		}
	}
	{
		CharU5BU5D_t3528271667* L_47 = ___json0;
		NullCheck(L_47);
		int32_t* L_48 = ___index1;
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_47)->max_length)))), (int32_t)(*((int32_t*)L_48))));
		int32_t L_49 = V_4;
		if ((((int32_t)L_49) < ((int32_t)4)))
		{
			goto IL_016c;
		}
	}
	{
		V_5 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)4));
		CharU5BU5D_t3528271667* L_50 = ___json0;
		int32_t* L_51 = ___index1;
		CharU5BU5D_t3528271667* L_52 = V_5;
		Array_Copy_m344457298(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_50, (*((int32_t*)L_51)), (RuntimeArray *)(RuntimeArray *)L_52, 0, 4, /*hidden argument*/NULL);
		StringBuilder_t * L_53 = V_0;
		CharU5BU5D_t3528271667* L_54 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1986677933, (RuntimeObject *)(RuntimeObject *)L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		StringBuilder_AppendFormat_m921870684(L_53, L_55, ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		int32_t* L_56 = ___index1;
		int32_t* L_57 = ___index1;
		*((int32_t*)(L_56)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_57)), (int32_t)4));
		goto IL_0171;
	}

IL_016c:
	{
		goto IL_0184;
	}

IL_0171:
	{
		goto IL_017e;
	}

IL_0176:
	{
		StringBuilder_t * L_58 = V_0;
		Il2CppChar L_59 = V_1;
		NullCheck(L_58);
		StringBuilder_Append_m2383614642(L_58, L_59, /*hidden argument*/NULL);
	}

IL_017e:
	{
		bool L_60 = V_3;
		if (!L_60)
		{
			goto IL_0020;
		}
	}

IL_0184:
	{
		bool L_61 = V_3;
		if (L_61)
		{
			goto IL_018c;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_018c:
	{
		StringBuilder_t * L_62 = V_0;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_62);
		return L_63;
	}
}
// System.Object LumosJson::ParseNumber(System.Char[],System.Int32&)
extern "C"  RuntimeObject * LumosJson_ParseNumber_m3864485428 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_ParseNumber_m3864485428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CharU5BU5D_t3528271667* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_EatWhitespace_m2987301487(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t3528271667* L_2 = ___json0;
		int32_t* L_3 = ___index1;
		int32_t L_4 = LumosJson_GetLastIndexOfNumber_m999849873(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)(*((int32_t*)L_6)))), (int32_t)1));
		int32_t L_7 = V_1;
		V_2 = ((CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)L_7));
		CharU5BU5D_t3528271667* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		CharU5BU5D_t3528271667* L_10 = V_2;
		int32_t L_11 = V_1;
		Array_Copy_m344457298(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_8, (*((int32_t*)L_9)), (RuntimeArray *)(RuntimeArray *)L_10, 0, L_11, /*hidden argument*/NULL);
		int32_t* L_12 = ___index1;
		int32_t L_13 = V_0;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		CharU5BU5D_t3528271667* L_14 = V_2;
		String_t* L_15 = String_CreateString_m2818852475(NULL, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		String_t* L_16 = V_3;
		NullCheck(L_16);
		int32_t L_17 = String_IndexOf_m363431711(L_16, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)(-1)))))
		{
			goto IL_004f;
		}
	}
	{
		String_t* L_18 = V_3;
		int64_t L_19 = Int64_Parse_m662659148(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		int64_t L_20 = L_19;
		RuntimeObject * L_21 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_20);
		return L_21;
	}

IL_004f:
	{
		String_t* L_22 = V_3;
		double L_23 = Double_Parse_m4153729520(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		double L_24 = L_23;
		RuntimeObject * L_25 = Box(Double_t594665363_il2cpp_TypeInfo_var, &L_24);
		return L_25;
	}
}
// System.Int32 LumosJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t LumosJson_GetLastIndexOfNumber_m999849873 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_GetLastIndexOfNumber_m999849873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		goto IL_0023;
	}

IL_0007:
	{
		CharU5BU5D_t3528271667* L_1 = ___json0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(_stringLiteral2206812729);
		int32_t L_5 = String_IndexOf_m363431711(_stringLiteral2206812729, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002c;
	}

IL_001f:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_0;
		CharU5BU5D_t3528271667* L_8 = ___json0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_002c:
	{
		int32_t L_9 = V_0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
	}
}
// System.Void LumosJson::EatWhitespace(System.Char[],System.Int32&)
extern "C"  void LumosJson_EatWhitespace_m2987301487 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_EatWhitespace_m2987301487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_0024;
	}

IL_0005:
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		NullCheck(L_0);
		int32_t L_2 = (*((int32_t*)L_1));
		uint16_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(_stringLiteral1596281288);
		int32_t L_4 = String_IndexOf_m363431711(_stringLiteral1596281288, L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_002e;
	}

IL_001e:
	{
		int32_t* L_5 = ___index1;
		int32_t* L_6 = ___index1;
		*((int32_t*)(L_5)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_6)), (int32_t)1));
	}

IL_0024:
	{
		int32_t* L_7 = ___index1;
		CharU5BU5D_t3528271667* L_8 = ___json0;
		NullCheck(L_8);
		if ((((int32_t)(*((int32_t*)L_7))) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))))))
		{
			goto IL_0005;
		}
	}

IL_002e:
	{
		return;
	}
}
// LumosJson/TOKEN LumosJson::LookAhead(System.Char[],System.Int32)
extern "C"  int32_t LumosJson_LookAhead_m2682965121 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_LookAhead_m2682965121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		CharU5BU5D_t3528271667* L_1 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		int32_t L_2 = LumosJson_NextToken_m857433679(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// LumosJson/TOKEN LumosJson::NextToken(System.Char[],System.Int32&)
extern "C"  int32_t LumosJson_NextToken_m857433679 (RuntimeObject * __this /* static, unused */, CharU5BU5D_t3528271667* ___json0, int32_t* ___index1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_NextToken_m857433679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	{
		CharU5BU5D_t3528271667* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_EatWhitespace_m2987301487(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index1;
		CharU5BU5D_t3528271667* L_3 = ___json0;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))))))
		{
			goto IL_0013;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0013:
	{
		CharU5BU5D_t3528271667* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		NullCheck(L_4);
		int32_t L_6 = (*((int32_t*)L_5));
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		int32_t* L_8 = ___index1;
		int32_t* L_9 = ___index1;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_9)), (int32_t)1));
		Il2CppChar L_10 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)((int32_t)44))))
		{
			case 0:
			{
				goto IL_00a2;
			}
			case 1:
			{
				goto IL_00a6;
			}
			case 2:
			{
				goto IL_0063;
			}
			case 3:
			{
				goto IL_0063;
			}
			case 4:
			{
				goto IL_00a6;
			}
			case 5:
			{
				goto IL_00a6;
			}
			case 6:
			{
				goto IL_00a6;
			}
			case 7:
			{
				goto IL_00a6;
			}
			case 8:
			{
				goto IL_00a6;
			}
			case 9:
			{
				goto IL_00a6;
			}
			case 10:
			{
				goto IL_00a6;
			}
			case 11:
			{
				goto IL_00a6;
			}
			case 12:
			{
				goto IL_00a6;
			}
			case 13:
			{
				goto IL_00a6;
			}
			case 14:
			{
				goto IL_00a8;
			}
		}
	}

IL_0063:
	{
		Il2CppChar L_11 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)((int32_t)91))))
		{
			case 0:
			{
				goto IL_009e;
			}
			case 1:
			{
				goto IL_0078;
			}
			case 2:
			{
				goto IL_00a0;
			}
		}
	}

IL_0078:
	{
		Il2CppChar L_12 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)((int32_t)123))))
		{
			case 0:
			{
				goto IL_009a;
			}
			case 1:
			{
				goto IL_008d;
			}
			case 2:
			{
				goto IL_009c;
			}
		}
	}

IL_008d:
	{
		Il2CppChar L_13 = V_0;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00aa;
	}

IL_009a:
	{
		return (int32_t)(1);
	}

IL_009c:
	{
		return (int32_t)(2);
	}

IL_009e:
	{
		return (int32_t)(3);
	}

IL_00a0:
	{
		return (int32_t)(4);
	}

IL_00a2:
	{
		return (int32_t)(6);
	}

IL_00a4:
	{
		return (int32_t)(7);
	}

IL_00a6:
	{
		return (int32_t)(8);
	}

IL_00a8:
	{
		return (int32_t)(5);
	}

IL_00aa:
	{
		int32_t* L_14 = ___index1;
		int32_t* L_15 = ___index1;
		*((int32_t*)(L_14)) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(*((int32_t*)L_15)), (int32_t)1));
		CharU5BU5D_t3528271667* L_16 = ___json0;
		NullCheck(L_16);
		int32_t* L_17 = ___index1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))), (int32_t)(*((int32_t*)L_17))));
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) < ((int32_t)5)))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_19 = ___json0;
		int32_t* L_20 = ___index1;
		NullCheck(L_19);
		int32_t L_21 = (*((int32_t*)L_20));
		uint16_t L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		NullCheck(L_23);
		int32_t L_25 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_24)), (int32_t)1));
		uint16_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_27 = ___json0;
		int32_t* L_28 = ___index1;
		NullCheck(L_27);
		int32_t L_29 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_28)), (int32_t)2));
		uint16_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_31 = ___json0;
		int32_t* L_32 = ___index1;
		NullCheck(L_31);
		int32_t L_33 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_32)), (int32_t)3));
		uint16_t L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0106;
		}
	}
	{
		CharU5BU5D_t3528271667* L_35 = ___json0;
		int32_t* L_36 = ___index1;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_36)), (int32_t)4));
		uint16_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0106;
		}
	}
	{
		int32_t* L_39 = ___index1;
		int32_t* L_40 = ___index1;
		*((int32_t*)(L_39)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_40)), (int32_t)5));
		return (int32_t)(((int32_t)10));
	}

IL_0106:
	{
		int32_t L_41 = V_1;
		if ((((int32_t)L_41) < ((int32_t)4)))
		{
			goto IL_0183;
		}
	}
	{
		CharU5BU5D_t3528271667* L_42 = ___json0;
		int32_t* L_43 = ___index1;
		NullCheck(L_42);
		int32_t L_44 = (*((int32_t*)L_43));
		uint16_t L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_46 = ___json0;
		int32_t* L_47 = ___index1;
		NullCheck(L_46);
		int32_t L_48 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_47)), (int32_t)1));
		uint16_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		if ((!(((uint32_t)L_49) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_50 = ___json0;
		int32_t* L_51 = ___index1;
		NullCheck(L_50);
		int32_t L_52 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_51)), (int32_t)2));
		uint16_t L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		if ((!(((uint32_t)L_53) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0148;
		}
	}
	{
		CharU5BU5D_t3528271667* L_54 = ___json0;
		int32_t* L_55 = ___index1;
		NullCheck(L_54);
		int32_t L_56 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_55)), (int32_t)3));
		uint16_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t* L_58 = ___index1;
		int32_t* L_59 = ___index1;
		*((int32_t*)(L_58)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_59)), (int32_t)4));
		return (int32_t)(((int32_t)9));
	}

IL_0148:
	{
		CharU5BU5D_t3528271667* L_60 = ___json0;
		int32_t* L_61 = ___index1;
		NullCheck(L_60);
		int32_t L_62 = (*((int32_t*)L_61));
		uint16_t L_63 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		if ((!(((uint32_t)L_63) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0183;
		}
	}
	{
		CharU5BU5D_t3528271667* L_64 = ___json0;
		int32_t* L_65 = ___index1;
		NullCheck(L_64);
		int32_t L_66 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_65)), (int32_t)1));
		uint16_t L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0183;
		}
	}
	{
		CharU5BU5D_t3528271667* L_68 = ___json0;
		int32_t* L_69 = ___index1;
		NullCheck(L_68);
		int32_t L_70 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_69)), (int32_t)2));
		uint16_t L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		if ((!(((uint32_t)L_71) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0183;
		}
	}
	{
		CharU5BU5D_t3528271667* L_72 = ___json0;
		int32_t* L_73 = ___index1;
		NullCheck(L_72);
		int32_t L_74 = ((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_73)), (int32_t)3));
		uint16_t L_75 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		if ((!(((uint32_t)L_75) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0183;
		}
	}
	{
		int32_t* L_76 = ___index1;
		int32_t* L_77 = ___index1;
		*((int32_t*)(L_76)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_77)), (int32_t)4));
		return (int32_t)(((int32_t)11));
	}

IL_0183:
	{
		return (int32_t)(0);
	}
}
// System.Boolean LumosJson::SerializeObject(System.Collections.IDictionary,System.Text.StringBuilder)
extern "C"  bool LumosJson_SerializeObject_m2207719924 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___anObject0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_SerializeObject_m2207719924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	bool V_3 = false;
	RuntimeObject* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m2383614642(L_0, ((int32_t)123), /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___anObject0;
		NullCheck(L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		RuntimeObject* L_3 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0062;
		}

IL_001c:
		{
			RuntimeObject* L_4 = V_2;
			NullCheck(L_4);
			RuntimeObject * L_5 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			bool L_6 = V_0;
			if (L_6)
			{
				goto IL_0032;
			}
		}

IL_0029:
		{
			StringBuilder_t * L_7 = ___builder1;
			NullCheck(L_7);
			StringBuilder_Append_m2383614642(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0032:
		{
			RuntimeObject * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
			StringBuilder_t * L_10 = ___builder1;
			IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
			LumosJson_SerializeString_m1899413942(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			StringBuilder_t * L_11 = ___builder1;
			NullCheck(L_11);
			StringBuilder_Append_m2383614642(L_11, ((int32_t)58), /*hidden argument*/NULL);
			RuntimeObject* L_12 = ___anObject0;
			RuntimeObject * L_13 = V_1;
			NullCheck(L_12);
			RuntimeObject * L_14 = InterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_12, L_13);
			StringBuilder_t * L_15 = ___builder1;
			bool L_16 = LumosJson_SerializeValue_m1474558115(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			if (L_16)
			{
				goto IL_0060;
			}
		}

IL_0059:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0x93, FINALLY_0072);
		}

IL_0060:
		{
			V_0 = (bool)0;
		}

IL_0062:
		{
			RuntimeObject* L_17 = V_2;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_001c;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x88, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_19 = V_2;
			RuntimeObject* L_20 = ((RuntimeObject*)IsInst((RuntimeObject*)L_19, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_4 = L_20;
			if (!L_20)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			RuntimeObject* L_21 = V_4;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_21);
		}

IL_0087:
		{
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0088:
	{
		StringBuilder_t * L_22 = ___builder1;
		NullCheck(L_22);
		StringBuilder_Append_m2383614642(L_22, ((int32_t)125), /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0093:
	{
		bool L_23 = V_3;
		return L_23;
	}
}
// System.Boolean LumosJson::SerializeArray(System.Collections.IList,System.Text.StringBuilder)
extern "C"  bool LumosJson_SerializeArray_m369273678 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___anArray0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_SerializeArray_m369273678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	bool V_3 = false;
	RuntimeObject* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m2383614642(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = (bool)1;
		RuntimeObject* L_1 = ___anArray0;
		NullCheck(L_1);
		RuntimeObject* L_2 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1941168011_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0017:
		{
			RuntimeObject* L_3 = V_2;
			NullCheck(L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_002d;
			}
		}

IL_0024:
		{
			StringBuilder_t * L_6 = ___builder1;
			NullCheck(L_6);
			StringBuilder_Append_m2383614642(L_6, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_002d:
		{
			RuntimeObject * L_7 = V_1;
			StringBuilder_t * L_8 = ___builder1;
			IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
			bool L_9 = LumosJson_SerializeValue_m1474558115(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_0040;
			}
		}

IL_0039:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0x73, FINALLY_0052);
		}

IL_0040:
		{
			V_0 = (bool)0;
		}

IL_0042:
		{
			RuntimeObject* L_10 = V_2;
			NullCheck(L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0017;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x68, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_12 = V_2;
			RuntimeObject* L_13 = ((RuntimeObject*)IsInst((RuntimeObject*)L_12, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_4 = L_13;
			if (!L_13)
			{
				goto IL_0067;
			}
		}

IL_0060:
		{
			RuntimeObject* L_14 = V_4;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_14);
		}

IL_0067:
		{
			IL2CPP_END_FINALLY(82)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0068:
	{
		StringBuilder_t * L_15 = ___builder1;
		NullCheck(L_15);
		StringBuilder_Append_m2383614642(L_15, ((int32_t)93), /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0073:
	{
		bool L_16 = V_3;
		return L_16;
	}
}
// System.Boolean LumosJson::SerializeValue(System.Object,System.Text.StringBuilder)
extern "C"  bool LumosJson_SerializeValue_m1474558115 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_SerializeValue_m1474558115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * G_B15_0 = NULL;
	StringBuilder_t * G_B14_0 = NULL;
	String_t* G_B16_0 = NULL;
	StringBuilder_t * G_B16_1 = NULL;
	{
		RuntimeObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		StringBuilder_t * L_1 = ___builder1;
		NullCheck(L_1);
		StringBuilder_Append_m1965104174(L_1, _stringLiteral1202628576, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0017:
	{
		RuntimeObject * L_2 = ___value0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m88164663(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = Type_get_IsArray_m2591212821(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_5 = ___value0;
		StringBuilder_t * L_6 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_SerializeArray_m369273678(NULL /*static, unused*/, ((RuntimeObject*)Castclass((RuntimeObject*)L_5, IList_t2094931216_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0039:
	{
		RuntimeObject * L_7 = ___value0;
		if (!((String_t*)IsInstSealed((RuntimeObject*)L_7, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0055;
		}
	}
	{
		RuntimeObject * L_8 = ___value0;
		StringBuilder_t * L_9 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_SerializeString_m1899413942(NULL /*static, unused*/, ((String_t*)CastclassSealed((RuntimeObject*)L_8, String_t_il2cpp_TypeInfo_var)), L_9, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0055:
	{
		RuntimeObject * L_10 = ___value0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_10, Char_t3634460470_il2cpp_TypeInfo_var)))
		{
			goto IL_0076;
		}
	}
	{
		RuntimeObject * L_11 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_12 = Convert_ToString_m1553911740(NULL /*static, unused*/, ((*(Il2CppChar*)((Il2CppChar*)UnBox(L_11, Char_t3634460470_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		StringBuilder_t * L_13 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_SerializeString_m1899413942(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0076:
	{
		RuntimeObject * L_14 = ___value0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_14, IDictionary_t1363984059_il2cpp_TypeInfo_var)))
		{
			goto IL_0093;
		}
	}
	{
		RuntimeObject * L_15 = ___value0;
		StringBuilder_t * L_16 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_SerializeObject_m2207719924(NULL /*static, unused*/, ((RuntimeObject*)Castclass((RuntimeObject*)L_15, IDictionary_t1363984059_il2cpp_TypeInfo_var)), L_16, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0093:
	{
		RuntimeObject * L_17 = ___value0;
		if (!((RuntimeObject*)IsInst((RuntimeObject*)L_17, IList_t2094931216_il2cpp_TypeInfo_var)))
		{
			goto IL_00b0;
		}
	}
	{
		RuntimeObject * L_18 = ___value0;
		StringBuilder_t * L_19 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_SerializeArray_m369273678(NULL /*static, unused*/, ((RuntimeObject*)Castclass((RuntimeObject*)L_18, IList_t2094931216_il2cpp_TypeInfo_var)), L_19, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_00b0:
	{
		RuntimeObject * L_20 = ___value0;
		if (!((RuntimeObject *)IsInstSealed((RuntimeObject*)L_20, Boolean_t97287965_il2cpp_TypeInfo_var)))
		{
			goto IL_00e1;
		}
	}
	{
		StringBuilder_t * L_21 = ___builder1;
		RuntimeObject * L_22 = ___value0;
		G_B14_0 = L_21;
		if (!((*(bool*)((bool*)UnBox(L_22, Boolean_t97287965_il2cpp_TypeInfo_var)))))
		{
			G_B15_0 = L_21;
			goto IL_00d1;
		}
	}
	{
		G_B16_0 = _stringLiteral4002445229;
		G_B16_1 = G_B14_0;
		goto IL_00d6;
	}

IL_00d1:
	{
		G_B16_0 = _stringLiteral3875954633;
		G_B16_1 = G_B15_0;
	}

IL_00d6:
	{
		NullCheck(G_B16_1);
		StringBuilder_Append_m1965104174(G_B16_1, G_B16_0, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_00e1:
	{
		RuntimeObject * L_23 = ___value0;
		NullCheck(L_23);
		Type_t * L_24 = Object_GetType_m88164663(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		bool L_25 = Type_get_IsPrimitive_m1114712797(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0102;
		}
	}
	{
		RuntimeObject * L_26 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		double L_27 = Convert_ToDouble_m4025515304(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		StringBuilder_t * L_28 = ___builder1;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		LumosJson_SerializeNumber_m944602447(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		goto IL_0104;
	}

IL_0102:
	{
		return (bool)0;
	}

IL_0104:
	{
		return (bool)1;
	}
}
// System.Void LumosJson::SerializeString(System.String,System.Text.StringBuilder)
extern "C"  void LumosJson_SerializeString_m1899413942 (RuntimeObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson_SerializeString_m1899413942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t3528271667* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	CharU5BU5D_t3528271667* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		StringBuilder_t * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m2383614642(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___aString0;
		NullCheck(L_1);
		CharU5BU5D_t3528271667* L_2 = String_ToCharArray_m1492846834(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t3528271667* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0118;
	}

IL_0019:
	{
		CharU5BU5D_t3528271667* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		Il2CppChar L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0036;
		}
	}
	{
		StringBuilder_t * L_9 = ___builder1;
		NullCheck(L_9);
		StringBuilder_Append_m1965104174(L_9, _stringLiteral3450386420, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_0036:
	{
		Il2CppChar L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_004f;
		}
	}
	{
		StringBuilder_t * L_11 = ___builder1;
		NullCheck(L_11);
		StringBuilder_Append_m1965104174(L_11, _stringLiteral3458119668, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_004f:
	{
		Il2CppChar L_12 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)8))))
		{
			goto IL_0067;
		}
	}
	{
		StringBuilder_t * L_13 = ___builder1;
		NullCheck(L_13);
		StringBuilder_Append_m1965104174(L_13, _stringLiteral3454580724, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_0067:
	{
		Il2CppChar L_14 = V_1;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0080;
		}
	}
	{
		StringBuilder_t * L_15 = ___builder1;
		NullCheck(L_15);
		StringBuilder_Append_m1965104174(L_15, _stringLiteral3454318580, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_0080:
	{
		Il2CppChar L_16 = V_1;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0099;
		}
	}
	{
		StringBuilder_t * L_17 = ___builder1;
		NullCheck(L_17);
		StringBuilder_Append_m1965104174(L_17, _stringLiteral3454842868, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_0099:
	{
		Il2CppChar L_18 = V_1;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00b2;
		}
	}
	{
		StringBuilder_t * L_19 = ___builder1;
		NullCheck(L_19);
		StringBuilder_Append_m1965104174(L_19, _stringLiteral3455629300, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_00b2:
	{
		Il2CppChar L_20 = V_1;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00cb;
		}
	}
	{
		StringBuilder_t * L_21 = ___builder1;
		NullCheck(L_21);
		StringBuilder_Append_m1965104174(L_21, _stringLiteral3455498228, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_00cb:
	{
		Il2CppChar L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		int32_t L_23 = Convert_ToInt32_m1876369743(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		int32_t L_24 = V_4;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)32))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_25 = V_4;
		if ((((int32_t)L_25) > ((int32_t)((int32_t)126))))
		{
			goto IL_00f2;
		}
	}
	{
		StringBuilder_t * L_26 = ___builder1;
		Il2CppChar L_27 = V_1;
		NullCheck(L_26);
		StringBuilder_Append_m2383614642(L_26, L_27, /*hidden argument*/NULL);
		goto IL_0114;
	}

IL_00f2:
	{
		StringBuilder_t * L_28 = ___builder1;
		int32_t L_29 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_30 = Convert_ToString_m2142825503(NULL /*static, unused*/, L_29, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = String_PadLeft_m1263950263(L_30, 4, ((int32_t)48), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3455432692, L_31, /*hidden argument*/NULL);
		NullCheck(L_28);
		StringBuilder_Append_m1965104174(L_28, L_32, /*hidden argument*/NULL);
	}

IL_0114:
	{
		int32_t L_33 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_0118:
	{
		int32_t L_34 = V_3;
		CharU5BU5D_t3528271667* L_35 = V_2;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		StringBuilder_t * L_36 = ___builder1;
		NullCheck(L_36);
		StringBuilder_Append_m2383614642(L_36, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosJson::SerializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void LumosJson_SerializeNumber_m944602447 (RuntimeObject * __this /* static, unused */, double ___number0, StringBuilder_t * ___builder1, const RuntimeMethod* method)
{
	{
		StringBuilder_t * L_0 = ___builder1;
		String_t* L_1 = Double_ToString_m1229922074((&___number0), /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1965104174(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosJson::.cctor()
extern "C"  void LumosJson__cctor_m1132831327 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosJson__cctor_m1132831327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LumosJson_t1197320855_StaticFields*)il2cpp_codegen_static_fields_for(LumosJson_t1197320855_il2cpp_TypeInfo_var))->set_lastErrorIndex_1((-1));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosLocation::Record()
extern "C"  void LumosLocation_Record_m3373362066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLocation_Record_m3373362066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CRecordU3Ec__AnonStorey0_t811208082 * V_0 = NULL;
	String_t* V_1 = NULL;
	Dictionary_2_t2865362463 * V_2 = NULL;
	Dictionary_2_t2865362463 * V_3 = NULL;
	int32_t V_4 = 0;
	Action_1_t3252573759 * G_B4_0 = NULL;
	Dictionary_2_t2865362463 * G_B4_1 = NULL;
	String_t* G_B4_2 = NULL;
	Action_1_t3252573759 * G_B3_0 = NULL;
	Dictionary_2_t2865362463 * G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		U3CRecordU3Ec__AnonStorey0_t811208082 * L_0 = (U3CRecordU3Ec__AnonStorey0_t811208082 *)il2cpp_codegen_object_new(U3CRecordU3Ec__AnonStorey0_t811208082_il2cpp_TypeInfo_var);
		U3CRecordU3Ec__AnonStorey0__ctor_m1024376416(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRecordU3Ec__AnonStorey0_t811208082 * L_1 = V_0;
		StringU5BU5D_t1281789340* L_2 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral3050654888);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3050654888);
		StringU5BU5D_t1281789340* L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		LumosCredentials_t1718792749 * L_4 = Lumos_get_credentials_m1762176871(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = LumosCredentials_get_gameID_m318748771(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_5);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_5);
		StringU5BU5D_t1281789340* L_6 = L_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3452614641);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614641);
		StringU5BU5D_t1281789340* L_7 = L_6;
		String_t* L_8 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_8);
		StringU5BU5D_t1281789340* L_9 = L_7;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1541660959);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1541660959);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1809518182(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_prefsKey_0(L_10);
		U3CRecordU3Ec__AnonStorey0_t811208082 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_prefsKey_0();
		bool L_13 = PlayerPrefs_HasKey_m2794939670(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0055;
		}
	}
	{
		return;
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosAnalytics_t2745971597_il2cpp_TypeInfo_var);
		String_t* L_14 = LumosAnalytics_get_baseUrl_m394505943(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		String_t* L_15 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2163913788(NULL /*static, unused*/, L_14, _stringLiteral2113053237, L_15, _stringLiteral3467912857, /*hidden argument*/NULL);
		V_1 = L_16;
		Dictionary_2_t2865362463 * L_17 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3962145734(L_17, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
		V_3 = L_17;
		Dictionary_2_t2865362463 * L_18 = V_3;
		int32_t L_19 = Application_get_systemLanguage_m3110370732(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_19;
		RuntimeObject * L_20 = Box(SystemLanguage_t949212163_il2cpp_TypeInfo_var, (&V_4));
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = *(int32_t*)UnBox(L_20);
		NullCheck(L_18);
		Dictionary_2_Add_m825500632(L_18, _stringLiteral2143290860, L_21, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_22 = V_3;
		V_2 = L_22;
		String_t* L_23 = V_1;
		Dictionary_2_t2865362463 * L_24 = V_2;
		U3CRecordU3Ec__AnonStorey0_t811208082 * L_25 = V_0;
		intptr_t L_26 = (intptr_t)U3CRecordU3Ec__AnonStorey0_U3CU3Em__0_m4184800487_RuntimeMethod_var;
		Action_1_t3252573759 * L_27 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_27, L_25, L_26, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		Action_1_t3252573759 * L_28 = ((LumosLocation_t2816019121_StaticFields*)il2cpp_codegen_static_fields_for(LumosLocation_t2816019121_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_0();
		G_B3_0 = L_27;
		G_B3_1 = L_24;
		G_B3_2 = L_23;
		if (L_28)
		{
			G_B4_0 = L_27;
			G_B4_1 = L_24;
			G_B4_2 = L_23;
			goto IL_00bc;
		}
	}
	{
		intptr_t L_29 = (intptr_t)LumosLocation_U3CRecordU3Em__0_m71196215_RuntimeMethod_var;
		Action_1_t3252573759 * L_30 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_30, NULL, L_29, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		((LumosLocation_t2816019121_StaticFields*)il2cpp_codegen_static_fields_for(LumosLocation_t2816019121_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_0(L_30);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_00bc:
	{
		Action_1_t3252573759 * L_31 = ((LumosLocation_t2816019121_StaticFields*)il2cpp_codegen_static_fields_for(LumosLocation_t2816019121_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_0();
		LumosRequest_Send_m552353156(NULL /*static, unused*/, G_B4_2, G_B4_1, G_B4_0, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosLocation::<Record>m__0(System.Object)
extern "C"  void LumosLocation_U3CRecordU3Em__0_m71196215 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLocation_U3CRecordU3Em__0_m71196215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogError_m3535111913(NULL /*static, unused*/, _stringLiteral2939991487, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosLocation/<Record>c__AnonStorey0::.ctor()
extern "C"  void U3CRecordU3Ec__AnonStorey0__ctor_m1024376416 (U3CRecordU3Ec__AnonStorey0_t811208082 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosLocation/<Record>c__AnonStorey0::<>m__0(System.Object)
extern "C"  void U3CRecordU3Ec__AnonStorey0_U3CU3Em__0_m4184800487 (U3CRecordU3Ec__AnonStorey0_t811208082 * __this, RuntimeObject * ___success0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRecordU3Ec__AnonStorey0_U3CU3Em__0_m4184800487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_prefsKey_0();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_1 = DateTime_get_Now_m1277138875(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = DateTime_ToString_m884486936((&V_0), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_Log_m2411948670(NULL /*static, unused*/, _stringLiteral2970753464, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosLogs::Record(System.String,System.String,UnityEngine.LogType)
extern "C"  void LumosLogs_Record_m1639051678 (RuntimeObject * __this /* static, unused */, String_t* ___message0, String_t* ___trace1, int32_t ___type2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLogs_Record_m1639051678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t913802012  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Dictionary_2_t2865362463 * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		bool L_0 = LumosDiagnostics_IsInitialized_m1820736181(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		bool L_1 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		bool L_2 = Lumos_get_runInEditor_m2527088602(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		return;
	}

IL_0020:
	{
		String_t* L_3 = ___message0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1759067526(L_3, _stringLiteral4051234214, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		return;
	}

IL_0031:
	{
		String_t* L_5 = ___message0;
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		String_t* L_6 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0048;
		}
	}

IL_0047:
	{
		return;
	}

IL_0048:
	{
		int32_t L_9 = ___type2;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0092;
		}
	}
	{
		int32_t L_10 = ___type2;
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		bool L_11 = LumosDiagnostics_get_recordDebugLogs_m2269547550(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0092;
		}
	}

IL_0060:
	{
		int32_t L_12 = ___type2;
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		bool L_13 = LumosDiagnostics_get_recordDebugWarnings_m2284271601(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0092;
		}
	}

IL_0071:
	{
		int32_t L_14 = ___type2;
		if (L_14)
		{
			goto IL_0081;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		bool L_15 = LumosDiagnostics_get_recordDebugErrors_m1784451440(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0092;
		}
	}

IL_0081:
	{
		int32_t L_16 = ___type2;
		if ((!(((uint32_t)L_16) == ((uint32_t)4))))
		{
			goto IL_0093;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		bool L_17 = LumosDiagnostics_get_recordDebugErrors_m1784451440(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0093;
		}
	}

IL_0092:
	{
		return;
	}

IL_0093:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		List_1_t3319525431 * L_18 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_toIgnore_0();
		NullCheck(L_18);
		Enumerator_t913802012  L_19 = List_1_GetEnumerator_m2598014212(L_18, /*hidden argument*/List_1_GetEnumerator_m2598014212_RuntimeMethod_var);
		V_1 = L_19;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bc;
		}

IL_00a3:
		{
			String_t* L_20 = Enumerator_get_Current_m2973281003((&V_1), /*hidden argument*/Enumerator_get_Current_m2973281003_RuntimeMethod_var);
			V_0 = L_20;
			String_t* L_21 = ___message0;
			String_t* L_22 = V_0;
			NullCheck(L_21);
			bool L_23 = String_StartsWith_m1759067526(L_21, L_22, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00bc;
			}
		}

IL_00b7:
		{
			IL2CPP_LEAVE(0x1BF, FINALLY_00cd);
		}

IL_00bc:
		{
			bool L_24 = Enumerator_MoveNext_m3556324971((&V_1), /*hidden argument*/Enumerator_MoveNext_m3556324971_RuntimeMethod_var);
			if (L_24)
			{
				goto IL_00a3;
			}
		}

IL_00c8:
		{
			IL2CPP_LEAVE(0xDB, FINALLY_00cd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00cd;
	}

FINALLY_00cd:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2026665411((&V_1), /*hidden argument*/Enumerator_Dispose_m2026665411_RuntimeMethod_var);
		IL2CPP_END_FINALLY(205)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(205)
	{
		IL2CPP_JUMP_TBL(0x1BF, IL_01bf)
		IL2CPP_JUMP_TBL(0xDB, IL_00db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00db:
	{
		StringU5BU5D_t1281789340* L_25 = ((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Dictionary_2_t1123770287 * L_26 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_typeLabels_2();
		int32_t L_27 = ___type2;
		NullCheck(L_26);
		String_t* L_28 = Dictionary_2_get_Item_m2887862096(L_26, L_27, /*hidden argument*/Dictionary_2_get_Item_m2887862096_RuntimeMethod_var);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_28);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_28);
		StringU5BU5D_t1281789340* L_29 = L_25;
		String_t* L_30 = ___message0;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_30);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_30);
		StringU5BU5D_t1281789340* L_31 = L_29;
		String_t* L_32 = ___trace1;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_32);
		String_t* L_33 = LumosUtil_MD5Hash_m2689163647(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_2 = L_33;
		Dictionary_2_t2650618762 * L_34 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		String_t* L_35 = V_2;
		NullCheck(L_34);
		bool L_36 = Dictionary_2_ContainsKey_m24148094(L_34, L_35, /*hidden argument*/Dictionary_2_ContainsKey_m24148094_RuntimeMethod_var);
		if (!L_36)
		{
			goto IL_0148;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_37 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		String_t* L_38 = V_2;
		NullCheck(L_37);
		Dictionary_2_t2865362463 * L_39 = Dictionary_2_get_Item_m845009296(L_37, L_38, /*hidden argument*/Dictionary_2_get_Item_m845009296_RuntimeMethod_var);
		Dictionary_2_t2650618762 * L_40 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		String_t* L_41 = V_2;
		NullCheck(L_40);
		Dictionary_2_t2865362463 * L_42 = Dictionary_2_get_Item_m845009296(L_40, L_41, /*hidden argument*/Dictionary_2_get_Item_m845009296_RuntimeMethod_var);
		NullCheck(L_42);
		RuntimeObject * L_43 = Dictionary_2_get_Item_m3179620279(L_42, _stringLiteral847691140, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		int32_t L_44 = ((int32_t)il2cpp_codegen_add((int32_t)((*(int32_t*)((int32_t*)UnBox(L_43, Int32_t2950945753_il2cpp_TypeInfo_var)))), (int32_t)1));
		RuntimeObject * L_45 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_39);
		Dictionary_2_set_Item_m2329160973(L_39, _stringLiteral847691140, L_45, /*hidden argument*/Dictionary_2_set_Item_m2329160973_RuntimeMethod_var);
		goto IL_01bf;
	}

IL_0148:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_46 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		String_t* L_47 = V_2;
		Dictionary_2_t2865362463 * L_48 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3962145734(L_48, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
		V_3 = L_48;
		Dictionary_2_t2865362463 * L_49 = V_3;
		String_t* L_50 = V_2;
		NullCheck(L_49);
		Dictionary_2_Add_m825500632(L_49, _stringLiteral2122998792, L_50, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_51 = V_3;
		Dictionary_2_t1123770287 * L_52 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_typeLabels_2();
		int32_t L_53 = ___type2;
		NullCheck(L_52);
		String_t* L_54 = Dictionary_2_get_Item_m2887862096(L_52, L_53, /*hidden argument*/Dictionary_2_get_Item_m2887862096_RuntimeMethod_var);
		NullCheck(L_51);
		Dictionary_2_Add_m825500632(L_51, _stringLiteral3243520166, L_54, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_55 = V_3;
		String_t* L_56 = ___message0;
		NullCheck(L_55);
		Dictionary_2_Add_m825500632(L_55, _stringLiteral3253941996, L_56, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_57 = V_3;
		String_t* L_58 = Application_get_loadedLevelName_m1849536804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		Dictionary_2_Add_m825500632(L_57, _stringLiteral1232840130, L_58, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_59 = V_3;
		int32_t L_60 = 1;
		RuntimeObject * L_61 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_59);
		Dictionary_2_Add_m825500632(L_59, _stringLiteral847691140, L_61, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_62 = V_3;
		NullCheck(L_46);
		Dictionary_2_set_Item_m761006959(L_46, L_47, L_62, /*hidden argument*/Dictionary_2_set_Item_m761006959_RuntimeMethod_var);
		Dictionary_2_t2650618762 * L_63 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		String_t* L_64 = V_2;
		NullCheck(L_63);
		Dictionary_2_t2865362463 * L_65 = Dictionary_2_get_Item_m845009296(L_63, L_64, /*hidden argument*/Dictionary_2_get_Item_m845009296_RuntimeMethod_var);
		String_t* L_66 = ___trace1;
		LumosUtil_AddToDictionaryIfNonempty_m1393722542(NULL /*static, unused*/, L_65, _stringLiteral1609214314, L_66, /*hidden argument*/NULL);
	}

IL_01bf:
	{
		return;
	}
}
// System.Void LumosLogs::Send()
extern "C"  void LumosLogs_Send_m4102305593 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLogs_Send_m4102305593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t42469909 * V_1 = NULL;
	List_1_t42469909 * G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	List_1_t42469909 * G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	Action_1_t3252573759 * G_B6_0 = NULL;
	List_1_t42469909 * G_B6_1 = NULL;
	String_t* G_B6_2 = NULL;
	Action_1_t3252573759 * G_B5_0 = NULL;
	List_1_t42469909 * G_B5_1 = NULL;
	String_t* G_B5_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_0 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m3447859782(L_0, /*hidden argument*/Dictionary_2_get_Count_m3447859782_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosDiagnostics_t279239158_il2cpp_TypeInfo_var);
		String_t* L_2 = LumosDiagnostics_get_baseUrl_m476843529(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, L_2, _stringLiteral3955078645, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_4 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		NullCheck(L_4);
		ValueCollection_t71695784 * L_5 = Dictionary_2_get_Values_m2758554421(L_4, /*hidden argument*/Dictionary_2_get_Values_m2758554421_RuntimeMethod_var);
		List_1_t42469909 * L_6 = (List_1_t42469909 *)il2cpp_codegen_object_new(List_1_t42469909_il2cpp_TypeInfo_var);
		List_1__ctor_m1152355348(L_6, L_5, /*hidden argument*/List_1__ctor_m1152355348_RuntimeMethod_var);
		V_1 = L_6;
		String_t* L_7 = V_0;
		List_1_t42469909 * L_8 = V_1;
		Action_1_t3252573759 * L_9 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_3();
		G_B3_0 = L_8;
		G_B3_1 = L_7;
		if (L_9)
		{
			G_B4_0 = L_8;
			G_B4_1 = L_7;
			goto IL_004a;
		}
	}
	{
		intptr_t L_10 = (intptr_t)LumosLogs_U3CSendU3Em__0_m3334083460_RuntimeMethod_var;
		Action_1_t3252573759 * L_11 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_11, NULL, L_10, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_3(L_11);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_12 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_3();
		Action_1_t3252573759 * L_13 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_4();
		G_B5_0 = L_12;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
		if (L_13)
		{
			G_B6_0 = L_12;
			G_B6_1 = G_B4_0;
			G_B6_2 = G_B4_1;
			goto IL_0067;
		}
	}
	{
		intptr_t L_14 = (intptr_t)LumosLogs_U3CSendU3Em__1_m754180927_RuntimeMethod_var;
		Action_1_t3252573759 * L_15 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_15, NULL, L_14, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache1_4(L_15);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0067:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_16 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_4();
		LumosRequest_Send_m552353156(NULL /*static, unused*/, G_B6_2, G_B6_1, G_B6_0, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosLogs::AddMessagesToIgnore(System.String[])
extern "C"  void LumosLogs_AddMessagesToIgnore_m880380652 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* ___messages0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLogs_AddMessagesToIgnore_m880380652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		List_1_t3319525431 * L_0 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_toIgnore_0();
		StringU5BU5D_t1281789340* L_1 = ___messages0;
		NullCheck(L_0);
		List_1_AddRange_m1670619200(L_0, (RuntimeObject*)(RuntimeObject*)L_1, /*hidden argument*/List_1_AddRange_m1670619200_RuntimeMethod_var);
		return;
	}
}
// System.Void LumosLogs::.cctor()
extern "C"  void LumosLogs__cctor_m991303528 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLogs__cctor_m991303528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3319525431 * V_0 = NULL;
	Dictionary_2_t1123770287 * V_1 = NULL;
	{
		List_1_t3319525431 * L_0 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_0, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t3319525431 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m1685793073(L_1, _stringLiteral3718524431, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		List_1_t3319525431 * L_2 = V_0;
		((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->set_toIgnore_0(L_2);
		Dictionary_2_t2650618762 * L_3 = (Dictionary_2_t2650618762 *)il2cpp_codegen_object_new(Dictionary_2_t2650618762_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m199014843(L_3, /*hidden argument*/Dictionary_2__ctor_m199014843_RuntimeMethod_var);
		((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->set_logs_1(L_3);
		Dictionary_2_t1123770287 * L_4 = (Dictionary_2_t1123770287 *)il2cpp_codegen_object_new(Dictionary_2_t1123770287_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3365230958(L_4, /*hidden argument*/Dictionary_2__ctor_m3365230958_RuntimeMethod_var);
		V_1 = L_4;
		Dictionary_2_t1123770287 * L_5 = V_1;
		NullCheck(L_5);
		Dictionary_2_Add_m474478774(L_5, 1, _stringLiteral2595384961, /*hidden argument*/Dictionary_2_Add_m474478774_RuntimeMethod_var);
		Dictionary_2_t1123770287 * L_6 = V_1;
		NullCheck(L_6);
		Dictionary_2_Add_m474478774(L_6, 0, _stringLiteral95342995, /*hidden argument*/Dictionary_2_Add_m474478774_RuntimeMethod_var);
		Dictionary_2_t1123770287 * L_7 = V_1;
		NullCheck(L_7);
		Dictionary_2_Add_m474478774(L_7, 4, _stringLiteral2618865335, /*hidden argument*/Dictionary_2_Add_m474478774_RuntimeMethod_var);
		Dictionary_2_t1123770287 * L_8 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m474478774(L_8, 3, _stringLiteral79347, /*hidden argument*/Dictionary_2_Add_m474478774_RuntimeMethod_var);
		Dictionary_2_t1123770287 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m474478774(L_9, 2, _stringLiteral1461069931, /*hidden argument*/Dictionary_2_Add_m474478774_RuntimeMethod_var);
		Dictionary_2_t1123770287 * L_10 = V_1;
		((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->set_typeLabels_2(L_10);
		return;
	}
}
// System.Void LumosLogs::<Send>m__0(System.Object)
extern "C"  void LumosLogs_U3CSendU3Em__0_m3334083460 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___success0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLogs_U3CSendU3Em__0_m3334083460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosLogs_t761625864_il2cpp_TypeInfo_var);
		Dictionary_2_t2650618762 * L_0 = ((LumosLogs_t761625864_StaticFields*)il2cpp_codegen_static_fields_for(LumosLogs_t761625864_il2cpp_TypeInfo_var))->get_logs_1();
		NullCheck(L_0);
		Dictionary_2_Clear_m41144274(L_0, /*hidden argument*/Dictionary_2_Clear_m41144274_RuntimeMethod_var);
		return;
	}
}
// System.Void LumosLogs::<Send>m__1(System.Object)
extern "C"  void LumosLogs_U3CSendU3Em__1_m754180927 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosLogs_U3CSendU3Em__1_m754180927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogWarning_m3534960519(NULL /*static, unused*/, _stringLiteral3260327796, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosPlayer::.ctor()
extern "C"  void LumosPlayer__ctor_m2357924849 (LumosPlayer_t370097434 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String LumosPlayer::get_baseUrl()
extern "C"  String_t* LumosPlayer_get_baseUrl_m2504180808 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosPlayer_get_baseUrl_m2504180808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		String_t* L_0 = ((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->get__baseUrl_0();
		return L_0;
	}
}
// System.Void LumosPlayer::set_baseUrl(System.String)
extern "C"  void LumosPlayer_set_baseUrl_m1864604462 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosPlayer_set_baseUrl_m1864604462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->set__baseUrl_0(L_0);
		return;
	}
}
// System.Void LumosPlayer::Init(System.Action`1<System.Boolean>)
extern "C"  void LumosPlayer_Init_m3905959549 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosPlayer_Init_m3905959549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		LumosCredentials_t1718792749 * L_0 = Lumos_get_credentials_m1762176871(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = LumosCredentials_get_gameID_m318748771(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3050654888, L_1, _stringLiteral1887700128, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		bool L_4 = PlayerPrefs_HasKey_m2794939670(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0055;
		}
	}
	{
		String_t* L_5 = V_0;
		String_t* L_6 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_set_playerId_m314876524(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3464815593, L_7, /*hidden argument*/NULL);
		Lumos_Log_m2411948670(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Action_1_t269755560 * L_9 = ___callback0;
		NullCheck(L_9);
		Action_1_Invoke_m1933767679(L_9, (bool)1, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		LumosPlayer_Ping_m3221862318(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0055:
	{
		Action_1_t269755560 * L_10 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		LumosPlayer_RequestPlayerId_m1064872458(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void LumosPlayer::RequestPlayerId(System.Action`1<System.Boolean>)
extern "C"  void LumosPlayer_RequestPlayerId_m1064872458 (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosPlayer_RequestPlayerId_m1064872458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * L_0 = (U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 *)il2cpp_codegen_object_new(U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854_il2cpp_TypeInfo_var);
		U3CRequestPlayerIdU3Ec__AnonStorey0__ctor_m112441722(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * L_1 = V_0;
		Action_1_t269755560 * L_2 = ___callback0;
		NullCheck(L_1);
		L_1->set_callback_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		String_t* L_3 = LumosPlayer_get_baseUrl_m2504180808(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral381117126, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CRequestPlayerIdU3Ec__AnonStorey0_U3CU3Em__0_m2106923745_RuntimeMethod_var;
		Action_1_t3252573759 * L_8 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_8, L_6, L_7, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		LumosRequest_Send_m1124333148(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosPlayer::Ping()
extern "C"  void LumosPlayer_Ping_m3221862318 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosPlayer_Ping_m3221862318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t2865362463 * V_1 = NULL;
	Dictionary_2_t2865362463 * V_2 = NULL;
	Dictionary_2_t2865362463 * G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Dictionary_2_t2865362463 * G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Action_1_t3252573759 * G_B4_0 = NULL;
	Dictionary_2_t2865362463 * G_B4_1 = NULL;
	String_t* G_B4_2 = NULL;
	Action_1_t3252573759 * G_B3_0 = NULL;
	Dictionary_2_t2865362463 * G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		String_t* L_0 = LumosPlayer_get_baseUrl_m2504180808(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		String_t* L_1 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2163913788(NULL /*static, unused*/, L_0, _stringLiteral740565913, L_1, _stringLiteral3467912857, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t2865362463 * L_3 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3962145734(L_3, /*hidden argument*/Dictionary_2__ctor_m3962145734_RuntimeMethod_var);
		V_2 = L_3;
		Dictionary_2_t2865362463 * L_4 = V_2;
		String_t* L_5 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m825500632(L_4, _stringLiteral2215313024, L_5, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_6 = V_2;
		NullCheck(_stringLiteral2670441722);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, _stringLiteral2670441722);
		NullCheck(L_6);
		Dictionary_2_Add_m825500632(L_6, _stringLiteral3984296161, L_7, /*hidden argument*/Dictionary_2_Add_m825500632_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_8 = V_2;
		V_1 = L_8;
		String_t* L_9 = V_0;
		Dictionary_2_t2865362463 * L_10 = V_1;
		Action_1_t3252573759 * L_11 = ((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_1();
		G_B1_0 = L_10;
		G_B1_1 = L_9;
		if (L_11)
		{
			G_B2_0 = L_10;
			G_B2_1 = L_9;
			goto IL_0061;
		}
	}
	{
		intptr_t L_12 = (intptr_t)LumosPlayer_U3CPingU3Em__0_m3675009066_RuntimeMethod_var;
		Action_1_t3252573759 * L_13 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_13, NULL, L_12, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_1(L_13);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_14 = ((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_1();
		Action_1_t3252573759 * L_15 = ((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_2();
		G_B3_0 = L_14;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		if (L_15)
		{
			G_B4_0 = L_14;
			G_B4_1 = G_B2_0;
			G_B4_2 = G_B2_1;
			goto IL_007e;
		}
	}
	{
		intptr_t L_16 = (intptr_t)LumosPlayer_U3CPingU3Em__1_m3675104263_RuntimeMethod_var;
		Action_1_t3252573759 * L_17 = (Action_1_t3252573759 *)il2cpp_codegen_object_new(Action_1_t3252573759_il2cpp_TypeInfo_var);
		Action_1__ctor_m118522912(L_17, NULL, L_16, /*hidden argument*/Action_1__ctor_m118522912_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache1_2(L_17);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(LumosPlayer_t370097434_il2cpp_TypeInfo_var);
		Action_1_t3252573759 * L_18 = ((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_2();
		LumosRequest_Send_m552353156(NULL /*static, unused*/, G_B4_2, G_B4_1, G_B4_0, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosPlayer::.cctor()
extern "C"  void LumosPlayer__cctor_m1608523625 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosPlayer__cctor_m1608523625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LumosPlayer_t370097434_StaticFields*)il2cpp_codegen_static_fields_for(LumosPlayer_t370097434_il2cpp_TypeInfo_var))->set__baseUrl_0(_stringLiteral3018646317);
		return;
	}
}
// System.Void LumosPlayer::<Ping>m__0(System.Object)
extern "C"  void LumosPlayer_U3CPingU3Em__0_m3675009066 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___success0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void LumosPlayer::<Ping>m__1(System.Object)
extern "C"  void LumosPlayer_U3CPingU3Em__1_m3675104263 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___error0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosPlayer/<RequestPlayerId>c__AnonStorey0::.ctor()
extern "C"  void U3CRequestPlayerIdU3Ec__AnonStorey0__ctor_m112441722 (U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LumosPlayer/<RequestPlayerId>c__AnonStorey0::<>m__0(System.Object)
extern "C"  void U3CRequestPlayerIdU3Ec__AnonStorey0_U3CU3Em__0_m2106923745 (U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854 * __this, RuntimeObject * ___success0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRequestPlayerIdU3Ec__AnonStorey0_U3CU3Em__0_m2106923745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t2865362463 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		LumosCredentials_t1718792749 * L_0 = Lumos_get_credentials_m1762176871(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = LumosCredentials_get_gameID_m318748771(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3050654888, L_1, _stringLiteral1887700128, /*hidden argument*/NULL);
		V_0 = L_2;
		RuntimeObject * L_3 = ___success0;
		V_1 = ((Dictionary_2_t2865362463 *)IsInstClass((RuntimeObject*)L_3, Dictionary_2_t2865362463_il2cpp_TypeInfo_var));
		Dictionary_2_t2865362463 * L_4 = V_1;
		NullCheck(L_4);
		RuntimeObject * L_5 = Dictionary_2_get_Item_m3179620279(L_4, _stringLiteral2215313024, /*hidden argument*/Dictionary_2_get_Item_m3179620279_RuntimeMethod_var);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		Lumos_set_playerId_m314876524(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		String_t* L_8 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		String_t* L_9 = Lumos_get_playerId_m505766044(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2466698272, L_9, /*hidden argument*/NULL);
		Lumos_Log_m2411948670(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Action_1_t269755560 * L_11 = __this->get_callback_0();
		if (!L_11)
		{
			goto IL_006c;
		}
	}
	{
		Action_1_t269755560 * L_12 = __this->get_callback_0();
		NullCheck(L_12);
		Action_1_Invoke_m1933767679(L_12, (bool)1, /*hidden argument*/Action_1_Invoke_m1933767679_RuntimeMethod_var);
	}

IL_006c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosRequest::.ctor()
extern "C"  void LumosRequest__ctor_m1260183409 (LumosRequest_t457072260 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine LumosRequest::Send(System.String)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m812021116 (RuntimeObject * __this /* static, unused */, String_t* ___url0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_Send_m812021116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___url0;
		RuntimeObject* L_1 = LumosRequest_SendCoroutine_m2726591602(NULL /*static, unused*/, L_0, NULL, (Action_1_t3252573759 *)NULL, (Action_1_t3252573759 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_2 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Action`1<System.Object>)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m1124333148 (RuntimeObject * __this /* static, unused */, String_t* ___url0, Action_1_t3252573759 * ___successCallback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_Send_m1124333148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___url0;
		Action_1_t3252573759 * L_1 = ___successCallback1;
		RuntimeObject* L_2 = LumosRequest_SendCoroutine_m2726591602(NULL /*static, unused*/, L_0, NULL, L_1, (Action_1_t3252573759 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_3 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Action`1<System.Object>,System.Action`1<System.Object>)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m3116052310 (RuntimeObject * __this /* static, unused */, String_t* ___url0, Action_1_t3252573759 * ___successCallback1, Action_1_t3252573759 * ___errorCallback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_Send_m3116052310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___url0;
		Action_1_t3252573759 * L_1 = ___successCallback1;
		Action_1_t3252573759 * L_2 = ___errorCallback2;
		RuntimeObject* L_3 = LumosRequest_SendCoroutine_m2726591602(NULL /*static, unused*/, L_0, NULL, L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_4 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Object)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m1223465123 (RuntimeObject * __this /* static, unused */, String_t* ___url0, RuntimeObject * ___parameters1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_Send_m1223465123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___url0;
		RuntimeObject * L_1 = ___parameters1;
		RuntimeObject* L_2 = LumosRequest_SendCoroutine_m2726591602(NULL /*static, unused*/, L_0, L_1, (Action_1_t3252573759 *)NULL, (Action_1_t3252573759 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_3 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Object,System.Action`1<System.Object>)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m1094114333 (RuntimeObject * __this /* static, unused */, String_t* ___url0, RuntimeObject * ___parameters1, Action_1_t3252573759 * ___successCallback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_Send_m1094114333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___url0;
		RuntimeObject * L_1 = ___parameters1;
		Action_1_t3252573759 * L_2 = ___successCallback2;
		RuntimeObject* L_3 = LumosRequest_SendCoroutine_m2726591602(NULL /*static, unused*/, L_0, L_1, L_2, (Action_1_t3252573759 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_4 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Coroutine LumosRequest::Send(System.String,System.Object,System.Action`1<System.Object>,System.Action`1<System.Object>)
extern "C"  Coroutine_t3829159415 * LumosRequest_Send_m552353156 (RuntimeObject * __this /* static, unused */, String_t* ___url0, RuntimeObject * ___parameters1, Action_1_t3252573759 * ___successCallback2, Action_1_t3252573759 * ___errorCallback3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_Send_m552353156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___url0;
		RuntimeObject * L_1 = ___parameters1;
		Action_1_t3252573759 * L_2 = ___successCallback2;
		Action_1_t3252573759 * L_3 = ___errorCallback3;
		RuntimeObject* L_4 = LumosRequest_SendCoroutine_m2726591602(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_5 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.IEnumerator LumosRequest::SendCoroutine(System.String,System.Object,System.Action`1<System.Object>,System.Action`1<System.Object>)
extern "C"  RuntimeObject* LumosRequest_SendCoroutine_m2726591602 (RuntimeObject * __this /* static, unused */, String_t* ___url0, RuntimeObject * ___parameters1, Action_1_t3252573759 * ___successCallback2, Action_1_t3252573759 * ___errorCallback3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_SendCoroutine_m2726591602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSendCoroutineU3Ec__Iterator0_t597473103 * V_0 = NULL;
	{
		U3CSendCoroutineU3Ec__Iterator0_t597473103 * L_0 = (U3CSendCoroutineU3Ec__Iterator0_t597473103 *)il2cpp_codegen_object_new(U3CSendCoroutineU3Ec__Iterator0_t597473103_il2cpp_TypeInfo_var);
		U3CSendCoroutineU3Ec__Iterator0__ctor_m701103518(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendCoroutineU3Ec__Iterator0_t597473103 * L_1 = V_0;
		Action_1_t3252573759 * L_2 = ___errorCallback3;
		NullCheck(L_1);
		L_1->set_errorCallback_0(L_2);
		U3CSendCoroutineU3Ec__Iterator0_t597473103 * L_3 = V_0;
		return L_3;
	}
}
// System.Byte[] LumosRequest::SerializePostData(System.Object)
extern "C"  ByteU5BU5D_t4116647657* LumosRequest_SerializePostData_m3510791873 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___parameters0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_SerializePostData_m3510791873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t4116647657* V_1 = NULL;
	{
		RuntimeObject * L_0 = ___parameters0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		V_0 = _stringLiteral3455956949;
		goto IL_0018;
	}

IL_0011:
	{
		RuntimeObject * L_1 = ___parameters0;
		IL2CPP_RUNTIME_CLASS_INIT(LumosJson_t1197320855_il2cpp_TypeInfo_var);
		String_t* L_2 = LumosJson_Serialize_m1033319704(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_3 = Encoding_get_ASCII_m3595602635(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		NullCheck(L_3);
		ByteU5BU5D_t4116647657* L_5 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, L_4);
		V_1 = L_5;
		ByteU5BU5D_t4116647657* L_6 = V_1;
		return L_6;
	}
}
// System.String LumosRequest::GenerateAuthorizationHeader(LumosCredentials,System.Byte[])
extern "C"  String_t* LumosRequest_GenerateAuthorizationHeader_m1225326569 (RuntimeObject * __this /* static, unused */, LumosCredentials_t1718792749 * ___credentials0, ByteU5BU5D_t4116647657* ___postData1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_GenerateAuthorizationHeader_m1225326569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	HMACSHA1_t1952596188 * V_1 = NULL;
	ByteU5BU5D_t4116647657* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		ByteU5BU5D_t4116647657* L_0 = ___postData1;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		___postData1 = ((ByteU5BU5D_t4116647657*)SZArrayNew(ByteU5BU5D_t4116647657_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_1 = Encoding_get_ASCII_m3595602635(NULL /*static, unused*/, /*hidden argument*/NULL);
		LumosCredentials_t1718792749 * L_2 = ___credentials0;
		NullCheck(L_2);
		String_t* L_3 = L_2->get_apiKey_2();
		NullCheck(L_1);
		ByteU5BU5D_t4116647657* L_4 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, L_3);
		V_0 = L_4;
		ByteU5BU5D_t4116647657* L_5 = V_0;
		HMACSHA1_t1952596188 * L_6 = (HMACSHA1_t1952596188 *)il2cpp_codegen_object_new(HMACSHA1_t1952596188_il2cpp_TypeInfo_var);
		HMACSHA1__ctor_m446190279(L_6, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		HMACSHA1_t1952596188 * L_7 = V_1;
		ByteU5BU5D_t4116647657* L_8 = ___postData1;
		NullCheck(L_7);
		ByteU5BU5D_t4116647657* L_9 = HashAlgorithm_ComputeHash_m2825542963(L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		ByteU5BU5D_t4116647657* L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		String_t* L_11 = Convert_ToBase64String_m3839334935(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		LumosCredentials_t1718792749 * L_12 = ___credentials0;
		NullCheck(L_12);
		String_t* L_13 = LumosCredentials_get_gameID_m318748771(L_12, /*hidden argument*/NULL);
		String_t* L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2163913788(NULL /*static, unused*/, _stringLiteral39705060, L_13, _stringLiteral3452614550, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		String_t* L_16 = V_4;
		return L_16;
	}
}
// UnityEngine.Coroutine LumosRequest::LoadImage(System.String,UnityEngine.Texture2D)
extern "C"  Coroutine_t3829159415 * LumosRequest_LoadImage_m640554234 (RuntimeObject * __this /* static, unused */, String_t* ___imageLocation0, Texture2D_t3840446185 * ___texture1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_LoadImage_m640554234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___imageLocation0;
		Texture2D_t3840446185 * L_1 = ___texture1;
		RuntimeObject* L_2 = LumosRequest_LoadImageCoroutine_m281388845(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_3 = Lumos_RunRoutine_m1158721104(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Collections.IEnumerator LumosRequest::LoadImageCoroutine(System.String,UnityEngine.Texture2D)
extern "C"  RuntimeObject* LumosRequest_LoadImageCoroutine_m281388845 (RuntimeObject * __this /* static, unused */, String_t* ___imageLocation0, Texture2D_t3840446185 * ___texture1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosRequest_LoadImageCoroutine_m281388845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * V_0 = NULL;
	{
		U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * L_0 = (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 *)il2cpp_codegen_object_new(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463_il2cpp_TypeInfo_var);
		U3CLoadImageCoroutineU3Ec__Iterator1__ctor_m3559792677(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * L_1 = V_0;
		String_t* L_2 = ___imageLocation0;
		NullCheck(L_1);
		L_1->set_imageLocation_0(L_2);
		U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * L_3 = V_0;
		Texture2D_t3840446185 * L_4 = ___texture1;
		NullCheck(L_3);
		L_3->set_texture_2(L_4);
		U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosRequest/<LoadImageCoroutine>c__Iterator1::.ctor()
extern "C"  void U3CLoadImageCoroutineU3Ec__Iterator1__ctor_m3559792677 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LumosRequest/<LoadImageCoroutine>c__Iterator1::MoveNext()
extern "C"  bool U3CLoadImageCoroutineU3Ec__Iterator1_MoveNext_m1188241163 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageCoroutineU3Ec__Iterator1_MoveNext_m1188241163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0052;
			}
		}
	}
	{
		goto IL_00ab;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_imageLocation_0();
		WWW_t3688466362 * L_3 = (WWW_t3688466362 *)il2cpp_codegen_object_new(WWW_t3688466362_il2cpp_TypeInfo_var);
		WWW__ctor_m2915079343(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_3);
		WWW_t3688466362 * L_4 = __this->get_U3CwwwU3E__0_1();
		__this->set_U24current_3(L_4);
		bool L_5 = __this->get_U24disposing_4();
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_004d:
	{
		goto IL_00ad;
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		{
			WWW_t3688466362 * L_6 = __this->get_U3CwwwU3E__0_1();
			NullCheck(L_6);
			String_t* L_7 = WWW_get_error_m3055313367(L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0073;
			}
		}

IL_0062:
		{
			WWW_t3688466362 * L_8 = __this->get_U3CwwwU3E__0_1();
			NullCheck(L_8);
			String_t* L_9 = WWW_get_error_m3055313367(L_8, /*hidden argument*/NULL);
			Exception_t * L_10 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
			Exception__ctor_m1152696503(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10,U3CLoadImageCoroutineU3Ec__Iterator1_MoveNext_m1188241163_RuntimeMethod_var);
		}

IL_0073:
		{
			WWW_t3688466362 * L_11 = __this->get_U3CwwwU3E__0_1();
			Texture2D_t3840446185 * L_12 = __this->get_texture_2();
			NullCheck(L_11);
			WWW_LoadImageIntoTexture_m3209745837(L_11, L_12, /*hidden argument*/NULL);
			goto IL_00a4;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0089;
		throw e;
	}

CATCH_0089:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t *)__exception_local);
		Exception_t * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2183713642, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		Lumos_LogError_m3535111913(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		goto IL_00a4;
	} // end catch (depth: 1)

IL_00a4:
	{
		__this->set_U24PC_5((-1));
	}

IL_00ab:
	{
		return (bool)0;
	}

IL_00ad:
	{
		return (bool)1;
	}
}
// System.Object LumosRequest/<LoadImageCoroutine>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadImageCoroutineU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m446171029 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object LumosRequest/<LoadImageCoroutine>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadImageCoroutineU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2228432328 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void LumosRequest/<LoadImageCoroutine>c__Iterator1::Dispose()
extern "C"  void U3CLoadImageCoroutineU3Ec__Iterator1_Dispose_m3867444509 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void LumosRequest/<LoadImageCoroutine>c__Iterator1::Reset()
extern "C"  void U3CLoadImageCoroutineU3Ec__Iterator1_Reset_m836804549 (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadImageCoroutineU3Ec__Iterator1_Reset_m836804549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CLoadImageCoroutineU3Ec__Iterator1_Reset_m836804549_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosRequest/<SendCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__Iterator0__ctor_m701103518 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LumosRequest/<SendCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CSendCoroutineU3Ec__Iterator0_MoveNext_m592653404 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendCoroutineU3Ec__Iterator0_MoveNext_m592653404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_U24PC_3();
		__this->set_U24PC_3((-1));
		if (L_0)
		{
			goto IL_0051;
		}
	}
	{
		bool L_1 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lumos_t1046626606_il2cpp_TypeInfo_var);
		bool L_2 = Lumos_get_runInEditor_m2527088602(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0051;
	}

IL_002b:
	{
		int32_t L_3 = Application_get_internetReachability_m432100819(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0051;
		}
	}
	{
		Action_1_t3252573759 * L_4 = __this->get_errorCallback_0();
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		Action_1_t3252573759 * L_5 = __this->get_errorCallback_0();
		NullCheck(L_5);
		Action_1_Invoke_m2461023210(L_5, NULL, /*hidden argument*/Action_1_Invoke_m2461023210_RuntimeMethod_var);
	}

IL_004c:
	{
		goto IL_0051;
	}

IL_0051:
	{
		return (bool)0;
	}
}
// System.Object LumosRequest/<SendCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CSendCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3530221186 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object LumosRequest/<SendCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CSendCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1400105092 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void LumosRequest/<SendCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CSendCoroutineU3Ec__Iterator0_Dispose_m687879758 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void LumosRequest/<SendCoroutine>c__Iterator0::Reset()
extern "C"  void U3CSendCoroutineU3Ec__Iterator0_Reset_m3763864985 (U3CSendCoroutineU3Ec__Iterator0_t597473103 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendCoroutineU3Ec__Iterator0_Reset_m3763864985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CSendCoroutineU3Ec__Iterator0_Reset_m3763864985_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LumosSpecs::Record()
extern "C"  void LumosSpecs_Record_m3944775057 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.DateTime LumosUtil::UnixTimestampToDateTime(System.Double)
extern "C"  DateTime_t3738529785  LumosUtil_UnixTimestampToDateTime_m858877056 (RuntimeObject * __this /* static, unused */, double ___timestamp0, const RuntimeMethod* method)
{
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DateTime__ctor_m2030998145((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		double L_0 = ___timestamp0;
		DateTime_t3738529785  L_1 = DateTime_AddSeconds_m332574389((&V_0), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String LumosUtil::MD5Hash(System.String[])
extern "C"  String_t* LumosUtil_MD5Hash_m2689163647 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* ___strings0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosUtil_MD5Hash_m2689163647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	StringU5BU5D_t1281789340* V_2 = NULL;
	int32_t V_3 = 0;
	ByteU5BU5D_t4116647657* V_4 = NULL;
	MD5CryptoServiceProvider_t3005586042 * V_5 = NULL;
	ByteU5BU5D_t4116647657* V_6 = NULL;
	StringBuilder_t * V_7 = NULL;
	uint8_t V_8 = 0x0;
	ByteU5BU5D_t4116647657* V_9 = NULL;
	int32_t V_10 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		V_0 = L_0;
		StringU5BU5D_t1281789340* L_1 = ___strings0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_001f;
	}

IL_000f:
	{
		StringU5BU5D_t1281789340* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		String_t* L_6 = V_0;
		String_t* L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_001f:
	{
		int32_t L_10 = V_3;
		StringU5BU5D_t1281789340* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1523322056_il2cpp_TypeInfo_var);
		Encoding_t1523322056 * L_12 = Encoding_get_ASCII_m3595602635(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_13 = V_0;
		NullCheck(L_12);
		ByteU5BU5D_t4116647657* L_14 = VirtFuncInvoker1< ByteU5BU5D_t4116647657*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_12, L_13);
		V_4 = L_14;
		MD5CryptoServiceProvider_t3005586042 * L_15 = (MD5CryptoServiceProvider_t3005586042 *)il2cpp_codegen_object_new(MD5CryptoServiceProvider_t3005586042_il2cpp_TypeInfo_var);
		MD5CryptoServiceProvider__ctor_m3271163125(L_15, /*hidden argument*/NULL);
		V_5 = L_15;
		MD5CryptoServiceProvider_t3005586042 * L_16 = V_5;
		ByteU5BU5D_t4116647657* L_17 = V_4;
		NullCheck(L_16);
		ByteU5BU5D_t4116647657* L_18 = HashAlgorithm_ComputeHash_m2825542963(L_16, L_17, /*hidden argument*/NULL);
		V_6 = L_18;
		StringBuilder_t * L_19 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3121283359(L_19, /*hidden argument*/NULL);
		V_7 = L_19;
		ByteU5BU5D_t4116647657* L_20 = V_6;
		V_9 = L_20;
		V_10 = 0;
		goto IL_0080;
	}

IL_005a:
	{
		ByteU5BU5D_t4116647657* L_21 = V_9;
		int32_t L_22 = V_10;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		uint8_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_8 = L_24;
		StringBuilder_t * L_25 = V_7;
		String_t* L_26 = Byte_ToString_m3735479648((&V_8), _stringLiteral3451434968, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = String_ToLower_m2029374922(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m1965104174(L_25, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_0080:
	{
		int32_t L_29 = V_10;
		ByteU5BU5D_t4116647657* L_30 = V_9;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_30)->max_length)))))))
		{
			goto IL_005a;
		}
	}
	{
		StringBuilder_t * L_31 = V_7;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_31);
		return L_32;
	}
}
// System.Void LumosUtil::AddToDictionaryIfNonempty(System.Collections.IDictionary,System.Object,System.String)
extern "C"  void LumosUtil_AddToDictionaryIfNonempty_m1393722542 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___dict0, RuntimeObject * ___key1, String_t* ___val2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LumosUtil_AddToDictionaryIfNonempty_m1393722542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___val2;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_1 = ___val2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		bool L_3 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		RuntimeObject* L_4 = ___dict0;
		RuntimeObject * L_5 = ___key1;
		String_t* L_6 = ___val2;
		NullCheck(L_4);
		InterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(1 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1363984059_il2cpp_TypeInfo_var, L_4, L_5, L_6);
	}

IL_001e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
