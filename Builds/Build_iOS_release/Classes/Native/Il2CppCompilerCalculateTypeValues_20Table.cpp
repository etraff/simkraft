﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<TestResultEntry>
struct List_1_t1918742919;
// System.String
struct String_t;
// UUnitAssertException
struct UUnitAssertException_t2913956255;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Void
struct Void_t1185182177;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef UUNITASSERT_T3903203386_H
#define UUNITASSERT_T3903203386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitAssert
struct  UUnitAssert_t3903203386  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITASSERT_T3903203386_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UUNITTESTCASE_T1875215041_H
#define UUNITTESTCASE_T1875215041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitTestCase
struct  UUnitTestCase_t1875215041  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITTESTCASE_T1875215041_H
#ifndef UUNITTESTRESULT_T2145825754_H
#define UUNITTESTRESULT_T2145825754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitTestResult
struct  UUnitTestResult_t2145825754  : public RuntimeObject
{
public:
	// System.Int32 UUnitTestResult::runCount
	int32_t ___runCount_0;
	// System.Int32 UUnitTestResult::failedCount
	int32_t ___failedCount_1;
	// System.Double UUnitTestResult::duration
	double ___duration_2;
	// System.Collections.Generic.List`1<TestResultEntry> UUnitTestResult::resultList
	List_1_t1918742919 * ___resultList_3;

public:
	inline static int32_t get_offset_of_runCount_0() { return static_cast<int32_t>(offsetof(UUnitTestResult_t2145825754, ___runCount_0)); }
	inline int32_t get_runCount_0() const { return ___runCount_0; }
	inline int32_t* get_address_of_runCount_0() { return &___runCount_0; }
	inline void set_runCount_0(int32_t value)
	{
		___runCount_0 = value;
	}

	inline static int32_t get_offset_of_failedCount_1() { return static_cast<int32_t>(offsetof(UUnitTestResult_t2145825754, ___failedCount_1)); }
	inline int32_t get_failedCount_1() const { return ___failedCount_1; }
	inline int32_t* get_address_of_failedCount_1() { return &___failedCount_1; }
	inline void set_failedCount_1(int32_t value)
	{
		___failedCount_1 = value;
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(UUnitTestResult_t2145825754, ___duration_2)); }
	inline double get_duration_2() const { return ___duration_2; }
	inline double* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(double value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_resultList_3() { return static_cast<int32_t>(offsetof(UUnitTestResult_t2145825754, ___resultList_3)); }
	inline List_1_t1918742919 * get_resultList_3() const { return ___resultList_3; }
	inline List_1_t1918742919 ** get_address_of_resultList_3() { return &___resultList_3; }
	inline void set_resultList_3(List_1_t1918742919 * value)
	{
		___resultList_3 = value;
		Il2CppCodeGenWriteBarrier((&___resultList_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITTESTRESULT_T2145825754_H
#ifndef TESTRESULTENTRY_T446668177_H
#define TESTRESULTENTRY_T446668177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestResultEntry
struct  TestResultEntry_t446668177  : public RuntimeObject
{
public:
	// System.String TestResultEntry::className
	String_t* ___className_0;
	// System.String TestResultEntry::methodName
	String_t* ___methodName_1;
	// System.Boolean TestResultEntry::success
	bool ___success_2;
	// UUnitAssertException TestResultEntry::exception
	UUnitAssertException_t2913956255 * ___exception_3;
	// System.Double TestResultEntry::duration
	double ___duration_4;
	// System.String TestResultEntry::errormsg
	String_t* ___errormsg_5;

public:
	inline static int32_t get_offset_of_className_0() { return static_cast<int32_t>(offsetof(TestResultEntry_t446668177, ___className_0)); }
	inline String_t* get_className_0() const { return ___className_0; }
	inline String_t** get_address_of_className_0() { return &___className_0; }
	inline void set_className_0(String_t* value)
	{
		___className_0 = value;
		Il2CppCodeGenWriteBarrier((&___className_0), value);
	}

	inline static int32_t get_offset_of_methodName_1() { return static_cast<int32_t>(offsetof(TestResultEntry_t446668177, ___methodName_1)); }
	inline String_t* get_methodName_1() const { return ___methodName_1; }
	inline String_t** get_address_of_methodName_1() { return &___methodName_1; }
	inline void set_methodName_1(String_t* value)
	{
		___methodName_1 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_1), value);
	}

	inline static int32_t get_offset_of_success_2() { return static_cast<int32_t>(offsetof(TestResultEntry_t446668177, ___success_2)); }
	inline bool get_success_2() const { return ___success_2; }
	inline bool* get_address_of_success_2() { return &___success_2; }
	inline void set_success_2(bool value)
	{
		___success_2 = value;
	}

	inline static int32_t get_offset_of_exception_3() { return static_cast<int32_t>(offsetof(TestResultEntry_t446668177, ___exception_3)); }
	inline UUnitAssertException_t2913956255 * get_exception_3() const { return ___exception_3; }
	inline UUnitAssertException_t2913956255 ** get_address_of_exception_3() { return &___exception_3; }
	inline void set_exception_3(UUnitAssertException_t2913956255 * value)
	{
		___exception_3 = value;
		Il2CppCodeGenWriteBarrier((&___exception_3), value);
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(TestResultEntry_t446668177, ___duration_4)); }
	inline double get_duration_4() const { return ___duration_4; }
	inline double* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(double value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_errormsg_5() { return static_cast<int32_t>(offsetof(TestResultEntry_t446668177, ___errormsg_5)); }
	inline String_t* get_errormsg_5() const { return ___errormsg_5; }
	inline String_t** get_address_of_errormsg_5() { return &___errormsg_5; }
	inline void set_errormsg_5(String_t* value)
	{
		___errormsg_5 = value;
		Il2CppCodeGenWriteBarrier((&___errormsg_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTRESULTENTRY_T446668177_H
#ifndef UUNITTESTSUITE_T1613695852_H
#define UUNITTESTSUITE_T1613695852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitTestSuite
struct  UUnitTestSuite_t1613695852  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Type> UUnitTestSuite::testCases
	List_1_t3956019502 * ___testCases_0;

public:
	inline static int32_t get_offset_of_testCases_0() { return static_cast<int32_t>(offsetof(UUnitTestSuite_t1613695852, ___testCases_0)); }
	inline List_1_t3956019502 * get_testCases_0() const { return ___testCases_0; }
	inline List_1_t3956019502 ** get_address_of_testCases_0() { return &___testCases_0; }
	inline void set_testCases_0(List_1_t3956019502 * value)
	{
		___testCases_0 = value;
		Il2CppCodeGenWriteBarrier((&___testCases_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITTESTSUITE_T1613695852_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef TESTMAPPING_T3370873485_H
#define TESTMAPPING_T3370873485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestMapping
struct  TestMapping_t3370873485  : public UUnitTestCase_t1875215041
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTMAPPING_T3370873485_H
#ifndef U24ARRAYTYPEU3D28_T173484549_H
#define U24ARRAYTYPEU3D28_T173484549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=28
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D28_t173484549 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D28_t173484549__padding[28];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D28_T173484549_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef U24ARRAYTYPEU3D48_T1336283963_H
#define U24ARRAYTYPEU3D48_T1336283963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D48_t1336283963 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D48_t1336283963__padding[48];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D48_T1336283963_H
#ifndef U24ARRAYTYPEU3D144_T322384175_H
#define U24ARRAYTYPEU3D144_T322384175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=144
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D144_t322384175 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D144_t322384175__padding[144];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D144_T322384175_H
#ifndef U24ARRAYTYPEU3D40_T2865632059_H
#define U24ARRAYTYPEU3D40_T2865632059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2865632059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2865632059__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2865632059_H
#ifndef U24ARRAYTYPEU3D200_T3449392900_H
#define U24ARRAYTYPEU3D200_T3449392900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=200
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D200_t3449392900 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D200_t3449392900__padding[200];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D200_T3449392900_H
#ifndef U24ARRAYTYPEU3D152_T2278699313_H
#define U24ARRAYTYPEU3D152_T2278699313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=152
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D152_t2278699313 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D152_t2278699313__padding[152];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D152_T2278699313_H
#ifndef U24ARRAYTYPEU3D256_T1875414782_H
#define U24ARRAYTYPEU3D256_T1875414782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=256
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D256_t1875414782 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D256_t1875414782__padding[256];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D256_T1875414782_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D512_T3839624093_H
#define U24ARRAYTYPEU3D512_T3839624093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=512
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D512_t3839624093 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D512_t3839624093__padding[512];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D512_T3839624093_H
#ifndef U24ARRAYTYPEU3D20_T1702832645_H
#define U24ARRAYTYPEU3D20_T1702832645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832645 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832645__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832645_H
#ifndef U24ARRAYTYPEU3D116_T1514025261_H
#define U24ARRAYTYPEU3D116_T1514025261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=116
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D116_t1514025261 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D116_t1514025261__padding[116];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D116_T1514025261_H
#ifndef UUNITASSERTEXCEPTION_T2913956255_H
#define UUNITASSERTEXCEPTION_T2913956255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitAssertException
struct  UUnitAssertException_t2913956255  : public Exception_t
{
public:
	// System.String UUnitAssertException::msg
	String_t* ___msg_11;
	// System.Object UUnitAssertException::<Actual>k__BackingField
	RuntimeObject * ___U3CActualU3Ek__BackingField_12;
	// System.Object UUnitAssertException::<Expected>k__BackingField
	RuntimeObject * ___U3CExpectedU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_msg_11() { return static_cast<int32_t>(offsetof(UUnitAssertException_t2913956255, ___msg_11)); }
	inline String_t* get_msg_11() const { return ___msg_11; }
	inline String_t** get_address_of_msg_11() { return &___msg_11; }
	inline void set_msg_11(String_t* value)
	{
		___msg_11 = value;
		Il2CppCodeGenWriteBarrier((&___msg_11), value);
	}

	inline static int32_t get_offset_of_U3CActualU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UUnitAssertException_t2913956255, ___U3CActualU3Ek__BackingField_12)); }
	inline RuntimeObject * get_U3CActualU3Ek__BackingField_12() const { return ___U3CActualU3Ek__BackingField_12; }
	inline RuntimeObject ** get_address_of_U3CActualU3Ek__BackingField_12() { return &___U3CActualU3Ek__BackingField_12; }
	inline void set_U3CActualU3Ek__BackingField_12(RuntimeObject * value)
	{
		___U3CActualU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActualU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CExpectedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(UUnitAssertException_t2913956255, ___U3CExpectedU3Ek__BackingField_13)); }
	inline RuntimeObject * get_U3CExpectedU3Ek__BackingField_13() const { return ___U3CExpectedU3Ek__BackingField_13; }
	inline RuntimeObject ** get_address_of_U3CExpectedU3Ek__BackingField_13() { return &___U3CExpectedU3Ek__BackingField_13; }
	inline void set_U3CExpectedU3Ek__BackingField_13(RuntimeObject * value)
	{
		___U3CExpectedU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpectedU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITASSERTEXCEPTION_T2913956255_H
#ifndef TESTVIB_T3441911441_H
#define TESTVIB_T3441911441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestVIB
struct  TestVIB_t3441911441  : public UUnitTestCase_t1875215041
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTVIB_T3441911441_H
#ifndef TESTMASS_T1770853579_H
#define TESTMASS_T1770853579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestMass
struct  TestMass_t1770853579  : public UUnitTestCase_t1875215041
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTMASS_T1770853579_H
#ifndef U24ARRAYTYPEU3D76_T2446559190_H
#define U24ARRAYTYPEU3D76_T2446559190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=76
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D76_t2446559190 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D76_t2446559190__padding[76];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D76_T2446559190_H
#ifndef U24ARRAYTYPEU3D68_T2499083377_H
#define U24ARRAYTYPEU3D68_T2499083377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=68
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D68_t2499083377 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D68_t2499083377__padding[68];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D68_T2499083377_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU3D384_T3486128741_H
#define U24ARRAYTYPEU3D384_T3486128741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=384
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D384_t3486128741 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D384_t3486128741__padding[384];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D384_T3486128741_H
#ifndef U24ARRAYTYPEU3D124_T4235014447_H
#define U24ARRAYTYPEU3D124_T4235014447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=124
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D124_t4235014447 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D124_t4235014447__padding[124];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D124_T4235014447_H
#ifndef U24ARRAYTYPEU3D120_T4235014451_H
#define U24ARRAYTYPEU3D120_T4235014451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=120
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D120_t4235014451 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D120_t4235014451__padding[120];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D120_T4235014451_H
#ifndef U24ARRAYTYPEU3D1152_T1514942768_H
#define U24ARRAYTYPEU3D1152_T1514942768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=1152
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D1152_t1514942768 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D1152_t1514942768__padding[1152];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D1152_T1514942768_H
#ifndef U24ARRAYTYPEU3D6144_T3437650284_H
#define U24ARRAYTYPEU3D6144_T3437650284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=6144
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D6144_t3437650284 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D6144_t3437650284__padding[6144];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D6144_T3437650284_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=76 <PrivateImplementationDetails>::$field-1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38
	U24ArrayTypeU3D76_t2446559190  ___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0;
	// <PrivateImplementationDetails>/$ArrayType=68 <PrivateImplementationDetails>::$field-F584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF
	U24ArrayTypeU3D68_t2499083377  ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1;
	// <PrivateImplementationDetails>/$ArrayType=6144 <PrivateImplementationDetails>::$field-A474A0BEC4E2CE8491839502AE85F6EA8504C6BD
	U24ArrayTypeU3D6144_t3437650284  ___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2;
	// <PrivateImplementationDetails>/$ArrayType=384 <PrivateImplementationDetails>::$field-1B180C6E41F096D53222F5E8EF558B78182CA401
	U24ArrayTypeU3D384_t3486128741  ___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3;
	// <PrivateImplementationDetails>/$ArrayType=124 <PrivateImplementationDetails>::$field-8ED8F61DAA454B49CD5059AE4486C59174324E9E
	U24ArrayTypeU3D124_t4235014447  ___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4;
	// <PrivateImplementationDetails>/$ArrayType=124 <PrivateImplementationDetails>::$field-DACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A
	U24ArrayTypeU3D124_t4235014447  ___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5;
	// <PrivateImplementationDetails>/$ArrayType=120 <PrivateImplementationDetails>::$field-D068832E6B13A623916709C1E0E25ADCBE7B455F
	U24ArrayTypeU3D120_t4235014451  ___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6;
	// <PrivateImplementationDetails>/$ArrayType=120 <PrivateImplementationDetails>::$field-79D521E6E3E55103005E9CC3FA43B3174FAF090F
	U24ArrayTypeU3D120_t4235014451  ___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7;
	// <PrivateImplementationDetails>/$ArrayType=1152 <PrivateImplementationDetails>::$field-EB6F545AEF284339D25594F900E7A395212460EB
	U24ArrayTypeU3D1152_t1514942768  ___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8;
	// <PrivateImplementationDetails>/$ArrayType=120 <PrivateImplementationDetails>::$field-850D4DC092689E1F0D8A70B6281848B27DEC0014
	U24ArrayTypeU3D120_t4235014451  ___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9;
	// <PrivateImplementationDetails>/$ArrayType=116 <PrivateImplementationDetails>::$field-67C0E784F3654B008A81E2988588CF4956CCF3DA
	U24ArrayTypeU3D116_t1514025261  ___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10;
	// <PrivateImplementationDetails>/$ArrayType=76 <PrivateImplementationDetails>::$field-8457F44B035C9073EE2D1F132D0A8AF5631DCDC8
	U24ArrayTypeU3D76_t2446559190  ___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11;
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-AE6B2897A8B88E297D61124152931A88D5D977F4
	U24ArrayTypeU3D20_t1702832645  ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12;
	// <PrivateImplementationDetails>/$ArrayType=512 <PrivateImplementationDetails>::$field-3544182260B8A15D332367E48C7530FC0E901FD3
	U24ArrayTypeU3D512_t3839624093  ___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13;
	// <PrivateImplementationDetails>/$ArrayType=256 <PrivateImplementationDetails>::$field-6A316789EED01119DE92841832701A40AB0CABD6
	U24ArrayTypeU3D256_t1875414782  ___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14;
	// <PrivateImplementationDetails>/$ArrayType=116 <PrivateImplementationDetails>::$field-2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71
	U24ArrayTypeU3D116_t1514025261  ___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15;
	// <PrivateImplementationDetails>/$ArrayType=120 <PrivateImplementationDetails>::$field-5581A70566F03554D8048EDBFC6E6B399AF9BCB1
	U24ArrayTypeU3D120_t4235014451  ___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16;
	// <PrivateImplementationDetails>/$ArrayType=256 <PrivateImplementationDetails>::$field-DA6038AFCD18C1290D6F9A5C53F32DCE0913B738
	U24ArrayTypeU3D256_t1875414782  ___U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17;
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-ECE11D613EE4FE828CF9DA2C98E334A98F7D1102
	U24ArrayTypeU3D28_t173484549  ___U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-6A81DB1271804755E228BC6D2BDC4E6A43999834
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-A5E6593800B54C15B7F4AA1A5C583DF8410628C3
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-526058E8BA02E5CAD80CAD49057E8447F2ECC702
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-447845D5FDF33EEDEC23291AD12BF79EE2599025
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-A140BF37B1B6A26E996EC54E360961B37D80EFA6
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-BC0594FF642AA85BD7E365EAB35B0987AA64FE9F
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-1B287FBF8016A1357E0FA5B7B88403B0659ECE97
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-ED140CC99917675EF54A43104FC4D795714C50B8
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-274707B9D7F03DF7BFE3B6790988CC13D6031A87
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-F9D6976D366C3588627280E9AC00F10FC10C4015
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-9A998C9F29B19029A9636C07CF65240DCFABD066
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-110042DAF87061C2E74CF32CE3AC95BD238C4D0A
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32;
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-993A0EDC421AF6DA70DAA7FF54F4BA4B76931945
	U24ArrayTypeU3D28_t173484549  ___U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33;
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-D5A71C186567FAF78762EC3FF826C078F220B2F4
	U24ArrayTypeU3D28_t173484549  ___U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34;
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9
	U24ArrayTypeU3D28_t173484549  ___U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-96AD768CFC44CFDB16F058637F75C80B6C1022ED
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-1CD039265231F6FF03056CFC9E1DC925C5CE2E3F
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-BED6BA6A513A0C30453CF6758F396CC811182402
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-9D4897EF5B23034D453BB68535026A5A77F515ED
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-2B80BA08BBC772966610D701841FD42E3D435204
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-A449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42;
	// <PrivateImplementationDetails>/$ArrayType=48 <PrivateImplementationDetails>::$field-28451B5309E7CC7A64464EBA40E2E9CABD9373EB
	U24ArrayTypeU3D48_t1336283963  ___U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-E1B9047C2299BE4AB8DD7384DD4E41268C50FE2A
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44;
	// <PrivateImplementationDetails>/$ArrayType=200 <PrivateImplementationDetails>::$field-AA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4
	U24ArrayTypeU3D200_t3449392900  ___U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45;
	// <PrivateImplementationDetails>/$ArrayType=200 <PrivateImplementationDetails>::$field-A7423A7135C5F799B43293F20F3ECDAB29AC315F
	U24ArrayTypeU3D200_t3449392900  ___U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46;
	// <PrivateImplementationDetails>/$ArrayType=152 <PrivateImplementationDetails>::$field-3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97
	U24ArrayTypeU3D152_t2278699313  ___U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47;
	// <PrivateImplementationDetails>/$ArrayType=144 <PrivateImplementationDetails>::$field-C6184C6B6E096DF28ED6DAE47D121887AEC39604
	U24ArrayTypeU3D144_t322384175  ___U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48;
	// <PrivateImplementationDetails>/$ArrayType=144 <PrivateImplementationDetails>::$field-51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE
	U24ArrayTypeU3D144_t322384175  ___U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-470FAB5E9F1298296E2E4806513B7B01FF823D36
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50;

public:
	inline static int32_t get_offset_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0)); }
	inline U24ArrayTypeU3D76_t2446559190  get_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0() const { return ___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0; }
	inline U24ArrayTypeU3D76_t2446559190 * get_address_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0() { return &___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0; }
	inline void set_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0(U24ArrayTypeU3D76_t2446559190  value)
	{
		___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1)); }
	inline U24ArrayTypeU3D68_t2499083377  get_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1() const { return ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1; }
	inline U24ArrayTypeU3D68_t2499083377 * get_address_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1() { return &___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1; }
	inline void set_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1(U24ArrayTypeU3D68_t2499083377  value)
	{
		___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2)); }
	inline U24ArrayTypeU3D6144_t3437650284  get_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2() const { return ___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2; }
	inline U24ArrayTypeU3D6144_t3437650284 * get_address_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2() { return &___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2; }
	inline void set_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2(U24ArrayTypeU3D6144_t3437650284  value)
	{
		___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3)); }
	inline U24ArrayTypeU3D384_t3486128741  get_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3() const { return ___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3; }
	inline U24ArrayTypeU3D384_t3486128741 * get_address_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3() { return &___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3; }
	inline void set_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3(U24ArrayTypeU3D384_t3486128741  value)
	{
		___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4)); }
	inline U24ArrayTypeU3D124_t4235014447  get_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4() const { return ___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4; }
	inline U24ArrayTypeU3D124_t4235014447 * get_address_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4() { return &___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4; }
	inline void set_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4(U24ArrayTypeU3D124_t4235014447  value)
	{
		___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5)); }
	inline U24ArrayTypeU3D124_t4235014447  get_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5() const { return ___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5; }
	inline U24ArrayTypeU3D124_t4235014447 * get_address_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5() { return &___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5; }
	inline void set_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5(U24ArrayTypeU3D124_t4235014447  value)
	{
		___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6)); }
	inline U24ArrayTypeU3D120_t4235014451  get_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6() const { return ___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6; }
	inline U24ArrayTypeU3D120_t4235014451 * get_address_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6() { return &___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6; }
	inline void set_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6(U24ArrayTypeU3D120_t4235014451  value)
	{
		___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7)); }
	inline U24ArrayTypeU3D120_t4235014451  get_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7() const { return ___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7; }
	inline U24ArrayTypeU3D120_t4235014451 * get_address_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7() { return &___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7; }
	inline void set_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7(U24ArrayTypeU3D120_t4235014451  value)
	{
		___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8)); }
	inline U24ArrayTypeU3D1152_t1514942768  get_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8() const { return ___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8; }
	inline U24ArrayTypeU3D1152_t1514942768 * get_address_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8() { return &___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8; }
	inline void set_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8(U24ArrayTypeU3D1152_t1514942768  value)
	{
		___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9)); }
	inline U24ArrayTypeU3D120_t4235014451  get_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9() const { return ___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9; }
	inline U24ArrayTypeU3D120_t4235014451 * get_address_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9() { return &___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9; }
	inline void set_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9(U24ArrayTypeU3D120_t4235014451  value)
	{
		___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10)); }
	inline U24ArrayTypeU3D116_t1514025261  get_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10() const { return ___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10; }
	inline U24ArrayTypeU3D116_t1514025261 * get_address_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10() { return &___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10; }
	inline void set_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10(U24ArrayTypeU3D116_t1514025261  value)
	{
		___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11)); }
	inline U24ArrayTypeU3D76_t2446559190  get_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11() const { return ___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11; }
	inline U24ArrayTypeU3D76_t2446559190 * get_address_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11() { return &___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11; }
	inline void set_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11(U24ArrayTypeU3D76_t2446559190  value)
	{
		___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12)); }
	inline U24ArrayTypeU3D20_t1702832645  get_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12() const { return ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12; }
	inline U24ArrayTypeU3D20_t1702832645 * get_address_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12() { return &___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12; }
	inline void set_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12(U24ArrayTypeU3D20_t1702832645  value)
	{
		___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13)); }
	inline U24ArrayTypeU3D512_t3839624093  get_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13() const { return ___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13; }
	inline U24ArrayTypeU3D512_t3839624093 * get_address_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13() { return &___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13; }
	inline void set_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13(U24ArrayTypeU3D512_t3839624093  value)
	{
		___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14)); }
	inline U24ArrayTypeU3D256_t1875414782  get_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14() const { return ___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14; }
	inline U24ArrayTypeU3D256_t1875414782 * get_address_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14() { return &___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14; }
	inline void set_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14(U24ArrayTypeU3D256_t1875414782  value)
	{
		___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15)); }
	inline U24ArrayTypeU3D116_t1514025261  get_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15() const { return ___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15; }
	inline U24ArrayTypeU3D116_t1514025261 * get_address_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15() { return &___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15; }
	inline void set_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15(U24ArrayTypeU3D116_t1514025261  value)
	{
		___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16)); }
	inline U24ArrayTypeU3D120_t4235014451  get_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16() const { return ___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16; }
	inline U24ArrayTypeU3D120_t4235014451 * get_address_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16() { return &___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16; }
	inline void set_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16(U24ArrayTypeU3D120_t4235014451  value)
	{
		___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17)); }
	inline U24ArrayTypeU3D256_t1875414782  get_U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17() const { return ___U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17; }
	inline U24ArrayTypeU3D256_t1875414782 * get_address_of_U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17() { return &___U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17; }
	inline void set_U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17(U24ArrayTypeU3D256_t1875414782  value)
	{
		___U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18)); }
	inline U24ArrayTypeU3D28_t173484549  get_U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18() const { return ___U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18; }
	inline U24ArrayTypeU3D28_t173484549 * get_address_of_U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18() { return &___U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18; }
	inline void set_U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18(U24ArrayTypeU3D28_t173484549  value)
	{
		___U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19() const { return ___U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19() { return &___U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19; }
	inline void set_U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20() const { return ___U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20() { return &___U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20; }
	inline void set_U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21() const { return ___U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21() { return &___U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21; }
	inline void set_U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22() const { return ___U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22() { return &___U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22; }
	inline void set_U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23() const { return ___U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23() { return &___U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23; }
	inline void set_U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24() const { return ___U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24() { return &___U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24; }
	inline void set_U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25() const { return ___U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25() { return &___U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25; }
	inline void set_U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26() const { return ___U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26() { return &___U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26; }
	inline void set_U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27() const { return ___U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27() { return &___U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27; }
	inline void set_U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28() const { return ___U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28() { return &___U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28; }
	inline void set_U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29() const { return ___U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29() { return &___U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29; }
	inline void set_U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30() const { return ___U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30() { return &___U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30; }
	inline void set_U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31() const { return ___U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31() { return &___U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31; }
	inline void set_U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32() const { return ___U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32() { return &___U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32; }
	inline void set_U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33)); }
	inline U24ArrayTypeU3D28_t173484549  get_U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33() const { return ___U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33; }
	inline U24ArrayTypeU3D28_t173484549 * get_address_of_U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33() { return &___U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33; }
	inline void set_U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33(U24ArrayTypeU3D28_t173484549  value)
	{
		___U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34)); }
	inline U24ArrayTypeU3D28_t173484549  get_U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34() const { return ___U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34; }
	inline U24ArrayTypeU3D28_t173484549 * get_address_of_U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34() { return &___U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34; }
	inline void set_U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34(U24ArrayTypeU3D28_t173484549  value)
	{
		___U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35)); }
	inline U24ArrayTypeU3D28_t173484549  get_U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35() const { return ___U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35; }
	inline U24ArrayTypeU3D28_t173484549 * get_address_of_U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35() { return &___U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35; }
	inline void set_U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35(U24ArrayTypeU3D28_t173484549  value)
	{
		___U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36() const { return ___U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36() { return &___U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36; }
	inline void set_U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37() const { return ___U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37() { return &___U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37; }
	inline void set_U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38() const { return ___U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38() { return &___U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38; }
	inline void set_U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39() const { return ___U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39() { return &___U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39; }
	inline void set_U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40() const { return ___U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40() { return &___U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40; }
	inline void set_U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41() const { return ___U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41() { return &___U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41; }
	inline void set_U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42() const { return ___U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42() { return &___U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42; }
	inline void set_U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43)); }
	inline U24ArrayTypeU3D48_t1336283963  get_U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43() const { return ___U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43; }
	inline U24ArrayTypeU3D48_t1336283963 * get_address_of_U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43() { return &___U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43; }
	inline void set_U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43(U24ArrayTypeU3D48_t1336283963  value)
	{
		___U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44() const { return ___U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44() { return &___U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44; }
	inline void set_U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45)); }
	inline U24ArrayTypeU3D200_t3449392900  get_U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45() const { return ___U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45; }
	inline U24ArrayTypeU3D200_t3449392900 * get_address_of_U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45() { return &___U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45; }
	inline void set_U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45(U24ArrayTypeU3D200_t3449392900  value)
	{
		___U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46)); }
	inline U24ArrayTypeU3D200_t3449392900  get_U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46() const { return ___U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46; }
	inline U24ArrayTypeU3D200_t3449392900 * get_address_of_U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46() { return &___U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46; }
	inline void set_U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46(U24ArrayTypeU3D200_t3449392900  value)
	{
		___U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47)); }
	inline U24ArrayTypeU3D152_t2278699313  get_U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47() const { return ___U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47; }
	inline U24ArrayTypeU3D152_t2278699313 * get_address_of_U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47() { return &___U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47; }
	inline void set_U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47(U24ArrayTypeU3D152_t2278699313  value)
	{
		___U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48)); }
	inline U24ArrayTypeU3D144_t322384175  get_U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48() const { return ___U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48; }
	inline U24ArrayTypeU3D144_t322384175 * get_address_of_U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48() { return &___U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48; }
	inline void set_U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48(U24ArrayTypeU3D144_t322384175  value)
	{
		___U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49)); }
	inline U24ArrayTypeU3D144_t322384175  get_U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49() const { return ___U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49; }
	inline U24ArrayTypeU3D144_t322384175 * get_address_of_U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49() { return &___U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49; }
	inline void set_U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49(U24ArrayTypeU3D144_t322384175  value)
	{
		___U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50() const { return ___U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50() { return &___U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50; }
	inline void set_U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef WORKINGANIMATION_T2928320557_H
#define WORKINGANIMATION_T2928320557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorkingAnimation
struct  WorkingAnimation_t2928320557  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKINGANIMATION_T2928320557_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (TestMapping_t3370873485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (UUnitAssert_t3903203386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (UUnitAssertException_t2913956255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[3] = 
{
	UUnitAssertException_t2913956255::get_offset_of_msg_11(),
	UUnitAssertException_t2913956255::get_offset_of_U3CActualU3Ek__BackingField_12(),
	UUnitAssertException_t2913956255::get_offset_of_U3CExpectedU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (UUnitTestCase_t1875215041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (UUnitTestResult_t2145825754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[4] = 
{
	UUnitTestResult_t2145825754::get_offset_of_runCount_0(),
	UUnitTestResult_t2145825754::get_offset_of_failedCount_1(),
	UUnitTestResult_t2145825754::get_offset_of_duration_2(),
	UUnitTestResult_t2145825754::get_offset_of_resultList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (TestResultEntry_t446668177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[6] = 
{
	TestResultEntry_t446668177::get_offset_of_className_0(),
	TestResultEntry_t446668177::get_offset_of_methodName_1(),
	TestResultEntry_t446668177::get_offset_of_success_2(),
	TestResultEntry_t446668177::get_offset_of_exception_3(),
	TestResultEntry_t446668177::get_offset_of_duration_4(),
	TestResultEntry_t446668177::get_offset_of_errormsg_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (UUnitTestSuite_t1613695852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[1] = 
{
	UUnitTestSuite_t1613695852::get_offset_of_testCases_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (TestVIB_t3441911441), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (TestMass_t1770853579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (WorkingAnimation_t2928320557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2010[51] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_1(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_2(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_3(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_4(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_5(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_6(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_7(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_8(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_9(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_10(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_11(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_12(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_13(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_14(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_15(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_16(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DDA6038AFCD18C1290D6F9A5C53F32DCE0913B738_17(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DECE11D613EE4FE828CF9DA2C98E334A98F7D1102_18(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D6A81DB1271804755E228BC6D2BDC4E6A43999834_19(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DA5E6593800B54C15B7F4AA1A5C583DF8410628C3_20(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D526058E8BA02E5CAD80CAD49057E8447F2ECC702_21(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D447845D5FDF33EEDEC23291AD12BF79EE2599025_22(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DA140BF37B1B6A26E996EC54E360961B37D80EFA6_23(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D3141BEA4580DDFAC3E137B7FA9CCBA7A88304A24_24(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DBC0594FF642AA85BD7E365EAB35B0987AA64FE9F_25(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D1B287FBF8016A1357E0FA5B7B88403B0659ECE97_26(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DED140CC99917675EF54A43104FC4D795714C50B8_27(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D5BAA3A1BE4E6D56160AA961C0DA63C0DE7EDE5D7_28(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D274707B9D7F03DF7BFE3B6790988CC13D6031A87_29(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DF9D6976D366C3588627280E9AC00F10FC10C4015_30(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D9A998C9F29B19029A9636C07CF65240DCFABD066_31(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D110042DAF87061C2E74CF32CE3AC95BD238C4D0A_32(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D993A0EDC421AF6DA70DAA7FF54F4BA4B76931945_33(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DD5A71C186567FAF78762EC3FF826C078F220B2F4_34(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D5CB5276D28F714364ACB9BDDF2EE29B6A2DA1FA9_35(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D96AD768CFC44CFDB16F058637F75C80B6C1022ED_36(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D1CD039265231F6FF03056CFC9E1DC925C5CE2E3F_37(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DBED6BA6A513A0C30453CF6758F396CC811182402_38(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D9D4897EF5B23034D453BB68535026A5A77F515ED_39(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D2B80BA08BBC772966610D701841FD42E3D435204_40(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D2EA4FD6BD63DAE4C5979C4D0B517AC9ED2E90786_41(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DA449440BBF3BDD437C1B3DA13A3CA6120C7F5EA3_42(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D28451B5309E7CC7A64464EBA40E2E9CABD9373EB_43(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DE1B9047C2299BE4AB8DD7384DD4E41268C50FE2A_44(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DAA56E23BFEE8EF5B86EEBD365BC5CFF05B7DB0F4_45(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DA7423A7135C5F799B43293F20F3ECDAB29AC315F_46(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D3FDA267AF2D42DEF16AD23DB56CCAFE426D15F97_47(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DC6184C6B6E096DF28ED6DAE47D121887AEC39604_48(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D51E40E4B8507675BCCA11B4EE7ACB5F24E57ADCE_49(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D470FAB5E9F1298296E2E4806513B7B01FF823D36_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (U24ArrayTypeU3D76_t2446559190)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D76_t2446559190 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U24ArrayTypeU3D68_t2499083377)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D68_t2499083377 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (U24ArrayTypeU3D6144_t3437650284)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D6144_t3437650284 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (U24ArrayTypeU3D384_t3486128741)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D384_t3486128741 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (U24ArrayTypeU3D124_t4235014447)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D124_t4235014447 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (U24ArrayTypeU3D120_t4235014451)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D120_t4235014451 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (U24ArrayTypeU3D1152_t1514942768)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D1152_t1514942768 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (U24ArrayTypeU3D116_t1514025261)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D116_t1514025261 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U24ArrayTypeU3D20_t1702832645)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t1702832645 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U24ArrayTypeU3D512_t3839624093)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D512_t3839624093 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U24ArrayTypeU3D256_t1875414782)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D256_t1875414782 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (U24ArrayTypeU3D28_t173484549)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D28_t173484549 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U24ArrayTypeU3D48_t1336283963)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D48_t1336283963 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (U24ArrayTypeU3D40_t2865632059)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2865632059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (U24ArrayTypeU3D200_t3449392900)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D200_t3449392900 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (U24ArrayTypeU3D152_t2278699313)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D152_t2278699313 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U24ArrayTypeU3D144_t322384175)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D144_t322384175 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
