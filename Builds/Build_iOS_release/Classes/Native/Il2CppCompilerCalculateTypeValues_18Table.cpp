﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Uri
struct Uri_t100236324;
// HTTP.Response
struct Response_t2509227019;
// System.Exception
struct Exception_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t3104781730;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3653667923;
// System.Int16[]
struct Int16U5BU5D_t3686840178;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// System.SByte[]
struct SByteU5BU5D_t2651576203;
// Ionic.Zlib.StaticTree
struct StaticTree_t2118636970;
// HTTP.Request
struct Request_t1151508915;
// NuGAEvent
struct NuGAEvent_t4041358949;
// NuGATransaction
struct NuGATransaction_t3923616719;
// NuTracking
struct NuTracking_t472478902;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Dictionary_2_t2650618762;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String>
struct Dictionary_2_t1123770287;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// Ionic.Zlib.ZlibBaseStream
struct ZlibBaseStream_t1591821133;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.Stream
struct Stream_t1273022909;
// Ionic.Zlib.CRC32
struct CRC32_t1348930856;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// LumosFeedbackGUI/CloseHandler
struct CloseHandler_t719506259;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t3146511083;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.String[]
struct StringU5BU5D_t1281789340;
// Ionic.Zlib.DeflateManager
struct DeflateManager_t740995428;
// Ionic.Zlib.InflateManager
struct InflateManager_t3407211616;
// Ionic.Zlib.InflateCodes
struct InflateCodes_t3874557824;
// Ionic.Zlib.InfTree
struct InfTree_t3075871205;
// Ionic.Zlib.DeflateManager/CompressFunc
struct CompressFunc_t3594827279;
// Ionic.Zlib.DeflateManager/Config
struct Config_t1905746077;
// Ionic.Zlib.Tree
struct Tree_t526993154;
// Ionic.Zlib.DeflateManager/Config[]
struct ConfigU5BU5D_t2096431184;
// Ionic.Zlib.InflateBlocks
struct InflateBlocks_t175244798;
// System.Collections.Generic.List`1<Ionic.Zlib.WorkItem>
struct List_1_t72680944;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Random
struct Random_t108471755;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// System.Text.Encoding
struct Encoding_t1523322056;
// NuGAService
struct NuGAService_t1586892016;
// System.Collections.Generic.List`1<NuTracking/TrackingRequest>
struct List_1_t4066654888;
// System.Action
struct Action_t1264377477;
// UnityEngine.Application/LogCallback
struct LogCallback_t3588208630;
// LumosCredentials
struct LumosCredentials_t1718792749;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;




#ifndef U3CMODULEU3E_T692745545_H
#define U3CMODULEU3E_T692745545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745545 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745545_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef LUMOSREQUEST_T457072260_H
#define LUMOSREQUEST_T457072260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosRequest
struct  LumosRequest_t457072260  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSREQUEST_T457072260_H
#ifndef U3CSENDCOROUTINEU3EC__ITERATOR0_T597473103_H
#define U3CSENDCOROUTINEU3EC__ITERATOR0_T597473103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosRequest/<SendCoroutine>c__Iterator0
struct  U3CSendCoroutineU3Ec__Iterator0_t597473103  : public RuntimeObject
{
public:
	// System.Action`1<System.Object> LumosRequest/<SendCoroutine>c__Iterator0::errorCallback
	Action_1_t3252573759 * ___errorCallback_0;
	// System.Object LumosRequest/<SendCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean LumosRequest/<SendCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 LumosRequest/<SendCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_errorCallback_0() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___errorCallback_0)); }
	inline Action_1_t3252573759 * get_errorCallback_0() const { return ___errorCallback_0; }
	inline Action_1_t3252573759 ** get_address_of_errorCallback_0() { return &___errorCallback_0; }
	inline void set_errorCallback_0(Action_1_t3252573759 * value)
	{
		___errorCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___errorCallback_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__Iterator0_t597473103, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDCOROUTINEU3EC__ITERATOR0_T597473103_H
#ifndef U3CLOADIMAGECOROUTINEU3EC__ITERATOR1_T3113194463_H
#define U3CLOADIMAGECOROUTINEU3EC__ITERATOR1_T3113194463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosRequest/<LoadImageCoroutine>c__Iterator1
struct  U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463  : public RuntimeObject
{
public:
	// System.String LumosRequest/<LoadImageCoroutine>c__Iterator1::imageLocation
	String_t* ___imageLocation_0;
	// UnityEngine.WWW LumosRequest/<LoadImageCoroutine>c__Iterator1::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// UnityEngine.Texture2D LumosRequest/<LoadImageCoroutine>c__Iterator1::texture
	Texture2D_t3840446185 * ___texture_2;
	// System.Object LumosRequest/<LoadImageCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LumosRequest/<LoadImageCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 LumosRequest/<LoadImageCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_imageLocation_0() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___imageLocation_0)); }
	inline String_t* get_imageLocation_0() const { return ___imageLocation_0; }
	inline String_t** get_address_of_imageLocation_0() { return &___imageLocation_0; }
	inline void set_imageLocation_0(String_t* value)
	{
		___imageLocation_0 = value;
		Il2CppCodeGenWriteBarrier((&___imageLocation_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___texture_2)); }
	inline Texture2D_t3840446185 * get_texture_2() const { return ___texture_2; }
	inline Texture2D_t3840446185 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(Texture2D_t3840446185 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGECOROUTINEU3EC__ITERATOR1_T3113194463_H
#ifndef LUMOSUTIL_T3964483066_H
#define LUMOSUTIL_T3964483066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosUtil
struct  LumosUtil_t3964483066  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSUTIL_T3964483066_H
#ifndef REQUEST_T1151508915_H
#define REQUEST_T1151508915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HTTP.Request
struct  Request_t1151508915  : public RuntimeObject
{
public:
	// System.String HTTP.Request::method
	String_t* ___method_0;
	// System.String HTTP.Request::protocol
	String_t* ___protocol_1;
	// System.Byte[] HTTP.Request::bytes
	ByteU5BU5D_t4116647657* ___bytes_2;
	// System.Uri HTTP.Request::uri
	Uri_t100236324 * ___uri_3;
	// HTTP.Response HTTP.Request::response
	Response_t2509227019 * ___response_5;
	// System.Int32 HTTP.Request::maximumRetryCount
	int32_t ___maximumRetryCount_6;
	// System.Boolean HTTP.Request::acceptGzip
	bool ___acceptGzip_7;
	// System.Exception HTTP.Request::exception
	Exception_t * ___exception_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> HTTP.Request::headers
	Dictionary_2_t3104781730 * ___headers_9;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}

	inline static int32_t get_offset_of_protocol_1() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___protocol_1)); }
	inline String_t* get_protocol_1() const { return ___protocol_1; }
	inline String_t** get_address_of_protocol_1() { return &___protocol_1; }
	inline void set_protocol_1(String_t* value)
	{
		___protocol_1 = value;
		Il2CppCodeGenWriteBarrier((&___protocol_1), value);
	}

	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___bytes_2)); }
	inline ByteU5BU5D_t4116647657* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_t4116647657* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_2), value);
	}

	inline static int32_t get_offset_of_uri_3() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___uri_3)); }
	inline Uri_t100236324 * get_uri_3() const { return ___uri_3; }
	inline Uri_t100236324 ** get_address_of_uri_3() { return &___uri_3; }
	inline void set_uri_3(Uri_t100236324 * value)
	{
		___uri_3 = value;
		Il2CppCodeGenWriteBarrier((&___uri_3), value);
	}

	inline static int32_t get_offset_of_response_5() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___response_5)); }
	inline Response_t2509227019 * get_response_5() const { return ___response_5; }
	inline Response_t2509227019 ** get_address_of_response_5() { return &___response_5; }
	inline void set_response_5(Response_t2509227019 * value)
	{
		___response_5 = value;
		Il2CppCodeGenWriteBarrier((&___response_5), value);
	}

	inline static int32_t get_offset_of_maximumRetryCount_6() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___maximumRetryCount_6)); }
	inline int32_t get_maximumRetryCount_6() const { return ___maximumRetryCount_6; }
	inline int32_t* get_address_of_maximumRetryCount_6() { return &___maximumRetryCount_6; }
	inline void set_maximumRetryCount_6(int32_t value)
	{
		___maximumRetryCount_6 = value;
	}

	inline static int32_t get_offset_of_acceptGzip_7() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___acceptGzip_7)); }
	inline bool get_acceptGzip_7() const { return ___acceptGzip_7; }
	inline bool* get_address_of_acceptGzip_7() { return &___acceptGzip_7; }
	inline void set_acceptGzip_7(bool value)
	{
		___acceptGzip_7 = value;
	}

	inline static int32_t get_offset_of_exception_8() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___exception_8)); }
	inline Exception_t * get_exception_8() const { return ___exception_8; }
	inline Exception_t ** get_address_of_exception_8() { return &___exception_8; }
	inline void set_exception_8(Exception_t * value)
	{
		___exception_8 = value;
		Il2CppCodeGenWriteBarrier((&___exception_8), value);
	}

	inline static int32_t get_offset_of_headers_9() { return static_cast<int32_t>(offsetof(Request_t1151508915, ___headers_9)); }
	inline Dictionary_2_t3104781730 * get_headers_9() const { return ___headers_9; }
	inline Dictionary_2_t3104781730 ** get_address_of_headers_9() { return &___headers_9; }
	inline void set_headers_9(Dictionary_2_t3104781730 * value)
	{
		___headers_9 = value;
		Il2CppCodeGenWriteBarrier((&___headers_9), value);
	}
};

struct Request_t1151508915_StaticFields
{
public:
	// System.Byte[] HTTP.Request::EOL
	ByteU5BU5D_t4116647657* ___EOL_4;

public:
	inline static int32_t get_offset_of_EOL_4() { return static_cast<int32_t>(offsetof(Request_t1151508915_StaticFields, ___EOL_4)); }
	inline ByteU5BU5D_t4116647657* get_EOL_4() const { return ___EOL_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_EOL_4() { return &___EOL_4; }
	inline void set_EOL_4(ByteU5BU5D_t4116647657* value)
	{
		___EOL_4 = value;
		Il2CppCodeGenWriteBarrier((&___EOL_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUEST_T1151508915_H
#ifndef RESPONSE_T2509227019_H
#define RESPONSE_T2509227019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HTTP.Response
struct  Response_t2509227019  : public RuntimeObject
{
public:
	// System.Int32 HTTP.Response::status
	int32_t ___status_0;
	// System.String HTTP.Response::message
	String_t* ___message_1;
	// System.Byte[] HTTP.Response::bytes
	ByteU5BU5D_t4116647657* ___bytes_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> HTTP.Response::headers
	Dictionary_2_t3104781730 * ___headers_3;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(Response_t2509227019, ___status_0)); }
	inline int32_t get_status_0() const { return ___status_0; }
	inline int32_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int32_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(Response_t2509227019, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}

	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(Response_t2509227019, ___bytes_2)); }
	inline ByteU5BU5D_t4116647657* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_t4116647657* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_2), value);
	}

	inline static int32_t get_offset_of_headers_3() { return static_cast<int32_t>(offsetof(Response_t2509227019, ___headers_3)); }
	inline Dictionary_2_t3104781730 * get_headers_3() const { return ___headers_3; }
	inline Dictionary_2_t3104781730 ** get_address_of_headers_3() { return &___headers_3; }
	inline void set_headers_3(Dictionary_2_t3104781730 * value)
	{
		___headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___headers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSE_T2509227019_H
#ifndef CRC32_T1348930856_H
#define CRC32_T1348930856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.CRC32
struct  CRC32_t1348930856  : public RuntimeObject
{
public:
	static const Il2CppGuid CLSID;

public:
	// System.Int64 Ionic.Zlib.CRC32::_TotalBytesRead
	int64_t ____TotalBytesRead_0;
	// System.UInt32 Ionic.Zlib.CRC32::_RunningCrc32Result
	uint32_t ____RunningCrc32Result_3;

public:
	inline static int32_t get_offset_of__TotalBytesRead_0() { return static_cast<int32_t>(offsetof(CRC32_t1348930856, ____TotalBytesRead_0)); }
	inline int64_t get__TotalBytesRead_0() const { return ____TotalBytesRead_0; }
	inline int64_t* get_address_of__TotalBytesRead_0() { return &____TotalBytesRead_0; }
	inline void set__TotalBytesRead_0(int64_t value)
	{
		____TotalBytesRead_0 = value;
	}

	inline static int32_t get_offset_of__RunningCrc32Result_3() { return static_cast<int32_t>(offsetof(CRC32_t1348930856, ____RunningCrc32Result_3)); }
	inline uint32_t get__RunningCrc32Result_3() const { return ____RunningCrc32Result_3; }
	inline uint32_t* get_address_of__RunningCrc32Result_3() { return &____RunningCrc32Result_3; }
	inline void set__RunningCrc32Result_3(uint32_t value)
	{
		____RunningCrc32Result_3 = value;
	}
};

struct CRC32_t1348930856_StaticFields
{
public:
	// System.UInt32[] Ionic.Zlib.CRC32::crc32Table
	UInt32U5BU5D_t2770800703* ___crc32Table_1;

public:
	inline static int32_t get_offset_of_crc32Table_1() { return static_cast<int32_t>(offsetof(CRC32_t1348930856_StaticFields, ___crc32Table_1)); }
	inline UInt32U5BU5D_t2770800703* get_crc32Table_1() const { return ___crc32Table_1; }
	inline UInt32U5BU5D_t2770800703** get_address_of_crc32Table_1() { return &___crc32Table_1; }
	inline void set_crc32Table_1(UInt32U5BU5D_t2770800703* value)
	{
		___crc32Table_1 = value;
		Il2CppCodeGenWriteBarrier((&___crc32Table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T1348930856_H
#ifndef INTERNALINFLATECONSTANTS_T3129902576_H
#define INTERNALINFLATECONSTANTS_T3129902576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InternalInflateConstants
struct  InternalInflateConstants_t3129902576  : public RuntimeObject
{
public:

public:
};

struct InternalInflateConstants_t3129902576_StaticFields
{
public:
	// System.Int32[] Ionic.Zlib.InternalInflateConstants::InflateMask
	Int32U5BU5D_t385246372* ___InflateMask_0;

public:
	inline static int32_t get_offset_of_InflateMask_0() { return static_cast<int32_t>(offsetof(InternalInflateConstants_t3129902576_StaticFields, ___InflateMask_0)); }
	inline Int32U5BU5D_t385246372* get_InflateMask_0() const { return ___InflateMask_0; }
	inline Int32U5BU5D_t385246372** get_address_of_InflateMask_0() { return &___InflateMask_0; }
	inline void set_InflateMask_0(Int32U5BU5D_t385246372* value)
	{
		___InflateMask_0 = value;
		Il2CppCodeGenWriteBarrier((&___InflateMask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALINFLATECONSTANTS_T3129902576_H
#ifndef INFLATECODES_T3874557824_H
#define INFLATECODES_T3874557824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InflateCodes
struct  InflateCodes_t3874557824  : public RuntimeObject
{
public:
	// System.Int32 Ionic.Zlib.InflateCodes::mode
	int32_t ___mode_10;
	// System.Int32 Ionic.Zlib.InflateCodes::len
	int32_t ___len_11;
	// System.Int32[] Ionic.Zlib.InflateCodes::tree
	Int32U5BU5D_t385246372* ___tree_12;
	// System.Int32 Ionic.Zlib.InflateCodes::tree_index
	int32_t ___tree_index_13;
	// System.Int32 Ionic.Zlib.InflateCodes::need
	int32_t ___need_14;
	// System.Int32 Ionic.Zlib.InflateCodes::lit
	int32_t ___lit_15;
	// System.Int32 Ionic.Zlib.InflateCodes::bitsToGet
	int32_t ___bitsToGet_16;
	// System.Int32 Ionic.Zlib.InflateCodes::dist
	int32_t ___dist_17;
	// System.Byte Ionic.Zlib.InflateCodes::lbits
	uint8_t ___lbits_18;
	// System.Byte Ionic.Zlib.InflateCodes::dbits
	uint8_t ___dbits_19;
	// System.Int32[] Ionic.Zlib.InflateCodes::ltree
	Int32U5BU5D_t385246372* ___ltree_20;
	// System.Int32 Ionic.Zlib.InflateCodes::ltree_index
	int32_t ___ltree_index_21;
	// System.Int32[] Ionic.Zlib.InflateCodes::dtree
	Int32U5BU5D_t385246372* ___dtree_22;
	// System.Int32 Ionic.Zlib.InflateCodes::dtree_index
	int32_t ___dtree_index_23;

public:
	inline static int32_t get_offset_of_mode_10() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___mode_10)); }
	inline int32_t get_mode_10() const { return ___mode_10; }
	inline int32_t* get_address_of_mode_10() { return &___mode_10; }
	inline void set_mode_10(int32_t value)
	{
		___mode_10 = value;
	}

	inline static int32_t get_offset_of_len_11() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___len_11)); }
	inline int32_t get_len_11() const { return ___len_11; }
	inline int32_t* get_address_of_len_11() { return &___len_11; }
	inline void set_len_11(int32_t value)
	{
		___len_11 = value;
	}

	inline static int32_t get_offset_of_tree_12() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___tree_12)); }
	inline Int32U5BU5D_t385246372* get_tree_12() const { return ___tree_12; }
	inline Int32U5BU5D_t385246372** get_address_of_tree_12() { return &___tree_12; }
	inline void set_tree_12(Int32U5BU5D_t385246372* value)
	{
		___tree_12 = value;
		Il2CppCodeGenWriteBarrier((&___tree_12), value);
	}

	inline static int32_t get_offset_of_tree_index_13() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___tree_index_13)); }
	inline int32_t get_tree_index_13() const { return ___tree_index_13; }
	inline int32_t* get_address_of_tree_index_13() { return &___tree_index_13; }
	inline void set_tree_index_13(int32_t value)
	{
		___tree_index_13 = value;
	}

	inline static int32_t get_offset_of_need_14() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___need_14)); }
	inline int32_t get_need_14() const { return ___need_14; }
	inline int32_t* get_address_of_need_14() { return &___need_14; }
	inline void set_need_14(int32_t value)
	{
		___need_14 = value;
	}

	inline static int32_t get_offset_of_lit_15() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___lit_15)); }
	inline int32_t get_lit_15() const { return ___lit_15; }
	inline int32_t* get_address_of_lit_15() { return &___lit_15; }
	inline void set_lit_15(int32_t value)
	{
		___lit_15 = value;
	}

	inline static int32_t get_offset_of_bitsToGet_16() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___bitsToGet_16)); }
	inline int32_t get_bitsToGet_16() const { return ___bitsToGet_16; }
	inline int32_t* get_address_of_bitsToGet_16() { return &___bitsToGet_16; }
	inline void set_bitsToGet_16(int32_t value)
	{
		___bitsToGet_16 = value;
	}

	inline static int32_t get_offset_of_dist_17() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___dist_17)); }
	inline int32_t get_dist_17() const { return ___dist_17; }
	inline int32_t* get_address_of_dist_17() { return &___dist_17; }
	inline void set_dist_17(int32_t value)
	{
		___dist_17 = value;
	}

	inline static int32_t get_offset_of_lbits_18() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___lbits_18)); }
	inline uint8_t get_lbits_18() const { return ___lbits_18; }
	inline uint8_t* get_address_of_lbits_18() { return &___lbits_18; }
	inline void set_lbits_18(uint8_t value)
	{
		___lbits_18 = value;
	}

	inline static int32_t get_offset_of_dbits_19() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___dbits_19)); }
	inline uint8_t get_dbits_19() const { return ___dbits_19; }
	inline uint8_t* get_address_of_dbits_19() { return &___dbits_19; }
	inline void set_dbits_19(uint8_t value)
	{
		___dbits_19 = value;
	}

	inline static int32_t get_offset_of_ltree_20() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___ltree_20)); }
	inline Int32U5BU5D_t385246372* get_ltree_20() const { return ___ltree_20; }
	inline Int32U5BU5D_t385246372** get_address_of_ltree_20() { return &___ltree_20; }
	inline void set_ltree_20(Int32U5BU5D_t385246372* value)
	{
		___ltree_20 = value;
		Il2CppCodeGenWriteBarrier((&___ltree_20), value);
	}

	inline static int32_t get_offset_of_ltree_index_21() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___ltree_index_21)); }
	inline int32_t get_ltree_index_21() const { return ___ltree_index_21; }
	inline int32_t* get_address_of_ltree_index_21() { return &___ltree_index_21; }
	inline void set_ltree_index_21(int32_t value)
	{
		___ltree_index_21 = value;
	}

	inline static int32_t get_offset_of_dtree_22() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___dtree_22)); }
	inline Int32U5BU5D_t385246372* get_dtree_22() const { return ___dtree_22; }
	inline Int32U5BU5D_t385246372** get_address_of_dtree_22() { return &___dtree_22; }
	inline void set_dtree_22(Int32U5BU5D_t385246372* value)
	{
		___dtree_22 = value;
		Il2CppCodeGenWriteBarrier((&___dtree_22), value);
	}

	inline static int32_t get_offset_of_dtree_index_23() { return static_cast<int32_t>(offsetof(InflateCodes_t3874557824, ___dtree_index_23)); }
	inline int32_t get_dtree_index_23() const { return ___dtree_index_23; }
	inline int32_t* get_address_of_dtree_index_23() { return &___dtree_index_23; }
	inline void set_dtree_index_23(int32_t value)
	{
		___dtree_index_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATECODES_T3874557824_H
#ifndef WORKITEM_T2895573498_H
#define WORKITEM_T2895573498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.WorkItem
struct  WorkItem_t2895573498  : public RuntimeObject
{
public:
	// System.Byte[] Ionic.Zlib.WorkItem::buffer
	ByteU5BU5D_t4116647657* ___buffer_0;
	// System.Byte[] Ionic.Zlib.WorkItem::compressed
	ByteU5BU5D_t4116647657* ___compressed_1;
	// System.Int32 Ionic.Zlib.WorkItem::status
	int32_t ___status_2;
	// System.Int32 Ionic.Zlib.WorkItem::crc
	int32_t ___crc_3;
	// System.Int32 Ionic.Zlib.WorkItem::index
	int32_t ___index_4;
	// System.Int32 Ionic.Zlib.WorkItem::inputBytesAvailable
	int32_t ___inputBytesAvailable_5;
	// System.Int32 Ionic.Zlib.WorkItem::compressedBytesAvailable
	int32_t ___compressedBytesAvailable_6;
	// Ionic.Zlib.ZlibCodec Ionic.Zlib.WorkItem::compressor
	ZlibCodec_t3653667923 * ___compressor_7;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_compressed_1() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___compressed_1)); }
	inline ByteU5BU5D_t4116647657* get_compressed_1() const { return ___compressed_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_compressed_1() { return &___compressed_1; }
	inline void set_compressed_1(ByteU5BU5D_t4116647657* value)
	{
		___compressed_1 = value;
		Il2CppCodeGenWriteBarrier((&___compressed_1), value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___status_2)); }
	inline int32_t get_status_2() const { return ___status_2; }
	inline int32_t* get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(int32_t value)
	{
		___status_2 = value;
	}

	inline static int32_t get_offset_of_crc_3() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___crc_3)); }
	inline int32_t get_crc_3() const { return ___crc_3; }
	inline int32_t* get_address_of_crc_3() { return &___crc_3; }
	inline void set_crc_3(int32_t value)
	{
		___crc_3 = value;
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_inputBytesAvailable_5() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___inputBytesAvailable_5)); }
	inline int32_t get_inputBytesAvailable_5() const { return ___inputBytesAvailable_5; }
	inline int32_t* get_address_of_inputBytesAvailable_5() { return &___inputBytesAvailable_5; }
	inline void set_inputBytesAvailable_5(int32_t value)
	{
		___inputBytesAvailable_5 = value;
	}

	inline static int32_t get_offset_of_compressedBytesAvailable_6() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___compressedBytesAvailable_6)); }
	inline int32_t get_compressedBytesAvailable_6() const { return ___compressedBytesAvailable_6; }
	inline int32_t* get_address_of_compressedBytesAvailable_6() { return &___compressedBytesAvailable_6; }
	inline void set_compressedBytesAvailable_6(int32_t value)
	{
		___compressedBytesAvailable_6 = value;
	}

	inline static int32_t get_offset_of_compressor_7() { return static_cast<int32_t>(offsetof(WorkItem_t2895573498, ___compressor_7)); }
	inline ZlibCodec_t3653667923 * get_compressor_7() const { return ___compressor_7; }
	inline ZlibCodec_t3653667923 ** get_address_of_compressor_7() { return &___compressor_7; }
	inline void set_compressor_7(ZlibCodec_t3653667923 * value)
	{
		___compressor_7 = value;
		Il2CppCodeGenWriteBarrier((&___compressor_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORKITEM_T2895573498_H
#ifndef SHAREDUTILS_T3538106298_H
#define SHAREDUTILS_T3538106298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.SharedUtils
struct  SharedUtils_t3538106298  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDUTILS_T3538106298_H
#ifndef INTERNALCONSTANTS_T2406264312_H
#define INTERNALCONSTANTS_T2406264312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InternalConstants
struct  InternalConstants_t2406264312  : public RuntimeObject
{
public:

public:
};

struct InternalConstants_t2406264312_StaticFields
{
public:
	// System.Int32 Ionic.Zlib.InternalConstants::MAX_BITS
	int32_t ___MAX_BITS_0;
	// System.Int32 Ionic.Zlib.InternalConstants::BL_CODES
	int32_t ___BL_CODES_1;
	// System.Int32 Ionic.Zlib.InternalConstants::D_CODES
	int32_t ___D_CODES_2;
	// System.Int32 Ionic.Zlib.InternalConstants::LITERALS
	int32_t ___LITERALS_3;
	// System.Int32 Ionic.Zlib.InternalConstants::LENGTH_CODES
	int32_t ___LENGTH_CODES_4;
	// System.Int32 Ionic.Zlib.InternalConstants::L_CODES
	int32_t ___L_CODES_5;
	// System.Int32 Ionic.Zlib.InternalConstants::MAX_BL_BITS
	int32_t ___MAX_BL_BITS_6;
	// System.Int32 Ionic.Zlib.InternalConstants::REP_3_6
	int32_t ___REP_3_6_7;
	// System.Int32 Ionic.Zlib.InternalConstants::REPZ_3_10
	int32_t ___REPZ_3_10_8;
	// System.Int32 Ionic.Zlib.InternalConstants::REPZ_11_138
	int32_t ___REPZ_11_138_9;

public:
	inline static int32_t get_offset_of_MAX_BITS_0() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___MAX_BITS_0)); }
	inline int32_t get_MAX_BITS_0() const { return ___MAX_BITS_0; }
	inline int32_t* get_address_of_MAX_BITS_0() { return &___MAX_BITS_0; }
	inline void set_MAX_BITS_0(int32_t value)
	{
		___MAX_BITS_0 = value;
	}

	inline static int32_t get_offset_of_BL_CODES_1() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___BL_CODES_1)); }
	inline int32_t get_BL_CODES_1() const { return ___BL_CODES_1; }
	inline int32_t* get_address_of_BL_CODES_1() { return &___BL_CODES_1; }
	inline void set_BL_CODES_1(int32_t value)
	{
		___BL_CODES_1 = value;
	}

	inline static int32_t get_offset_of_D_CODES_2() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___D_CODES_2)); }
	inline int32_t get_D_CODES_2() const { return ___D_CODES_2; }
	inline int32_t* get_address_of_D_CODES_2() { return &___D_CODES_2; }
	inline void set_D_CODES_2(int32_t value)
	{
		___D_CODES_2 = value;
	}

	inline static int32_t get_offset_of_LITERALS_3() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___LITERALS_3)); }
	inline int32_t get_LITERALS_3() const { return ___LITERALS_3; }
	inline int32_t* get_address_of_LITERALS_3() { return &___LITERALS_3; }
	inline void set_LITERALS_3(int32_t value)
	{
		___LITERALS_3 = value;
	}

	inline static int32_t get_offset_of_LENGTH_CODES_4() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___LENGTH_CODES_4)); }
	inline int32_t get_LENGTH_CODES_4() const { return ___LENGTH_CODES_4; }
	inline int32_t* get_address_of_LENGTH_CODES_4() { return &___LENGTH_CODES_4; }
	inline void set_LENGTH_CODES_4(int32_t value)
	{
		___LENGTH_CODES_4 = value;
	}

	inline static int32_t get_offset_of_L_CODES_5() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___L_CODES_5)); }
	inline int32_t get_L_CODES_5() const { return ___L_CODES_5; }
	inline int32_t* get_address_of_L_CODES_5() { return &___L_CODES_5; }
	inline void set_L_CODES_5(int32_t value)
	{
		___L_CODES_5 = value;
	}

	inline static int32_t get_offset_of_MAX_BL_BITS_6() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___MAX_BL_BITS_6)); }
	inline int32_t get_MAX_BL_BITS_6() const { return ___MAX_BL_BITS_6; }
	inline int32_t* get_address_of_MAX_BL_BITS_6() { return &___MAX_BL_BITS_6; }
	inline void set_MAX_BL_BITS_6(int32_t value)
	{
		___MAX_BL_BITS_6 = value;
	}

	inline static int32_t get_offset_of_REP_3_6_7() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___REP_3_6_7)); }
	inline int32_t get_REP_3_6_7() const { return ___REP_3_6_7; }
	inline int32_t* get_address_of_REP_3_6_7() { return &___REP_3_6_7; }
	inline void set_REP_3_6_7(int32_t value)
	{
		___REP_3_6_7 = value;
	}

	inline static int32_t get_offset_of_REPZ_3_10_8() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___REPZ_3_10_8)); }
	inline int32_t get_REPZ_3_10_8() const { return ___REPZ_3_10_8; }
	inline int32_t* get_address_of_REPZ_3_10_8() { return &___REPZ_3_10_8; }
	inline void set_REPZ_3_10_8(int32_t value)
	{
		___REPZ_3_10_8 = value;
	}

	inline static int32_t get_offset_of_REPZ_11_138_9() { return static_cast<int32_t>(offsetof(InternalConstants_t2406264312_StaticFields, ___REPZ_11_138_9)); }
	inline int32_t get_REPZ_11_138_9() const { return ___REPZ_11_138_9; }
	inline int32_t* get_address_of_REPZ_11_138_9() { return &___REPZ_11_138_9; }
	inline void set_REPZ_11_138_9(int32_t value)
	{
		___REPZ_11_138_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONSTANTS_T2406264312_H
#ifndef STATICTREE_T2118636970_H
#define STATICTREE_T2118636970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.StaticTree
struct  StaticTree_t2118636970  : public RuntimeObject
{
public:
	// System.Int16[] Ionic.Zlib.StaticTree::treeCodes
	Int16U5BU5D_t3686840178* ___treeCodes_5;
	// System.Int32[] Ionic.Zlib.StaticTree::extraBits
	Int32U5BU5D_t385246372* ___extraBits_6;
	// System.Int32 Ionic.Zlib.StaticTree::extraBase
	int32_t ___extraBase_7;
	// System.Int32 Ionic.Zlib.StaticTree::elems
	int32_t ___elems_8;
	// System.Int32 Ionic.Zlib.StaticTree::maxLength
	int32_t ___maxLength_9;

public:
	inline static int32_t get_offset_of_treeCodes_5() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970, ___treeCodes_5)); }
	inline Int16U5BU5D_t3686840178* get_treeCodes_5() const { return ___treeCodes_5; }
	inline Int16U5BU5D_t3686840178** get_address_of_treeCodes_5() { return &___treeCodes_5; }
	inline void set_treeCodes_5(Int16U5BU5D_t3686840178* value)
	{
		___treeCodes_5 = value;
		Il2CppCodeGenWriteBarrier((&___treeCodes_5), value);
	}

	inline static int32_t get_offset_of_extraBits_6() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970, ___extraBits_6)); }
	inline Int32U5BU5D_t385246372* get_extraBits_6() const { return ___extraBits_6; }
	inline Int32U5BU5D_t385246372** get_address_of_extraBits_6() { return &___extraBits_6; }
	inline void set_extraBits_6(Int32U5BU5D_t385246372* value)
	{
		___extraBits_6 = value;
		Il2CppCodeGenWriteBarrier((&___extraBits_6), value);
	}

	inline static int32_t get_offset_of_extraBase_7() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970, ___extraBase_7)); }
	inline int32_t get_extraBase_7() const { return ___extraBase_7; }
	inline int32_t* get_address_of_extraBase_7() { return &___extraBase_7; }
	inline void set_extraBase_7(int32_t value)
	{
		___extraBase_7 = value;
	}

	inline static int32_t get_offset_of_elems_8() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970, ___elems_8)); }
	inline int32_t get_elems_8() const { return ___elems_8; }
	inline int32_t* get_address_of_elems_8() { return &___elems_8; }
	inline void set_elems_8(int32_t value)
	{
		___elems_8 = value;
	}

	inline static int32_t get_offset_of_maxLength_9() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970, ___maxLength_9)); }
	inline int32_t get_maxLength_9() const { return ___maxLength_9; }
	inline int32_t* get_address_of_maxLength_9() { return &___maxLength_9; }
	inline void set_maxLength_9(int32_t value)
	{
		___maxLength_9 = value;
	}
};

struct StaticTree_t2118636970_StaticFields
{
public:
	// System.Int16[] Ionic.Zlib.StaticTree::lengthAndLiteralsTreeCodes
	Int16U5BU5D_t3686840178* ___lengthAndLiteralsTreeCodes_0;
	// System.Int16[] Ionic.Zlib.StaticTree::distTreeCodes
	Int16U5BU5D_t3686840178* ___distTreeCodes_1;
	// Ionic.Zlib.StaticTree Ionic.Zlib.StaticTree::Literals
	StaticTree_t2118636970 * ___Literals_2;
	// Ionic.Zlib.StaticTree Ionic.Zlib.StaticTree::Distances
	StaticTree_t2118636970 * ___Distances_3;
	// Ionic.Zlib.StaticTree Ionic.Zlib.StaticTree::BitLengths
	StaticTree_t2118636970 * ___BitLengths_4;

public:
	inline static int32_t get_offset_of_lengthAndLiteralsTreeCodes_0() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970_StaticFields, ___lengthAndLiteralsTreeCodes_0)); }
	inline Int16U5BU5D_t3686840178* get_lengthAndLiteralsTreeCodes_0() const { return ___lengthAndLiteralsTreeCodes_0; }
	inline Int16U5BU5D_t3686840178** get_address_of_lengthAndLiteralsTreeCodes_0() { return &___lengthAndLiteralsTreeCodes_0; }
	inline void set_lengthAndLiteralsTreeCodes_0(Int16U5BU5D_t3686840178* value)
	{
		___lengthAndLiteralsTreeCodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___lengthAndLiteralsTreeCodes_0), value);
	}

	inline static int32_t get_offset_of_distTreeCodes_1() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970_StaticFields, ___distTreeCodes_1)); }
	inline Int16U5BU5D_t3686840178* get_distTreeCodes_1() const { return ___distTreeCodes_1; }
	inline Int16U5BU5D_t3686840178** get_address_of_distTreeCodes_1() { return &___distTreeCodes_1; }
	inline void set_distTreeCodes_1(Int16U5BU5D_t3686840178* value)
	{
		___distTreeCodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___distTreeCodes_1), value);
	}

	inline static int32_t get_offset_of_Literals_2() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970_StaticFields, ___Literals_2)); }
	inline StaticTree_t2118636970 * get_Literals_2() const { return ___Literals_2; }
	inline StaticTree_t2118636970 ** get_address_of_Literals_2() { return &___Literals_2; }
	inline void set_Literals_2(StaticTree_t2118636970 * value)
	{
		___Literals_2 = value;
		Il2CppCodeGenWriteBarrier((&___Literals_2), value);
	}

	inline static int32_t get_offset_of_Distances_3() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970_StaticFields, ___Distances_3)); }
	inline StaticTree_t2118636970 * get_Distances_3() const { return ___Distances_3; }
	inline StaticTree_t2118636970 ** get_address_of_Distances_3() { return &___Distances_3; }
	inline void set_Distances_3(StaticTree_t2118636970 * value)
	{
		___Distances_3 = value;
		Il2CppCodeGenWriteBarrier((&___Distances_3), value);
	}

	inline static int32_t get_offset_of_BitLengths_4() { return static_cast<int32_t>(offsetof(StaticTree_t2118636970_StaticFields, ___BitLengths_4)); }
	inline StaticTree_t2118636970 * get_BitLengths_4() const { return ___BitLengths_4; }
	inline StaticTree_t2118636970 ** get_address_of_BitLengths_4() { return &___BitLengths_4; }
	inline void set_BitLengths_4(StaticTree_t2118636970 * value)
	{
		___BitLengths_4 = value;
		Il2CppCodeGenWriteBarrier((&___BitLengths_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICTREE_T2118636970_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef ADLER_T1959216880_H
#define ADLER_T1959216880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.Adler
struct  Adler_t1959216880  : public RuntimeObject
{
public:

public:
};

struct Adler_t1959216880_StaticFields
{
public:
	// System.Int32 Ionic.Zlib.Adler::BASE
	int32_t ___BASE_0;
	// System.Int32 Ionic.Zlib.Adler::NMAX
	int32_t ___NMAX_1;

public:
	inline static int32_t get_offset_of_BASE_0() { return static_cast<int32_t>(offsetof(Adler_t1959216880_StaticFields, ___BASE_0)); }
	inline int32_t get_BASE_0() const { return ___BASE_0; }
	inline int32_t* get_address_of_BASE_0() { return &___BASE_0; }
	inline void set_BASE_0(int32_t value)
	{
		___BASE_0 = value;
	}

	inline static int32_t get_offset_of_NMAX_1() { return static_cast<int32_t>(offsetof(Adler_t1959216880_StaticFields, ___NMAX_1)); }
	inline int32_t get_NMAX_1() const { return ___NMAX_1; }
	inline int32_t* get_address_of_NMAX_1() { return &___NMAX_1; }
	inline void set_NMAX_1(int32_t value)
	{
		___NMAX_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLER_T1959216880_H
#ifndef ZLIBCONSTANTS_T3760349222_H
#define ZLIBCONSTANTS_T3760349222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibConstants
struct  ZlibConstants_t3760349222  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBCONSTANTS_T3760349222_H
#ifndef TREE_T526993154_H
#define TREE_T526993154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.Tree
struct  Tree_t526993154  : public RuntimeObject
{
public:
	// System.Int16[] Ionic.Zlib.Tree::dyn_tree
	Int16U5BU5D_t3686840178* ___dyn_tree_10;
	// System.Int32 Ionic.Zlib.Tree::max_code
	int32_t ___max_code_11;
	// Ionic.Zlib.StaticTree Ionic.Zlib.Tree::staticTree
	StaticTree_t2118636970 * ___staticTree_12;

public:
	inline static int32_t get_offset_of_dyn_tree_10() { return static_cast<int32_t>(offsetof(Tree_t526993154, ___dyn_tree_10)); }
	inline Int16U5BU5D_t3686840178* get_dyn_tree_10() const { return ___dyn_tree_10; }
	inline Int16U5BU5D_t3686840178** get_address_of_dyn_tree_10() { return &___dyn_tree_10; }
	inline void set_dyn_tree_10(Int16U5BU5D_t3686840178* value)
	{
		___dyn_tree_10 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_tree_10), value);
	}

	inline static int32_t get_offset_of_max_code_11() { return static_cast<int32_t>(offsetof(Tree_t526993154, ___max_code_11)); }
	inline int32_t get_max_code_11() const { return ___max_code_11; }
	inline int32_t* get_address_of_max_code_11() { return &___max_code_11; }
	inline void set_max_code_11(int32_t value)
	{
		___max_code_11 = value;
	}

	inline static int32_t get_offset_of_staticTree_12() { return static_cast<int32_t>(offsetof(Tree_t526993154, ___staticTree_12)); }
	inline StaticTree_t2118636970 * get_staticTree_12() const { return ___staticTree_12; }
	inline StaticTree_t2118636970 ** get_address_of_staticTree_12() { return &___staticTree_12; }
	inline void set_staticTree_12(StaticTree_t2118636970 * value)
	{
		___staticTree_12 = value;
		Il2CppCodeGenWriteBarrier((&___staticTree_12), value);
	}
};

struct Tree_t526993154_StaticFields
{
public:
	// System.Int32 Ionic.Zlib.Tree::HEAP_SIZE
	int32_t ___HEAP_SIZE_0;
	// System.Int32[] Ionic.Zlib.Tree::ExtraLengthBits
	Int32U5BU5D_t385246372* ___ExtraLengthBits_1;
	// System.Int32[] Ionic.Zlib.Tree::ExtraDistanceBits
	Int32U5BU5D_t385246372* ___ExtraDistanceBits_2;
	// System.Int32[] Ionic.Zlib.Tree::extra_blbits
	Int32U5BU5D_t385246372* ___extra_blbits_3;
	// System.SByte[] Ionic.Zlib.Tree::bl_order
	SByteU5BU5D_t2651576203* ___bl_order_4;
	// System.SByte[] Ionic.Zlib.Tree::_dist_code
	SByteU5BU5D_t2651576203* ____dist_code_6;
	// System.SByte[] Ionic.Zlib.Tree::LengthCode
	SByteU5BU5D_t2651576203* ___LengthCode_7;
	// System.Int32[] Ionic.Zlib.Tree::LengthBase
	Int32U5BU5D_t385246372* ___LengthBase_8;
	// System.Int32[] Ionic.Zlib.Tree::DistanceBase
	Int32U5BU5D_t385246372* ___DistanceBase_9;

public:
	inline static int32_t get_offset_of_HEAP_SIZE_0() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___HEAP_SIZE_0)); }
	inline int32_t get_HEAP_SIZE_0() const { return ___HEAP_SIZE_0; }
	inline int32_t* get_address_of_HEAP_SIZE_0() { return &___HEAP_SIZE_0; }
	inline void set_HEAP_SIZE_0(int32_t value)
	{
		___HEAP_SIZE_0 = value;
	}

	inline static int32_t get_offset_of_ExtraLengthBits_1() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___ExtraLengthBits_1)); }
	inline Int32U5BU5D_t385246372* get_ExtraLengthBits_1() const { return ___ExtraLengthBits_1; }
	inline Int32U5BU5D_t385246372** get_address_of_ExtraLengthBits_1() { return &___ExtraLengthBits_1; }
	inline void set_ExtraLengthBits_1(Int32U5BU5D_t385246372* value)
	{
		___ExtraLengthBits_1 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraLengthBits_1), value);
	}

	inline static int32_t get_offset_of_ExtraDistanceBits_2() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___ExtraDistanceBits_2)); }
	inline Int32U5BU5D_t385246372* get_ExtraDistanceBits_2() const { return ___ExtraDistanceBits_2; }
	inline Int32U5BU5D_t385246372** get_address_of_ExtraDistanceBits_2() { return &___ExtraDistanceBits_2; }
	inline void set_ExtraDistanceBits_2(Int32U5BU5D_t385246372* value)
	{
		___ExtraDistanceBits_2 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraDistanceBits_2), value);
	}

	inline static int32_t get_offset_of_extra_blbits_3() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___extra_blbits_3)); }
	inline Int32U5BU5D_t385246372* get_extra_blbits_3() const { return ___extra_blbits_3; }
	inline Int32U5BU5D_t385246372** get_address_of_extra_blbits_3() { return &___extra_blbits_3; }
	inline void set_extra_blbits_3(Int32U5BU5D_t385246372* value)
	{
		___extra_blbits_3 = value;
		Il2CppCodeGenWriteBarrier((&___extra_blbits_3), value);
	}

	inline static int32_t get_offset_of_bl_order_4() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___bl_order_4)); }
	inline SByteU5BU5D_t2651576203* get_bl_order_4() const { return ___bl_order_4; }
	inline SByteU5BU5D_t2651576203** get_address_of_bl_order_4() { return &___bl_order_4; }
	inline void set_bl_order_4(SByteU5BU5D_t2651576203* value)
	{
		___bl_order_4 = value;
		Il2CppCodeGenWriteBarrier((&___bl_order_4), value);
	}

	inline static int32_t get_offset_of__dist_code_6() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ____dist_code_6)); }
	inline SByteU5BU5D_t2651576203* get__dist_code_6() const { return ____dist_code_6; }
	inline SByteU5BU5D_t2651576203** get_address_of__dist_code_6() { return &____dist_code_6; }
	inline void set__dist_code_6(SByteU5BU5D_t2651576203* value)
	{
		____dist_code_6 = value;
		Il2CppCodeGenWriteBarrier((&____dist_code_6), value);
	}

	inline static int32_t get_offset_of_LengthCode_7() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___LengthCode_7)); }
	inline SByteU5BU5D_t2651576203* get_LengthCode_7() const { return ___LengthCode_7; }
	inline SByteU5BU5D_t2651576203** get_address_of_LengthCode_7() { return &___LengthCode_7; }
	inline void set_LengthCode_7(SByteU5BU5D_t2651576203* value)
	{
		___LengthCode_7 = value;
		Il2CppCodeGenWriteBarrier((&___LengthCode_7), value);
	}

	inline static int32_t get_offset_of_LengthBase_8() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___LengthBase_8)); }
	inline Int32U5BU5D_t385246372* get_LengthBase_8() const { return ___LengthBase_8; }
	inline Int32U5BU5D_t385246372** get_address_of_LengthBase_8() { return &___LengthBase_8; }
	inline void set_LengthBase_8(Int32U5BU5D_t385246372* value)
	{
		___LengthBase_8 = value;
		Il2CppCodeGenWriteBarrier((&___LengthBase_8), value);
	}

	inline static int32_t get_offset_of_DistanceBase_9() { return static_cast<int32_t>(offsetof(Tree_t526993154_StaticFields, ___DistanceBase_9)); }
	inline Int32U5BU5D_t385246372* get_DistanceBase_9() const { return ___DistanceBase_9; }
	inline Int32U5BU5D_t385246372** get_address_of_DistanceBase_9() { return &___DistanceBase_9; }
	inline void set_DistanceBase_9(Int32U5BU5D_t385246372* value)
	{
		___DistanceBase_9 = value;
		Il2CppCodeGenWriteBarrier((&___DistanceBase_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREE_T526993154_H
#ifndef U3CRETRIEVEGLOBALIPU3EC__ITERATOR0_T1869265361_H
#define U3CRETRIEVEGLOBALIPU3EC__ITERATOR0_T1869265361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuTrackingHelpers/<RetrieveGlobalIP>c__Iterator0
struct  U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361  : public RuntimeObject
{
public:
	// HTTP.Request NuTrackingHelpers/<RetrieveGlobalIP>c__Iterator0::<r>__0
	Request_t1151508915 * ___U3CrU3E__0_0;
	// System.Object NuTrackingHelpers/<RetrieveGlobalIP>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean NuTrackingHelpers/<RetrieveGlobalIP>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 NuTrackingHelpers/<RetrieveGlobalIP>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361, ___U3CrU3E__0_0)); }
	inline Request_t1151508915 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Request_t1151508915 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Request_t1151508915 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRETRIEVEGLOBALIPU3EC__ITERATOR0_T1869265361_H
#ifndef NUGAREQUEST_T3358732621_H
#define NUGAREQUEST_T3358732621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuGARequest
struct  NuGARequest_t3358732621  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUGAREQUEST_T3358732621_H
#ifndef NUGASERVICE_T1586892016_H
#define NUGASERVICE_T1586892016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuGAService
struct  NuGAService_t1586892016  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUGASERVICE_T1586892016_H
#ifndef NUGATRANSACTION_T3923616719_H
#define NUGATRANSACTION_T3923616719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuGATransaction
struct  NuGATransaction_t3923616719  : public RuntimeObject
{
public:
	// System.String NuGATransaction::<ProductCode>k__BackingField
	String_t* ___U3CProductCodeU3Ek__BackingField_0;
	// System.String NuGATransaction::<ProductName>k__BackingField
	String_t* ___U3CProductNameU3Ek__BackingField_1;
	// System.Single NuGATransaction::<ProductUnitPrice>k__BackingField
	float ___U3CProductUnitPriceU3Ek__BackingField_2;
	// System.Int32 NuGATransaction::<ProductQuantity>k__BackingField
	int32_t ___U3CProductQuantityU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CProductCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NuGATransaction_t3923616719, ___U3CProductCodeU3Ek__BackingField_0)); }
	inline String_t* get_U3CProductCodeU3Ek__BackingField_0() const { return ___U3CProductCodeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CProductCodeU3Ek__BackingField_0() { return &___U3CProductCodeU3Ek__BackingField_0; }
	inline void set_U3CProductCodeU3Ek__BackingField_0(String_t* value)
	{
		___U3CProductCodeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProductCodeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProductNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NuGATransaction_t3923616719, ___U3CProductNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CProductNameU3Ek__BackingField_1() const { return ___U3CProductNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CProductNameU3Ek__BackingField_1() { return &___U3CProductNameU3Ek__BackingField_1; }
	inline void set_U3CProductNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CProductNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProductNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CProductUnitPriceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NuGATransaction_t3923616719, ___U3CProductUnitPriceU3Ek__BackingField_2)); }
	inline float get_U3CProductUnitPriceU3Ek__BackingField_2() const { return ___U3CProductUnitPriceU3Ek__BackingField_2; }
	inline float* get_address_of_U3CProductUnitPriceU3Ek__BackingField_2() { return &___U3CProductUnitPriceU3Ek__BackingField_2; }
	inline void set_U3CProductUnitPriceU3Ek__BackingField_2(float value)
	{
		___U3CProductUnitPriceU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CProductQuantityU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NuGATransaction_t3923616719, ___U3CProductQuantityU3Ek__BackingField_3)); }
	inline int32_t get_U3CProductQuantityU3Ek__BackingField_3() const { return ___U3CProductQuantityU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CProductQuantityU3Ek__BackingField_3() { return &___U3CProductQuantityU3Ek__BackingField_3; }
	inline void set_U3CProductQuantityU3Ek__BackingField_3(int32_t value)
	{
		___U3CProductQuantityU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUGATRANSACTION_T3923616719_H
#ifndef TRACKINGREQUEST_T2594580146_H
#define TRACKINGREQUEST_T2594580146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuTracking/TrackingRequest
struct  TrackingRequest_t2594580146  : public RuntimeObject
{
public:
	// System.String NuTracking/TrackingRequest::pageTitle
	String_t* ___pageTitle_0;
	// System.String NuTracking/TrackingRequest::pageUrl
	String_t* ___pageUrl_1;
	// NuGAEvent NuTracking/TrackingRequest::gaEvent
	NuGAEvent_t4041358949 * ___gaEvent_2;
	// NuGATransaction NuTracking/TrackingRequest::gaTransaction
	NuGATransaction_t3923616719 * ___gaTransaction_3;

public:
	inline static int32_t get_offset_of_pageTitle_0() { return static_cast<int32_t>(offsetof(TrackingRequest_t2594580146, ___pageTitle_0)); }
	inline String_t* get_pageTitle_0() const { return ___pageTitle_0; }
	inline String_t** get_address_of_pageTitle_0() { return &___pageTitle_0; }
	inline void set_pageTitle_0(String_t* value)
	{
		___pageTitle_0 = value;
		Il2CppCodeGenWriteBarrier((&___pageTitle_0), value);
	}

	inline static int32_t get_offset_of_pageUrl_1() { return static_cast<int32_t>(offsetof(TrackingRequest_t2594580146, ___pageUrl_1)); }
	inline String_t* get_pageUrl_1() const { return ___pageUrl_1; }
	inline String_t** get_address_of_pageUrl_1() { return &___pageUrl_1; }
	inline void set_pageUrl_1(String_t* value)
	{
		___pageUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&___pageUrl_1), value);
	}

	inline static int32_t get_offset_of_gaEvent_2() { return static_cast<int32_t>(offsetof(TrackingRequest_t2594580146, ___gaEvent_2)); }
	inline NuGAEvent_t4041358949 * get_gaEvent_2() const { return ___gaEvent_2; }
	inline NuGAEvent_t4041358949 ** get_address_of_gaEvent_2() { return &___gaEvent_2; }
	inline void set_gaEvent_2(NuGAEvent_t4041358949 * value)
	{
		___gaEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___gaEvent_2), value);
	}

	inline static int32_t get_offset_of_gaTransaction_3() { return static_cast<int32_t>(offsetof(TrackingRequest_t2594580146, ___gaTransaction_3)); }
	inline NuGATransaction_t3923616719 * get_gaTransaction_3() const { return ___gaTransaction_3; }
	inline NuGATransaction_t3923616719 ** get_address_of_gaTransaction_3() { return &___gaTransaction_3; }
	inline void set_gaTransaction_3(NuGATransaction_t3923616719 * value)
	{
		___gaTransaction_3 = value;
		Il2CppCodeGenWriteBarrier((&___gaTransaction_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGREQUEST_T2594580146_H
#ifndef U3CFLUSHTRACKINGQUESTSASYNCU3EC__ITERATOR0_T545484271_H
#define U3CFLUSHTRACKINGQUESTSASYNCU3EC__ITERATOR0_T545484271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuTracking/<FlushTrackingQuestsAsync>c__Iterator0
struct  U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271  : public RuntimeObject
{
public:
	// NuTracking NuTracking/<FlushTrackingQuestsAsync>c__Iterator0::$this
	NuTracking_t472478902 * ___U24this_0;
	// System.Object NuTracking/<FlushTrackingQuestsAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean NuTracking/<FlushTrackingQuestsAsync>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 NuTracking/<FlushTrackingQuestsAsync>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271, ___U24this_0)); }
	inline NuTracking_t472478902 * get_U24this_0() const { return ___U24this_0; }
	inline NuTracking_t472478902 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(NuTracking_t472478902 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLUSHTRACKINGQUESTSASYNCU3EC__ITERATOR0_T545484271_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef HASHALGORITHM_T1432317219_H
#define HASHALGORITHM_T1432317219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t1432317219  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t4116647657* ___HashValue_0;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::disposed
	bool ___disposed_3;

public:
	inline static int32_t get_offset_of_HashValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashValue_0)); }
	inline ByteU5BU5D_t4116647657* get_HashValue_0() const { return ___HashValue_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_HashValue_0() { return &___HashValue_0; }
	inline void set_HashValue_0(ByteU5BU5D_t4116647657* value)
	{
		___HashValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_0), value);
	}

	inline static int32_t get_offset_of_HashSizeValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___HashSizeValue_1)); }
	inline int32_t get_HashSizeValue_1() const { return ___HashSizeValue_1; }
	inline int32_t* get_address_of_HashSizeValue_1() { return &___HashSizeValue_1; }
	inline void set_HashSizeValue_1(int32_t value)
	{
		___HashSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t1432317219, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T1432317219_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef U3CREQUESTPLAYERIDU3EC__ANONSTOREY0_T2149773854_H
#define U3CREQUESTPLAYERIDU3EC__ANONSTOREY0_T2149773854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosPlayer/<RequestPlayerId>c__AnonStorey0
struct  U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> LumosPlayer/<RequestPlayerId>c__AnonStorey0::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREQUESTPLAYERIDU3EC__ANONSTOREY0_T2149773854_H
#ifndef LUMOSPLAYER_T370097434_H
#define LUMOSPLAYER_T370097434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosPlayer
struct  LumosPlayer_t370097434  : public RuntimeObject
{
public:

public:
};

struct LumosPlayer_t370097434_StaticFields
{
public:
	// System.String LumosPlayer::_baseUrl
	String_t* ____baseUrl_0;
	// System.Action`1<System.Object> LumosPlayer::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_1;
	// System.Action`1<System.Object> LumosPlayer::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of__baseUrl_0() { return static_cast<int32_t>(offsetof(LumosPlayer_t370097434_StaticFields, ____baseUrl_0)); }
	inline String_t* get__baseUrl_0() const { return ____baseUrl_0; }
	inline String_t** get_address_of__baseUrl_0() { return &____baseUrl_0; }
	inline void set__baseUrl_0(String_t* value)
	{
		____baseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(LumosPlayer_t370097434_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(LumosPlayer_t370097434_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSPLAYER_T370097434_H
#ifndef INFTREE_T3075871205_H
#define INFTREE_T3075871205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InfTree
struct  InfTree_t3075871205  : public RuntimeObject
{
public:
	// System.Int32[] Ionic.Zlib.InfTree::hn
	Int32U5BU5D_t385246372* ___hn_19;
	// System.Int32[] Ionic.Zlib.InfTree::v
	Int32U5BU5D_t385246372* ___v_20;
	// System.Int32[] Ionic.Zlib.InfTree::c
	Int32U5BU5D_t385246372* ___c_21;
	// System.Int32[] Ionic.Zlib.InfTree::r
	Int32U5BU5D_t385246372* ___r_22;
	// System.Int32[] Ionic.Zlib.InfTree::u
	Int32U5BU5D_t385246372* ___u_23;
	// System.Int32[] Ionic.Zlib.InfTree::x
	Int32U5BU5D_t385246372* ___x_24;

public:
	inline static int32_t get_offset_of_hn_19() { return static_cast<int32_t>(offsetof(InfTree_t3075871205, ___hn_19)); }
	inline Int32U5BU5D_t385246372* get_hn_19() const { return ___hn_19; }
	inline Int32U5BU5D_t385246372** get_address_of_hn_19() { return &___hn_19; }
	inline void set_hn_19(Int32U5BU5D_t385246372* value)
	{
		___hn_19 = value;
		Il2CppCodeGenWriteBarrier((&___hn_19), value);
	}

	inline static int32_t get_offset_of_v_20() { return static_cast<int32_t>(offsetof(InfTree_t3075871205, ___v_20)); }
	inline Int32U5BU5D_t385246372* get_v_20() const { return ___v_20; }
	inline Int32U5BU5D_t385246372** get_address_of_v_20() { return &___v_20; }
	inline void set_v_20(Int32U5BU5D_t385246372* value)
	{
		___v_20 = value;
		Il2CppCodeGenWriteBarrier((&___v_20), value);
	}

	inline static int32_t get_offset_of_c_21() { return static_cast<int32_t>(offsetof(InfTree_t3075871205, ___c_21)); }
	inline Int32U5BU5D_t385246372* get_c_21() const { return ___c_21; }
	inline Int32U5BU5D_t385246372** get_address_of_c_21() { return &___c_21; }
	inline void set_c_21(Int32U5BU5D_t385246372* value)
	{
		___c_21 = value;
		Il2CppCodeGenWriteBarrier((&___c_21), value);
	}

	inline static int32_t get_offset_of_r_22() { return static_cast<int32_t>(offsetof(InfTree_t3075871205, ___r_22)); }
	inline Int32U5BU5D_t385246372* get_r_22() const { return ___r_22; }
	inline Int32U5BU5D_t385246372** get_address_of_r_22() { return &___r_22; }
	inline void set_r_22(Int32U5BU5D_t385246372* value)
	{
		___r_22 = value;
		Il2CppCodeGenWriteBarrier((&___r_22), value);
	}

	inline static int32_t get_offset_of_u_23() { return static_cast<int32_t>(offsetof(InfTree_t3075871205, ___u_23)); }
	inline Int32U5BU5D_t385246372* get_u_23() const { return ___u_23; }
	inline Int32U5BU5D_t385246372** get_address_of_u_23() { return &___u_23; }
	inline void set_u_23(Int32U5BU5D_t385246372* value)
	{
		___u_23 = value;
		Il2CppCodeGenWriteBarrier((&___u_23), value);
	}

	inline static int32_t get_offset_of_x_24() { return static_cast<int32_t>(offsetof(InfTree_t3075871205, ___x_24)); }
	inline Int32U5BU5D_t385246372* get_x_24() const { return ___x_24; }
	inline Int32U5BU5D_t385246372** get_address_of_x_24() { return &___x_24; }
	inline void set_x_24(Int32U5BU5D_t385246372* value)
	{
		___x_24 = value;
		Il2CppCodeGenWriteBarrier((&___x_24), value);
	}
};

struct InfTree_t3075871205_StaticFields
{
public:
	// System.Int32[] Ionic.Zlib.InfTree::fixed_tl
	Int32U5BU5D_t385246372* ___fixed_tl_12;
	// System.Int32[] Ionic.Zlib.InfTree::fixed_td
	Int32U5BU5D_t385246372* ___fixed_td_13;
	// System.Int32[] Ionic.Zlib.InfTree::cplens
	Int32U5BU5D_t385246372* ___cplens_14;
	// System.Int32[] Ionic.Zlib.InfTree::cplext
	Int32U5BU5D_t385246372* ___cplext_15;
	// System.Int32[] Ionic.Zlib.InfTree::cpdist
	Int32U5BU5D_t385246372* ___cpdist_16;
	// System.Int32[] Ionic.Zlib.InfTree::cpdext
	Int32U5BU5D_t385246372* ___cpdext_17;

public:
	inline static int32_t get_offset_of_fixed_tl_12() { return static_cast<int32_t>(offsetof(InfTree_t3075871205_StaticFields, ___fixed_tl_12)); }
	inline Int32U5BU5D_t385246372* get_fixed_tl_12() const { return ___fixed_tl_12; }
	inline Int32U5BU5D_t385246372** get_address_of_fixed_tl_12() { return &___fixed_tl_12; }
	inline void set_fixed_tl_12(Int32U5BU5D_t385246372* value)
	{
		___fixed_tl_12 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_tl_12), value);
	}

	inline static int32_t get_offset_of_fixed_td_13() { return static_cast<int32_t>(offsetof(InfTree_t3075871205_StaticFields, ___fixed_td_13)); }
	inline Int32U5BU5D_t385246372* get_fixed_td_13() const { return ___fixed_td_13; }
	inline Int32U5BU5D_t385246372** get_address_of_fixed_td_13() { return &___fixed_td_13; }
	inline void set_fixed_td_13(Int32U5BU5D_t385246372* value)
	{
		___fixed_td_13 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_td_13), value);
	}

	inline static int32_t get_offset_of_cplens_14() { return static_cast<int32_t>(offsetof(InfTree_t3075871205_StaticFields, ___cplens_14)); }
	inline Int32U5BU5D_t385246372* get_cplens_14() const { return ___cplens_14; }
	inline Int32U5BU5D_t385246372** get_address_of_cplens_14() { return &___cplens_14; }
	inline void set_cplens_14(Int32U5BU5D_t385246372* value)
	{
		___cplens_14 = value;
		Il2CppCodeGenWriteBarrier((&___cplens_14), value);
	}

	inline static int32_t get_offset_of_cplext_15() { return static_cast<int32_t>(offsetof(InfTree_t3075871205_StaticFields, ___cplext_15)); }
	inline Int32U5BU5D_t385246372* get_cplext_15() const { return ___cplext_15; }
	inline Int32U5BU5D_t385246372** get_address_of_cplext_15() { return &___cplext_15; }
	inline void set_cplext_15(Int32U5BU5D_t385246372* value)
	{
		___cplext_15 = value;
		Il2CppCodeGenWriteBarrier((&___cplext_15), value);
	}

	inline static int32_t get_offset_of_cpdist_16() { return static_cast<int32_t>(offsetof(InfTree_t3075871205_StaticFields, ___cpdist_16)); }
	inline Int32U5BU5D_t385246372* get_cpdist_16() const { return ___cpdist_16; }
	inline Int32U5BU5D_t385246372** get_address_of_cpdist_16() { return &___cpdist_16; }
	inline void set_cpdist_16(Int32U5BU5D_t385246372* value)
	{
		___cpdist_16 = value;
		Il2CppCodeGenWriteBarrier((&___cpdist_16), value);
	}

	inline static int32_t get_offset_of_cpdext_17() { return static_cast<int32_t>(offsetof(InfTree_t3075871205_StaticFields, ___cpdext_17)); }
	inline Int32U5BU5D_t385246372* get_cpdext_17() const { return ___cpdext_17; }
	inline Int32U5BU5D_t385246372** get_address_of_cpdext_17() { return &___cpdext_17; }
	inline void set_cpdext_17(Int32U5BU5D_t385246372* value)
	{
		___cpdext_17 = value;
		Il2CppCodeGenWriteBarrier((&___cpdext_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFTREE_T3075871205_H
#ifndef LUMOSJSON_T1197320855_H
#define LUMOSJSON_T1197320855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosJson
struct  LumosJson_t1197320855  : public RuntimeObject
{
public:

public:
};

struct LumosJson_t1197320855_StaticFields
{
public:
	// System.Int32 LumosJson::lastErrorIndex
	int32_t ___lastErrorIndex_1;
	// System.String LumosJson::lastDecode
	String_t* ___lastDecode_2;

public:
	inline static int32_t get_offset_of_lastErrorIndex_1() { return static_cast<int32_t>(offsetof(LumosJson_t1197320855_StaticFields, ___lastErrorIndex_1)); }
	inline int32_t get_lastErrorIndex_1() const { return ___lastErrorIndex_1; }
	inline int32_t* get_address_of_lastErrorIndex_1() { return &___lastErrorIndex_1; }
	inline void set_lastErrorIndex_1(int32_t value)
	{
		___lastErrorIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastDecode_2() { return static_cast<int32_t>(offsetof(LumosJson_t1197320855_StaticFields, ___lastDecode_2)); }
	inline String_t* get_lastDecode_2() const { return ___lastDecode_2; }
	inline String_t** get_address_of_lastDecode_2() { return &___lastDecode_2; }
	inline void set_lastDecode_2(String_t* value)
	{
		___lastDecode_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastDecode_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSJSON_T1197320855_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef LUMOSANALYTICSSETUP_T3600549041_H
#define LUMOSANALYTICSSETUP_T3600549041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosAnalyticsSetup
struct  LumosAnalyticsSetup_t3600549041  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSANALYTICSSETUP_T3600549041_H
#ifndef LUMOSEVENTS_T3060038184_H
#define LUMOSEVENTS_T3060038184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosEvents
struct  LumosEvents_t3060038184  : public RuntimeObject
{
public:

public:
};

struct LumosEvents_t3060038184_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> LumosEvents::events
	Dictionary_2_t2650618762 * ___events_0;
	// System.Collections.Generic.HashSet`1<System.String> LumosEvents::unsentUniqueEvents
	HashSet_1_t412400163 * ___unsentUniqueEvents_1;
	// System.Action`1<System.Object> LumosEvents::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<System.Object> LumosEvents::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_events_0() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___events_0)); }
	inline Dictionary_2_t2650618762 * get_events_0() const { return ___events_0; }
	inline Dictionary_2_t2650618762 ** get_address_of_events_0() { return &___events_0; }
	inline void set_events_0(Dictionary_2_t2650618762 * value)
	{
		___events_0 = value;
		Il2CppCodeGenWriteBarrier((&___events_0), value);
	}

	inline static int32_t get_offset_of_unsentUniqueEvents_1() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___unsentUniqueEvents_1)); }
	inline HashSet_1_t412400163 * get_unsentUniqueEvents_1() const { return ___unsentUniqueEvents_1; }
	inline HashSet_1_t412400163 ** get_address_of_unsentUniqueEvents_1() { return &___unsentUniqueEvents_1; }
	inline void set_unsentUniqueEvents_1(HashSet_1_t412400163 * value)
	{
		___unsentUniqueEvents_1 = value;
		Il2CppCodeGenWriteBarrier((&___unsentUniqueEvents_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(LumosEvents_t3060038184_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSEVENTS_T3060038184_H
#ifndef U3CRECORDU3EC__ANONSTOREY0_T811208082_H
#define U3CRECORDU3EC__ANONSTOREY0_T811208082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosLocation/<Record>c__AnonStorey0
struct  U3CRecordU3Ec__AnonStorey0_t811208082  : public RuntimeObject
{
public:
	// System.String LumosLocation/<Record>c__AnonStorey0::prefsKey
	String_t* ___prefsKey_0;

public:
	inline static int32_t get_offset_of_prefsKey_0() { return static_cast<int32_t>(offsetof(U3CRecordU3Ec__AnonStorey0_t811208082, ___prefsKey_0)); }
	inline String_t* get_prefsKey_0() const { return ___prefsKey_0; }
	inline String_t** get_address_of_prefsKey_0() { return &___prefsKey_0; }
	inline void set_prefsKey_0(String_t* value)
	{
		___prefsKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefsKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECORDU3EC__ANONSTOREY0_T811208082_H
#ifndef LUMOSDIAGNOSTICSSETUP_T110199435_H
#define LUMOSDIAGNOSTICSSETUP_T110199435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosDiagnosticsSetup
struct  LumosDiagnosticsSetup_t110199435  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSDIAGNOSTICSSETUP_T110199435_H
#ifndef LUMOSLOCATION_T2816019121_H
#define LUMOSLOCATION_T2816019121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosLocation
struct  LumosLocation_t2816019121  : public RuntimeObject
{
public:

public:
};

struct LumosLocation_t2816019121_StaticFields
{
public:
	// System.Action`1<System.Object> LumosLocation::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LumosLocation_t2816019121_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSLOCATION_T2816019121_H
#ifndef U3CSENDQUEUEDDATAU3EC__ITERATOR0_T3572231512_H
#define U3CSENDQUEUEDDATAU3EC__ITERATOR0_T3572231512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lumos/<SendQueuedData>c__Iterator0
struct  U3CSendQueuedDataU3Ec__Iterator0_t3572231512  : public RuntimeObject
{
public:
	// System.Object Lumos/<SendQueuedData>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean Lumos/<SendQueuedData>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 Lumos/<SendQueuedData>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CSendQueuedDataU3Ec__Iterator0_t3572231512, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CSendQueuedDataU3Ec__Iterator0_t3572231512, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CSendQueuedDataU3Ec__Iterator0_t3572231512, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDQUEUEDDATAU3EC__ITERATOR0_T3572231512_H
#ifndef LUMOSLOGS_T761625864_H
#define LUMOSLOGS_T761625864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosLogs
struct  LumosLogs_t761625864  : public RuntimeObject
{
public:

public:
};

struct LumosLogs_t761625864_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> LumosLogs::toIgnore
	List_1_t3319525431 * ___toIgnore_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> LumosLogs::logs
	Dictionary_2_t2650618762 * ___logs_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.String> LumosLogs::typeLabels
	Dictionary_2_t1123770287 * ___typeLabels_2;
	// System.Action`1<System.Object> LumosLogs::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_3;
	// System.Action`1<System.Object> LumosLogs::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_4;

public:
	inline static int32_t get_offset_of_toIgnore_0() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___toIgnore_0)); }
	inline List_1_t3319525431 * get_toIgnore_0() const { return ___toIgnore_0; }
	inline List_1_t3319525431 ** get_address_of_toIgnore_0() { return &___toIgnore_0; }
	inline void set_toIgnore_0(List_1_t3319525431 * value)
	{
		___toIgnore_0 = value;
		Il2CppCodeGenWriteBarrier((&___toIgnore_0), value);
	}

	inline static int32_t get_offset_of_logs_1() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___logs_1)); }
	inline Dictionary_2_t2650618762 * get_logs_1() const { return ___logs_1; }
	inline Dictionary_2_t2650618762 ** get_address_of_logs_1() { return &___logs_1; }
	inline void set_logs_1(Dictionary_2_t2650618762 * value)
	{
		___logs_1 = value;
		Il2CppCodeGenWriteBarrier((&___logs_1), value);
	}

	inline static int32_t get_offset_of_typeLabels_2() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___typeLabels_2)); }
	inline Dictionary_2_t1123770287 * get_typeLabels_2() const { return ___typeLabels_2; }
	inline Dictionary_2_t1123770287 ** get_address_of_typeLabels_2() { return &___typeLabels_2; }
	inline void set_typeLabels_2(Dictionary_2_t1123770287 * value)
	{
		___typeLabels_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeLabels_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(LumosLogs_t761625864_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSLOGS_T761625864_H
#ifndef LUMOSFEEDBACK_T607479745_H
#define LUMOSFEEDBACK_T607479745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosFeedback
struct  LumosFeedback_t607479745  : public RuntimeObject
{
public:

public:
};

struct LumosFeedback_t607479745_StaticFields
{
public:
	// System.Action`1<System.Object> LumosFeedback::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`1<System.Object> LumosFeedback::<>f__am$cache1
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LumosFeedback_t607479745_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LumosFeedback_t607479745_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSFEEDBACK_T607479745_H
#ifndef LUMOSSPECS_T2754611293_H
#define LUMOSSPECS_T2754611293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosSpecs
struct  LumosSpecs_t2754611293  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSSPECS_T2754611293_H
#ifndef CRC16_T2217306282_H
#define CRC16_T2217306282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRC16
struct  CRC16_t2217306282  : public HashAlgorithm_t1432317219
{
public:
	// System.UInt16[] CRC16::_crc16Table
	UInt16U5BU5D_t3326319531* ____crc16Table_8;
	// System.UInt16 CRC16::_crc
	uint16_t ____crc_9;

public:
	inline static int32_t get_offset_of__crc16Table_8() { return static_cast<int32_t>(offsetof(CRC16_t2217306282, ____crc16Table_8)); }
	inline UInt16U5BU5D_t3326319531* get__crc16Table_8() const { return ____crc16Table_8; }
	inline UInt16U5BU5D_t3326319531** get_address_of__crc16Table_8() { return &____crc16Table_8; }
	inline void set__crc16Table_8(UInt16U5BU5D_t3326319531* value)
	{
		____crc16Table_8 = value;
		Il2CppCodeGenWriteBarrier((&____crc16Table_8), value);
	}

	inline static int32_t get_offset_of__crc_9() { return static_cast<int32_t>(offsetof(CRC16_t2217306282, ____crc_9)); }
	inline uint16_t get__crc_9() const { return ____crc_9; }
	inline uint16_t* get_address_of__crc_9() { return &____crc_9; }
	inline void set__crc_9(uint16_t value)
	{
		____crc_9 = value;
	}
};

struct CRC16_t2217306282_StaticFields
{
public:
	// System.UInt16 CRC16::DefaultPolynomial
	uint16_t ___DefaultPolynomial_4;
	// System.UInt16 CRC16::_allOnes
	uint16_t ____allOnes_5;
	// CRC16 CRC16::_defaultCRC
	CRC16_t2217306282 * ____defaultCRC_6;
	// System.Collections.Hashtable CRC16::_crc16TablesCache
	Hashtable_t1853889766 * ____crc16TablesCache_7;

public:
	inline static int32_t get_offset_of_DefaultPolynomial_4() { return static_cast<int32_t>(offsetof(CRC16_t2217306282_StaticFields, ___DefaultPolynomial_4)); }
	inline uint16_t get_DefaultPolynomial_4() const { return ___DefaultPolynomial_4; }
	inline uint16_t* get_address_of_DefaultPolynomial_4() { return &___DefaultPolynomial_4; }
	inline void set_DefaultPolynomial_4(uint16_t value)
	{
		___DefaultPolynomial_4 = value;
	}

	inline static int32_t get_offset_of__allOnes_5() { return static_cast<int32_t>(offsetof(CRC16_t2217306282_StaticFields, ____allOnes_5)); }
	inline uint16_t get__allOnes_5() const { return ____allOnes_5; }
	inline uint16_t* get_address_of__allOnes_5() { return &____allOnes_5; }
	inline void set__allOnes_5(uint16_t value)
	{
		____allOnes_5 = value;
	}

	inline static int32_t get_offset_of__defaultCRC_6() { return static_cast<int32_t>(offsetof(CRC16_t2217306282_StaticFields, ____defaultCRC_6)); }
	inline CRC16_t2217306282 * get__defaultCRC_6() const { return ____defaultCRC_6; }
	inline CRC16_t2217306282 ** get_address_of__defaultCRC_6() { return &____defaultCRC_6; }
	inline void set__defaultCRC_6(CRC16_t2217306282 * value)
	{
		____defaultCRC_6 = value;
		Il2CppCodeGenWriteBarrier((&____defaultCRC_6), value);
	}

	inline static int32_t get_offset_of__crc16TablesCache_7() { return static_cast<int32_t>(offsetof(CRC16_t2217306282_StaticFields, ____crc16TablesCache_7)); }
	inline Hashtable_t1853889766 * get__crc16TablesCache_7() const { return ____crc16TablesCache_7; }
	inline Hashtable_t1853889766 ** get_address_of__crc16TablesCache_7() { return &____crc16TablesCache_7; }
	inline void set__crc16TablesCache_7(Hashtable_t1853889766 * value)
	{
		____crc16TablesCache_7 = value;
		Il2CppCodeGenWriteBarrier((&____crc16TablesCache_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC16_T2217306282_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ZLIBEXCEPTION_T2135990123_H
#define ZLIBEXCEPTION_T2135990123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibException
struct  ZlibException_t2135990123  : public Exception_t
{
public:
	static const Il2CppGuid CLSID;

public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBEXCEPTION_T2135990123_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef ZLIBSTREAM_T1786730409_H
#define ZLIBSTREAM_T1786730409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibStream
struct  ZlibStream_t1786730409  : public Stream_t1273022909
{
public:
	// Ionic.Zlib.ZlibBaseStream Ionic.Zlib.ZlibStream::_baseStream
	ZlibBaseStream_t1591821133 * ____baseStream_1;
	// System.Boolean Ionic.Zlib.ZlibStream::_disposed
	bool ____disposed_2;

public:
	inline static int32_t get_offset_of__baseStream_1() { return static_cast<int32_t>(offsetof(ZlibStream_t1786730409, ____baseStream_1)); }
	inline ZlibBaseStream_t1591821133 * get__baseStream_1() const { return ____baseStream_1; }
	inline ZlibBaseStream_t1591821133 ** get_address_of__baseStream_1() { return &____baseStream_1; }
	inline void set__baseStream_1(ZlibBaseStream_t1591821133 * value)
	{
		____baseStream_1 = value;
		Il2CppCodeGenWriteBarrier((&____baseStream_1), value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(ZlibStream_t1786730409, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBSTREAM_T1786730409_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DEFLATESTREAM_T979017812_H
#define DEFLATESTREAM_T979017812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.DeflateStream
struct  DeflateStream_t979017812  : public Stream_t1273022909
{
public:
	// Ionic.Zlib.ZlibBaseStream Ionic.Zlib.DeflateStream::_baseStream
	ZlibBaseStream_t1591821133 * ____baseStream_1;
	// System.IO.Stream Ionic.Zlib.DeflateStream::_innerStream
	Stream_t1273022909 * ____innerStream_2;
	// System.Boolean Ionic.Zlib.DeflateStream::_disposed
	bool ____disposed_3;

public:
	inline static int32_t get_offset_of__baseStream_1() { return static_cast<int32_t>(offsetof(DeflateStream_t979017812, ____baseStream_1)); }
	inline ZlibBaseStream_t1591821133 * get__baseStream_1() const { return ____baseStream_1; }
	inline ZlibBaseStream_t1591821133 ** get_address_of__baseStream_1() { return &____baseStream_1; }
	inline void set__baseStream_1(ZlibBaseStream_t1591821133 * value)
	{
		____baseStream_1 = value;
		Il2CppCodeGenWriteBarrier((&____baseStream_1), value);
	}

	inline static int32_t get_offset_of__innerStream_2() { return static_cast<int32_t>(offsetof(DeflateStream_t979017812, ____innerStream_2)); }
	inline Stream_t1273022909 * get__innerStream_2() const { return ____innerStream_2; }
	inline Stream_t1273022909 ** get_address_of__innerStream_2() { return &____innerStream_2; }
	inline void set__innerStream_2(Stream_t1273022909 * value)
	{
		____innerStream_2 = value;
		Il2CppCodeGenWriteBarrier((&____innerStream_2), value);
	}

	inline static int32_t get_offset_of__disposed_3() { return static_cast<int32_t>(offsetof(DeflateStream_t979017812, ____disposed_3)); }
	inline bool get__disposed_3() const { return ____disposed_3; }
	inline bool* get_address_of__disposed_3() { return &____disposed_3; }
	inline void set__disposed_3(bool value)
	{
		____disposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAM_T979017812_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef ENUMERATOR_T913802012_H
#define ENUMERATOR_T913802012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t913802012 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3319525431 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___l_0)); }
	inline List_1_t3319525431 * get_l_0() const { return ___l_0; }
	inline List_1_t3319525431 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3319525431 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T913802012_H
#ifndef CRC32_T1834969254_H
#define CRC32_T1834969254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CRC32
struct  CRC32_t1834969254  : public HashAlgorithm_t1432317219
{
public:
	// System.UInt32[] CRC32::_crc32Table
	UInt32U5BU5D_t2770800703* ____crc32Table_8;
	// System.UInt32 CRC32::_crc
	uint32_t ____crc_9;

public:
	inline static int32_t get_offset_of__crc32Table_8() { return static_cast<int32_t>(offsetof(CRC32_t1834969254, ____crc32Table_8)); }
	inline UInt32U5BU5D_t2770800703* get__crc32Table_8() const { return ____crc32Table_8; }
	inline UInt32U5BU5D_t2770800703** get_address_of__crc32Table_8() { return &____crc32Table_8; }
	inline void set__crc32Table_8(UInt32U5BU5D_t2770800703* value)
	{
		____crc32Table_8 = value;
		Il2CppCodeGenWriteBarrier((&____crc32Table_8), value);
	}

	inline static int32_t get_offset_of__crc_9() { return static_cast<int32_t>(offsetof(CRC32_t1834969254, ____crc_9)); }
	inline uint32_t get__crc_9() const { return ____crc_9; }
	inline uint32_t* get_address_of__crc_9() { return &____crc_9; }
	inline void set__crc_9(uint32_t value)
	{
		____crc_9 = value;
	}
};

struct CRC32_t1834969254_StaticFields
{
public:
	// System.UInt32 CRC32::DefaultPolynomial
	uint32_t ___DefaultPolynomial_4;
	// System.UInt32 CRC32::_allOnes
	uint32_t ____allOnes_5;
	// CRC32 CRC32::_defaultCRC
	CRC32_t1834969254 * ____defaultCRC_6;
	// System.Collections.Hashtable CRC32::_crc32TablesCache
	Hashtable_t1853889766 * ____crc32TablesCache_7;

public:
	inline static int32_t get_offset_of_DefaultPolynomial_4() { return static_cast<int32_t>(offsetof(CRC32_t1834969254_StaticFields, ___DefaultPolynomial_4)); }
	inline uint32_t get_DefaultPolynomial_4() const { return ___DefaultPolynomial_4; }
	inline uint32_t* get_address_of_DefaultPolynomial_4() { return &___DefaultPolynomial_4; }
	inline void set_DefaultPolynomial_4(uint32_t value)
	{
		___DefaultPolynomial_4 = value;
	}

	inline static int32_t get_offset_of__allOnes_5() { return static_cast<int32_t>(offsetof(CRC32_t1834969254_StaticFields, ____allOnes_5)); }
	inline uint32_t get__allOnes_5() const { return ____allOnes_5; }
	inline uint32_t* get_address_of__allOnes_5() { return &____allOnes_5; }
	inline void set__allOnes_5(uint32_t value)
	{
		____allOnes_5 = value;
	}

	inline static int32_t get_offset_of__defaultCRC_6() { return static_cast<int32_t>(offsetof(CRC32_t1834969254_StaticFields, ____defaultCRC_6)); }
	inline CRC32_t1834969254 * get__defaultCRC_6() const { return ____defaultCRC_6; }
	inline CRC32_t1834969254 ** get_address_of__defaultCRC_6() { return &____defaultCRC_6; }
	inline void set__defaultCRC_6(CRC32_t1834969254 * value)
	{
		____defaultCRC_6 = value;
		Il2CppCodeGenWriteBarrier((&____defaultCRC_6), value);
	}

	inline static int32_t get_offset_of__crc32TablesCache_7() { return static_cast<int32_t>(offsetof(CRC32_t1834969254_StaticFields, ____crc32TablesCache_7)); }
	inline Hashtable_t1853889766 * get__crc32TablesCache_7() const { return ____crc32TablesCache_7; }
	inline Hashtable_t1853889766 ** get_address_of__crc32TablesCache_7() { return &____crc32TablesCache_7; }
	inline void set__crc32TablesCache_7(Hashtable_t1853889766 * value)
	{
		____crc32TablesCache_7 = value;
		Il2CppCodeGenWriteBarrier((&____crc32TablesCache_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T1834969254_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef HTTPEXCEPTION_T4127885469_H
#define HTTPEXCEPTION_T4127885469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HTTP.HTTPException
struct  HTTPException_t4127885469  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPEXCEPTION_T4127885469_H
#ifndef CRCCALCULATORSTREAM_T1469656756_H
#define CRCCALCULATORSTREAM_T1469656756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.CrcCalculatorStream
struct  CrcCalculatorStream_t1469656756  : public Stream_t1273022909
{
public:
	// System.IO.Stream Ionic.Zlib.CrcCalculatorStream::_innerStream
	Stream_t1273022909 * ____innerStream_2;
	// Ionic.Zlib.CRC32 Ionic.Zlib.CrcCalculatorStream::_Crc32
	CRC32_t1348930856 * ____Crc32_3;
	// System.Int64 Ionic.Zlib.CrcCalculatorStream::_lengthLimit
	int64_t ____lengthLimit_4;
	// System.Boolean Ionic.Zlib.CrcCalculatorStream::_leaveOpen
	bool ____leaveOpen_5;

public:
	inline static int32_t get_offset_of__innerStream_2() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t1469656756, ____innerStream_2)); }
	inline Stream_t1273022909 * get__innerStream_2() const { return ____innerStream_2; }
	inline Stream_t1273022909 ** get_address_of__innerStream_2() { return &____innerStream_2; }
	inline void set__innerStream_2(Stream_t1273022909 * value)
	{
		____innerStream_2 = value;
		Il2CppCodeGenWriteBarrier((&____innerStream_2), value);
	}

	inline static int32_t get_offset_of__Crc32_3() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t1469656756, ____Crc32_3)); }
	inline CRC32_t1348930856 * get__Crc32_3() const { return ____Crc32_3; }
	inline CRC32_t1348930856 ** get_address_of__Crc32_3() { return &____Crc32_3; }
	inline void set__Crc32_3(CRC32_t1348930856 * value)
	{
		____Crc32_3 = value;
		Il2CppCodeGenWriteBarrier((&____Crc32_3), value);
	}

	inline static int32_t get_offset_of__lengthLimit_4() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t1469656756, ____lengthLimit_4)); }
	inline int64_t get__lengthLimit_4() const { return ____lengthLimit_4; }
	inline int64_t* get_address_of__lengthLimit_4() { return &____lengthLimit_4; }
	inline void set__lengthLimit_4(int64_t value)
	{
		____lengthLimit_4 = value;
	}

	inline static int32_t get_offset_of__leaveOpen_5() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t1469656756, ____leaveOpen_5)); }
	inline bool get__leaveOpen_5() const { return ____leaveOpen_5; }
	inline bool* get_address_of__leaveOpen_5() { return &____leaveOpen_5; }
	inline void set__leaveOpen_5(bool value)
	{
		____leaveOpen_5 = value;
	}
};

struct CrcCalculatorStream_t1469656756_StaticFields
{
public:
	// System.Int64 Ionic.Zlib.CrcCalculatorStream::UnsetLengthLimit
	int64_t ___UnsetLengthLimit_1;

public:
	inline static int32_t get_offset_of_UnsetLengthLimit_1() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t1469656756_StaticFields, ___UnsetLengthLimit_1)); }
	inline int64_t get_UnsetLengthLimit_1() const { return ___UnsetLengthLimit_1; }
	inline int64_t* get_address_of_UnsetLengthLimit_1() { return &___UnsetLengthLimit_1; }
	inline void set_UnsetLengthLimit_1(int64_t value)
	{
		___UnsetLengthLimit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRCCALCULATORSTREAM_T1469656756_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef TOKEN_T776369722_H
#define TOKEN_T776369722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosJson/TOKEN
struct  TOKEN_t776369722 
{
public:
	// System.Int32 LumosJson/TOKEN::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TOKEN_t776369722, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T776369722_H
#ifndef NUGAEVENT_T4041358949_H
#define NUGAEVENT_T4041358949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuGAEvent
struct  NuGAEvent_t4041358949  : public RuntimeObject
{
public:
	// System.String NuGAEvent::<Object>k__BackingField
	String_t* ___U3CObjectU3Ek__BackingField_0;
	// System.String NuGAEvent::<Action>k__BackingField
	String_t* ___U3CActionU3Ek__BackingField_1;
	// System.String NuGAEvent::<Label>k__BackingField
	String_t* ___U3CLabelU3Ek__BackingField_2;
	// System.Nullable`1<System.Int32> NuGAEvent::<Value>k__BackingField
	Nullable_1_t378540539  ___U3CValueU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NuGAEvent_t4041358949, ___U3CObjectU3Ek__BackingField_0)); }
	inline String_t* get_U3CObjectU3Ek__BackingField_0() const { return ___U3CObjectU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CObjectU3Ek__BackingField_0() { return &___U3CObjectU3Ek__BackingField_0; }
	inline void set_U3CObjectU3Ek__BackingField_0(String_t* value)
	{
		___U3CObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NuGAEvent_t4041358949, ___U3CActionU3Ek__BackingField_1)); }
	inline String_t* get_U3CActionU3Ek__BackingField_1() const { return ___U3CActionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CActionU3Ek__BackingField_1() { return &___U3CActionU3Ek__BackingField_1; }
	inline void set_U3CActionU3Ek__BackingField_1(String_t* value)
	{
		___U3CActionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CLabelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NuGAEvent_t4041358949, ___U3CLabelU3Ek__BackingField_2)); }
	inline String_t* get_U3CLabelU3Ek__BackingField_2() const { return ___U3CLabelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CLabelU3Ek__BackingField_2() { return &___U3CLabelU3Ek__BackingField_2; }
	inline void set_U3CLabelU3Ek__BackingField_2(String_t* value)
	{
		___U3CLabelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLabelU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(NuGAEvent_t4041358949, ___U3CValueU3Ek__BackingField_3)); }
	inline Nullable_1_t378540539  get_U3CValueU3Ek__BackingField_3() const { return ___U3CValueU3Ek__BackingField_3; }
	inline Nullable_1_t378540539 * get_address_of_U3CValueU3Ek__BackingField_3() { return &___U3CValueU3Ek__BackingField_3; }
	inline void set_U3CValueU3Ek__BackingField_3(Nullable_1_t378540539  value)
	{
		___U3CValueU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUGAEVENT_T4041358949_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255365  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255365_H
#ifndef U3CSENDASYNCWEBREQUESTU3EC__ITERATOR0_T2096338983_H
#define U3CSENDASYNCWEBREQUESTU3EC__ITERATOR0_T2096338983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuGARequest/<SendAsyncWebRequest>c__Iterator0
struct  U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> NuGARequest/<SendAsyncWebRequest>c__Iterator0::_urlList
	List_1_t3319525431 * ____urlList_0;
	// System.Collections.Generic.List`1/Enumerator<System.String> NuGARequest/<SendAsyncWebRequest>c__Iterator0::$locvar0
	Enumerator_t913802012  ___U24locvar0_1;
	// System.String NuGARequest/<SendAsyncWebRequest>c__Iterator0::<url>__1
	String_t* ___U3CurlU3E__1_2;
	// HTTP.Request NuGARequest/<SendAsyncWebRequest>c__Iterator0::<r>__2
	Request_t1151508915 * ___U3CrU3E__2_3;
	// System.Object NuGARequest/<SendAsyncWebRequest>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean NuGARequest/<SendAsyncWebRequest>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 NuGARequest/<SendAsyncWebRequest>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of__urlList_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ____urlList_0)); }
	inline List_1_t3319525431 * get__urlList_0() const { return ____urlList_0; }
	inline List_1_t3319525431 ** get_address_of__urlList_0() { return &____urlList_0; }
	inline void set__urlList_0(List_1_t3319525431 * value)
	{
		____urlList_0 = value;
		Il2CppCodeGenWriteBarrier((&____urlList_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ___U24locvar0_1)); }
	inline Enumerator_t913802012  get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline Enumerator_t913802012 * get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(Enumerator_t913802012  value)
	{
		___U24locvar0_1 = value;
	}

	inline static int32_t get_offset_of_U3CurlU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ___U3CurlU3E__1_2)); }
	inline String_t* get_U3CurlU3E__1_2() const { return ___U3CurlU3E__1_2; }
	inline String_t** get_address_of_U3CurlU3E__1_2() { return &___U3CurlU3E__1_2; }
	inline void set_U3CurlU3E__1_2(String_t* value)
	{
		___U3CurlU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CrU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ___U3CrU3E__2_3)); }
	inline Request_t1151508915 * get_U3CrU3E__2_3() const { return ___U3CrU3E__2_3; }
	inline Request_t1151508915 ** get_address_of_U3CrU3E__2_3() { return &___U3CrU3E__2_3; }
	inline void set_U3CrU3E__2_3(Request_t1151508915 * value)
	{
		___U3CrU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCWEBREQUESTU3EC__ITERATOR0_T2096338983_H
#ifndef STREAMMODE_T645432565_H
#define STREAMMODE_T645432565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibBaseStream/StreamMode
struct  StreamMode_t645432565 
{
public:
	// System.Int32 Ionic.Zlib.ZlibBaseStream/StreamMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamMode_t645432565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMMODE_T645432565_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t2806799626 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline Collider2D_t2806799626 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t2806799626 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t2806799626 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_pinvoke
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t2279581989_marshaled_com
{
	Vector2_t2156229523  ___m_Centroid_0;
	Vector2_t2156229523  ___m_Point_1;
	Vector2_t2156229523  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t2806799626 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef TRACEBITS_T962761545_H
#define TRACEBITS_T962761545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ParallelDeflateOutputStream/TraceBits
struct  TraceBits_t962761545 
{
public:
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream/TraceBits::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TraceBits_t962761545, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEBITS_T962761545_H
#ifndef COMPRESSIONMODE_T1439136530_H
#define COMPRESSIONMODE_T1439136530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.CompressionMode
struct  CompressionMode_t1439136530 
{
public:
	// System.Int32 Ionic.Zlib.CompressionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMode_t1439136530, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMODE_T1439136530_H
#ifndef COMPRESSIONSTRATEGY_T2822174634_H
#define COMPRESSIONSTRATEGY_T2822174634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.CompressionStrategy
struct  CompressionStrategy_t2822174634 
{
public:
	// System.Int32 Ionic.Zlib.CompressionStrategy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionStrategy_t2822174634, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONSTRATEGY_T2822174634_H
#ifndef COMPRESSIONLEVEL_T957220238_H
#define COMPRESSIONLEVEL_T957220238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.CompressionLevel
struct  CompressionLevel_t957220238 
{
public:
	// System.Int32 Ionic.Zlib.CompressionLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionLevel_t957220238, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONLEVEL_T957220238_H
#ifndef STATUS_T137489413_H
#define STATUS_T137489413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.WorkItem/Status
struct  Status_t137489413 
{
public:
	// System.Int32 Ionic.Zlib.WorkItem/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t137489413, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T137489413_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef INFLATEMANAGERMODE_T696478391_H
#define INFLATEMANAGERMODE_T696478391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InflateManager/InflateManagerMode
struct  InflateManagerMode_t696478391 
{
public:
	// System.Int32 Ionic.Zlib.InflateManager/InflateManagerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InflateManagerMode_t696478391, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEMANAGERMODE_T696478391_H
#ifndef LUMOSFEEDBACKGUI_T1268982120_H
#define LUMOSFEEDBACKGUI_T1268982120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosFeedbackGUI
struct  LumosFeedbackGUI_t1268982120  : public RuntimeObject
{
public:

public:
};

struct LumosFeedbackGUI_t1268982120_StaticFields
{
public:
	// LumosFeedbackGUI/CloseHandler LumosFeedbackGUI::windowClosed
	CloseHandler_t719506259 * ___windowClosed_0;
	// UnityEngine.GUISkin LumosFeedbackGUI::<skin>k__BackingField
	GUISkin_t1244372282 * ___U3CskinU3Ek__BackingField_1;
	// UnityEngine.Rect LumosFeedbackGUI::windowRect
	Rect_t2360479859  ___windowRect_4;
	// System.Boolean LumosFeedbackGUI::visible
	bool ___visible_5;
	// System.Boolean LumosFeedbackGUI::sentMessage
	bool ___sentMessage_6;
	// System.String LumosFeedbackGUI::email
	String_t* ___email_7;
	// System.String LumosFeedbackGUI::message
	String_t* ___message_8;
	// System.String LumosFeedbackGUI::category
	String_t* ___category_9;
	// UnityEngine.GUI/WindowFunction LumosFeedbackGUI::<>f__mg$cache0
	WindowFunction_t3146511083 * ___U3CU3Ef__mgU24cache0_10;

public:
	inline static int32_t get_offset_of_windowClosed_0() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___windowClosed_0)); }
	inline CloseHandler_t719506259 * get_windowClosed_0() const { return ___windowClosed_0; }
	inline CloseHandler_t719506259 ** get_address_of_windowClosed_0() { return &___windowClosed_0; }
	inline void set_windowClosed_0(CloseHandler_t719506259 * value)
	{
		___windowClosed_0 = value;
		Il2CppCodeGenWriteBarrier((&___windowClosed_0), value);
	}

	inline static int32_t get_offset_of_U3CskinU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___U3CskinU3Ek__BackingField_1)); }
	inline GUISkin_t1244372282 * get_U3CskinU3Ek__BackingField_1() const { return ___U3CskinU3Ek__BackingField_1; }
	inline GUISkin_t1244372282 ** get_address_of_U3CskinU3Ek__BackingField_1() { return &___U3CskinU3Ek__BackingField_1; }
	inline void set_U3CskinU3Ek__BackingField_1(GUISkin_t1244372282 * value)
	{
		___U3CskinU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CskinU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_windowRect_4() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___windowRect_4)); }
	inline Rect_t2360479859  get_windowRect_4() const { return ___windowRect_4; }
	inline Rect_t2360479859 * get_address_of_windowRect_4() { return &___windowRect_4; }
	inline void set_windowRect_4(Rect_t2360479859  value)
	{
		___windowRect_4 = value;
	}

	inline static int32_t get_offset_of_visible_5() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___visible_5)); }
	inline bool get_visible_5() const { return ___visible_5; }
	inline bool* get_address_of_visible_5() { return &___visible_5; }
	inline void set_visible_5(bool value)
	{
		___visible_5 = value;
	}

	inline static int32_t get_offset_of_sentMessage_6() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___sentMessage_6)); }
	inline bool get_sentMessage_6() const { return ___sentMessage_6; }
	inline bool* get_address_of_sentMessage_6() { return &___sentMessage_6; }
	inline void set_sentMessage_6(bool value)
	{
		___sentMessage_6 = value;
	}

	inline static int32_t get_offset_of_email_7() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___email_7)); }
	inline String_t* get_email_7() const { return ___email_7; }
	inline String_t** get_address_of_email_7() { return &___email_7; }
	inline void set_email_7(String_t* value)
	{
		___email_7 = value;
		Il2CppCodeGenWriteBarrier((&___email_7), value);
	}

	inline static int32_t get_offset_of_message_8() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___message_8)); }
	inline String_t* get_message_8() const { return ___message_8; }
	inline String_t** get_address_of_message_8() { return &___message_8; }
	inline void set_message_8(String_t* value)
	{
		___message_8 = value;
		Il2CppCodeGenWriteBarrier((&___message_8), value);
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(LumosFeedbackGUI_t1268982120_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline WindowFunction_t3146511083 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline WindowFunction_t3146511083 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(WindowFunction_t3146511083 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSFEEDBACKGUI_T1268982120_H
#ifndef INFLATEBLOCKMODE_T2135077436_H
#define INFLATEBLOCKMODE_T2135077436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InflateBlocks/InflateBlockMode
struct  InflateBlockMode_t2135077436 
{
public:
	// System.Int32 Ionic.Zlib.InflateBlocks/InflateBlockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InflateBlockMode_t2135077436, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEBLOCKMODE_T2135077436_H
#ifndef ZLIBSTREAMFLAVOR_T704535502_H
#define ZLIBSTREAMFLAVOR_T704535502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibStreamFlavor
struct  ZlibStreamFlavor_t704535502 
{
public:
	// System.Int32 Ionic.Zlib.ZlibStreamFlavor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZlibStreamFlavor_t704535502, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBSTREAMFLAVOR_T704535502_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DEFLATEFLAVOR_T738395756_H
#define DEFLATEFLAVOR_T738395756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.DeflateFlavor
struct  DeflateFlavor_t738395756 
{
public:
	// System.Int32 Ionic.Zlib.DeflateFlavor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeflateFlavor_t738395756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEFLAVOR_T738395756_H
#ifndef BLOCKSTATE_T410253493_H
#define BLOCKSTATE_T410253493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.BlockState
struct  BlockState_t410253493 
{
public:
	// System.Int32 Ionic.Zlib.BlockState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockState_t410253493, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKSTATE_T410253493_H
#ifndef FLUSHTYPE_T1474040364_H
#define FLUSHTYPE_T1474040364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.FlushType
struct  FlushType_t1474040364 
{
public:
	// System.Int32 Ionic.Zlib.FlushType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FlushType_t1474040364, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLUSHTYPE_T1474040364_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ZLIBCODEC_T3653667923_H
#define ZLIBCODEC_T3653667923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibCodec
struct  ZlibCodec_t3653667923  : public RuntimeObject
{
public:
	static const Il2CppGuid CLSID;

public:
	// System.Byte[] Ionic.Zlib.ZlibCodec::InputBuffer
	ByteU5BU5D_t4116647657* ___InputBuffer_0;
	// System.Int32 Ionic.Zlib.ZlibCodec::NextIn
	int32_t ___NextIn_1;
	// System.Int32 Ionic.Zlib.ZlibCodec::AvailableBytesIn
	int32_t ___AvailableBytesIn_2;
	// System.Int64 Ionic.Zlib.ZlibCodec::TotalBytesIn
	int64_t ___TotalBytesIn_3;
	// System.Byte[] Ionic.Zlib.ZlibCodec::OutputBuffer
	ByteU5BU5D_t4116647657* ___OutputBuffer_4;
	// System.Int32 Ionic.Zlib.ZlibCodec::NextOut
	int32_t ___NextOut_5;
	// System.Int32 Ionic.Zlib.ZlibCodec::AvailableBytesOut
	int32_t ___AvailableBytesOut_6;
	// System.Int64 Ionic.Zlib.ZlibCodec::TotalBytesOut
	int64_t ___TotalBytesOut_7;
	// System.String Ionic.Zlib.ZlibCodec::Message
	String_t* ___Message_8;
	// Ionic.Zlib.DeflateManager Ionic.Zlib.ZlibCodec::dstate
	DeflateManager_t740995428 * ___dstate_9;
	// Ionic.Zlib.InflateManager Ionic.Zlib.ZlibCodec::istate
	InflateManager_t3407211616 * ___istate_10;
	// System.UInt32 Ionic.Zlib.ZlibCodec::_Adler32
	uint32_t ____Adler32_11;
	// Ionic.Zlib.CompressionLevel Ionic.Zlib.ZlibCodec::CompressLevel
	int32_t ___CompressLevel_12;
	// System.Int32 Ionic.Zlib.ZlibCodec::WindowBits
	int32_t ___WindowBits_13;
	// Ionic.Zlib.CompressionStrategy Ionic.Zlib.ZlibCodec::Strategy
	int32_t ___Strategy_14;

public:
	inline static int32_t get_offset_of_InputBuffer_0() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___InputBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_InputBuffer_0() const { return ___InputBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_InputBuffer_0() { return &___InputBuffer_0; }
	inline void set_InputBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___InputBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___InputBuffer_0), value);
	}

	inline static int32_t get_offset_of_NextIn_1() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___NextIn_1)); }
	inline int32_t get_NextIn_1() const { return ___NextIn_1; }
	inline int32_t* get_address_of_NextIn_1() { return &___NextIn_1; }
	inline void set_NextIn_1(int32_t value)
	{
		___NextIn_1 = value;
	}

	inline static int32_t get_offset_of_AvailableBytesIn_2() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___AvailableBytesIn_2)); }
	inline int32_t get_AvailableBytesIn_2() const { return ___AvailableBytesIn_2; }
	inline int32_t* get_address_of_AvailableBytesIn_2() { return &___AvailableBytesIn_2; }
	inline void set_AvailableBytesIn_2(int32_t value)
	{
		___AvailableBytesIn_2 = value;
	}

	inline static int32_t get_offset_of_TotalBytesIn_3() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___TotalBytesIn_3)); }
	inline int64_t get_TotalBytesIn_3() const { return ___TotalBytesIn_3; }
	inline int64_t* get_address_of_TotalBytesIn_3() { return &___TotalBytesIn_3; }
	inline void set_TotalBytesIn_3(int64_t value)
	{
		___TotalBytesIn_3 = value;
	}

	inline static int32_t get_offset_of_OutputBuffer_4() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___OutputBuffer_4)); }
	inline ByteU5BU5D_t4116647657* get_OutputBuffer_4() const { return ___OutputBuffer_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_OutputBuffer_4() { return &___OutputBuffer_4; }
	inline void set_OutputBuffer_4(ByteU5BU5D_t4116647657* value)
	{
		___OutputBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___OutputBuffer_4), value);
	}

	inline static int32_t get_offset_of_NextOut_5() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___NextOut_5)); }
	inline int32_t get_NextOut_5() const { return ___NextOut_5; }
	inline int32_t* get_address_of_NextOut_5() { return &___NextOut_5; }
	inline void set_NextOut_5(int32_t value)
	{
		___NextOut_5 = value;
	}

	inline static int32_t get_offset_of_AvailableBytesOut_6() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___AvailableBytesOut_6)); }
	inline int32_t get_AvailableBytesOut_6() const { return ___AvailableBytesOut_6; }
	inline int32_t* get_address_of_AvailableBytesOut_6() { return &___AvailableBytesOut_6; }
	inline void set_AvailableBytesOut_6(int32_t value)
	{
		___AvailableBytesOut_6 = value;
	}

	inline static int32_t get_offset_of_TotalBytesOut_7() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___TotalBytesOut_7)); }
	inline int64_t get_TotalBytesOut_7() const { return ___TotalBytesOut_7; }
	inline int64_t* get_address_of_TotalBytesOut_7() { return &___TotalBytesOut_7; }
	inline void set_TotalBytesOut_7(int64_t value)
	{
		___TotalBytesOut_7 = value;
	}

	inline static int32_t get_offset_of_Message_8() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___Message_8)); }
	inline String_t* get_Message_8() const { return ___Message_8; }
	inline String_t** get_address_of_Message_8() { return &___Message_8; }
	inline void set_Message_8(String_t* value)
	{
		___Message_8 = value;
		Il2CppCodeGenWriteBarrier((&___Message_8), value);
	}

	inline static int32_t get_offset_of_dstate_9() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___dstate_9)); }
	inline DeflateManager_t740995428 * get_dstate_9() const { return ___dstate_9; }
	inline DeflateManager_t740995428 ** get_address_of_dstate_9() { return &___dstate_9; }
	inline void set_dstate_9(DeflateManager_t740995428 * value)
	{
		___dstate_9 = value;
		Il2CppCodeGenWriteBarrier((&___dstate_9), value);
	}

	inline static int32_t get_offset_of_istate_10() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___istate_10)); }
	inline InflateManager_t3407211616 * get_istate_10() const { return ___istate_10; }
	inline InflateManager_t3407211616 ** get_address_of_istate_10() { return &___istate_10; }
	inline void set_istate_10(InflateManager_t3407211616 * value)
	{
		___istate_10 = value;
		Il2CppCodeGenWriteBarrier((&___istate_10), value);
	}

	inline static int32_t get_offset_of__Adler32_11() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ____Adler32_11)); }
	inline uint32_t get__Adler32_11() const { return ____Adler32_11; }
	inline uint32_t* get_address_of__Adler32_11() { return &____Adler32_11; }
	inline void set__Adler32_11(uint32_t value)
	{
		____Adler32_11 = value;
	}

	inline static int32_t get_offset_of_CompressLevel_12() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___CompressLevel_12)); }
	inline int32_t get_CompressLevel_12() const { return ___CompressLevel_12; }
	inline int32_t* get_address_of_CompressLevel_12() { return &___CompressLevel_12; }
	inline void set_CompressLevel_12(int32_t value)
	{
		___CompressLevel_12 = value;
	}

	inline static int32_t get_offset_of_WindowBits_13() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___WindowBits_13)); }
	inline int32_t get_WindowBits_13() const { return ___WindowBits_13; }
	inline int32_t* get_address_of_WindowBits_13() { return &___WindowBits_13; }
	inline void set_WindowBits_13(int32_t value)
	{
		___WindowBits_13 = value;
	}

	inline static int32_t get_offset_of_Strategy_14() { return static_cast<int32_t>(offsetof(ZlibCodec_t3653667923, ___Strategy_14)); }
	inline int32_t get_Strategy_14() const { return ___Strategy_14; }
	inline int32_t* get_address_of_Strategy_14() { return &___Strategy_14; }
	inline void set_Strategy_14(int32_t value)
	{
		___Strategy_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBCODEC_T3653667923_H
#ifndef INFLATEBLOCKS_T175244798_H
#define INFLATEBLOCKS_T175244798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InflateBlocks
struct  InflateBlocks_t175244798  : public RuntimeObject
{
public:
	// Ionic.Zlib.InflateBlocks/InflateBlockMode Ionic.Zlib.InflateBlocks::mode
	int32_t ___mode_2;
	// System.Int32 Ionic.Zlib.InflateBlocks::left
	int32_t ___left_3;
	// System.Int32 Ionic.Zlib.InflateBlocks::table
	int32_t ___table_4;
	// System.Int32 Ionic.Zlib.InflateBlocks::index
	int32_t ___index_5;
	// System.Int32[] Ionic.Zlib.InflateBlocks::blens
	Int32U5BU5D_t385246372* ___blens_6;
	// System.Int32[] Ionic.Zlib.InflateBlocks::bb
	Int32U5BU5D_t385246372* ___bb_7;
	// System.Int32[] Ionic.Zlib.InflateBlocks::tb
	Int32U5BU5D_t385246372* ___tb_8;
	// Ionic.Zlib.InflateCodes Ionic.Zlib.InflateBlocks::codes
	InflateCodes_t3874557824 * ___codes_9;
	// System.Int32 Ionic.Zlib.InflateBlocks::last
	int32_t ___last_10;
	// Ionic.Zlib.ZlibCodec Ionic.Zlib.InflateBlocks::_codec
	ZlibCodec_t3653667923 * ____codec_11;
	// System.Int32 Ionic.Zlib.InflateBlocks::bitk
	int32_t ___bitk_12;
	// System.Int32 Ionic.Zlib.InflateBlocks::bitb
	int32_t ___bitb_13;
	// System.Int32[] Ionic.Zlib.InflateBlocks::hufts
	Int32U5BU5D_t385246372* ___hufts_14;
	// System.Byte[] Ionic.Zlib.InflateBlocks::window
	ByteU5BU5D_t4116647657* ___window_15;
	// System.Int32 Ionic.Zlib.InflateBlocks::end
	int32_t ___end_16;
	// System.Int32 Ionic.Zlib.InflateBlocks::readAt
	int32_t ___readAt_17;
	// System.Int32 Ionic.Zlib.InflateBlocks::writeAt
	int32_t ___writeAt_18;
	// System.Object Ionic.Zlib.InflateBlocks::checkfn
	RuntimeObject * ___checkfn_19;
	// System.UInt32 Ionic.Zlib.InflateBlocks::check
	uint32_t ___check_20;
	// Ionic.Zlib.InfTree Ionic.Zlib.InflateBlocks::inftree
	InfTree_t3075871205 * ___inftree_21;

public:
	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_left_3() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___left_3)); }
	inline int32_t get_left_3() const { return ___left_3; }
	inline int32_t* get_address_of_left_3() { return &___left_3; }
	inline void set_left_3(int32_t value)
	{
		___left_3 = value;
	}

	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___table_4)); }
	inline int32_t get_table_4() const { return ___table_4; }
	inline int32_t* get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(int32_t value)
	{
		___table_4 = value;
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_blens_6() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___blens_6)); }
	inline Int32U5BU5D_t385246372* get_blens_6() const { return ___blens_6; }
	inline Int32U5BU5D_t385246372** get_address_of_blens_6() { return &___blens_6; }
	inline void set_blens_6(Int32U5BU5D_t385246372* value)
	{
		___blens_6 = value;
		Il2CppCodeGenWriteBarrier((&___blens_6), value);
	}

	inline static int32_t get_offset_of_bb_7() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___bb_7)); }
	inline Int32U5BU5D_t385246372* get_bb_7() const { return ___bb_7; }
	inline Int32U5BU5D_t385246372** get_address_of_bb_7() { return &___bb_7; }
	inline void set_bb_7(Int32U5BU5D_t385246372* value)
	{
		___bb_7 = value;
		Il2CppCodeGenWriteBarrier((&___bb_7), value);
	}

	inline static int32_t get_offset_of_tb_8() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___tb_8)); }
	inline Int32U5BU5D_t385246372* get_tb_8() const { return ___tb_8; }
	inline Int32U5BU5D_t385246372** get_address_of_tb_8() { return &___tb_8; }
	inline void set_tb_8(Int32U5BU5D_t385246372* value)
	{
		___tb_8 = value;
		Il2CppCodeGenWriteBarrier((&___tb_8), value);
	}

	inline static int32_t get_offset_of_codes_9() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___codes_9)); }
	inline InflateCodes_t3874557824 * get_codes_9() const { return ___codes_9; }
	inline InflateCodes_t3874557824 ** get_address_of_codes_9() { return &___codes_9; }
	inline void set_codes_9(InflateCodes_t3874557824 * value)
	{
		___codes_9 = value;
		Il2CppCodeGenWriteBarrier((&___codes_9), value);
	}

	inline static int32_t get_offset_of_last_10() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___last_10)); }
	inline int32_t get_last_10() const { return ___last_10; }
	inline int32_t* get_address_of_last_10() { return &___last_10; }
	inline void set_last_10(int32_t value)
	{
		___last_10 = value;
	}

	inline static int32_t get_offset_of__codec_11() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ____codec_11)); }
	inline ZlibCodec_t3653667923 * get__codec_11() const { return ____codec_11; }
	inline ZlibCodec_t3653667923 ** get_address_of__codec_11() { return &____codec_11; }
	inline void set__codec_11(ZlibCodec_t3653667923 * value)
	{
		____codec_11 = value;
		Il2CppCodeGenWriteBarrier((&____codec_11), value);
	}

	inline static int32_t get_offset_of_bitk_12() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___bitk_12)); }
	inline int32_t get_bitk_12() const { return ___bitk_12; }
	inline int32_t* get_address_of_bitk_12() { return &___bitk_12; }
	inline void set_bitk_12(int32_t value)
	{
		___bitk_12 = value;
	}

	inline static int32_t get_offset_of_bitb_13() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___bitb_13)); }
	inline int32_t get_bitb_13() const { return ___bitb_13; }
	inline int32_t* get_address_of_bitb_13() { return &___bitb_13; }
	inline void set_bitb_13(int32_t value)
	{
		___bitb_13 = value;
	}

	inline static int32_t get_offset_of_hufts_14() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___hufts_14)); }
	inline Int32U5BU5D_t385246372* get_hufts_14() const { return ___hufts_14; }
	inline Int32U5BU5D_t385246372** get_address_of_hufts_14() { return &___hufts_14; }
	inline void set_hufts_14(Int32U5BU5D_t385246372* value)
	{
		___hufts_14 = value;
		Il2CppCodeGenWriteBarrier((&___hufts_14), value);
	}

	inline static int32_t get_offset_of_window_15() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___window_15)); }
	inline ByteU5BU5D_t4116647657* get_window_15() const { return ___window_15; }
	inline ByteU5BU5D_t4116647657** get_address_of_window_15() { return &___window_15; }
	inline void set_window_15(ByteU5BU5D_t4116647657* value)
	{
		___window_15 = value;
		Il2CppCodeGenWriteBarrier((&___window_15), value);
	}

	inline static int32_t get_offset_of_end_16() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___end_16)); }
	inline int32_t get_end_16() const { return ___end_16; }
	inline int32_t* get_address_of_end_16() { return &___end_16; }
	inline void set_end_16(int32_t value)
	{
		___end_16 = value;
	}

	inline static int32_t get_offset_of_readAt_17() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___readAt_17)); }
	inline int32_t get_readAt_17() const { return ___readAt_17; }
	inline int32_t* get_address_of_readAt_17() { return &___readAt_17; }
	inline void set_readAt_17(int32_t value)
	{
		___readAt_17 = value;
	}

	inline static int32_t get_offset_of_writeAt_18() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___writeAt_18)); }
	inline int32_t get_writeAt_18() const { return ___writeAt_18; }
	inline int32_t* get_address_of_writeAt_18() { return &___writeAt_18; }
	inline void set_writeAt_18(int32_t value)
	{
		___writeAt_18 = value;
	}

	inline static int32_t get_offset_of_checkfn_19() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___checkfn_19)); }
	inline RuntimeObject * get_checkfn_19() const { return ___checkfn_19; }
	inline RuntimeObject ** get_address_of_checkfn_19() { return &___checkfn_19; }
	inline void set_checkfn_19(RuntimeObject * value)
	{
		___checkfn_19 = value;
		Il2CppCodeGenWriteBarrier((&___checkfn_19), value);
	}

	inline static int32_t get_offset_of_check_20() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___check_20)); }
	inline uint32_t get_check_20() const { return ___check_20; }
	inline uint32_t* get_address_of_check_20() { return &___check_20; }
	inline void set_check_20(uint32_t value)
	{
		___check_20 = value;
	}

	inline static int32_t get_offset_of_inftree_21() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798, ___inftree_21)); }
	inline InfTree_t3075871205 * get_inftree_21() const { return ___inftree_21; }
	inline InfTree_t3075871205 ** get_address_of_inftree_21() { return &___inftree_21; }
	inline void set_inftree_21(InfTree_t3075871205 * value)
	{
		___inftree_21 = value;
		Il2CppCodeGenWriteBarrier((&___inftree_21), value);
	}
};

struct InflateBlocks_t175244798_StaticFields
{
public:
	// System.Int32[] Ionic.Zlib.InflateBlocks::border
	Int32U5BU5D_t385246372* ___border_1;

public:
	inline static int32_t get_offset_of_border_1() { return static_cast<int32_t>(offsetof(InflateBlocks_t175244798_StaticFields, ___border_1)); }
	inline Int32U5BU5D_t385246372* get_border_1() const { return ___border_1; }
	inline Int32U5BU5D_t385246372** get_address_of_border_1() { return &___border_1; }
	inline void set_border_1(Int32U5BU5D_t385246372* value)
	{
		___border_1 = value;
		Il2CppCodeGenWriteBarrier((&___border_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEBLOCKS_T175244798_H
#ifndef DEFLATEMANAGER_T740995428_H
#define DEFLATEMANAGER_T740995428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.DeflateManager
struct  DeflateManager_t740995428  : public RuntimeObject
{
public:
	// Ionic.Zlib.DeflateManager/CompressFunc Ionic.Zlib.DeflateManager::DeflateFunction
	CompressFunc_t3594827279 * ___DeflateFunction_2;
	// Ionic.Zlib.ZlibCodec Ionic.Zlib.DeflateManager::_codec
	ZlibCodec_t3653667923 * ____codec_21;
	// System.Int32 Ionic.Zlib.DeflateManager::status
	int32_t ___status_22;
	// System.Byte[] Ionic.Zlib.DeflateManager::pending
	ByteU5BU5D_t4116647657* ___pending_23;
	// System.Int32 Ionic.Zlib.DeflateManager::nextPending
	int32_t ___nextPending_24;
	// System.Int32 Ionic.Zlib.DeflateManager::pendingCount
	int32_t ___pendingCount_25;
	// System.SByte Ionic.Zlib.DeflateManager::data_type
	int8_t ___data_type_26;
	// System.Int32 Ionic.Zlib.DeflateManager::last_flush
	int32_t ___last_flush_27;
	// System.Int32 Ionic.Zlib.DeflateManager::w_size
	int32_t ___w_size_28;
	// System.Int32 Ionic.Zlib.DeflateManager::w_bits
	int32_t ___w_bits_29;
	// System.Int32 Ionic.Zlib.DeflateManager::w_mask
	int32_t ___w_mask_30;
	// System.Byte[] Ionic.Zlib.DeflateManager::window
	ByteU5BU5D_t4116647657* ___window_31;
	// System.Int32 Ionic.Zlib.DeflateManager::window_size
	int32_t ___window_size_32;
	// System.Int16[] Ionic.Zlib.DeflateManager::prev
	Int16U5BU5D_t3686840178* ___prev_33;
	// System.Int16[] Ionic.Zlib.DeflateManager::head
	Int16U5BU5D_t3686840178* ___head_34;
	// System.Int32 Ionic.Zlib.DeflateManager::ins_h
	int32_t ___ins_h_35;
	// System.Int32 Ionic.Zlib.DeflateManager::hash_size
	int32_t ___hash_size_36;
	// System.Int32 Ionic.Zlib.DeflateManager::hash_bits
	int32_t ___hash_bits_37;
	// System.Int32 Ionic.Zlib.DeflateManager::hash_mask
	int32_t ___hash_mask_38;
	// System.Int32 Ionic.Zlib.DeflateManager::hash_shift
	int32_t ___hash_shift_39;
	// System.Int32 Ionic.Zlib.DeflateManager::block_start
	int32_t ___block_start_40;
	// Ionic.Zlib.DeflateManager/Config Ionic.Zlib.DeflateManager::config
	Config_t1905746077 * ___config_41;
	// System.Int32 Ionic.Zlib.DeflateManager::match_length
	int32_t ___match_length_42;
	// System.Int32 Ionic.Zlib.DeflateManager::prev_match
	int32_t ___prev_match_43;
	// System.Int32 Ionic.Zlib.DeflateManager::match_available
	int32_t ___match_available_44;
	// System.Int32 Ionic.Zlib.DeflateManager::strstart
	int32_t ___strstart_45;
	// System.Int32 Ionic.Zlib.DeflateManager::match_start
	int32_t ___match_start_46;
	// System.Int32 Ionic.Zlib.DeflateManager::lookahead
	int32_t ___lookahead_47;
	// System.Int32 Ionic.Zlib.DeflateManager::prev_length
	int32_t ___prev_length_48;
	// Ionic.Zlib.CompressionLevel Ionic.Zlib.DeflateManager::compressionLevel
	int32_t ___compressionLevel_49;
	// Ionic.Zlib.CompressionStrategy Ionic.Zlib.DeflateManager::compressionStrategy
	int32_t ___compressionStrategy_50;
	// System.Int16[] Ionic.Zlib.DeflateManager::dyn_ltree
	Int16U5BU5D_t3686840178* ___dyn_ltree_51;
	// System.Int16[] Ionic.Zlib.DeflateManager::dyn_dtree
	Int16U5BU5D_t3686840178* ___dyn_dtree_52;
	// System.Int16[] Ionic.Zlib.DeflateManager::bl_tree
	Int16U5BU5D_t3686840178* ___bl_tree_53;
	// Ionic.Zlib.Tree Ionic.Zlib.DeflateManager::treeLiterals
	Tree_t526993154 * ___treeLiterals_54;
	// Ionic.Zlib.Tree Ionic.Zlib.DeflateManager::treeDistances
	Tree_t526993154 * ___treeDistances_55;
	// Ionic.Zlib.Tree Ionic.Zlib.DeflateManager::treeBitLengths
	Tree_t526993154 * ___treeBitLengths_56;
	// System.Int16[] Ionic.Zlib.DeflateManager::bl_count
	Int16U5BU5D_t3686840178* ___bl_count_57;
	// System.Int32[] Ionic.Zlib.DeflateManager::heap
	Int32U5BU5D_t385246372* ___heap_58;
	// System.Int32 Ionic.Zlib.DeflateManager::heap_len
	int32_t ___heap_len_59;
	// System.Int32 Ionic.Zlib.DeflateManager::heap_max
	int32_t ___heap_max_60;
	// System.SByte[] Ionic.Zlib.DeflateManager::depth
	SByteU5BU5D_t2651576203* ___depth_61;
	// System.Int32 Ionic.Zlib.DeflateManager::_lengthOffset
	int32_t ____lengthOffset_62;
	// System.Int32 Ionic.Zlib.DeflateManager::lit_bufsize
	int32_t ___lit_bufsize_63;
	// System.Int32 Ionic.Zlib.DeflateManager::last_lit
	int32_t ___last_lit_64;
	// System.Int32 Ionic.Zlib.DeflateManager::_distanceOffset
	int32_t ____distanceOffset_65;
	// System.Int32 Ionic.Zlib.DeflateManager::opt_len
	int32_t ___opt_len_66;
	// System.Int32 Ionic.Zlib.DeflateManager::static_len
	int32_t ___static_len_67;
	// System.Int32 Ionic.Zlib.DeflateManager::matches
	int32_t ___matches_68;
	// System.Int32 Ionic.Zlib.DeflateManager::last_eob_len
	int32_t ___last_eob_len_69;
	// System.Int16 Ionic.Zlib.DeflateManager::bi_buf
	int16_t ___bi_buf_70;
	// System.Int32 Ionic.Zlib.DeflateManager::bi_valid
	int32_t ___bi_valid_71;
	// System.Boolean Ionic.Zlib.DeflateManager::Rfc1950BytesEmitted
	bool ___Rfc1950BytesEmitted_72;
	// System.Boolean Ionic.Zlib.DeflateManager::_WantRfc1950HeaderBytes
	bool ____WantRfc1950HeaderBytes_73;

public:
	inline static int32_t get_offset_of_DeflateFunction_2() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___DeflateFunction_2)); }
	inline CompressFunc_t3594827279 * get_DeflateFunction_2() const { return ___DeflateFunction_2; }
	inline CompressFunc_t3594827279 ** get_address_of_DeflateFunction_2() { return &___DeflateFunction_2; }
	inline void set_DeflateFunction_2(CompressFunc_t3594827279 * value)
	{
		___DeflateFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___DeflateFunction_2), value);
	}

	inline static int32_t get_offset_of__codec_21() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ____codec_21)); }
	inline ZlibCodec_t3653667923 * get__codec_21() const { return ____codec_21; }
	inline ZlibCodec_t3653667923 ** get_address_of__codec_21() { return &____codec_21; }
	inline void set__codec_21(ZlibCodec_t3653667923 * value)
	{
		____codec_21 = value;
		Il2CppCodeGenWriteBarrier((&____codec_21), value);
	}

	inline static int32_t get_offset_of_status_22() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___status_22)); }
	inline int32_t get_status_22() const { return ___status_22; }
	inline int32_t* get_address_of_status_22() { return &___status_22; }
	inline void set_status_22(int32_t value)
	{
		___status_22 = value;
	}

	inline static int32_t get_offset_of_pending_23() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___pending_23)); }
	inline ByteU5BU5D_t4116647657* get_pending_23() const { return ___pending_23; }
	inline ByteU5BU5D_t4116647657** get_address_of_pending_23() { return &___pending_23; }
	inline void set_pending_23(ByteU5BU5D_t4116647657* value)
	{
		___pending_23 = value;
		Il2CppCodeGenWriteBarrier((&___pending_23), value);
	}

	inline static int32_t get_offset_of_nextPending_24() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___nextPending_24)); }
	inline int32_t get_nextPending_24() const { return ___nextPending_24; }
	inline int32_t* get_address_of_nextPending_24() { return &___nextPending_24; }
	inline void set_nextPending_24(int32_t value)
	{
		___nextPending_24 = value;
	}

	inline static int32_t get_offset_of_pendingCount_25() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___pendingCount_25)); }
	inline int32_t get_pendingCount_25() const { return ___pendingCount_25; }
	inline int32_t* get_address_of_pendingCount_25() { return &___pendingCount_25; }
	inline void set_pendingCount_25(int32_t value)
	{
		___pendingCount_25 = value;
	}

	inline static int32_t get_offset_of_data_type_26() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___data_type_26)); }
	inline int8_t get_data_type_26() const { return ___data_type_26; }
	inline int8_t* get_address_of_data_type_26() { return &___data_type_26; }
	inline void set_data_type_26(int8_t value)
	{
		___data_type_26 = value;
	}

	inline static int32_t get_offset_of_last_flush_27() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___last_flush_27)); }
	inline int32_t get_last_flush_27() const { return ___last_flush_27; }
	inline int32_t* get_address_of_last_flush_27() { return &___last_flush_27; }
	inline void set_last_flush_27(int32_t value)
	{
		___last_flush_27 = value;
	}

	inline static int32_t get_offset_of_w_size_28() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___w_size_28)); }
	inline int32_t get_w_size_28() const { return ___w_size_28; }
	inline int32_t* get_address_of_w_size_28() { return &___w_size_28; }
	inline void set_w_size_28(int32_t value)
	{
		___w_size_28 = value;
	}

	inline static int32_t get_offset_of_w_bits_29() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___w_bits_29)); }
	inline int32_t get_w_bits_29() const { return ___w_bits_29; }
	inline int32_t* get_address_of_w_bits_29() { return &___w_bits_29; }
	inline void set_w_bits_29(int32_t value)
	{
		___w_bits_29 = value;
	}

	inline static int32_t get_offset_of_w_mask_30() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___w_mask_30)); }
	inline int32_t get_w_mask_30() const { return ___w_mask_30; }
	inline int32_t* get_address_of_w_mask_30() { return &___w_mask_30; }
	inline void set_w_mask_30(int32_t value)
	{
		___w_mask_30 = value;
	}

	inline static int32_t get_offset_of_window_31() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___window_31)); }
	inline ByteU5BU5D_t4116647657* get_window_31() const { return ___window_31; }
	inline ByteU5BU5D_t4116647657** get_address_of_window_31() { return &___window_31; }
	inline void set_window_31(ByteU5BU5D_t4116647657* value)
	{
		___window_31 = value;
		Il2CppCodeGenWriteBarrier((&___window_31), value);
	}

	inline static int32_t get_offset_of_window_size_32() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___window_size_32)); }
	inline int32_t get_window_size_32() const { return ___window_size_32; }
	inline int32_t* get_address_of_window_size_32() { return &___window_size_32; }
	inline void set_window_size_32(int32_t value)
	{
		___window_size_32 = value;
	}

	inline static int32_t get_offset_of_prev_33() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___prev_33)); }
	inline Int16U5BU5D_t3686840178* get_prev_33() const { return ___prev_33; }
	inline Int16U5BU5D_t3686840178** get_address_of_prev_33() { return &___prev_33; }
	inline void set_prev_33(Int16U5BU5D_t3686840178* value)
	{
		___prev_33 = value;
		Il2CppCodeGenWriteBarrier((&___prev_33), value);
	}

	inline static int32_t get_offset_of_head_34() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___head_34)); }
	inline Int16U5BU5D_t3686840178* get_head_34() const { return ___head_34; }
	inline Int16U5BU5D_t3686840178** get_address_of_head_34() { return &___head_34; }
	inline void set_head_34(Int16U5BU5D_t3686840178* value)
	{
		___head_34 = value;
		Il2CppCodeGenWriteBarrier((&___head_34), value);
	}

	inline static int32_t get_offset_of_ins_h_35() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___ins_h_35)); }
	inline int32_t get_ins_h_35() const { return ___ins_h_35; }
	inline int32_t* get_address_of_ins_h_35() { return &___ins_h_35; }
	inline void set_ins_h_35(int32_t value)
	{
		___ins_h_35 = value;
	}

	inline static int32_t get_offset_of_hash_size_36() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___hash_size_36)); }
	inline int32_t get_hash_size_36() const { return ___hash_size_36; }
	inline int32_t* get_address_of_hash_size_36() { return &___hash_size_36; }
	inline void set_hash_size_36(int32_t value)
	{
		___hash_size_36 = value;
	}

	inline static int32_t get_offset_of_hash_bits_37() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___hash_bits_37)); }
	inline int32_t get_hash_bits_37() const { return ___hash_bits_37; }
	inline int32_t* get_address_of_hash_bits_37() { return &___hash_bits_37; }
	inline void set_hash_bits_37(int32_t value)
	{
		___hash_bits_37 = value;
	}

	inline static int32_t get_offset_of_hash_mask_38() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___hash_mask_38)); }
	inline int32_t get_hash_mask_38() const { return ___hash_mask_38; }
	inline int32_t* get_address_of_hash_mask_38() { return &___hash_mask_38; }
	inline void set_hash_mask_38(int32_t value)
	{
		___hash_mask_38 = value;
	}

	inline static int32_t get_offset_of_hash_shift_39() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___hash_shift_39)); }
	inline int32_t get_hash_shift_39() const { return ___hash_shift_39; }
	inline int32_t* get_address_of_hash_shift_39() { return &___hash_shift_39; }
	inline void set_hash_shift_39(int32_t value)
	{
		___hash_shift_39 = value;
	}

	inline static int32_t get_offset_of_block_start_40() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___block_start_40)); }
	inline int32_t get_block_start_40() const { return ___block_start_40; }
	inline int32_t* get_address_of_block_start_40() { return &___block_start_40; }
	inline void set_block_start_40(int32_t value)
	{
		___block_start_40 = value;
	}

	inline static int32_t get_offset_of_config_41() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___config_41)); }
	inline Config_t1905746077 * get_config_41() const { return ___config_41; }
	inline Config_t1905746077 ** get_address_of_config_41() { return &___config_41; }
	inline void set_config_41(Config_t1905746077 * value)
	{
		___config_41 = value;
		Il2CppCodeGenWriteBarrier((&___config_41), value);
	}

	inline static int32_t get_offset_of_match_length_42() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___match_length_42)); }
	inline int32_t get_match_length_42() const { return ___match_length_42; }
	inline int32_t* get_address_of_match_length_42() { return &___match_length_42; }
	inline void set_match_length_42(int32_t value)
	{
		___match_length_42 = value;
	}

	inline static int32_t get_offset_of_prev_match_43() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___prev_match_43)); }
	inline int32_t get_prev_match_43() const { return ___prev_match_43; }
	inline int32_t* get_address_of_prev_match_43() { return &___prev_match_43; }
	inline void set_prev_match_43(int32_t value)
	{
		___prev_match_43 = value;
	}

	inline static int32_t get_offset_of_match_available_44() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___match_available_44)); }
	inline int32_t get_match_available_44() const { return ___match_available_44; }
	inline int32_t* get_address_of_match_available_44() { return &___match_available_44; }
	inline void set_match_available_44(int32_t value)
	{
		___match_available_44 = value;
	}

	inline static int32_t get_offset_of_strstart_45() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___strstart_45)); }
	inline int32_t get_strstart_45() const { return ___strstart_45; }
	inline int32_t* get_address_of_strstart_45() { return &___strstart_45; }
	inline void set_strstart_45(int32_t value)
	{
		___strstart_45 = value;
	}

	inline static int32_t get_offset_of_match_start_46() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___match_start_46)); }
	inline int32_t get_match_start_46() const { return ___match_start_46; }
	inline int32_t* get_address_of_match_start_46() { return &___match_start_46; }
	inline void set_match_start_46(int32_t value)
	{
		___match_start_46 = value;
	}

	inline static int32_t get_offset_of_lookahead_47() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___lookahead_47)); }
	inline int32_t get_lookahead_47() const { return ___lookahead_47; }
	inline int32_t* get_address_of_lookahead_47() { return &___lookahead_47; }
	inline void set_lookahead_47(int32_t value)
	{
		___lookahead_47 = value;
	}

	inline static int32_t get_offset_of_prev_length_48() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___prev_length_48)); }
	inline int32_t get_prev_length_48() const { return ___prev_length_48; }
	inline int32_t* get_address_of_prev_length_48() { return &___prev_length_48; }
	inline void set_prev_length_48(int32_t value)
	{
		___prev_length_48 = value;
	}

	inline static int32_t get_offset_of_compressionLevel_49() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___compressionLevel_49)); }
	inline int32_t get_compressionLevel_49() const { return ___compressionLevel_49; }
	inline int32_t* get_address_of_compressionLevel_49() { return &___compressionLevel_49; }
	inline void set_compressionLevel_49(int32_t value)
	{
		___compressionLevel_49 = value;
	}

	inline static int32_t get_offset_of_compressionStrategy_50() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___compressionStrategy_50)); }
	inline int32_t get_compressionStrategy_50() const { return ___compressionStrategy_50; }
	inline int32_t* get_address_of_compressionStrategy_50() { return &___compressionStrategy_50; }
	inline void set_compressionStrategy_50(int32_t value)
	{
		___compressionStrategy_50 = value;
	}

	inline static int32_t get_offset_of_dyn_ltree_51() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___dyn_ltree_51)); }
	inline Int16U5BU5D_t3686840178* get_dyn_ltree_51() const { return ___dyn_ltree_51; }
	inline Int16U5BU5D_t3686840178** get_address_of_dyn_ltree_51() { return &___dyn_ltree_51; }
	inline void set_dyn_ltree_51(Int16U5BU5D_t3686840178* value)
	{
		___dyn_ltree_51 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_ltree_51), value);
	}

	inline static int32_t get_offset_of_dyn_dtree_52() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___dyn_dtree_52)); }
	inline Int16U5BU5D_t3686840178* get_dyn_dtree_52() const { return ___dyn_dtree_52; }
	inline Int16U5BU5D_t3686840178** get_address_of_dyn_dtree_52() { return &___dyn_dtree_52; }
	inline void set_dyn_dtree_52(Int16U5BU5D_t3686840178* value)
	{
		___dyn_dtree_52 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_dtree_52), value);
	}

	inline static int32_t get_offset_of_bl_tree_53() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___bl_tree_53)); }
	inline Int16U5BU5D_t3686840178* get_bl_tree_53() const { return ___bl_tree_53; }
	inline Int16U5BU5D_t3686840178** get_address_of_bl_tree_53() { return &___bl_tree_53; }
	inline void set_bl_tree_53(Int16U5BU5D_t3686840178* value)
	{
		___bl_tree_53 = value;
		Il2CppCodeGenWriteBarrier((&___bl_tree_53), value);
	}

	inline static int32_t get_offset_of_treeLiterals_54() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___treeLiterals_54)); }
	inline Tree_t526993154 * get_treeLiterals_54() const { return ___treeLiterals_54; }
	inline Tree_t526993154 ** get_address_of_treeLiterals_54() { return &___treeLiterals_54; }
	inline void set_treeLiterals_54(Tree_t526993154 * value)
	{
		___treeLiterals_54 = value;
		Il2CppCodeGenWriteBarrier((&___treeLiterals_54), value);
	}

	inline static int32_t get_offset_of_treeDistances_55() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___treeDistances_55)); }
	inline Tree_t526993154 * get_treeDistances_55() const { return ___treeDistances_55; }
	inline Tree_t526993154 ** get_address_of_treeDistances_55() { return &___treeDistances_55; }
	inline void set_treeDistances_55(Tree_t526993154 * value)
	{
		___treeDistances_55 = value;
		Il2CppCodeGenWriteBarrier((&___treeDistances_55), value);
	}

	inline static int32_t get_offset_of_treeBitLengths_56() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___treeBitLengths_56)); }
	inline Tree_t526993154 * get_treeBitLengths_56() const { return ___treeBitLengths_56; }
	inline Tree_t526993154 ** get_address_of_treeBitLengths_56() { return &___treeBitLengths_56; }
	inline void set_treeBitLengths_56(Tree_t526993154 * value)
	{
		___treeBitLengths_56 = value;
		Il2CppCodeGenWriteBarrier((&___treeBitLengths_56), value);
	}

	inline static int32_t get_offset_of_bl_count_57() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___bl_count_57)); }
	inline Int16U5BU5D_t3686840178* get_bl_count_57() const { return ___bl_count_57; }
	inline Int16U5BU5D_t3686840178** get_address_of_bl_count_57() { return &___bl_count_57; }
	inline void set_bl_count_57(Int16U5BU5D_t3686840178* value)
	{
		___bl_count_57 = value;
		Il2CppCodeGenWriteBarrier((&___bl_count_57), value);
	}

	inline static int32_t get_offset_of_heap_58() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___heap_58)); }
	inline Int32U5BU5D_t385246372* get_heap_58() const { return ___heap_58; }
	inline Int32U5BU5D_t385246372** get_address_of_heap_58() { return &___heap_58; }
	inline void set_heap_58(Int32U5BU5D_t385246372* value)
	{
		___heap_58 = value;
		Il2CppCodeGenWriteBarrier((&___heap_58), value);
	}

	inline static int32_t get_offset_of_heap_len_59() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___heap_len_59)); }
	inline int32_t get_heap_len_59() const { return ___heap_len_59; }
	inline int32_t* get_address_of_heap_len_59() { return &___heap_len_59; }
	inline void set_heap_len_59(int32_t value)
	{
		___heap_len_59 = value;
	}

	inline static int32_t get_offset_of_heap_max_60() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___heap_max_60)); }
	inline int32_t get_heap_max_60() const { return ___heap_max_60; }
	inline int32_t* get_address_of_heap_max_60() { return &___heap_max_60; }
	inline void set_heap_max_60(int32_t value)
	{
		___heap_max_60 = value;
	}

	inline static int32_t get_offset_of_depth_61() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___depth_61)); }
	inline SByteU5BU5D_t2651576203* get_depth_61() const { return ___depth_61; }
	inline SByteU5BU5D_t2651576203** get_address_of_depth_61() { return &___depth_61; }
	inline void set_depth_61(SByteU5BU5D_t2651576203* value)
	{
		___depth_61 = value;
		Il2CppCodeGenWriteBarrier((&___depth_61), value);
	}

	inline static int32_t get_offset_of__lengthOffset_62() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ____lengthOffset_62)); }
	inline int32_t get__lengthOffset_62() const { return ____lengthOffset_62; }
	inline int32_t* get_address_of__lengthOffset_62() { return &____lengthOffset_62; }
	inline void set__lengthOffset_62(int32_t value)
	{
		____lengthOffset_62 = value;
	}

	inline static int32_t get_offset_of_lit_bufsize_63() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___lit_bufsize_63)); }
	inline int32_t get_lit_bufsize_63() const { return ___lit_bufsize_63; }
	inline int32_t* get_address_of_lit_bufsize_63() { return &___lit_bufsize_63; }
	inline void set_lit_bufsize_63(int32_t value)
	{
		___lit_bufsize_63 = value;
	}

	inline static int32_t get_offset_of_last_lit_64() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___last_lit_64)); }
	inline int32_t get_last_lit_64() const { return ___last_lit_64; }
	inline int32_t* get_address_of_last_lit_64() { return &___last_lit_64; }
	inline void set_last_lit_64(int32_t value)
	{
		___last_lit_64 = value;
	}

	inline static int32_t get_offset_of__distanceOffset_65() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ____distanceOffset_65)); }
	inline int32_t get__distanceOffset_65() const { return ____distanceOffset_65; }
	inline int32_t* get_address_of__distanceOffset_65() { return &____distanceOffset_65; }
	inline void set__distanceOffset_65(int32_t value)
	{
		____distanceOffset_65 = value;
	}

	inline static int32_t get_offset_of_opt_len_66() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___opt_len_66)); }
	inline int32_t get_opt_len_66() const { return ___opt_len_66; }
	inline int32_t* get_address_of_opt_len_66() { return &___opt_len_66; }
	inline void set_opt_len_66(int32_t value)
	{
		___opt_len_66 = value;
	}

	inline static int32_t get_offset_of_static_len_67() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___static_len_67)); }
	inline int32_t get_static_len_67() const { return ___static_len_67; }
	inline int32_t* get_address_of_static_len_67() { return &___static_len_67; }
	inline void set_static_len_67(int32_t value)
	{
		___static_len_67 = value;
	}

	inline static int32_t get_offset_of_matches_68() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___matches_68)); }
	inline int32_t get_matches_68() const { return ___matches_68; }
	inline int32_t* get_address_of_matches_68() { return &___matches_68; }
	inline void set_matches_68(int32_t value)
	{
		___matches_68 = value;
	}

	inline static int32_t get_offset_of_last_eob_len_69() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___last_eob_len_69)); }
	inline int32_t get_last_eob_len_69() const { return ___last_eob_len_69; }
	inline int32_t* get_address_of_last_eob_len_69() { return &___last_eob_len_69; }
	inline void set_last_eob_len_69(int32_t value)
	{
		___last_eob_len_69 = value;
	}

	inline static int32_t get_offset_of_bi_buf_70() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___bi_buf_70)); }
	inline int16_t get_bi_buf_70() const { return ___bi_buf_70; }
	inline int16_t* get_address_of_bi_buf_70() { return &___bi_buf_70; }
	inline void set_bi_buf_70(int16_t value)
	{
		___bi_buf_70 = value;
	}

	inline static int32_t get_offset_of_bi_valid_71() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___bi_valid_71)); }
	inline int32_t get_bi_valid_71() const { return ___bi_valid_71; }
	inline int32_t* get_address_of_bi_valid_71() { return &___bi_valid_71; }
	inline void set_bi_valid_71(int32_t value)
	{
		___bi_valid_71 = value;
	}

	inline static int32_t get_offset_of_Rfc1950BytesEmitted_72() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ___Rfc1950BytesEmitted_72)); }
	inline bool get_Rfc1950BytesEmitted_72() const { return ___Rfc1950BytesEmitted_72; }
	inline bool* get_address_of_Rfc1950BytesEmitted_72() { return &___Rfc1950BytesEmitted_72; }
	inline void set_Rfc1950BytesEmitted_72(bool value)
	{
		___Rfc1950BytesEmitted_72 = value;
	}

	inline static int32_t get_offset_of__WantRfc1950HeaderBytes_73() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428, ____WantRfc1950HeaderBytes_73)); }
	inline bool get__WantRfc1950HeaderBytes_73() const { return ____WantRfc1950HeaderBytes_73; }
	inline bool* get_address_of__WantRfc1950HeaderBytes_73() { return &____WantRfc1950HeaderBytes_73; }
	inline void set__WantRfc1950HeaderBytes_73(bool value)
	{
		____WantRfc1950HeaderBytes_73 = value;
	}
};

struct DeflateManager_t740995428_StaticFields
{
public:
	// System.Int32 Ionic.Zlib.DeflateManager::MEM_LEVEL_MAX
	int32_t ___MEM_LEVEL_MAX_0;
	// System.Int32 Ionic.Zlib.DeflateManager::MEM_LEVEL_DEFAULT
	int32_t ___MEM_LEVEL_DEFAULT_1;
	// System.String[] Ionic.Zlib.DeflateManager::_ErrorMessage
	StringU5BU5D_t1281789340* ____ErrorMessage_3;
	// System.Int32 Ionic.Zlib.DeflateManager::PRESET_DICT
	int32_t ___PRESET_DICT_4;
	// System.Int32 Ionic.Zlib.DeflateManager::INIT_STATE
	int32_t ___INIT_STATE_5;
	// System.Int32 Ionic.Zlib.DeflateManager::BUSY_STATE
	int32_t ___BUSY_STATE_6;
	// System.Int32 Ionic.Zlib.DeflateManager::FINISH_STATE
	int32_t ___FINISH_STATE_7;
	// System.Int32 Ionic.Zlib.DeflateManager::Z_DEFLATED
	int32_t ___Z_DEFLATED_8;
	// System.Int32 Ionic.Zlib.DeflateManager::STORED_BLOCK
	int32_t ___STORED_BLOCK_9;
	// System.Int32 Ionic.Zlib.DeflateManager::STATIC_TREES
	int32_t ___STATIC_TREES_10;
	// System.Int32 Ionic.Zlib.DeflateManager::DYN_TREES
	int32_t ___DYN_TREES_11;
	// System.Int32 Ionic.Zlib.DeflateManager::Z_BINARY
	int32_t ___Z_BINARY_12;
	// System.Int32 Ionic.Zlib.DeflateManager::Z_ASCII
	int32_t ___Z_ASCII_13;
	// System.Int32 Ionic.Zlib.DeflateManager::Z_UNKNOWN
	int32_t ___Z_UNKNOWN_14;
	// System.Int32 Ionic.Zlib.DeflateManager::Buf_size
	int32_t ___Buf_size_15;
	// System.Int32 Ionic.Zlib.DeflateManager::MIN_MATCH
	int32_t ___MIN_MATCH_16;
	// System.Int32 Ionic.Zlib.DeflateManager::MAX_MATCH
	int32_t ___MAX_MATCH_17;
	// System.Int32 Ionic.Zlib.DeflateManager::MIN_LOOKAHEAD
	int32_t ___MIN_LOOKAHEAD_18;
	// System.Int32 Ionic.Zlib.DeflateManager::HEAP_SIZE
	int32_t ___HEAP_SIZE_19;
	// System.Int32 Ionic.Zlib.DeflateManager::END_BLOCK
	int32_t ___END_BLOCK_20;

public:
	inline static int32_t get_offset_of_MEM_LEVEL_MAX_0() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___MEM_LEVEL_MAX_0)); }
	inline int32_t get_MEM_LEVEL_MAX_0() const { return ___MEM_LEVEL_MAX_0; }
	inline int32_t* get_address_of_MEM_LEVEL_MAX_0() { return &___MEM_LEVEL_MAX_0; }
	inline void set_MEM_LEVEL_MAX_0(int32_t value)
	{
		___MEM_LEVEL_MAX_0 = value;
	}

	inline static int32_t get_offset_of_MEM_LEVEL_DEFAULT_1() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___MEM_LEVEL_DEFAULT_1)); }
	inline int32_t get_MEM_LEVEL_DEFAULT_1() const { return ___MEM_LEVEL_DEFAULT_1; }
	inline int32_t* get_address_of_MEM_LEVEL_DEFAULT_1() { return &___MEM_LEVEL_DEFAULT_1; }
	inline void set_MEM_LEVEL_DEFAULT_1(int32_t value)
	{
		___MEM_LEVEL_DEFAULT_1 = value;
	}

	inline static int32_t get_offset_of__ErrorMessage_3() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ____ErrorMessage_3)); }
	inline StringU5BU5D_t1281789340* get__ErrorMessage_3() const { return ____ErrorMessage_3; }
	inline StringU5BU5D_t1281789340** get_address_of__ErrorMessage_3() { return &____ErrorMessage_3; }
	inline void set__ErrorMessage_3(StringU5BU5D_t1281789340* value)
	{
		____ErrorMessage_3 = value;
		Il2CppCodeGenWriteBarrier((&____ErrorMessage_3), value);
	}

	inline static int32_t get_offset_of_PRESET_DICT_4() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___PRESET_DICT_4)); }
	inline int32_t get_PRESET_DICT_4() const { return ___PRESET_DICT_4; }
	inline int32_t* get_address_of_PRESET_DICT_4() { return &___PRESET_DICT_4; }
	inline void set_PRESET_DICT_4(int32_t value)
	{
		___PRESET_DICT_4 = value;
	}

	inline static int32_t get_offset_of_INIT_STATE_5() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___INIT_STATE_5)); }
	inline int32_t get_INIT_STATE_5() const { return ___INIT_STATE_5; }
	inline int32_t* get_address_of_INIT_STATE_5() { return &___INIT_STATE_5; }
	inline void set_INIT_STATE_5(int32_t value)
	{
		___INIT_STATE_5 = value;
	}

	inline static int32_t get_offset_of_BUSY_STATE_6() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___BUSY_STATE_6)); }
	inline int32_t get_BUSY_STATE_6() const { return ___BUSY_STATE_6; }
	inline int32_t* get_address_of_BUSY_STATE_6() { return &___BUSY_STATE_6; }
	inline void set_BUSY_STATE_6(int32_t value)
	{
		___BUSY_STATE_6 = value;
	}

	inline static int32_t get_offset_of_FINISH_STATE_7() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___FINISH_STATE_7)); }
	inline int32_t get_FINISH_STATE_7() const { return ___FINISH_STATE_7; }
	inline int32_t* get_address_of_FINISH_STATE_7() { return &___FINISH_STATE_7; }
	inline void set_FINISH_STATE_7(int32_t value)
	{
		___FINISH_STATE_7 = value;
	}

	inline static int32_t get_offset_of_Z_DEFLATED_8() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___Z_DEFLATED_8)); }
	inline int32_t get_Z_DEFLATED_8() const { return ___Z_DEFLATED_8; }
	inline int32_t* get_address_of_Z_DEFLATED_8() { return &___Z_DEFLATED_8; }
	inline void set_Z_DEFLATED_8(int32_t value)
	{
		___Z_DEFLATED_8 = value;
	}

	inline static int32_t get_offset_of_STORED_BLOCK_9() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___STORED_BLOCK_9)); }
	inline int32_t get_STORED_BLOCK_9() const { return ___STORED_BLOCK_9; }
	inline int32_t* get_address_of_STORED_BLOCK_9() { return &___STORED_BLOCK_9; }
	inline void set_STORED_BLOCK_9(int32_t value)
	{
		___STORED_BLOCK_9 = value;
	}

	inline static int32_t get_offset_of_STATIC_TREES_10() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___STATIC_TREES_10)); }
	inline int32_t get_STATIC_TREES_10() const { return ___STATIC_TREES_10; }
	inline int32_t* get_address_of_STATIC_TREES_10() { return &___STATIC_TREES_10; }
	inline void set_STATIC_TREES_10(int32_t value)
	{
		___STATIC_TREES_10 = value;
	}

	inline static int32_t get_offset_of_DYN_TREES_11() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___DYN_TREES_11)); }
	inline int32_t get_DYN_TREES_11() const { return ___DYN_TREES_11; }
	inline int32_t* get_address_of_DYN_TREES_11() { return &___DYN_TREES_11; }
	inline void set_DYN_TREES_11(int32_t value)
	{
		___DYN_TREES_11 = value;
	}

	inline static int32_t get_offset_of_Z_BINARY_12() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___Z_BINARY_12)); }
	inline int32_t get_Z_BINARY_12() const { return ___Z_BINARY_12; }
	inline int32_t* get_address_of_Z_BINARY_12() { return &___Z_BINARY_12; }
	inline void set_Z_BINARY_12(int32_t value)
	{
		___Z_BINARY_12 = value;
	}

	inline static int32_t get_offset_of_Z_ASCII_13() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___Z_ASCII_13)); }
	inline int32_t get_Z_ASCII_13() const { return ___Z_ASCII_13; }
	inline int32_t* get_address_of_Z_ASCII_13() { return &___Z_ASCII_13; }
	inline void set_Z_ASCII_13(int32_t value)
	{
		___Z_ASCII_13 = value;
	}

	inline static int32_t get_offset_of_Z_UNKNOWN_14() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___Z_UNKNOWN_14)); }
	inline int32_t get_Z_UNKNOWN_14() const { return ___Z_UNKNOWN_14; }
	inline int32_t* get_address_of_Z_UNKNOWN_14() { return &___Z_UNKNOWN_14; }
	inline void set_Z_UNKNOWN_14(int32_t value)
	{
		___Z_UNKNOWN_14 = value;
	}

	inline static int32_t get_offset_of_Buf_size_15() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___Buf_size_15)); }
	inline int32_t get_Buf_size_15() const { return ___Buf_size_15; }
	inline int32_t* get_address_of_Buf_size_15() { return &___Buf_size_15; }
	inline void set_Buf_size_15(int32_t value)
	{
		___Buf_size_15 = value;
	}

	inline static int32_t get_offset_of_MIN_MATCH_16() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___MIN_MATCH_16)); }
	inline int32_t get_MIN_MATCH_16() const { return ___MIN_MATCH_16; }
	inline int32_t* get_address_of_MIN_MATCH_16() { return &___MIN_MATCH_16; }
	inline void set_MIN_MATCH_16(int32_t value)
	{
		___MIN_MATCH_16 = value;
	}

	inline static int32_t get_offset_of_MAX_MATCH_17() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___MAX_MATCH_17)); }
	inline int32_t get_MAX_MATCH_17() const { return ___MAX_MATCH_17; }
	inline int32_t* get_address_of_MAX_MATCH_17() { return &___MAX_MATCH_17; }
	inline void set_MAX_MATCH_17(int32_t value)
	{
		___MAX_MATCH_17 = value;
	}

	inline static int32_t get_offset_of_MIN_LOOKAHEAD_18() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___MIN_LOOKAHEAD_18)); }
	inline int32_t get_MIN_LOOKAHEAD_18() const { return ___MIN_LOOKAHEAD_18; }
	inline int32_t* get_address_of_MIN_LOOKAHEAD_18() { return &___MIN_LOOKAHEAD_18; }
	inline void set_MIN_LOOKAHEAD_18(int32_t value)
	{
		___MIN_LOOKAHEAD_18 = value;
	}

	inline static int32_t get_offset_of_HEAP_SIZE_19() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___HEAP_SIZE_19)); }
	inline int32_t get_HEAP_SIZE_19() const { return ___HEAP_SIZE_19; }
	inline int32_t* get_address_of_HEAP_SIZE_19() { return &___HEAP_SIZE_19; }
	inline void set_HEAP_SIZE_19(int32_t value)
	{
		___HEAP_SIZE_19 = value;
	}

	inline static int32_t get_offset_of_END_BLOCK_20() { return static_cast<int32_t>(offsetof(DeflateManager_t740995428_StaticFields, ___END_BLOCK_20)); }
	inline int32_t get_END_BLOCK_20() const { return ___END_BLOCK_20; }
	inline int32_t* get_address_of_END_BLOCK_20() { return &___END_BLOCK_20; }
	inline void set_END_BLOCK_20(int32_t value)
	{
		___END_BLOCK_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEMANAGER_T740995428_H
#ifndef CONFIG_T1905746077_H
#define CONFIG_T1905746077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.DeflateManager/Config
struct  Config_t1905746077  : public RuntimeObject
{
public:
	// System.Int32 Ionic.Zlib.DeflateManager/Config::GoodLength
	int32_t ___GoodLength_0;
	// System.Int32 Ionic.Zlib.DeflateManager/Config::MaxLazy
	int32_t ___MaxLazy_1;
	// System.Int32 Ionic.Zlib.DeflateManager/Config::NiceLength
	int32_t ___NiceLength_2;
	// System.Int32 Ionic.Zlib.DeflateManager/Config::MaxChainLength
	int32_t ___MaxChainLength_3;
	// Ionic.Zlib.DeflateFlavor Ionic.Zlib.DeflateManager/Config::Flavor
	int32_t ___Flavor_4;

public:
	inline static int32_t get_offset_of_GoodLength_0() { return static_cast<int32_t>(offsetof(Config_t1905746077, ___GoodLength_0)); }
	inline int32_t get_GoodLength_0() const { return ___GoodLength_0; }
	inline int32_t* get_address_of_GoodLength_0() { return &___GoodLength_0; }
	inline void set_GoodLength_0(int32_t value)
	{
		___GoodLength_0 = value;
	}

	inline static int32_t get_offset_of_MaxLazy_1() { return static_cast<int32_t>(offsetof(Config_t1905746077, ___MaxLazy_1)); }
	inline int32_t get_MaxLazy_1() const { return ___MaxLazy_1; }
	inline int32_t* get_address_of_MaxLazy_1() { return &___MaxLazy_1; }
	inline void set_MaxLazy_1(int32_t value)
	{
		___MaxLazy_1 = value;
	}

	inline static int32_t get_offset_of_NiceLength_2() { return static_cast<int32_t>(offsetof(Config_t1905746077, ___NiceLength_2)); }
	inline int32_t get_NiceLength_2() const { return ___NiceLength_2; }
	inline int32_t* get_address_of_NiceLength_2() { return &___NiceLength_2; }
	inline void set_NiceLength_2(int32_t value)
	{
		___NiceLength_2 = value;
	}

	inline static int32_t get_offset_of_MaxChainLength_3() { return static_cast<int32_t>(offsetof(Config_t1905746077, ___MaxChainLength_3)); }
	inline int32_t get_MaxChainLength_3() const { return ___MaxChainLength_3; }
	inline int32_t* get_address_of_MaxChainLength_3() { return &___MaxChainLength_3; }
	inline void set_MaxChainLength_3(int32_t value)
	{
		___MaxChainLength_3 = value;
	}

	inline static int32_t get_offset_of_Flavor_4() { return static_cast<int32_t>(offsetof(Config_t1905746077, ___Flavor_4)); }
	inline int32_t get_Flavor_4() const { return ___Flavor_4; }
	inline int32_t* get_address_of_Flavor_4() { return &___Flavor_4; }
	inline void set_Flavor_4(int32_t value)
	{
		___Flavor_4 = value;
	}
};

struct Config_t1905746077_StaticFields
{
public:
	// Ionic.Zlib.DeflateManager/Config[] Ionic.Zlib.DeflateManager/Config::Table
	ConfigU5BU5D_t2096431184* ___Table_5;

public:
	inline static int32_t get_offset_of_Table_5() { return static_cast<int32_t>(offsetof(Config_t1905746077_StaticFields, ___Table_5)); }
	inline ConfigU5BU5D_t2096431184* get_Table_5() const { return ___Table_5; }
	inline ConfigU5BU5D_t2096431184** get_address_of_Table_5() { return &___Table_5; }
	inline void set_Table_5(ConfigU5BU5D_t2096431184* value)
	{
		___Table_5 = value;
		Il2CppCodeGenWriteBarrier((&___Table_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T1905746077_H
#ifndef INFLATEMANAGER_T3407211616_H
#define INFLATEMANAGER_T3407211616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InflateManager
struct  InflateManager_t3407211616  : public RuntimeObject
{
public:
	// Ionic.Zlib.InflateManager/InflateManagerMode Ionic.Zlib.InflateManager::mode
	int32_t ___mode_2;
	// Ionic.Zlib.ZlibCodec Ionic.Zlib.InflateManager::_codec
	ZlibCodec_t3653667923 * ____codec_3;
	// System.Int32 Ionic.Zlib.InflateManager::method
	int32_t ___method_4;
	// System.UInt32 Ionic.Zlib.InflateManager::computedCheck
	uint32_t ___computedCheck_5;
	// System.UInt32 Ionic.Zlib.InflateManager::expectedCheck
	uint32_t ___expectedCheck_6;
	// System.Int32 Ionic.Zlib.InflateManager::marker
	int32_t ___marker_7;
	// System.Boolean Ionic.Zlib.InflateManager::_handleRfc1950HeaderBytes
	bool ____handleRfc1950HeaderBytes_8;
	// System.Int32 Ionic.Zlib.InflateManager::wbits
	int32_t ___wbits_9;
	// Ionic.Zlib.InflateBlocks Ionic.Zlib.InflateManager::blocks
	InflateBlocks_t175244798 * ___blocks_10;

public:
	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of__codec_3() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ____codec_3)); }
	inline ZlibCodec_t3653667923 * get__codec_3() const { return ____codec_3; }
	inline ZlibCodec_t3653667923 ** get_address_of__codec_3() { return &____codec_3; }
	inline void set__codec_3(ZlibCodec_t3653667923 * value)
	{
		____codec_3 = value;
		Il2CppCodeGenWriteBarrier((&____codec_3), value);
	}

	inline static int32_t get_offset_of_method_4() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___method_4)); }
	inline int32_t get_method_4() const { return ___method_4; }
	inline int32_t* get_address_of_method_4() { return &___method_4; }
	inline void set_method_4(int32_t value)
	{
		___method_4 = value;
	}

	inline static int32_t get_offset_of_computedCheck_5() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___computedCheck_5)); }
	inline uint32_t get_computedCheck_5() const { return ___computedCheck_5; }
	inline uint32_t* get_address_of_computedCheck_5() { return &___computedCheck_5; }
	inline void set_computedCheck_5(uint32_t value)
	{
		___computedCheck_5 = value;
	}

	inline static int32_t get_offset_of_expectedCheck_6() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___expectedCheck_6)); }
	inline uint32_t get_expectedCheck_6() const { return ___expectedCheck_6; }
	inline uint32_t* get_address_of_expectedCheck_6() { return &___expectedCheck_6; }
	inline void set_expectedCheck_6(uint32_t value)
	{
		___expectedCheck_6 = value;
	}

	inline static int32_t get_offset_of_marker_7() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___marker_7)); }
	inline int32_t get_marker_7() const { return ___marker_7; }
	inline int32_t* get_address_of_marker_7() { return &___marker_7; }
	inline void set_marker_7(int32_t value)
	{
		___marker_7 = value;
	}

	inline static int32_t get_offset_of__handleRfc1950HeaderBytes_8() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ____handleRfc1950HeaderBytes_8)); }
	inline bool get__handleRfc1950HeaderBytes_8() const { return ____handleRfc1950HeaderBytes_8; }
	inline bool* get_address_of__handleRfc1950HeaderBytes_8() { return &____handleRfc1950HeaderBytes_8; }
	inline void set__handleRfc1950HeaderBytes_8(bool value)
	{
		____handleRfc1950HeaderBytes_8 = value;
	}

	inline static int32_t get_offset_of_wbits_9() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___wbits_9)); }
	inline int32_t get_wbits_9() const { return ___wbits_9; }
	inline int32_t* get_address_of_wbits_9() { return &___wbits_9; }
	inline void set_wbits_9(int32_t value)
	{
		___wbits_9 = value;
	}

	inline static int32_t get_offset_of_blocks_10() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616, ___blocks_10)); }
	inline InflateBlocks_t175244798 * get_blocks_10() const { return ___blocks_10; }
	inline InflateBlocks_t175244798 ** get_address_of_blocks_10() { return &___blocks_10; }
	inline void set_blocks_10(InflateBlocks_t175244798 * value)
	{
		___blocks_10 = value;
		Il2CppCodeGenWriteBarrier((&___blocks_10), value);
	}
};

struct InflateManager_t3407211616_StaticFields
{
public:
	// System.Byte[] Ionic.Zlib.InflateManager::mark
	ByteU5BU5D_t4116647657* ___mark_11;

public:
	inline static int32_t get_offset_of_mark_11() { return static_cast<int32_t>(offsetof(InflateManager_t3407211616_StaticFields, ___mark_11)); }
	inline ByteU5BU5D_t4116647657* get_mark_11() const { return ___mark_11; }
	inline ByteU5BU5D_t4116647657** get_address_of_mark_11() { return &___mark_11; }
	inline void set_mark_11(ByteU5BU5D_t4116647657* value)
	{
		___mark_11 = value;
		Il2CppCodeGenWriteBarrier((&___mark_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATEMANAGER_T3407211616_H
#ifndef PARALLELDEFLATEOUTPUTSTREAM_T2864412598_H
#define PARALLELDEFLATEOUTPUTSTREAM_T2864412598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ParallelDeflateOutputStream
struct  ParallelDeflateOutputStream_t2864412598  : public Stream_t1273022909
{
public:
	// System.Collections.Generic.List`1<Ionic.Zlib.WorkItem> Ionic.Zlib.ParallelDeflateOutputStream::_pool
	List_1_t72680944 * ____pool_2;
	// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::_leaveOpen
	bool ____leaveOpen_3;
	// System.IO.Stream Ionic.Zlib.ParallelDeflateOutputStream::_outStream
	Stream_t1273022909 * ____outStream_4;
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::_nextToFill
	int32_t ____nextToFill_5;
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::_nextToWrite
	int32_t ____nextToWrite_6;
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::_bufferSize
	int32_t ____bufferSize_7;
	// System.Threading.ManualResetEvent Ionic.Zlib.ParallelDeflateOutputStream::_writingDone
	ManualResetEvent_t451242010 * ____writingDone_8;
	// System.Threading.ManualResetEvent Ionic.Zlib.ParallelDeflateOutputStream::_sessionReset
	ManualResetEvent_t451242010 * ____sessionReset_9;
	// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::_noMoreInputForThisSegment
	bool ____noMoreInputForThisSegment_10;
	// System.Object Ionic.Zlib.ParallelDeflateOutputStream::_outputLock
	RuntimeObject * ____outputLock_11;
	// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::_isClosed
	bool ____isClosed_12;
	// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::_isDisposed
	bool ____isDisposed_13;
	// System.Boolean Ionic.Zlib.ParallelDeflateOutputStream::_firstWriteDone
	bool ____firstWriteDone_14;
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::_pc
	int32_t ____pc_15;
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::_Crc32
	int32_t ____Crc32_16;
	// System.Int64 Ionic.Zlib.ParallelDeflateOutputStream::_totalBytesProcessed
	int64_t ____totalBytesProcessed_17;
	// Ionic.Zlib.CompressionLevel Ionic.Zlib.ParallelDeflateOutputStream::_compressLevel
	int32_t ____compressLevel_18;
	// System.Exception modreq(System.Runtime.CompilerServices.IsVolatile) Ionic.Zlib.ParallelDeflateOutputStream::_pendingException
	Exception_t * ____pendingException_19;
	// System.Object Ionic.Zlib.ParallelDeflateOutputStream::_eLock
	RuntimeObject * ____eLock_20;
	// Ionic.Zlib.ParallelDeflateOutputStream/TraceBits Ionic.Zlib.ParallelDeflateOutputStream::_DesiredTrace
	int32_t ____DesiredTrace_21;
	// Ionic.Zlib.CompressionStrategy Ionic.Zlib.ParallelDeflateOutputStream::<Strategy>k__BackingField
	int32_t ___U3CStrategyU3Ek__BackingField_22;
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::<BuffersPerCore>k__BackingField
	int32_t ___U3CBuffersPerCoreU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of__pool_2() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____pool_2)); }
	inline List_1_t72680944 * get__pool_2() const { return ____pool_2; }
	inline List_1_t72680944 ** get_address_of__pool_2() { return &____pool_2; }
	inline void set__pool_2(List_1_t72680944 * value)
	{
		____pool_2 = value;
		Il2CppCodeGenWriteBarrier((&____pool_2), value);
	}

	inline static int32_t get_offset_of__leaveOpen_3() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____leaveOpen_3)); }
	inline bool get__leaveOpen_3() const { return ____leaveOpen_3; }
	inline bool* get_address_of__leaveOpen_3() { return &____leaveOpen_3; }
	inline void set__leaveOpen_3(bool value)
	{
		____leaveOpen_3 = value;
	}

	inline static int32_t get_offset_of__outStream_4() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____outStream_4)); }
	inline Stream_t1273022909 * get__outStream_4() const { return ____outStream_4; }
	inline Stream_t1273022909 ** get_address_of__outStream_4() { return &____outStream_4; }
	inline void set__outStream_4(Stream_t1273022909 * value)
	{
		____outStream_4 = value;
		Il2CppCodeGenWriteBarrier((&____outStream_4), value);
	}

	inline static int32_t get_offset_of__nextToFill_5() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____nextToFill_5)); }
	inline int32_t get__nextToFill_5() const { return ____nextToFill_5; }
	inline int32_t* get_address_of__nextToFill_5() { return &____nextToFill_5; }
	inline void set__nextToFill_5(int32_t value)
	{
		____nextToFill_5 = value;
	}

	inline static int32_t get_offset_of__nextToWrite_6() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____nextToWrite_6)); }
	inline int32_t get__nextToWrite_6() const { return ____nextToWrite_6; }
	inline int32_t* get_address_of__nextToWrite_6() { return &____nextToWrite_6; }
	inline void set__nextToWrite_6(int32_t value)
	{
		____nextToWrite_6 = value;
	}

	inline static int32_t get_offset_of__bufferSize_7() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____bufferSize_7)); }
	inline int32_t get__bufferSize_7() const { return ____bufferSize_7; }
	inline int32_t* get_address_of__bufferSize_7() { return &____bufferSize_7; }
	inline void set__bufferSize_7(int32_t value)
	{
		____bufferSize_7 = value;
	}

	inline static int32_t get_offset_of__writingDone_8() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____writingDone_8)); }
	inline ManualResetEvent_t451242010 * get__writingDone_8() const { return ____writingDone_8; }
	inline ManualResetEvent_t451242010 ** get_address_of__writingDone_8() { return &____writingDone_8; }
	inline void set__writingDone_8(ManualResetEvent_t451242010 * value)
	{
		____writingDone_8 = value;
		Il2CppCodeGenWriteBarrier((&____writingDone_8), value);
	}

	inline static int32_t get_offset_of__sessionReset_9() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____sessionReset_9)); }
	inline ManualResetEvent_t451242010 * get__sessionReset_9() const { return ____sessionReset_9; }
	inline ManualResetEvent_t451242010 ** get_address_of__sessionReset_9() { return &____sessionReset_9; }
	inline void set__sessionReset_9(ManualResetEvent_t451242010 * value)
	{
		____sessionReset_9 = value;
		Il2CppCodeGenWriteBarrier((&____sessionReset_9), value);
	}

	inline static int32_t get_offset_of__noMoreInputForThisSegment_10() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____noMoreInputForThisSegment_10)); }
	inline bool get__noMoreInputForThisSegment_10() const { return ____noMoreInputForThisSegment_10; }
	inline bool* get_address_of__noMoreInputForThisSegment_10() { return &____noMoreInputForThisSegment_10; }
	inline void set__noMoreInputForThisSegment_10(bool value)
	{
		____noMoreInputForThisSegment_10 = value;
	}

	inline static int32_t get_offset_of__outputLock_11() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____outputLock_11)); }
	inline RuntimeObject * get__outputLock_11() const { return ____outputLock_11; }
	inline RuntimeObject ** get_address_of__outputLock_11() { return &____outputLock_11; }
	inline void set__outputLock_11(RuntimeObject * value)
	{
		____outputLock_11 = value;
		Il2CppCodeGenWriteBarrier((&____outputLock_11), value);
	}

	inline static int32_t get_offset_of__isClosed_12() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____isClosed_12)); }
	inline bool get__isClosed_12() const { return ____isClosed_12; }
	inline bool* get_address_of__isClosed_12() { return &____isClosed_12; }
	inline void set__isClosed_12(bool value)
	{
		____isClosed_12 = value;
	}

	inline static int32_t get_offset_of__isDisposed_13() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____isDisposed_13)); }
	inline bool get__isDisposed_13() const { return ____isDisposed_13; }
	inline bool* get_address_of__isDisposed_13() { return &____isDisposed_13; }
	inline void set__isDisposed_13(bool value)
	{
		____isDisposed_13 = value;
	}

	inline static int32_t get_offset_of__firstWriteDone_14() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____firstWriteDone_14)); }
	inline bool get__firstWriteDone_14() const { return ____firstWriteDone_14; }
	inline bool* get_address_of__firstWriteDone_14() { return &____firstWriteDone_14; }
	inline void set__firstWriteDone_14(bool value)
	{
		____firstWriteDone_14 = value;
	}

	inline static int32_t get_offset_of__pc_15() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____pc_15)); }
	inline int32_t get__pc_15() const { return ____pc_15; }
	inline int32_t* get_address_of__pc_15() { return &____pc_15; }
	inline void set__pc_15(int32_t value)
	{
		____pc_15 = value;
	}

	inline static int32_t get_offset_of__Crc32_16() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____Crc32_16)); }
	inline int32_t get__Crc32_16() const { return ____Crc32_16; }
	inline int32_t* get_address_of__Crc32_16() { return &____Crc32_16; }
	inline void set__Crc32_16(int32_t value)
	{
		____Crc32_16 = value;
	}

	inline static int32_t get_offset_of__totalBytesProcessed_17() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____totalBytesProcessed_17)); }
	inline int64_t get__totalBytesProcessed_17() const { return ____totalBytesProcessed_17; }
	inline int64_t* get_address_of__totalBytesProcessed_17() { return &____totalBytesProcessed_17; }
	inline void set__totalBytesProcessed_17(int64_t value)
	{
		____totalBytesProcessed_17 = value;
	}

	inline static int32_t get_offset_of__compressLevel_18() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____compressLevel_18)); }
	inline int32_t get__compressLevel_18() const { return ____compressLevel_18; }
	inline int32_t* get_address_of__compressLevel_18() { return &____compressLevel_18; }
	inline void set__compressLevel_18(int32_t value)
	{
		____compressLevel_18 = value;
	}

	inline static int32_t get_offset_of__pendingException_19() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____pendingException_19)); }
	inline Exception_t * get__pendingException_19() const { return ____pendingException_19; }
	inline Exception_t ** get_address_of__pendingException_19() { return &____pendingException_19; }
	inline void set__pendingException_19(Exception_t * value)
	{
		____pendingException_19 = value;
		Il2CppCodeGenWriteBarrier((&____pendingException_19), value);
	}

	inline static int32_t get_offset_of__eLock_20() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____eLock_20)); }
	inline RuntimeObject * get__eLock_20() const { return ____eLock_20; }
	inline RuntimeObject ** get_address_of__eLock_20() { return &____eLock_20; }
	inline void set__eLock_20(RuntimeObject * value)
	{
		____eLock_20 = value;
		Il2CppCodeGenWriteBarrier((&____eLock_20), value);
	}

	inline static int32_t get_offset_of__DesiredTrace_21() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ____DesiredTrace_21)); }
	inline int32_t get__DesiredTrace_21() const { return ____DesiredTrace_21; }
	inline int32_t* get_address_of__DesiredTrace_21() { return &____DesiredTrace_21; }
	inline void set__DesiredTrace_21(int32_t value)
	{
		____DesiredTrace_21 = value;
	}

	inline static int32_t get_offset_of_U3CStrategyU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ___U3CStrategyU3Ek__BackingField_22)); }
	inline int32_t get_U3CStrategyU3Ek__BackingField_22() const { return ___U3CStrategyU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CStrategyU3Ek__BackingField_22() { return &___U3CStrategyU3Ek__BackingField_22; }
	inline void set_U3CStrategyU3Ek__BackingField_22(int32_t value)
	{
		___U3CStrategyU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CBuffersPerCoreU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598, ___U3CBuffersPerCoreU3Ek__BackingField_23)); }
	inline int32_t get_U3CBuffersPerCoreU3Ek__BackingField_23() const { return ___U3CBuffersPerCoreU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CBuffersPerCoreU3Ek__BackingField_23() { return &___U3CBuffersPerCoreU3Ek__BackingField_23; }
	inline void set_U3CBuffersPerCoreU3Ek__BackingField_23(int32_t value)
	{
		___U3CBuffersPerCoreU3Ek__BackingField_23 = value;
	}
};

struct ParallelDeflateOutputStream_t2864412598_StaticFields
{
public:
	// System.Int32 Ionic.Zlib.ParallelDeflateOutputStream::IO_BUFFER_SIZE_DEFAULT
	int32_t ___IO_BUFFER_SIZE_DEFAULT_1;

public:
	inline static int32_t get_offset_of_IO_BUFFER_SIZE_DEFAULT_1() { return static_cast<int32_t>(offsetof(ParallelDeflateOutputStream_t2864412598_StaticFields, ___IO_BUFFER_SIZE_DEFAULT_1)); }
	inline int32_t get_IO_BUFFER_SIZE_DEFAULT_1() const { return ___IO_BUFFER_SIZE_DEFAULT_1; }
	inline int32_t* get_address_of_IO_BUFFER_SIZE_DEFAULT_1() { return &___IO_BUFFER_SIZE_DEFAULT_1; }
	inline void set_IO_BUFFER_SIZE_DEFAULT_1(int32_t value)
	{
		___IO_BUFFER_SIZE_DEFAULT_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARALLELDEFLATEOUTPUTSTREAM_T2864412598_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef LUMOSCREDENTIALS_T1718792749_H
#define LUMOSCREDENTIALS_T1718792749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosCredentials
struct  LumosCredentials_t1718792749  : public ScriptableObject_t2528358522
{
public:
	// System.String LumosCredentials::apiKey
	String_t* ___apiKey_2;

public:
	inline static int32_t get_offset_of_apiKey_2() { return static_cast<int32_t>(offsetof(LumosCredentials_t1718792749, ___apiKey_2)); }
	inline String_t* get_apiKey_2() const { return ___apiKey_2; }
	inline String_t** get_address_of_apiKey_2() { return &___apiKey_2; }
	inline void set_apiKey_2(String_t* value)
	{
		___apiKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___apiKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSCREDENTIALS_T1718792749_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CLOSEHANDLER_T719506259_H
#define CLOSEHANDLER_T719506259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosFeedbackGUI/CloseHandler
struct  CloseHandler_t719506259  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEHANDLER_T719506259_H
#ifndef COMPRESSFUNC_T3594827279_H
#define COMPRESSFUNC_T3594827279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.DeflateManager/CompressFunc
struct  CompressFunc_t3594827279  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSFUNC_T3594827279_H
#ifndef NULLABLE_1_T1166124571_H
#define NULLABLE_1_T1166124571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t1166124571 
{
public:
	// T System.Nullable`1::value
	DateTime_t3738529785  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___value_0)); }
	inline DateTime_t3738529785  get_value_0() const { return ___value_0; }
	inline DateTime_t3738529785 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t3738529785  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1166124571_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef NUTRACKINGHELPERS_T4202934765_H
#define NUTRACKINGHELPERS_T4202934765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuTrackingHelpers
struct  NuTrackingHelpers_t4202934765  : public RuntimeObject
{
public:

public:
};

struct NuTrackingHelpers_t4202934765_StaticFields
{
public:
	// System.String NuTrackingHelpers::gameShortName
	String_t* ___gameShortName_0;
	// System.String NuTrackingHelpers::<GAAccountCode>k__BackingField
	String_t* ___U3CGAAccountCodeU3Ek__BackingField_1;
	// System.String NuTrackingHelpers::_GAHostDomain
	String_t* ____GAHostDomain_2;
	// System.Int32 NuTrackingHelpers::_GAHostDomainHash
	int32_t ____GAHostDomainHash_3;
	// System.DateTime NuTrackingHelpers::<GameStartupTimeInUTC>k__BackingField
	DateTime_t3738529785  ___U3CGameStartupTimeInUTCU3Ek__BackingField_4;
	// System.Boolean NuTrackingHelpers::WantObfuscateGlobalIP
	bool ___WantObfuscateGlobalIP_5;
	// System.Boolean NuTrackingHelpers::WantMonitorEditor
	bool ___WantMonitorEditor_6;
	// System.Boolean NuTrackingHelpers::WantLog
	bool ___WantLog_7;
	// System.Boolean NuTrackingHelpers::globalIPRetrieved
	bool ___globalIPRetrieved_8;
	// System.String NuTrackingHelpers::globalIP
	String_t* ___globalIP_9;
	// System.Random NuTrackingHelpers::rand
	Random_t108471755 * ___rand_10;
	// System.Boolean NuTrackingHelpers::cookieCreated
	bool ___cookieCreated_14;
	// System.String NuTrackingHelpers::utmaCookie
	String_t* ___utmaCookie_15;
	// System.String NuTrackingHelpers::utmzCookie
	String_t* ___utmzCookie_16;

public:
	inline static int32_t get_offset_of_gameShortName_0() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___gameShortName_0)); }
	inline String_t* get_gameShortName_0() const { return ___gameShortName_0; }
	inline String_t** get_address_of_gameShortName_0() { return &___gameShortName_0; }
	inline void set_gameShortName_0(String_t* value)
	{
		___gameShortName_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameShortName_0), value);
	}

	inline static int32_t get_offset_of_U3CGAAccountCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___U3CGAAccountCodeU3Ek__BackingField_1)); }
	inline String_t* get_U3CGAAccountCodeU3Ek__BackingField_1() const { return ___U3CGAAccountCodeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CGAAccountCodeU3Ek__BackingField_1() { return &___U3CGAAccountCodeU3Ek__BackingField_1; }
	inline void set_U3CGAAccountCodeU3Ek__BackingField_1(String_t* value)
	{
		___U3CGAAccountCodeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGAAccountCodeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__GAHostDomain_2() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ____GAHostDomain_2)); }
	inline String_t* get__GAHostDomain_2() const { return ____GAHostDomain_2; }
	inline String_t** get_address_of__GAHostDomain_2() { return &____GAHostDomain_2; }
	inline void set__GAHostDomain_2(String_t* value)
	{
		____GAHostDomain_2 = value;
		Il2CppCodeGenWriteBarrier((&____GAHostDomain_2), value);
	}

	inline static int32_t get_offset_of__GAHostDomainHash_3() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ____GAHostDomainHash_3)); }
	inline int32_t get__GAHostDomainHash_3() const { return ____GAHostDomainHash_3; }
	inline int32_t* get_address_of__GAHostDomainHash_3() { return &____GAHostDomainHash_3; }
	inline void set__GAHostDomainHash_3(int32_t value)
	{
		____GAHostDomainHash_3 = value;
	}

	inline static int32_t get_offset_of_U3CGameStartupTimeInUTCU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___U3CGameStartupTimeInUTCU3Ek__BackingField_4)); }
	inline DateTime_t3738529785  get_U3CGameStartupTimeInUTCU3Ek__BackingField_4() const { return ___U3CGameStartupTimeInUTCU3Ek__BackingField_4; }
	inline DateTime_t3738529785 * get_address_of_U3CGameStartupTimeInUTCU3Ek__BackingField_4() { return &___U3CGameStartupTimeInUTCU3Ek__BackingField_4; }
	inline void set_U3CGameStartupTimeInUTCU3Ek__BackingField_4(DateTime_t3738529785  value)
	{
		___U3CGameStartupTimeInUTCU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_WantObfuscateGlobalIP_5() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___WantObfuscateGlobalIP_5)); }
	inline bool get_WantObfuscateGlobalIP_5() const { return ___WantObfuscateGlobalIP_5; }
	inline bool* get_address_of_WantObfuscateGlobalIP_5() { return &___WantObfuscateGlobalIP_5; }
	inline void set_WantObfuscateGlobalIP_5(bool value)
	{
		___WantObfuscateGlobalIP_5 = value;
	}

	inline static int32_t get_offset_of_WantMonitorEditor_6() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___WantMonitorEditor_6)); }
	inline bool get_WantMonitorEditor_6() const { return ___WantMonitorEditor_6; }
	inline bool* get_address_of_WantMonitorEditor_6() { return &___WantMonitorEditor_6; }
	inline void set_WantMonitorEditor_6(bool value)
	{
		___WantMonitorEditor_6 = value;
	}

	inline static int32_t get_offset_of_WantLog_7() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___WantLog_7)); }
	inline bool get_WantLog_7() const { return ___WantLog_7; }
	inline bool* get_address_of_WantLog_7() { return &___WantLog_7; }
	inline void set_WantLog_7(bool value)
	{
		___WantLog_7 = value;
	}

	inline static int32_t get_offset_of_globalIPRetrieved_8() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___globalIPRetrieved_8)); }
	inline bool get_globalIPRetrieved_8() const { return ___globalIPRetrieved_8; }
	inline bool* get_address_of_globalIPRetrieved_8() { return &___globalIPRetrieved_8; }
	inline void set_globalIPRetrieved_8(bool value)
	{
		___globalIPRetrieved_8 = value;
	}

	inline static int32_t get_offset_of_globalIP_9() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___globalIP_9)); }
	inline String_t* get_globalIP_9() const { return ___globalIP_9; }
	inline String_t** get_address_of_globalIP_9() { return &___globalIP_9; }
	inline void set_globalIP_9(String_t* value)
	{
		___globalIP_9 = value;
		Il2CppCodeGenWriteBarrier((&___globalIP_9), value);
	}

	inline static int32_t get_offset_of_rand_10() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___rand_10)); }
	inline Random_t108471755 * get_rand_10() const { return ___rand_10; }
	inline Random_t108471755 ** get_address_of_rand_10() { return &___rand_10; }
	inline void set_rand_10(Random_t108471755 * value)
	{
		___rand_10 = value;
		Il2CppCodeGenWriteBarrier((&___rand_10), value);
	}

	inline static int32_t get_offset_of_cookieCreated_14() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___cookieCreated_14)); }
	inline bool get_cookieCreated_14() const { return ___cookieCreated_14; }
	inline bool* get_address_of_cookieCreated_14() { return &___cookieCreated_14; }
	inline void set_cookieCreated_14(bool value)
	{
		___cookieCreated_14 = value;
	}

	inline static int32_t get_offset_of_utmaCookie_15() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___utmaCookie_15)); }
	inline String_t* get_utmaCookie_15() const { return ___utmaCookie_15; }
	inline String_t** get_address_of_utmaCookie_15() { return &___utmaCookie_15; }
	inline void set_utmaCookie_15(String_t* value)
	{
		___utmaCookie_15 = value;
		Il2CppCodeGenWriteBarrier((&___utmaCookie_15), value);
	}

	inline static int32_t get_offset_of_utmzCookie_16() { return static_cast<int32_t>(offsetof(NuTrackingHelpers_t4202934765_StaticFields, ___utmzCookie_16)); }
	inline String_t* get_utmzCookie_16() const { return ___utmzCookie_16; }
	inline String_t** get_address_of_utmzCookie_16() { return &___utmzCookie_16; }
	inline void set_utmzCookie_16(String_t* value)
	{
		___utmzCookie_16 = value;
		Il2CppCodeGenWriteBarrier((&___utmzCookie_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUTRACKINGHELPERS_T4202934765_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef ZLIBBASESTREAM_T1591821133_H
#define ZLIBBASESTREAM_T1591821133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.ZlibBaseStream
struct  ZlibBaseStream_t1591821133  : public Stream_t1273022909
{
public:
	// Ionic.Zlib.ZlibCodec Ionic.Zlib.ZlibBaseStream::_z
	ZlibCodec_t3653667923 * ____z_1;
	// Ionic.Zlib.ZlibBaseStream/StreamMode Ionic.Zlib.ZlibBaseStream::_streamMode
	int32_t ____streamMode_2;
	// Ionic.Zlib.FlushType Ionic.Zlib.ZlibBaseStream::_flushMode
	int32_t ____flushMode_3;
	// Ionic.Zlib.ZlibStreamFlavor Ionic.Zlib.ZlibBaseStream::_flavor
	int32_t ____flavor_4;
	// Ionic.Zlib.CompressionMode Ionic.Zlib.ZlibBaseStream::_compressionMode
	int32_t ____compressionMode_5;
	// Ionic.Zlib.CompressionLevel Ionic.Zlib.ZlibBaseStream::_level
	int32_t ____level_6;
	// System.Boolean Ionic.Zlib.ZlibBaseStream::_leaveOpen
	bool ____leaveOpen_7;
	// System.Byte[] Ionic.Zlib.ZlibBaseStream::_workingBuffer
	ByteU5BU5D_t4116647657* ____workingBuffer_8;
	// System.Int32 Ionic.Zlib.ZlibBaseStream::_bufferSize
	int32_t ____bufferSize_9;
	// System.Byte[] Ionic.Zlib.ZlibBaseStream::_buf1
	ByteU5BU5D_t4116647657* ____buf1_10;
	// System.IO.Stream Ionic.Zlib.ZlibBaseStream::_stream
	Stream_t1273022909 * ____stream_11;
	// Ionic.Zlib.CompressionStrategy Ionic.Zlib.ZlibBaseStream::Strategy
	int32_t ___Strategy_12;
	// Ionic.Zlib.CRC32 Ionic.Zlib.ZlibBaseStream::crc
	CRC32_t1348930856 * ___crc_13;
	// System.String Ionic.Zlib.ZlibBaseStream::_GzipFileName
	String_t* ____GzipFileName_14;
	// System.String Ionic.Zlib.ZlibBaseStream::_GzipComment
	String_t* ____GzipComment_15;
	// System.DateTime Ionic.Zlib.ZlibBaseStream::_GzipMtime
	DateTime_t3738529785  ____GzipMtime_16;
	// System.Int32 Ionic.Zlib.ZlibBaseStream::_gzipHeaderByteCount
	int32_t ____gzipHeaderByteCount_17;
	// System.Boolean Ionic.Zlib.ZlibBaseStream::nomoreinput
	bool ___nomoreinput_18;

public:
	inline static int32_t get_offset_of__z_1() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____z_1)); }
	inline ZlibCodec_t3653667923 * get__z_1() const { return ____z_1; }
	inline ZlibCodec_t3653667923 ** get_address_of__z_1() { return &____z_1; }
	inline void set__z_1(ZlibCodec_t3653667923 * value)
	{
		____z_1 = value;
		Il2CppCodeGenWriteBarrier((&____z_1), value);
	}

	inline static int32_t get_offset_of__streamMode_2() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____streamMode_2)); }
	inline int32_t get__streamMode_2() const { return ____streamMode_2; }
	inline int32_t* get_address_of__streamMode_2() { return &____streamMode_2; }
	inline void set__streamMode_2(int32_t value)
	{
		____streamMode_2 = value;
	}

	inline static int32_t get_offset_of__flushMode_3() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____flushMode_3)); }
	inline int32_t get__flushMode_3() const { return ____flushMode_3; }
	inline int32_t* get_address_of__flushMode_3() { return &____flushMode_3; }
	inline void set__flushMode_3(int32_t value)
	{
		____flushMode_3 = value;
	}

	inline static int32_t get_offset_of__flavor_4() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____flavor_4)); }
	inline int32_t get__flavor_4() const { return ____flavor_4; }
	inline int32_t* get_address_of__flavor_4() { return &____flavor_4; }
	inline void set__flavor_4(int32_t value)
	{
		____flavor_4 = value;
	}

	inline static int32_t get_offset_of__compressionMode_5() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____compressionMode_5)); }
	inline int32_t get__compressionMode_5() const { return ____compressionMode_5; }
	inline int32_t* get_address_of__compressionMode_5() { return &____compressionMode_5; }
	inline void set__compressionMode_5(int32_t value)
	{
		____compressionMode_5 = value;
	}

	inline static int32_t get_offset_of__level_6() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____level_6)); }
	inline int32_t get__level_6() const { return ____level_6; }
	inline int32_t* get_address_of__level_6() { return &____level_6; }
	inline void set__level_6(int32_t value)
	{
		____level_6 = value;
	}

	inline static int32_t get_offset_of__leaveOpen_7() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____leaveOpen_7)); }
	inline bool get__leaveOpen_7() const { return ____leaveOpen_7; }
	inline bool* get_address_of__leaveOpen_7() { return &____leaveOpen_7; }
	inline void set__leaveOpen_7(bool value)
	{
		____leaveOpen_7 = value;
	}

	inline static int32_t get_offset_of__workingBuffer_8() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____workingBuffer_8)); }
	inline ByteU5BU5D_t4116647657* get__workingBuffer_8() const { return ____workingBuffer_8; }
	inline ByteU5BU5D_t4116647657** get_address_of__workingBuffer_8() { return &____workingBuffer_8; }
	inline void set__workingBuffer_8(ByteU5BU5D_t4116647657* value)
	{
		____workingBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&____workingBuffer_8), value);
	}

	inline static int32_t get_offset_of__bufferSize_9() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____bufferSize_9)); }
	inline int32_t get__bufferSize_9() const { return ____bufferSize_9; }
	inline int32_t* get_address_of__bufferSize_9() { return &____bufferSize_9; }
	inline void set__bufferSize_9(int32_t value)
	{
		____bufferSize_9 = value;
	}

	inline static int32_t get_offset_of__buf1_10() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____buf1_10)); }
	inline ByteU5BU5D_t4116647657* get__buf1_10() const { return ____buf1_10; }
	inline ByteU5BU5D_t4116647657** get_address_of__buf1_10() { return &____buf1_10; }
	inline void set__buf1_10(ByteU5BU5D_t4116647657* value)
	{
		____buf1_10 = value;
		Il2CppCodeGenWriteBarrier((&____buf1_10), value);
	}

	inline static int32_t get_offset_of__stream_11() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____stream_11)); }
	inline Stream_t1273022909 * get__stream_11() const { return ____stream_11; }
	inline Stream_t1273022909 ** get_address_of__stream_11() { return &____stream_11; }
	inline void set__stream_11(Stream_t1273022909 * value)
	{
		____stream_11 = value;
		Il2CppCodeGenWriteBarrier((&____stream_11), value);
	}

	inline static int32_t get_offset_of_Strategy_12() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ___Strategy_12)); }
	inline int32_t get_Strategy_12() const { return ___Strategy_12; }
	inline int32_t* get_address_of_Strategy_12() { return &___Strategy_12; }
	inline void set_Strategy_12(int32_t value)
	{
		___Strategy_12 = value;
	}

	inline static int32_t get_offset_of_crc_13() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ___crc_13)); }
	inline CRC32_t1348930856 * get_crc_13() const { return ___crc_13; }
	inline CRC32_t1348930856 ** get_address_of_crc_13() { return &___crc_13; }
	inline void set_crc_13(CRC32_t1348930856 * value)
	{
		___crc_13 = value;
		Il2CppCodeGenWriteBarrier((&___crc_13), value);
	}

	inline static int32_t get_offset_of__GzipFileName_14() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____GzipFileName_14)); }
	inline String_t* get__GzipFileName_14() const { return ____GzipFileName_14; }
	inline String_t** get_address_of__GzipFileName_14() { return &____GzipFileName_14; }
	inline void set__GzipFileName_14(String_t* value)
	{
		____GzipFileName_14 = value;
		Il2CppCodeGenWriteBarrier((&____GzipFileName_14), value);
	}

	inline static int32_t get_offset_of__GzipComment_15() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____GzipComment_15)); }
	inline String_t* get__GzipComment_15() const { return ____GzipComment_15; }
	inline String_t** get_address_of__GzipComment_15() { return &____GzipComment_15; }
	inline void set__GzipComment_15(String_t* value)
	{
		____GzipComment_15 = value;
		Il2CppCodeGenWriteBarrier((&____GzipComment_15), value);
	}

	inline static int32_t get_offset_of__GzipMtime_16() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____GzipMtime_16)); }
	inline DateTime_t3738529785  get__GzipMtime_16() const { return ____GzipMtime_16; }
	inline DateTime_t3738529785 * get_address_of__GzipMtime_16() { return &____GzipMtime_16; }
	inline void set__GzipMtime_16(DateTime_t3738529785  value)
	{
		____GzipMtime_16 = value;
	}

	inline static int32_t get_offset_of__gzipHeaderByteCount_17() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ____gzipHeaderByteCount_17)); }
	inline int32_t get__gzipHeaderByteCount_17() const { return ____gzipHeaderByteCount_17; }
	inline int32_t* get_address_of__gzipHeaderByteCount_17() { return &____gzipHeaderByteCount_17; }
	inline void set__gzipHeaderByteCount_17(int32_t value)
	{
		____gzipHeaderByteCount_17 = value;
	}

	inline static int32_t get_offset_of_nomoreinput_18() { return static_cast<int32_t>(offsetof(ZlibBaseStream_t1591821133, ___nomoreinput_18)); }
	inline bool get_nomoreinput_18() const { return ___nomoreinput_18; }
	inline bool* get_address_of_nomoreinput_18() { return &___nomoreinput_18; }
	inline void set_nomoreinput_18(bool value)
	{
		___nomoreinput_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZLIBBASESTREAM_T1591821133_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GZIPSTREAM_T4229724673_H
#define GZIPSTREAM_T4229724673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.GZipStream
struct  GZipStream_t4229724673  : public Stream_t1273022909
{
public:
	// System.Nullable`1<System.DateTime> Ionic.Zlib.GZipStream::LastModified
	Nullable_1_t1166124571  ___LastModified_1;
	// System.Int32 Ionic.Zlib.GZipStream::_headerByteCount
	int32_t ____headerByteCount_2;
	// Ionic.Zlib.ZlibBaseStream Ionic.Zlib.GZipStream::_baseStream
	ZlibBaseStream_t1591821133 * ____baseStream_3;
	// System.Boolean Ionic.Zlib.GZipStream::_disposed
	bool ____disposed_4;
	// System.Boolean Ionic.Zlib.GZipStream::_firstReadDone
	bool ____firstReadDone_5;
	// System.String Ionic.Zlib.GZipStream::_FileName
	String_t* ____FileName_6;
	// System.String Ionic.Zlib.GZipStream::_Comment
	String_t* ____Comment_7;
	// System.Int32 Ionic.Zlib.GZipStream::_Crc32
	int32_t ____Crc32_8;

public:
	inline static int32_t get_offset_of_LastModified_1() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ___LastModified_1)); }
	inline Nullable_1_t1166124571  get_LastModified_1() const { return ___LastModified_1; }
	inline Nullable_1_t1166124571 * get_address_of_LastModified_1() { return &___LastModified_1; }
	inline void set_LastModified_1(Nullable_1_t1166124571  value)
	{
		___LastModified_1 = value;
	}

	inline static int32_t get_offset_of__headerByteCount_2() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____headerByteCount_2)); }
	inline int32_t get__headerByteCount_2() const { return ____headerByteCount_2; }
	inline int32_t* get_address_of__headerByteCount_2() { return &____headerByteCount_2; }
	inline void set__headerByteCount_2(int32_t value)
	{
		____headerByteCount_2 = value;
	}

	inline static int32_t get_offset_of__baseStream_3() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____baseStream_3)); }
	inline ZlibBaseStream_t1591821133 * get__baseStream_3() const { return ____baseStream_3; }
	inline ZlibBaseStream_t1591821133 ** get_address_of__baseStream_3() { return &____baseStream_3; }
	inline void set__baseStream_3(ZlibBaseStream_t1591821133 * value)
	{
		____baseStream_3 = value;
		Il2CppCodeGenWriteBarrier((&____baseStream_3), value);
	}

	inline static int32_t get_offset_of__disposed_4() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____disposed_4)); }
	inline bool get__disposed_4() const { return ____disposed_4; }
	inline bool* get_address_of__disposed_4() { return &____disposed_4; }
	inline void set__disposed_4(bool value)
	{
		____disposed_4 = value;
	}

	inline static int32_t get_offset_of__firstReadDone_5() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____firstReadDone_5)); }
	inline bool get__firstReadDone_5() const { return ____firstReadDone_5; }
	inline bool* get_address_of__firstReadDone_5() { return &____firstReadDone_5; }
	inline void set__firstReadDone_5(bool value)
	{
		____firstReadDone_5 = value;
	}

	inline static int32_t get_offset_of__FileName_6() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____FileName_6)); }
	inline String_t* get__FileName_6() const { return ____FileName_6; }
	inline String_t** get_address_of__FileName_6() { return &____FileName_6; }
	inline void set__FileName_6(String_t* value)
	{
		____FileName_6 = value;
		Il2CppCodeGenWriteBarrier((&____FileName_6), value);
	}

	inline static int32_t get_offset_of__Comment_7() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____Comment_7)); }
	inline String_t* get__Comment_7() const { return ____Comment_7; }
	inline String_t** get_address_of__Comment_7() { return &____Comment_7; }
	inline void set__Comment_7(String_t* value)
	{
		____Comment_7 = value;
		Il2CppCodeGenWriteBarrier((&____Comment_7), value);
	}

	inline static int32_t get_offset_of__Crc32_8() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673, ____Crc32_8)); }
	inline int32_t get__Crc32_8() const { return ____Crc32_8; }
	inline int32_t* get_address_of__Crc32_8() { return &____Crc32_8; }
	inline void set__Crc32_8(int32_t value)
	{
		____Crc32_8 = value;
	}
};

struct GZipStream_t4229724673_StaticFields
{
public:
	// System.DateTime Ionic.Zlib.GZipStream::_unixEpoch
	DateTime_t3738529785  ____unixEpoch_9;
	// System.Text.Encoding Ionic.Zlib.GZipStream::iso8859dash1
	Encoding_t1523322056 * ___iso8859dash1_10;

public:
	inline static int32_t get_offset_of__unixEpoch_9() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673_StaticFields, ____unixEpoch_9)); }
	inline DateTime_t3738529785  get__unixEpoch_9() const { return ____unixEpoch_9; }
	inline DateTime_t3738529785 * get_address_of__unixEpoch_9() { return &____unixEpoch_9; }
	inline void set__unixEpoch_9(DateTime_t3738529785  value)
	{
		____unixEpoch_9 = value;
	}

	inline static int32_t get_offset_of_iso8859dash1_10() { return static_cast<int32_t>(offsetof(GZipStream_t4229724673_StaticFields, ___iso8859dash1_10)); }
	inline Encoding_t1523322056 * get_iso8859dash1_10() const { return ___iso8859dash1_10; }
	inline Encoding_t1523322056 ** get_address_of_iso8859dash1_10() { return &___iso8859dash1_10; }
	inline void set_iso8859dash1_10(Encoding_t1523322056 * value)
	{
		___iso8859dash1_10 = value;
		Il2CppCodeGenWriteBarrier((&___iso8859dash1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPSTREAM_T4229724673_H
#ifndef NUTRACKING_T472478902_H
#define NUTRACKING_T472478902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NuTracking
struct  NuTracking_t472478902  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean NuTracking::isStarted
	bool ___isStarted_3;
	// NuGAService NuTracking::gaService
	NuGAService_t1586892016 * ___gaService_4;
	// System.Collections.Generic.List`1<NuTracking/TrackingRequest> NuTracking::requestQueue
	List_1_t4066654888 * ___requestQueue_5;

public:
	inline static int32_t get_offset_of_isStarted_3() { return static_cast<int32_t>(offsetof(NuTracking_t472478902, ___isStarted_3)); }
	inline bool get_isStarted_3() const { return ___isStarted_3; }
	inline bool* get_address_of_isStarted_3() { return &___isStarted_3; }
	inline void set_isStarted_3(bool value)
	{
		___isStarted_3 = value;
	}

	inline static int32_t get_offset_of_gaService_4() { return static_cast<int32_t>(offsetof(NuTracking_t472478902, ___gaService_4)); }
	inline NuGAService_t1586892016 * get_gaService_4() const { return ___gaService_4; }
	inline NuGAService_t1586892016 ** get_address_of_gaService_4() { return &___gaService_4; }
	inline void set_gaService_4(NuGAService_t1586892016 * value)
	{
		___gaService_4 = value;
		Il2CppCodeGenWriteBarrier((&___gaService_4), value);
	}

	inline static int32_t get_offset_of_requestQueue_5() { return static_cast<int32_t>(offsetof(NuTracking_t472478902, ___requestQueue_5)); }
	inline List_1_t4066654888 * get_requestQueue_5() const { return ___requestQueue_5; }
	inline List_1_t4066654888 ** get_address_of_requestQueue_5() { return &___requestQueue_5; }
	inline void set_requestQueue_5(List_1_t4066654888 * value)
	{
		___requestQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___requestQueue_5), value);
	}
};

struct NuTracking_t472478902_StaticFields
{
public:
	// NuTracking NuTracking::staticInstance
	NuTracking_t472478902 * ___staticInstance_2;

public:
	inline static int32_t get_offset_of_staticInstance_2() { return static_cast<int32_t>(offsetof(NuTracking_t472478902_StaticFields, ___staticInstance_2)); }
	inline NuTracking_t472478902 * get_staticInstance_2() const { return ___staticInstance_2; }
	inline NuTracking_t472478902 ** get_address_of_staticInstance_2() { return &___staticInstance_2; }
	inline void set_staticInstance_2(NuTracking_t472478902 * value)
	{
		___staticInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___staticInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUTRACKING_T472478902_H
#ifndef LUMOSANALYTICS_T2745971597_H
#define LUMOSANALYTICS_T2745971597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosAnalytics
struct  LumosAnalytics_t2745971597  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LumosAnalytics::useLevelsAsCategories
	bool ___useLevelsAsCategories_2;
	// System.Boolean LumosAnalytics::recordLevelCompletionEvents
	bool ___recordLevelCompletionEvents_3;

public:
	inline static int32_t get_offset_of_useLevelsAsCategories_2() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597, ___useLevelsAsCategories_2)); }
	inline bool get_useLevelsAsCategories_2() const { return ___useLevelsAsCategories_2; }
	inline bool* get_address_of_useLevelsAsCategories_2() { return &___useLevelsAsCategories_2; }
	inline void set_useLevelsAsCategories_2(bool value)
	{
		___useLevelsAsCategories_2 = value;
	}

	inline static int32_t get_offset_of_recordLevelCompletionEvents_3() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597, ___recordLevelCompletionEvents_3)); }
	inline bool get_recordLevelCompletionEvents_3() const { return ___recordLevelCompletionEvents_3; }
	inline bool* get_address_of_recordLevelCompletionEvents_3() { return &___recordLevelCompletionEvents_3; }
	inline void set_recordLevelCompletionEvents_3(bool value)
	{
		___recordLevelCompletionEvents_3 = value;
	}
};

struct LumosAnalytics_t2745971597_StaticFields
{
public:
	// System.String LumosAnalytics::_baseUrl
	String_t* ____baseUrl_4;
	// System.Single LumosAnalytics::levelStartTime
	float ___levelStartTime_5;
	// LumosAnalytics LumosAnalytics::instance
	LumosAnalytics_t2745971597 * ___instance_6;
	// System.Action LumosAnalytics::<>f__mg$cache0
	Action_t1264377477 * ___U3CU3Ef__mgU24cache0_7;
	// System.Action LumosAnalytics::<>f__mg$cache1
	Action_t1264377477 * ___U3CU3Ef__mgU24cache1_8;

public:
	inline static int32_t get_offset_of__baseUrl_4() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ____baseUrl_4)); }
	inline String_t* get__baseUrl_4() const { return ____baseUrl_4; }
	inline String_t** get_address_of__baseUrl_4() { return &____baseUrl_4; }
	inline void set__baseUrl_4(String_t* value)
	{
		____baseUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_4), value);
	}

	inline static int32_t get_offset_of_levelStartTime_5() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___levelStartTime_5)); }
	inline float get_levelStartTime_5() const { return ___levelStartTime_5; }
	inline float* get_address_of_levelStartTime_5() { return &___levelStartTime_5; }
	inline void set_levelStartTime_5(float value)
	{
		___levelStartTime_5 = value;
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___instance_6)); }
	inline LumosAnalytics_t2745971597 * get_instance_6() const { return ___instance_6; }
	inline LumosAnalytics_t2745971597 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(LumosAnalytics_t2745971597 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(LumosAnalytics_t2745971597_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSANALYTICS_T2745971597_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef LUMOSDIAGNOSTICS_T279239158_H
#define LUMOSDIAGNOSTICS_T279239158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumosDiagnostics
struct  LumosDiagnostics_t279239158  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LumosDiagnostics::recordLogs
	bool ___recordLogs_2;
	// System.Boolean LumosDiagnostics::recordWarnings
	bool ___recordWarnings_3;
	// System.Boolean LumosDiagnostics::recordErrors
	bool ___recordErrors_4;

public:
	inline static int32_t get_offset_of_recordLogs_2() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158, ___recordLogs_2)); }
	inline bool get_recordLogs_2() const { return ___recordLogs_2; }
	inline bool* get_address_of_recordLogs_2() { return &___recordLogs_2; }
	inline void set_recordLogs_2(bool value)
	{
		___recordLogs_2 = value;
	}

	inline static int32_t get_offset_of_recordWarnings_3() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158, ___recordWarnings_3)); }
	inline bool get_recordWarnings_3() const { return ___recordWarnings_3; }
	inline bool* get_address_of_recordWarnings_3() { return &___recordWarnings_3; }
	inline void set_recordWarnings_3(bool value)
	{
		___recordWarnings_3 = value;
	}

	inline static int32_t get_offset_of_recordErrors_4() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158, ___recordErrors_4)); }
	inline bool get_recordErrors_4() const { return ___recordErrors_4; }
	inline bool* get_address_of_recordErrors_4() { return &___recordErrors_4; }
	inline void set_recordErrors_4(bool value)
	{
		___recordErrors_4 = value;
	}
};

struct LumosDiagnostics_t279239158_StaticFields
{
public:
	// System.String LumosDiagnostics::_baseUrl
	String_t* ____baseUrl_5;
	// LumosDiagnostics LumosDiagnostics::instance
	LumosDiagnostics_t279239158 * ___instance_6;
	// System.Action LumosDiagnostics::<>f__mg$cache0
	Action_t1264377477 * ___U3CU3Ef__mgU24cache0_7;
	// System.Action LumosDiagnostics::<>f__mg$cache1
	Action_t1264377477 * ___U3CU3Ef__mgU24cache1_8;
	// UnityEngine.Application/LogCallback LumosDiagnostics::<>f__mg$cache2
	LogCallback_t3588208630 * ___U3CU3Ef__mgU24cache2_9;

public:
	inline static int32_t get_offset_of__baseUrl_5() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ____baseUrl_5)); }
	inline String_t* get__baseUrl_5() const { return ____baseUrl_5; }
	inline String_t** get_address_of__baseUrl_5() { return &____baseUrl_5; }
	inline void set__baseUrl_5(String_t* value)
	{
		____baseUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_5), value);
	}

	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___instance_6)); }
	inline LumosDiagnostics_t279239158 * get_instance_6() const { return ___instance_6; }
	inline LumosDiagnostics_t279239158 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(LumosDiagnostics_t279239158 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline Action_t1264377477 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(Action_t1264377477 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(LumosDiagnostics_t279239158_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline LogCallback_t3588208630 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline LogCallback_t3588208630 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(LogCallback_t3588208630 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOSDIAGNOSTICS_T279239158_H
#ifndef LUMOS_T1046626606_H
#define LUMOS_T1046626606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lumos
struct  Lumos_t1046626606  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Lumos::runWhileInEditor
	bool ___runWhileInEditor_2;

public:
	inline static int32_t get_offset_of_runWhileInEditor_2() { return static_cast<int32_t>(offsetof(Lumos_t1046626606, ___runWhileInEditor_2)); }
	inline bool get_runWhileInEditor_2() const { return ___runWhileInEditor_2; }
	inline bool* get_address_of_runWhileInEditor_2() { return &___runWhileInEditor_2; }
	inline void set_runWhileInEditor_2(bool value)
	{
		___runWhileInEditor_2 = value;
	}
};

struct Lumos_t1046626606_StaticFields
{
public:
	// System.Action Lumos::OnReady
	Action_t1264377477 * ___OnReady_3;
	// System.Action Lumos::OnTimerFinish
	Action_t1264377477 * ___OnTimerFinish_4;
	// System.Single Lumos::_timerInterval
	float ____timerInterval_5;
	// LumosCredentials Lumos::<credentials>k__BackingField
	LumosCredentials_t1718792749 * ___U3CcredentialsU3Ek__BackingField_7;
	// System.Boolean Lumos::<debug>k__BackingField
	bool ___U3CdebugU3Ek__BackingField_8;
	// System.String Lumos::<playerId>k__BackingField
	String_t* ___U3CplayerIdU3Ek__BackingField_9;
	// System.Boolean Lumos::<timerPaused>k__BackingField
	bool ___U3CtimerPausedU3Ek__BackingField_10;
	// Lumos Lumos::instance
	Lumos_t1046626606 * ___instance_11;
	// System.Action`1<System.Boolean> Lumos::<>f__am$cache0
	Action_1_t269755560 * ___U3CU3Ef__amU24cache0_12;
	// System.Action`1<System.Object> Lumos::<>f__mg$cache0
	Action_1_t3252573759 * ___U3CU3Ef__mgU24cache0_13;
	// System.Action`1<System.Object> Lumos::<>f__mg$cache1
	Action_1_t3252573759 * ___U3CU3Ef__mgU24cache1_14;
	// System.Action`1<System.Object> Lumos::<>f__mg$cache2
	Action_1_t3252573759 * ___U3CU3Ef__mgU24cache2_15;

public:
	inline static int32_t get_offset_of_OnReady_3() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___OnReady_3)); }
	inline Action_t1264377477 * get_OnReady_3() const { return ___OnReady_3; }
	inline Action_t1264377477 ** get_address_of_OnReady_3() { return &___OnReady_3; }
	inline void set_OnReady_3(Action_t1264377477 * value)
	{
		___OnReady_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnReady_3), value);
	}

	inline static int32_t get_offset_of_OnTimerFinish_4() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___OnTimerFinish_4)); }
	inline Action_t1264377477 * get_OnTimerFinish_4() const { return ___OnTimerFinish_4; }
	inline Action_t1264377477 ** get_address_of_OnTimerFinish_4() { return &___OnTimerFinish_4; }
	inline void set_OnTimerFinish_4(Action_t1264377477 * value)
	{
		___OnTimerFinish_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnTimerFinish_4), value);
	}

	inline static int32_t get_offset_of__timerInterval_5() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ____timerInterval_5)); }
	inline float get__timerInterval_5() const { return ____timerInterval_5; }
	inline float* get_address_of__timerInterval_5() { return &____timerInterval_5; }
	inline void set__timerInterval_5(float value)
	{
		____timerInterval_5 = value;
	}

	inline static int32_t get_offset_of_U3CcredentialsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CcredentialsU3Ek__BackingField_7)); }
	inline LumosCredentials_t1718792749 * get_U3CcredentialsU3Ek__BackingField_7() const { return ___U3CcredentialsU3Ek__BackingField_7; }
	inline LumosCredentials_t1718792749 ** get_address_of_U3CcredentialsU3Ek__BackingField_7() { return &___U3CcredentialsU3Ek__BackingField_7; }
	inline void set_U3CcredentialsU3Ek__BackingField_7(LumosCredentials_t1718792749 * value)
	{
		___U3CcredentialsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcredentialsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CdebugU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CdebugU3Ek__BackingField_8)); }
	inline bool get_U3CdebugU3Ek__BackingField_8() const { return ___U3CdebugU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CdebugU3Ek__BackingField_8() { return &___U3CdebugU3Ek__BackingField_8; }
	inline void set_U3CdebugU3Ek__BackingField_8(bool value)
	{
		___U3CdebugU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CplayerIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CplayerIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CplayerIdU3Ek__BackingField_9() const { return ___U3CplayerIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CplayerIdU3Ek__BackingField_9() { return &___U3CplayerIdU3Ek__BackingField_9; }
	inline void set_U3CplayerIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CplayerIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplayerIdU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CtimerPausedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CtimerPausedU3Ek__BackingField_10)); }
	inline bool get_U3CtimerPausedU3Ek__BackingField_10() const { return ___U3CtimerPausedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CtimerPausedU3Ek__BackingField_10() { return &___U3CtimerPausedU3Ek__BackingField_10; }
	inline void set_U3CtimerPausedU3Ek__BackingField_10(bool value)
	{
		___U3CtimerPausedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___instance_11)); }
	inline Lumos_t1046626606 * get_instance_11() const { return ___instance_11; }
	inline Lumos_t1046626606 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(Lumos_t1046626606 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t269755560 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t269755560 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t269755560 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_14() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__mgU24cache1_14)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__mgU24cache1_14() const { return ___U3CU3Ef__mgU24cache1_14; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__mgU24cache1_14() { return &___U3CU3Ef__mgU24cache1_14; }
	inline void set_U3CU3Ef__mgU24cache1_14(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__mgU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_15() { return static_cast<int32_t>(offsetof(Lumos_t1046626606_StaticFields, ___U3CU3Ef__mgU24cache2_15)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__mgU24cache2_15() const { return ___U3CU3Ef__mgU24cache2_15; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__mgU24cache2_15() { return &___U3CU3Ef__mgU24cache2_15; }
	inline void set_U3CU3Ef__mgU24cache2_15(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__mgU24cache2_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMOS_T1046626606_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_2)); }
	inline Graphic_t1660335611 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t1660335611 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_3)); }
	inline Color_t2555686324  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2555686324  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_4)); }
	inline Vector2_t2156229523  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2156229523  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1819[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (U3CModuleU3E_t692745545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (LumosAnalytics_t2745971597), -1, sizeof(LumosAnalytics_t2745971597_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[7] = 
{
	LumosAnalytics_t2745971597::get_offset_of_useLevelsAsCategories_2(),
	LumosAnalytics_t2745971597::get_offset_of_recordLevelCompletionEvents_3(),
	LumosAnalytics_t2745971597_StaticFields::get_offset_of__baseUrl_4(),
	LumosAnalytics_t2745971597_StaticFields::get_offset_of_levelStartTime_5(),
	LumosAnalytics_t2745971597_StaticFields::get_offset_of_instance_6(),
	LumosAnalytics_t2745971597_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	LumosAnalytics_t2745971597_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (LumosAnalyticsSetup_t3600549041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (LumosEvents_t3060038184), -1, sizeof(LumosEvents_t3060038184_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	LumosEvents_t3060038184_StaticFields::get_offset_of_events_0(),
	LumosEvents_t3060038184_StaticFields::get_offset_of_unsentUniqueEvents_1(),
	LumosEvents_t3060038184_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	LumosEvents_t3060038184_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (LumosLocation_t2816019121), -1, sizeof(LumosLocation_t2816019121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1827[1] = 
{
	LumosLocation_t2816019121_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (U3CRecordU3Ec__AnonStorey0_t811208082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[1] = 
{
	U3CRecordU3Ec__AnonStorey0_t811208082::get_offset_of_prefsKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (LumosFeedbackGUI_t1268982120), -1, sizeof(LumosFeedbackGUI_t1268982120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1829[11] = 
{
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_windowClosed_0(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_U3CskinU3Ek__BackingField_1(),
	0,
	0,
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_windowRect_4(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_visible_5(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_sentMessage_6(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_email_7(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_message_8(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_category_9(),
	LumosFeedbackGUI_t1268982120_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (CloseHandler_t719506259), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (LumosDiagnostics_t279239158), -1, sizeof(LumosDiagnostics_t279239158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[8] = 
{
	LumosDiagnostics_t279239158::get_offset_of_recordLogs_2(),
	LumosDiagnostics_t279239158::get_offset_of_recordWarnings_3(),
	LumosDiagnostics_t279239158::get_offset_of_recordErrors_4(),
	LumosDiagnostics_t279239158_StaticFields::get_offset_of__baseUrl_5(),
	LumosDiagnostics_t279239158_StaticFields::get_offset_of_instance_6(),
	LumosDiagnostics_t279239158_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	LumosDiagnostics_t279239158_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	LumosDiagnostics_t279239158_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (LumosDiagnosticsSetup_t110199435), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (LumosFeedback_t607479745), -1, sizeof(LumosFeedback_t607479745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	LumosFeedback_t607479745_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LumosFeedback_t607479745_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (LumosLogs_t761625864), -1, sizeof(LumosLogs_t761625864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[5] = 
{
	LumosLogs_t761625864_StaticFields::get_offset_of_toIgnore_0(),
	LumosLogs_t761625864_StaticFields::get_offset_of_logs_1(),
	LumosLogs_t761625864_StaticFields::get_offset_of_typeLabels_2(),
	LumosLogs_t761625864_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
	LumosLogs_t761625864_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (LumosSpecs_t2754611293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (Lumos_t1046626606), -1, sizeof(Lumos_t1046626606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1837[14] = 
{
	Lumos_t1046626606::get_offset_of_runWhileInEditor_2(),
	Lumos_t1046626606_StaticFields::get_offset_of_OnReady_3(),
	Lumos_t1046626606_StaticFields::get_offset_of_OnTimerFinish_4(),
	Lumos_t1046626606_StaticFields::get_offset_of__timerInterval_5(),
	0,
	Lumos_t1046626606_StaticFields::get_offset_of_U3CcredentialsU3Ek__BackingField_7(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CdebugU3Ek__BackingField_8(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CplayerIdU3Ek__BackingField_9(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CtimerPausedU3Ek__BackingField_10(),
	Lumos_t1046626606_StaticFields::get_offset_of_instance_11(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_14(),
	Lumos_t1046626606_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (U3CSendQueuedDataU3Ec__Iterator0_t3572231512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[3] = 
{
	U3CSendQueuedDataU3Ec__Iterator0_t3572231512::get_offset_of_U24current_0(),
	U3CSendQueuedDataU3Ec__Iterator0_t3572231512::get_offset_of_U24disposing_1(),
	U3CSendQueuedDataU3Ec__Iterator0_t3572231512::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (LumosCredentials_t1718792749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[1] = 
{
	LumosCredentials_t1718792749::get_offset_of_apiKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (LumosJson_t1197320855), -1, sizeof(LumosJson_t1197320855_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1840[3] = 
{
	0,
	LumosJson_t1197320855_StaticFields::get_offset_of_lastErrorIndex_1(),
	LumosJson_t1197320855_StaticFields::get_offset_of_lastDecode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (TOKEN_t776369722)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1841[13] = 
{
	TOKEN_t776369722::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (LumosPlayer_t370097434), -1, sizeof(LumosPlayer_t370097434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1842[3] = 
{
	LumosPlayer_t370097434_StaticFields::get_offset_of__baseUrl_0(),
	LumosPlayer_t370097434_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	LumosPlayer_t370097434_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[1] = 
{
	U3CRequestPlayerIdU3Ec__AnonStorey0_t2149773854::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (LumosRequest_t457072260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (U3CSendCoroutineU3Ec__Iterator0_t597473103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	U3CSendCoroutineU3Ec__Iterator0_t597473103::get_offset_of_errorCallback_0(),
	U3CSendCoroutineU3Ec__Iterator0_t597473103::get_offset_of_U24current_1(),
	U3CSendCoroutineU3Ec__Iterator0_t597473103::get_offset_of_U24disposing_2(),
	U3CSendCoroutineU3Ec__Iterator0_t597473103::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[6] = 
{
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463::get_offset_of_imageLocation_0(),
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463::get_offset_of_texture_2(),
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463::get_offset_of_U24current_3(),
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463::get_offset_of_U24disposing_4(),
	U3CLoadImageCoroutineU3Ec__Iterator1_t3113194463::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (LumosUtil_t3964483066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (CRC16_t2217306282), -1, sizeof(CRC16_t2217306282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[6] = 
{
	CRC16_t2217306282_StaticFields::get_offset_of_DefaultPolynomial_4(),
	CRC16_t2217306282_StaticFields::get_offset_of__allOnes_5(),
	CRC16_t2217306282_StaticFields::get_offset_of__defaultCRC_6(),
	CRC16_t2217306282_StaticFields::get_offset_of__crc16TablesCache_7(),
	CRC16_t2217306282::get_offset_of__crc16Table_8(),
	CRC16_t2217306282::get_offset_of__crc_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (CRC32_t1834969254), -1, sizeof(CRC32_t1834969254_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1850[6] = 
{
	CRC32_t1834969254_StaticFields::get_offset_of_DefaultPolynomial_4(),
	CRC32_t1834969254_StaticFields::get_offset_of__allOnes_5(),
	CRC32_t1834969254_StaticFields::get_offset_of__defaultCRC_6(),
	CRC32_t1834969254_StaticFields::get_offset_of__crc32TablesCache_7(),
	CRC32_t1834969254::get_offset_of__crc32Table_8(),
	CRC32_t1834969254::get_offset_of__crc_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (HTTPException_t4127885469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (Request_t1151508915), -1, sizeof(Request_t1151508915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1852[10] = 
{
	Request_t1151508915::get_offset_of_method_0(),
	Request_t1151508915::get_offset_of_protocol_1(),
	Request_t1151508915::get_offset_of_bytes_2(),
	Request_t1151508915::get_offset_of_uri_3(),
	Request_t1151508915_StaticFields::get_offset_of_EOL_4(),
	Request_t1151508915::get_offset_of_response_5(),
	Request_t1151508915::get_offset_of_maximumRetryCount_6(),
	Request_t1151508915::get_offset_of_acceptGzip_7(),
	Request_t1151508915::get_offset_of_exception_8(),
	Request_t1151508915::get_offset_of_headers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (Response_t2509227019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[4] = 
{
	Response_t2509227019::get_offset_of_status_0(),
	Response_t2509227019::get_offset_of_message_1(),
	Response_t2509227019::get_offset_of_bytes_2(),
	Response_t2509227019::get_offset_of_headers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (CRC32_t1348930856), -1, sizeof(CRC32_t1348930856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1854[4] = 
{
	CRC32_t1348930856::get_offset_of__TotalBytesRead_0(),
	CRC32_t1348930856_StaticFields::get_offset_of_crc32Table_1(),
	0,
	CRC32_t1348930856::get_offset_of__RunningCrc32Result_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (CrcCalculatorStream_t1469656756), -1, sizeof(CrcCalculatorStream_t1469656756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[5] = 
{
	CrcCalculatorStream_t1469656756_StaticFields::get_offset_of_UnsetLengthLimit_1(),
	CrcCalculatorStream_t1469656756::get_offset_of__innerStream_2(),
	CrcCalculatorStream_t1469656756::get_offset_of__Crc32_3(),
	CrcCalculatorStream_t1469656756::get_offset_of__lengthLimit_4(),
	CrcCalculatorStream_t1469656756::get_offset_of__leaveOpen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (FlushType_t1474040364)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[6] = 
{
	FlushType_t1474040364::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (BlockState_t410253493)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1857[5] = 
{
	BlockState_t410253493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (DeflateFlavor_t738395756)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1858[4] = 
{
	DeflateFlavor_t738395756::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (DeflateManager_t740995428), -1, sizeof(DeflateManager_t740995428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1859[74] = 
{
	DeflateManager_t740995428_StaticFields::get_offset_of_MEM_LEVEL_MAX_0(),
	DeflateManager_t740995428_StaticFields::get_offset_of_MEM_LEVEL_DEFAULT_1(),
	DeflateManager_t740995428::get_offset_of_DeflateFunction_2(),
	DeflateManager_t740995428_StaticFields::get_offset_of__ErrorMessage_3(),
	DeflateManager_t740995428_StaticFields::get_offset_of_PRESET_DICT_4(),
	DeflateManager_t740995428_StaticFields::get_offset_of_INIT_STATE_5(),
	DeflateManager_t740995428_StaticFields::get_offset_of_BUSY_STATE_6(),
	DeflateManager_t740995428_StaticFields::get_offset_of_FINISH_STATE_7(),
	DeflateManager_t740995428_StaticFields::get_offset_of_Z_DEFLATED_8(),
	DeflateManager_t740995428_StaticFields::get_offset_of_STORED_BLOCK_9(),
	DeflateManager_t740995428_StaticFields::get_offset_of_STATIC_TREES_10(),
	DeflateManager_t740995428_StaticFields::get_offset_of_DYN_TREES_11(),
	DeflateManager_t740995428_StaticFields::get_offset_of_Z_BINARY_12(),
	DeflateManager_t740995428_StaticFields::get_offset_of_Z_ASCII_13(),
	DeflateManager_t740995428_StaticFields::get_offset_of_Z_UNKNOWN_14(),
	DeflateManager_t740995428_StaticFields::get_offset_of_Buf_size_15(),
	DeflateManager_t740995428_StaticFields::get_offset_of_MIN_MATCH_16(),
	DeflateManager_t740995428_StaticFields::get_offset_of_MAX_MATCH_17(),
	DeflateManager_t740995428_StaticFields::get_offset_of_MIN_LOOKAHEAD_18(),
	DeflateManager_t740995428_StaticFields::get_offset_of_HEAP_SIZE_19(),
	DeflateManager_t740995428_StaticFields::get_offset_of_END_BLOCK_20(),
	DeflateManager_t740995428::get_offset_of__codec_21(),
	DeflateManager_t740995428::get_offset_of_status_22(),
	DeflateManager_t740995428::get_offset_of_pending_23(),
	DeflateManager_t740995428::get_offset_of_nextPending_24(),
	DeflateManager_t740995428::get_offset_of_pendingCount_25(),
	DeflateManager_t740995428::get_offset_of_data_type_26(),
	DeflateManager_t740995428::get_offset_of_last_flush_27(),
	DeflateManager_t740995428::get_offset_of_w_size_28(),
	DeflateManager_t740995428::get_offset_of_w_bits_29(),
	DeflateManager_t740995428::get_offset_of_w_mask_30(),
	DeflateManager_t740995428::get_offset_of_window_31(),
	DeflateManager_t740995428::get_offset_of_window_size_32(),
	DeflateManager_t740995428::get_offset_of_prev_33(),
	DeflateManager_t740995428::get_offset_of_head_34(),
	DeflateManager_t740995428::get_offset_of_ins_h_35(),
	DeflateManager_t740995428::get_offset_of_hash_size_36(),
	DeflateManager_t740995428::get_offset_of_hash_bits_37(),
	DeflateManager_t740995428::get_offset_of_hash_mask_38(),
	DeflateManager_t740995428::get_offset_of_hash_shift_39(),
	DeflateManager_t740995428::get_offset_of_block_start_40(),
	DeflateManager_t740995428::get_offset_of_config_41(),
	DeflateManager_t740995428::get_offset_of_match_length_42(),
	DeflateManager_t740995428::get_offset_of_prev_match_43(),
	DeflateManager_t740995428::get_offset_of_match_available_44(),
	DeflateManager_t740995428::get_offset_of_strstart_45(),
	DeflateManager_t740995428::get_offset_of_match_start_46(),
	DeflateManager_t740995428::get_offset_of_lookahead_47(),
	DeflateManager_t740995428::get_offset_of_prev_length_48(),
	DeflateManager_t740995428::get_offset_of_compressionLevel_49(),
	DeflateManager_t740995428::get_offset_of_compressionStrategy_50(),
	DeflateManager_t740995428::get_offset_of_dyn_ltree_51(),
	DeflateManager_t740995428::get_offset_of_dyn_dtree_52(),
	DeflateManager_t740995428::get_offset_of_bl_tree_53(),
	DeflateManager_t740995428::get_offset_of_treeLiterals_54(),
	DeflateManager_t740995428::get_offset_of_treeDistances_55(),
	DeflateManager_t740995428::get_offset_of_treeBitLengths_56(),
	DeflateManager_t740995428::get_offset_of_bl_count_57(),
	DeflateManager_t740995428::get_offset_of_heap_58(),
	DeflateManager_t740995428::get_offset_of_heap_len_59(),
	DeflateManager_t740995428::get_offset_of_heap_max_60(),
	DeflateManager_t740995428::get_offset_of_depth_61(),
	DeflateManager_t740995428::get_offset_of__lengthOffset_62(),
	DeflateManager_t740995428::get_offset_of_lit_bufsize_63(),
	DeflateManager_t740995428::get_offset_of_last_lit_64(),
	DeflateManager_t740995428::get_offset_of__distanceOffset_65(),
	DeflateManager_t740995428::get_offset_of_opt_len_66(),
	DeflateManager_t740995428::get_offset_of_static_len_67(),
	DeflateManager_t740995428::get_offset_of_matches_68(),
	DeflateManager_t740995428::get_offset_of_last_eob_len_69(),
	DeflateManager_t740995428::get_offset_of_bi_buf_70(),
	DeflateManager_t740995428::get_offset_of_bi_valid_71(),
	DeflateManager_t740995428::get_offset_of_Rfc1950BytesEmitted_72(),
	DeflateManager_t740995428::get_offset_of__WantRfc1950HeaderBytes_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (CompressFunc_t3594827279), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Config_t1905746077), -1, sizeof(Config_t1905746077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1861[6] = 
{
	Config_t1905746077::get_offset_of_GoodLength_0(),
	Config_t1905746077::get_offset_of_MaxLazy_1(),
	Config_t1905746077::get_offset_of_NiceLength_2(),
	Config_t1905746077::get_offset_of_MaxChainLength_3(),
	Config_t1905746077::get_offset_of_Flavor_4(),
	Config_t1905746077_StaticFields::get_offset_of_Table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (DeflateStream_t979017812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[3] = 
{
	DeflateStream_t979017812::get_offset_of__baseStream_1(),
	DeflateStream_t979017812::get_offset_of__innerStream_2(),
	DeflateStream_t979017812::get_offset_of__disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (GZipStream_t4229724673), -1, sizeof(GZipStream_t4229724673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1863[10] = 
{
	GZipStream_t4229724673::get_offset_of_LastModified_1(),
	GZipStream_t4229724673::get_offset_of__headerByteCount_2(),
	GZipStream_t4229724673::get_offset_of__baseStream_3(),
	GZipStream_t4229724673::get_offset_of__disposed_4(),
	GZipStream_t4229724673::get_offset_of__firstReadDone_5(),
	GZipStream_t4229724673::get_offset_of__FileName_6(),
	GZipStream_t4229724673::get_offset_of__Comment_7(),
	GZipStream_t4229724673::get_offset_of__Crc32_8(),
	GZipStream_t4229724673_StaticFields::get_offset_of__unixEpoch_9(),
	GZipStream_t4229724673_StaticFields::get_offset_of_iso8859dash1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (InflateBlocks_t175244798), -1, sizeof(InflateBlocks_t175244798_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1864[22] = 
{
	0,
	InflateBlocks_t175244798_StaticFields::get_offset_of_border_1(),
	InflateBlocks_t175244798::get_offset_of_mode_2(),
	InflateBlocks_t175244798::get_offset_of_left_3(),
	InflateBlocks_t175244798::get_offset_of_table_4(),
	InflateBlocks_t175244798::get_offset_of_index_5(),
	InflateBlocks_t175244798::get_offset_of_blens_6(),
	InflateBlocks_t175244798::get_offset_of_bb_7(),
	InflateBlocks_t175244798::get_offset_of_tb_8(),
	InflateBlocks_t175244798::get_offset_of_codes_9(),
	InflateBlocks_t175244798::get_offset_of_last_10(),
	InflateBlocks_t175244798::get_offset_of__codec_11(),
	InflateBlocks_t175244798::get_offset_of_bitk_12(),
	InflateBlocks_t175244798::get_offset_of_bitb_13(),
	InflateBlocks_t175244798::get_offset_of_hufts_14(),
	InflateBlocks_t175244798::get_offset_of_window_15(),
	InflateBlocks_t175244798::get_offset_of_end_16(),
	InflateBlocks_t175244798::get_offset_of_readAt_17(),
	InflateBlocks_t175244798::get_offset_of_writeAt_18(),
	InflateBlocks_t175244798::get_offset_of_checkfn_19(),
	InflateBlocks_t175244798::get_offset_of_check_20(),
	InflateBlocks_t175244798::get_offset_of_inftree_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (InflateBlockMode_t2135077436)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[11] = 
{
	InflateBlockMode_t2135077436::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (InternalInflateConstants_t3129902576), -1, sizeof(InternalInflateConstants_t3129902576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1866[1] = 
{
	InternalInflateConstants_t3129902576_StaticFields::get_offset_of_InflateMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (InflateCodes_t3874557824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InflateCodes_t3874557824::get_offset_of_mode_10(),
	InflateCodes_t3874557824::get_offset_of_len_11(),
	InflateCodes_t3874557824::get_offset_of_tree_12(),
	InflateCodes_t3874557824::get_offset_of_tree_index_13(),
	InflateCodes_t3874557824::get_offset_of_need_14(),
	InflateCodes_t3874557824::get_offset_of_lit_15(),
	InflateCodes_t3874557824::get_offset_of_bitsToGet_16(),
	InflateCodes_t3874557824::get_offset_of_dist_17(),
	InflateCodes_t3874557824::get_offset_of_lbits_18(),
	InflateCodes_t3874557824::get_offset_of_dbits_19(),
	InflateCodes_t3874557824::get_offset_of_ltree_20(),
	InflateCodes_t3874557824::get_offset_of_ltree_index_21(),
	InflateCodes_t3874557824::get_offset_of_dtree_22(),
	InflateCodes_t3874557824::get_offset_of_dtree_index_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (InflateManager_t3407211616), -1, sizeof(InflateManager_t3407211616_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1868[12] = 
{
	0,
	0,
	InflateManager_t3407211616::get_offset_of_mode_2(),
	InflateManager_t3407211616::get_offset_of__codec_3(),
	InflateManager_t3407211616::get_offset_of_method_4(),
	InflateManager_t3407211616::get_offset_of_computedCheck_5(),
	InflateManager_t3407211616::get_offset_of_expectedCheck_6(),
	InflateManager_t3407211616::get_offset_of_marker_7(),
	InflateManager_t3407211616::get_offset_of__handleRfc1950HeaderBytes_8(),
	InflateManager_t3407211616::get_offset_of_wbits_9(),
	InflateManager_t3407211616::get_offset_of_blocks_10(),
	InflateManager_t3407211616_StaticFields::get_offset_of_mark_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (InflateManagerMode_t696478391)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[15] = 
{
	InflateManagerMode_t696478391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (InfTree_t3075871205), -1, sizeof(InfTree_t3075871205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1870[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfTree_t3075871205_StaticFields::get_offset_of_fixed_tl_12(),
	InfTree_t3075871205_StaticFields::get_offset_of_fixed_td_13(),
	InfTree_t3075871205_StaticFields::get_offset_of_cplens_14(),
	InfTree_t3075871205_StaticFields::get_offset_of_cplext_15(),
	InfTree_t3075871205_StaticFields::get_offset_of_cpdist_16(),
	InfTree_t3075871205_StaticFields::get_offset_of_cpdext_17(),
	0,
	InfTree_t3075871205::get_offset_of_hn_19(),
	InfTree_t3075871205::get_offset_of_v_20(),
	InfTree_t3075871205::get_offset_of_c_21(),
	InfTree_t3075871205::get_offset_of_r_22(),
	InfTree_t3075871205::get_offset_of_u_23(),
	InfTree_t3075871205::get_offset_of_x_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (WorkItem_t2895573498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[8] = 
{
	WorkItem_t2895573498::get_offset_of_buffer_0(),
	WorkItem_t2895573498::get_offset_of_compressed_1(),
	WorkItem_t2895573498::get_offset_of_status_2(),
	WorkItem_t2895573498::get_offset_of_crc_3(),
	WorkItem_t2895573498::get_offset_of_index_4(),
	WorkItem_t2895573498::get_offset_of_inputBytesAvailable_5(),
	WorkItem_t2895573498::get_offset_of_compressedBytesAvailable_6(),
	WorkItem_t2895573498::get_offset_of_compressor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (Status_t137489413)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1872[8] = 
{
	Status_t137489413::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (ParallelDeflateOutputStream_t2864412598), -1, sizeof(ParallelDeflateOutputStream_t2864412598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1873[23] = 
{
	ParallelDeflateOutputStream_t2864412598_StaticFields::get_offset_of_IO_BUFFER_SIZE_DEFAULT_1(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__pool_2(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__leaveOpen_3(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__outStream_4(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__nextToFill_5(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__nextToWrite_6(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__bufferSize_7(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__writingDone_8(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__sessionReset_9(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__noMoreInputForThisSegment_10(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__outputLock_11(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__isClosed_12(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__isDisposed_13(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__firstWriteDone_14(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__pc_15(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__Crc32_16(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__totalBytesProcessed_17(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__compressLevel_18(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__pendingException_19(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__eLock_20(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of__DesiredTrace_21(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of_U3CStrategyU3Ek__BackingField_22(),
	ParallelDeflateOutputStream_t2864412598::get_offset_of_U3CBuffersPerCoreU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (TraceBits_t962761545)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1874[13] = 
{
	TraceBits_t962761545::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (CompressionLevel_t957220238)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1875[15] = 
{
	CompressionLevel_t957220238::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (CompressionStrategy_t2822174634)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[4] = 
{
	CompressionStrategy_t2822174634::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (CompressionMode_t1439136530)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1877[3] = 
{
	CompressionMode_t1439136530::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (ZlibException_t2135990123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (SharedUtils_t3538106298), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (InternalConstants_t2406264312), -1, sizeof(InternalConstants_t2406264312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1880[10] = 
{
	InternalConstants_t2406264312_StaticFields::get_offset_of_MAX_BITS_0(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_BL_CODES_1(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_D_CODES_2(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_LITERALS_3(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_LENGTH_CODES_4(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_L_CODES_5(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_MAX_BL_BITS_6(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_REP_3_6_7(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_REPZ_3_10_8(),
	InternalConstants_t2406264312_StaticFields::get_offset_of_REPZ_11_138_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (StaticTree_t2118636970), -1, sizeof(StaticTree_t2118636970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1881[10] = 
{
	StaticTree_t2118636970_StaticFields::get_offset_of_lengthAndLiteralsTreeCodes_0(),
	StaticTree_t2118636970_StaticFields::get_offset_of_distTreeCodes_1(),
	StaticTree_t2118636970_StaticFields::get_offset_of_Literals_2(),
	StaticTree_t2118636970_StaticFields::get_offset_of_Distances_3(),
	StaticTree_t2118636970_StaticFields::get_offset_of_BitLengths_4(),
	StaticTree_t2118636970::get_offset_of_treeCodes_5(),
	StaticTree_t2118636970::get_offset_of_extraBits_6(),
	StaticTree_t2118636970::get_offset_of_extraBase_7(),
	StaticTree_t2118636970::get_offset_of_elems_8(),
	StaticTree_t2118636970::get_offset_of_maxLength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (Adler_t1959216880), -1, sizeof(Adler_t1959216880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1882[2] = 
{
	Adler_t1959216880_StaticFields::get_offset_of_BASE_0(),
	Adler_t1959216880_StaticFields::get_offset_of_NMAX_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (ZlibStreamFlavor_t704535502)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[4] = 
{
	ZlibStreamFlavor_t704535502::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ZlibBaseStream_t1591821133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[18] = 
{
	ZlibBaseStream_t1591821133::get_offset_of__z_1(),
	ZlibBaseStream_t1591821133::get_offset_of__streamMode_2(),
	ZlibBaseStream_t1591821133::get_offset_of__flushMode_3(),
	ZlibBaseStream_t1591821133::get_offset_of__flavor_4(),
	ZlibBaseStream_t1591821133::get_offset_of__compressionMode_5(),
	ZlibBaseStream_t1591821133::get_offset_of__level_6(),
	ZlibBaseStream_t1591821133::get_offset_of__leaveOpen_7(),
	ZlibBaseStream_t1591821133::get_offset_of__workingBuffer_8(),
	ZlibBaseStream_t1591821133::get_offset_of__bufferSize_9(),
	ZlibBaseStream_t1591821133::get_offset_of__buf1_10(),
	ZlibBaseStream_t1591821133::get_offset_of__stream_11(),
	ZlibBaseStream_t1591821133::get_offset_of_Strategy_12(),
	ZlibBaseStream_t1591821133::get_offset_of_crc_13(),
	ZlibBaseStream_t1591821133::get_offset_of__GzipFileName_14(),
	ZlibBaseStream_t1591821133::get_offset_of__GzipComment_15(),
	ZlibBaseStream_t1591821133::get_offset_of__GzipMtime_16(),
	ZlibBaseStream_t1591821133::get_offset_of__gzipHeaderByteCount_17(),
	ZlibBaseStream_t1591821133::get_offset_of_nomoreinput_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (StreamMode_t645432565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[4] = 
{
	StreamMode_t645432565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (ZlibCodec_t3653667923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[15] = 
{
	ZlibCodec_t3653667923::get_offset_of_InputBuffer_0(),
	ZlibCodec_t3653667923::get_offset_of_NextIn_1(),
	ZlibCodec_t3653667923::get_offset_of_AvailableBytesIn_2(),
	ZlibCodec_t3653667923::get_offset_of_TotalBytesIn_3(),
	ZlibCodec_t3653667923::get_offset_of_OutputBuffer_4(),
	ZlibCodec_t3653667923::get_offset_of_NextOut_5(),
	ZlibCodec_t3653667923::get_offset_of_AvailableBytesOut_6(),
	ZlibCodec_t3653667923::get_offset_of_TotalBytesOut_7(),
	ZlibCodec_t3653667923::get_offset_of_Message_8(),
	ZlibCodec_t3653667923::get_offset_of_dstate_9(),
	ZlibCodec_t3653667923::get_offset_of_istate_10(),
	ZlibCodec_t3653667923::get_offset_of__Adler32_11(),
	ZlibCodec_t3653667923::get_offset_of_CompressLevel_12(),
	ZlibCodec_t3653667923::get_offset_of_WindowBits_13(),
	ZlibCodec_t3653667923::get_offset_of_Strategy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (ZlibConstants_t3760349222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (ZlibStream_t1786730409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[2] = 
{
	ZlibStream_t1786730409::get_offset_of__baseStream_1(),
	ZlibStream_t1786730409::get_offset_of__disposed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (Tree_t526993154), -1, sizeof(Tree_t526993154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1889[13] = 
{
	Tree_t526993154_StaticFields::get_offset_of_HEAP_SIZE_0(),
	Tree_t526993154_StaticFields::get_offset_of_ExtraLengthBits_1(),
	Tree_t526993154_StaticFields::get_offset_of_ExtraDistanceBits_2(),
	Tree_t526993154_StaticFields::get_offset_of_extra_blbits_3(),
	Tree_t526993154_StaticFields::get_offset_of_bl_order_4(),
	0,
	Tree_t526993154_StaticFields::get_offset_of__dist_code_6(),
	Tree_t526993154_StaticFields::get_offset_of_LengthCode_7(),
	Tree_t526993154_StaticFields::get_offset_of_LengthBase_8(),
	Tree_t526993154_StaticFields::get_offset_of_DistanceBase_9(),
	Tree_t526993154::get_offset_of_dyn_tree_10(),
	Tree_t526993154::get_offset_of_max_code_11(),
	Tree_t526993154::get_offset_of_staticTree_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (NuTrackingHelpers_t4202934765), -1, sizeof(NuTrackingHelpers_t4202934765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1890[17] = 
{
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_gameShortName_0(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_U3CGAAccountCodeU3Ek__BackingField_1(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of__GAHostDomain_2(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of__GAHostDomainHash_3(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_U3CGameStartupTimeInUTCU3Ek__BackingField_4(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_WantObfuscateGlobalIP_5(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_WantMonitorEditor_6(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_WantLog_7(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_globalIPRetrieved_8(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_globalIP_9(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_rand_10(),
	0,
	0,
	0,
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_cookieCreated_14(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_utmaCookie_15(),
	NuTrackingHelpers_t4202934765_StaticFields::get_offset_of_utmzCookie_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[4] = 
{
	U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361::get_offset_of_U3CrU3E__0_0(),
	U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361::get_offset_of_U24current_1(),
	U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361::get_offset_of_U24disposing_2(),
	U3CRetrieveGlobalIPU3Ec__Iterator0_t1869265361::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (NuGAEvent_t4041358949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[4] = 
{
	NuGAEvent_t4041358949::get_offset_of_U3CObjectU3Ek__BackingField_0(),
	NuGAEvent_t4041358949::get_offset_of_U3CActionU3Ek__BackingField_1(),
	NuGAEvent_t4041358949::get_offset_of_U3CLabelU3Ek__BackingField_2(),
	NuGAEvent_t4041358949::get_offset_of_U3CValueU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (NuGARequest_t3358732621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[7] = 
{
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of__urlList_0(),
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of_U24locvar0_1(),
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of_U3CurlU3E__1_2(),
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of_U3CrU3E__2_3(),
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of_U24current_4(),
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of_U24disposing_5(),
	U3CSendAsyncWebRequestU3Ec__Iterator0_t2096338983::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (NuGAService_t1586892016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (NuGATransaction_t3923616719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[4] = 
{
	NuGATransaction_t3923616719::get_offset_of_U3CProductCodeU3Ek__BackingField_0(),
	NuGATransaction_t3923616719::get_offset_of_U3CProductNameU3Ek__BackingField_1(),
	NuGATransaction_t3923616719::get_offset_of_U3CProductUnitPriceU3Ek__BackingField_2(),
	NuGATransaction_t3923616719::get_offset_of_U3CProductQuantityU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (NuTracking_t472478902), -1, sizeof(NuTracking_t472478902_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1897[4] = 
{
	NuTracking_t472478902_StaticFields::get_offset_of_staticInstance_2(),
	NuTracking_t472478902::get_offset_of_isStarted_3(),
	NuTracking_t472478902::get_offset_of_gaService_4(),
	NuTracking_t472478902::get_offset_of_requestQueue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (TrackingRequest_t2594580146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[4] = 
{
	TrackingRequest_t2594580146::get_offset_of_pageTitle_0(),
	TrackingRequest_t2594580146::get_offset_of_pageUrl_1(),
	TrackingRequest_t2594580146::get_offset_of_gaEvent_2(),
	TrackingRequest_t2594580146::get_offset_of_gaTransaction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[4] = 
{
	U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271::get_offset_of_U24this_0(),
	U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271::get_offset_of_U24current_1(),
	U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271::get_offset_of_U24disposing_2(),
	U3CFlushTrackingQuestsAsyncU3Ec__Iterator0_t545484271::get_offset_of_U24PC_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
