﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// MatrixCCS
struct MatrixCCS_t3519621953;
// FEAVibrate/SendNextTimestep
struct SendNextTimestep_t1962693451;
// System.String
struct String_t;
// System.Collections.Generic.List`1<LevelComponentLoad>
struct List_1_t1725606226;
// System.Collections.Generic.List`1<LevelComponentSupport>
struct List_1_t26227495;
// System.Collections.Generic.List`1<LevelComponentDispl>
struct List_1_t2739867305;
// System.Collections.Generic.List`1<LevelComponentDisplEnd>
struct List_1_t1908428420;
// System.Collections.Generic.List`1<LevelComponentDispl2>
struct List_1_t2943684265;
// System.Collections.Generic.List`1<LevelComponentDisplEnd2>
struct List_1_t3691557916;
// System.Collections.Generic.List`1<Vec2i>
struct List_1_t2665353730;
// Level[]
struct LevelU5BU5D_t979096037;
// Screenshot
struct Screenshot_t3680149077;
// System.Byte[0...,0...]
struct ByteU5BU2CU5D_t4116647658;
// Svec[]
struct SvecU5BU5D_t2458350361;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// PainterCanvas
struct PainterCanvas_t4160673718;
// PassiveBlock
struct PassiveBlock_t513448541;
// GameController
struct GameController_t2330501625;
// GUIPaintHandler
struct GUIPaintHandler_t437174526;
// FEAComponentData[]
struct FEAComponentDataU5BU5D_t1047043642;
// SKStateMachine`1<TopologySelector>
struct SKStateMachine_1_t2564681790;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// TopologySelector
struct TopologySelector_t1922582608;
// UnityEngine.Transform
struct Transform_t3600365921;
// TopologyComponent
struct TopologyComponent_t4181154066;
// DrawMenuGUI
struct DrawMenuGUI_t2806387517;
// BoundingConstraintsGUI
struct BoundingConstraintsGUI_t795470468;
// TopologyOptimization/NotifyUpdateObjectiveFunction
struct NotifyUpdateObjectiveFunction_t2430789460;
// TopologyOptimization/GetUpdatedTopologyElementDensity
struct GetUpdatedTopologyElementDensity_t3009975164;
// System.Collections.Generic.Dictionary`2<System.Int32,TopologyComponent>
struct Dictionary_2_t3069867397;
// SparseLDL
struct SparseLDL_t1701177157;
// FEAMatrixCompress
struct FEAMatrixCompress_t1508710683;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// FEAGameResult
struct FEAGameResult_t3745188880;
// FEAGameData
struct FEAGameData_t1628373741;
// UnityEngine.GUISkin[]
struct GUISkinU5BU5D_t4017239199;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.Material
struct Material_t340375123;
// LevelComponentSupport
struct LevelComponentSupport_t2849120049;
// LevelComponentDispl
struct LevelComponentDispl_t1267792563;
// LevelComponentDisplEnd
struct LevelComponentDisplEnd_t436353678;
// LevelComponentDispl2
struct LevelComponentDispl2_t1471609523;
// LevelComponentDisplEnd2
struct LevelComponentDisplEnd2_t2219483174;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// LinePainter`1<PassiveBlock/ElementType>
struct LinePainter_1_t2650701617;
// System.EventHandler
struct EventHandler_t1348719766;
// GUIPaintHandler/UpdateTool
struct UpdateTool_t905929733;
// MiniGUI
struct MiniGUI_t213183967;
// ThinThickTool
struct ThinThickTool_t2872687508;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Level
struct Level_t2237665516;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// Menu[]
struct MenuU5BU5D_t2527869020;
// UnityEngine.Camera
struct Camera_t4157153871;
// CanvasMesh
struct CanvasMesh_t1528759999;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// ColorBarGUI
struct ColorBarGUI_t1806409552;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.UI.Image
struct Image_t2670269651;
// GameGUI
struct GameGUI_t2956616616;
// ThreadRunner`2<System.Int32,FEAGameResult>
struct ThreadRunner_2_t2510904282;
// ThreadRunner`2<FEAGameData,FEAGameResult>
struct ThreadRunner_2_t3453383158;
// FEA
struct FEA_t3250963177;
// FEAVibrate
struct FEAVibrate_t3015876168;
// GameController/NotifyEvaluationUpdate
struct NotifyEvaluationUpdate_t2480692004;
// WorkingAnimation
struct WorkingAnimation_t2928320557;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LEVELCOMPONENTDISPL2_T1471609523_H
#define LEVELCOMPONENTDISPL2_T1471609523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponentDispl2
struct  LevelComponentDispl2_t1471609523  : public RuntimeObject
{
public:
	// System.Int32 LevelComponentDispl2::positionX
	int32_t ___positionX_0;
	// System.Int32 LevelComponentDispl2::positionY
	int32_t ___positionY_1;
	// System.Single LevelComponentDispl2::directionX
	float ___directionX_2;
	// System.Single LevelComponentDispl2::directionY
	float ___directionY_3;
	// System.Int32 LevelComponentDispl2::displid
	int32_t ___displid_4;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(LevelComponentDispl2_t1471609523, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(LevelComponentDispl2_t1471609523, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(LevelComponentDispl2_t1471609523, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(LevelComponentDispl2_t1471609523, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_displid_4() { return static_cast<int32_t>(offsetof(LevelComponentDispl2_t1471609523, ___displid_4)); }
	inline int32_t get_displid_4() const { return ___displid_4; }
	inline int32_t* get_address_of_displid_4() { return &___displid_4; }
	inline void set_displid_4(int32_t value)
	{
		___displid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENTDISPL2_T1471609523_H
#ifndef FEAVIBRATE_T3015876168_H
#define FEAVIBRATE_T3015876168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEAVibrate
struct  FEAVibrate_t3015876168  : public RuntimeObject
{
public:
	// System.Single FEAVibrate::dt
	float ___dt_12;
	// System.Single FEAVibrate::max_norm
	float ___max_norm_13;
	// System.Single[] FEAVibrate::M
	SingleU5BU5D_t1444911251* ___M_14;
	// System.Single[] FEAVibrate::D
	SingleU5BU5D_t1444911251* ___D_15;
	// System.Single[] FEAVibrate::D_old
	SingleU5BU5D_t1444911251* ___D_old_16;
	// System.Single[] FEAVibrate::D_new
	SingleU5BU5D_t1444911251* ___D_new_17;
	// System.Single[] FEAVibrate::LHS_inv
	SingleU5BU5D_t1444911251* ___LHS_inv_18;
	// System.Single[] FEAVibrate::RHS_Dold
	SingleU5BU5D_t1444911251* ___RHS_Dold_19;
	// System.Single[] FEAVibrate::RHS_D
	SingleU5BU5D_t1444911251* ___RHS_D_20;
	// System.Single[] FEAVibrate::R_int
	SingleU5BU5D_t1444911251* ___R_int_21;
	// System.Int32[] FEAVibrate::ElementMaterials
	Int32U5BU5D_t385246372* ___ElementMaterials_22;
	// System.Int32[] FEAVibrate::edof_expander
	Int32U5BU5D_t385246372* ___edof_expander_23;
	// System.Int32[] FEAVibrate::edof_reductor
	Int32U5BU5D_t385246372* ___edof_reductor_24;
	// System.Int32 FEAVibrate::ndof
	int32_t ___ndof_25;
	// System.Int32 FEAVibrate::ney
	int32_t ___ney_26;
	// System.Int32 FEAVibrate::nex
	int32_t ___nex_27;
	// System.Int32 FEAVibrate::nelem
	int32_t ___nelem_28;
	// System.Int32 FEAVibrate::ndof_full
	int32_t ___ndof_full_29;
	// MatrixCCS FEAVibrate::KK
	MatrixCCS_t3519621953 * ___KK_30;
	// MatrixCCS FEAVibrate::CD
	MatrixCCS_t3519621953 * ___CD_31;
	// MatrixCCS FEAVibrate::CD_old
	MatrixCCS_t3519621953 * ___CD_old_32;
	// System.Int32[] FEAVibrate::displs2_dof
	Int32U5BU5D_t385246372* ___displs2_dof_33;
	// System.Single[] FEAVibrate::displs2_val
	SingleU5BU5D_t1444911251* ___displs2_val_34;
	// System.Int32[] FEAVibrate::fdof
	Int32U5BU5D_t385246372* ___fdof_35;
	// System.Int32 FEAVibrate::PlotChoice
	int32_t ___PlotChoice_36;
	// System.Boolean FEAVibrate::destroyed
	bool ___destroyed_37;
	// FEAVibrate/SendNextTimestep FEAVibrate::sendnexttimestep
	SendNextTimestep_t1962693451 * ___sendnexttimestep_38;

public:
	inline static int32_t get_offset_of_dt_12() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___dt_12)); }
	inline float get_dt_12() const { return ___dt_12; }
	inline float* get_address_of_dt_12() { return &___dt_12; }
	inline void set_dt_12(float value)
	{
		___dt_12 = value;
	}

	inline static int32_t get_offset_of_max_norm_13() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___max_norm_13)); }
	inline float get_max_norm_13() const { return ___max_norm_13; }
	inline float* get_address_of_max_norm_13() { return &___max_norm_13; }
	inline void set_max_norm_13(float value)
	{
		___max_norm_13 = value;
	}

	inline static int32_t get_offset_of_M_14() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___M_14)); }
	inline SingleU5BU5D_t1444911251* get_M_14() const { return ___M_14; }
	inline SingleU5BU5D_t1444911251** get_address_of_M_14() { return &___M_14; }
	inline void set_M_14(SingleU5BU5D_t1444911251* value)
	{
		___M_14 = value;
		Il2CppCodeGenWriteBarrier((&___M_14), value);
	}

	inline static int32_t get_offset_of_D_15() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___D_15)); }
	inline SingleU5BU5D_t1444911251* get_D_15() const { return ___D_15; }
	inline SingleU5BU5D_t1444911251** get_address_of_D_15() { return &___D_15; }
	inline void set_D_15(SingleU5BU5D_t1444911251* value)
	{
		___D_15 = value;
		Il2CppCodeGenWriteBarrier((&___D_15), value);
	}

	inline static int32_t get_offset_of_D_old_16() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___D_old_16)); }
	inline SingleU5BU5D_t1444911251* get_D_old_16() const { return ___D_old_16; }
	inline SingleU5BU5D_t1444911251** get_address_of_D_old_16() { return &___D_old_16; }
	inline void set_D_old_16(SingleU5BU5D_t1444911251* value)
	{
		___D_old_16 = value;
		Il2CppCodeGenWriteBarrier((&___D_old_16), value);
	}

	inline static int32_t get_offset_of_D_new_17() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___D_new_17)); }
	inline SingleU5BU5D_t1444911251* get_D_new_17() const { return ___D_new_17; }
	inline SingleU5BU5D_t1444911251** get_address_of_D_new_17() { return &___D_new_17; }
	inline void set_D_new_17(SingleU5BU5D_t1444911251* value)
	{
		___D_new_17 = value;
		Il2CppCodeGenWriteBarrier((&___D_new_17), value);
	}

	inline static int32_t get_offset_of_LHS_inv_18() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___LHS_inv_18)); }
	inline SingleU5BU5D_t1444911251* get_LHS_inv_18() const { return ___LHS_inv_18; }
	inline SingleU5BU5D_t1444911251** get_address_of_LHS_inv_18() { return &___LHS_inv_18; }
	inline void set_LHS_inv_18(SingleU5BU5D_t1444911251* value)
	{
		___LHS_inv_18 = value;
		Il2CppCodeGenWriteBarrier((&___LHS_inv_18), value);
	}

	inline static int32_t get_offset_of_RHS_Dold_19() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___RHS_Dold_19)); }
	inline SingleU5BU5D_t1444911251* get_RHS_Dold_19() const { return ___RHS_Dold_19; }
	inline SingleU5BU5D_t1444911251** get_address_of_RHS_Dold_19() { return &___RHS_Dold_19; }
	inline void set_RHS_Dold_19(SingleU5BU5D_t1444911251* value)
	{
		___RHS_Dold_19 = value;
		Il2CppCodeGenWriteBarrier((&___RHS_Dold_19), value);
	}

	inline static int32_t get_offset_of_RHS_D_20() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___RHS_D_20)); }
	inline SingleU5BU5D_t1444911251* get_RHS_D_20() const { return ___RHS_D_20; }
	inline SingleU5BU5D_t1444911251** get_address_of_RHS_D_20() { return &___RHS_D_20; }
	inline void set_RHS_D_20(SingleU5BU5D_t1444911251* value)
	{
		___RHS_D_20 = value;
		Il2CppCodeGenWriteBarrier((&___RHS_D_20), value);
	}

	inline static int32_t get_offset_of_R_int_21() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___R_int_21)); }
	inline SingleU5BU5D_t1444911251* get_R_int_21() const { return ___R_int_21; }
	inline SingleU5BU5D_t1444911251** get_address_of_R_int_21() { return &___R_int_21; }
	inline void set_R_int_21(SingleU5BU5D_t1444911251* value)
	{
		___R_int_21 = value;
		Il2CppCodeGenWriteBarrier((&___R_int_21), value);
	}

	inline static int32_t get_offset_of_ElementMaterials_22() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___ElementMaterials_22)); }
	inline Int32U5BU5D_t385246372* get_ElementMaterials_22() const { return ___ElementMaterials_22; }
	inline Int32U5BU5D_t385246372** get_address_of_ElementMaterials_22() { return &___ElementMaterials_22; }
	inline void set_ElementMaterials_22(Int32U5BU5D_t385246372* value)
	{
		___ElementMaterials_22 = value;
		Il2CppCodeGenWriteBarrier((&___ElementMaterials_22), value);
	}

	inline static int32_t get_offset_of_edof_expander_23() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___edof_expander_23)); }
	inline Int32U5BU5D_t385246372* get_edof_expander_23() const { return ___edof_expander_23; }
	inline Int32U5BU5D_t385246372** get_address_of_edof_expander_23() { return &___edof_expander_23; }
	inline void set_edof_expander_23(Int32U5BU5D_t385246372* value)
	{
		___edof_expander_23 = value;
		Il2CppCodeGenWriteBarrier((&___edof_expander_23), value);
	}

	inline static int32_t get_offset_of_edof_reductor_24() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___edof_reductor_24)); }
	inline Int32U5BU5D_t385246372* get_edof_reductor_24() const { return ___edof_reductor_24; }
	inline Int32U5BU5D_t385246372** get_address_of_edof_reductor_24() { return &___edof_reductor_24; }
	inline void set_edof_reductor_24(Int32U5BU5D_t385246372* value)
	{
		___edof_reductor_24 = value;
		Il2CppCodeGenWriteBarrier((&___edof_reductor_24), value);
	}

	inline static int32_t get_offset_of_ndof_25() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___ndof_25)); }
	inline int32_t get_ndof_25() const { return ___ndof_25; }
	inline int32_t* get_address_of_ndof_25() { return &___ndof_25; }
	inline void set_ndof_25(int32_t value)
	{
		___ndof_25 = value;
	}

	inline static int32_t get_offset_of_ney_26() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___ney_26)); }
	inline int32_t get_ney_26() const { return ___ney_26; }
	inline int32_t* get_address_of_ney_26() { return &___ney_26; }
	inline void set_ney_26(int32_t value)
	{
		___ney_26 = value;
	}

	inline static int32_t get_offset_of_nex_27() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___nex_27)); }
	inline int32_t get_nex_27() const { return ___nex_27; }
	inline int32_t* get_address_of_nex_27() { return &___nex_27; }
	inline void set_nex_27(int32_t value)
	{
		___nex_27 = value;
	}

	inline static int32_t get_offset_of_nelem_28() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___nelem_28)); }
	inline int32_t get_nelem_28() const { return ___nelem_28; }
	inline int32_t* get_address_of_nelem_28() { return &___nelem_28; }
	inline void set_nelem_28(int32_t value)
	{
		___nelem_28 = value;
	}

	inline static int32_t get_offset_of_ndof_full_29() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___ndof_full_29)); }
	inline int32_t get_ndof_full_29() const { return ___ndof_full_29; }
	inline int32_t* get_address_of_ndof_full_29() { return &___ndof_full_29; }
	inline void set_ndof_full_29(int32_t value)
	{
		___ndof_full_29 = value;
	}

	inline static int32_t get_offset_of_KK_30() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___KK_30)); }
	inline MatrixCCS_t3519621953 * get_KK_30() const { return ___KK_30; }
	inline MatrixCCS_t3519621953 ** get_address_of_KK_30() { return &___KK_30; }
	inline void set_KK_30(MatrixCCS_t3519621953 * value)
	{
		___KK_30 = value;
		Il2CppCodeGenWriteBarrier((&___KK_30), value);
	}

	inline static int32_t get_offset_of_CD_31() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___CD_31)); }
	inline MatrixCCS_t3519621953 * get_CD_31() const { return ___CD_31; }
	inline MatrixCCS_t3519621953 ** get_address_of_CD_31() { return &___CD_31; }
	inline void set_CD_31(MatrixCCS_t3519621953 * value)
	{
		___CD_31 = value;
		Il2CppCodeGenWriteBarrier((&___CD_31), value);
	}

	inline static int32_t get_offset_of_CD_old_32() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___CD_old_32)); }
	inline MatrixCCS_t3519621953 * get_CD_old_32() const { return ___CD_old_32; }
	inline MatrixCCS_t3519621953 ** get_address_of_CD_old_32() { return &___CD_old_32; }
	inline void set_CD_old_32(MatrixCCS_t3519621953 * value)
	{
		___CD_old_32 = value;
		Il2CppCodeGenWriteBarrier((&___CD_old_32), value);
	}

	inline static int32_t get_offset_of_displs2_dof_33() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___displs2_dof_33)); }
	inline Int32U5BU5D_t385246372* get_displs2_dof_33() const { return ___displs2_dof_33; }
	inline Int32U5BU5D_t385246372** get_address_of_displs2_dof_33() { return &___displs2_dof_33; }
	inline void set_displs2_dof_33(Int32U5BU5D_t385246372* value)
	{
		___displs2_dof_33 = value;
		Il2CppCodeGenWriteBarrier((&___displs2_dof_33), value);
	}

	inline static int32_t get_offset_of_displs2_val_34() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___displs2_val_34)); }
	inline SingleU5BU5D_t1444911251* get_displs2_val_34() const { return ___displs2_val_34; }
	inline SingleU5BU5D_t1444911251** get_address_of_displs2_val_34() { return &___displs2_val_34; }
	inline void set_displs2_val_34(SingleU5BU5D_t1444911251* value)
	{
		___displs2_val_34 = value;
		Il2CppCodeGenWriteBarrier((&___displs2_val_34), value);
	}

	inline static int32_t get_offset_of_fdof_35() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___fdof_35)); }
	inline Int32U5BU5D_t385246372* get_fdof_35() const { return ___fdof_35; }
	inline Int32U5BU5D_t385246372** get_address_of_fdof_35() { return &___fdof_35; }
	inline void set_fdof_35(Int32U5BU5D_t385246372* value)
	{
		___fdof_35 = value;
		Il2CppCodeGenWriteBarrier((&___fdof_35), value);
	}

	inline static int32_t get_offset_of_PlotChoice_36() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___PlotChoice_36)); }
	inline int32_t get_PlotChoice_36() const { return ___PlotChoice_36; }
	inline int32_t* get_address_of_PlotChoice_36() { return &___PlotChoice_36; }
	inline void set_PlotChoice_36(int32_t value)
	{
		___PlotChoice_36 = value;
	}

	inline static int32_t get_offset_of_destroyed_37() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___destroyed_37)); }
	inline bool get_destroyed_37() const { return ___destroyed_37; }
	inline bool* get_address_of_destroyed_37() { return &___destroyed_37; }
	inline void set_destroyed_37(bool value)
	{
		___destroyed_37 = value;
	}

	inline static int32_t get_offset_of_sendnexttimestep_38() { return static_cast<int32_t>(offsetof(FEAVibrate_t3015876168, ___sendnexttimestep_38)); }
	inline SendNextTimestep_t1962693451 * get_sendnexttimestep_38() const { return ___sendnexttimestep_38; }
	inline SendNextTimestep_t1962693451 ** get_address_of_sendnexttimestep_38() { return &___sendnexttimestep_38; }
	inline void set_sendnexttimestep_38(SendNextTimestep_t1962693451 * value)
	{
		___sendnexttimestep_38 = value;
		Il2CppCodeGenWriteBarrier((&___sendnexttimestep_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEAVIBRATE_T3015876168_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LEVEL_T2237665516_H
#define LEVEL_T2237665516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level
struct  Level_t2237665516  : public RuntimeObject
{
public:
	// System.String Level::name
	String_t* ___name_0;
	// System.Single Level::maxTime
	float ___maxTime_1;
	// System.Single Level::volumeFraction
	float ___volumeFraction_2;
	// System.Int32 Level::resolutionX
	int32_t ___resolutionX_3;
	// System.Int32 Level::resolutionY
	int32_t ___resolutionY_4;
	// System.Int32 Level::numberOfLoadCases
	int32_t ___numberOfLoadCases_5;
	// System.Collections.Generic.List`1<LevelComponentLoad> Level::loads
	List_1_t1725606226 * ___loads_6;
	// System.Collections.Generic.List`1<LevelComponentSupport> Level::supports
	List_1_t26227495 * ___supports_7;
	// System.Collections.Generic.List`1<LevelComponentDispl> Level::displs
	List_1_t2739867305 * ___displs_8;
	// System.Collections.Generic.List`1<LevelComponentDisplEnd> Level::displEnds
	List_1_t1908428420 * ___displEnds_9;
	// System.Collections.Generic.List`1<LevelComponentDispl2> Level::displs2
	List_1_t2943684265 * ___displs2_10;
	// System.Collections.Generic.List`1<LevelComponentDisplEnd2> Level::displEnds2
	List_1_t3691557916 * ___displEnds2_11;
	// System.Collections.Generic.List`1<Vec2i> Level::voidElements
	List_1_t2665353730 * ___voidElements_12;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_maxTime_1() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___maxTime_1)); }
	inline float get_maxTime_1() const { return ___maxTime_1; }
	inline float* get_address_of_maxTime_1() { return &___maxTime_1; }
	inline void set_maxTime_1(float value)
	{
		___maxTime_1 = value;
	}

	inline static int32_t get_offset_of_volumeFraction_2() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___volumeFraction_2)); }
	inline float get_volumeFraction_2() const { return ___volumeFraction_2; }
	inline float* get_address_of_volumeFraction_2() { return &___volumeFraction_2; }
	inline void set_volumeFraction_2(float value)
	{
		___volumeFraction_2 = value;
	}

	inline static int32_t get_offset_of_resolutionX_3() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___resolutionX_3)); }
	inline int32_t get_resolutionX_3() const { return ___resolutionX_3; }
	inline int32_t* get_address_of_resolutionX_3() { return &___resolutionX_3; }
	inline void set_resolutionX_3(int32_t value)
	{
		___resolutionX_3 = value;
	}

	inline static int32_t get_offset_of_resolutionY_4() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___resolutionY_4)); }
	inline int32_t get_resolutionY_4() const { return ___resolutionY_4; }
	inline int32_t* get_address_of_resolutionY_4() { return &___resolutionY_4; }
	inline void set_resolutionY_4(int32_t value)
	{
		___resolutionY_4 = value;
	}

	inline static int32_t get_offset_of_numberOfLoadCases_5() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___numberOfLoadCases_5)); }
	inline int32_t get_numberOfLoadCases_5() const { return ___numberOfLoadCases_5; }
	inline int32_t* get_address_of_numberOfLoadCases_5() { return &___numberOfLoadCases_5; }
	inline void set_numberOfLoadCases_5(int32_t value)
	{
		___numberOfLoadCases_5 = value;
	}

	inline static int32_t get_offset_of_loads_6() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___loads_6)); }
	inline List_1_t1725606226 * get_loads_6() const { return ___loads_6; }
	inline List_1_t1725606226 ** get_address_of_loads_6() { return &___loads_6; }
	inline void set_loads_6(List_1_t1725606226 * value)
	{
		___loads_6 = value;
		Il2CppCodeGenWriteBarrier((&___loads_6), value);
	}

	inline static int32_t get_offset_of_supports_7() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___supports_7)); }
	inline List_1_t26227495 * get_supports_7() const { return ___supports_7; }
	inline List_1_t26227495 ** get_address_of_supports_7() { return &___supports_7; }
	inline void set_supports_7(List_1_t26227495 * value)
	{
		___supports_7 = value;
		Il2CppCodeGenWriteBarrier((&___supports_7), value);
	}

	inline static int32_t get_offset_of_displs_8() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___displs_8)); }
	inline List_1_t2739867305 * get_displs_8() const { return ___displs_8; }
	inline List_1_t2739867305 ** get_address_of_displs_8() { return &___displs_8; }
	inline void set_displs_8(List_1_t2739867305 * value)
	{
		___displs_8 = value;
		Il2CppCodeGenWriteBarrier((&___displs_8), value);
	}

	inline static int32_t get_offset_of_displEnds_9() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___displEnds_9)); }
	inline List_1_t1908428420 * get_displEnds_9() const { return ___displEnds_9; }
	inline List_1_t1908428420 ** get_address_of_displEnds_9() { return &___displEnds_9; }
	inline void set_displEnds_9(List_1_t1908428420 * value)
	{
		___displEnds_9 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds_9), value);
	}

	inline static int32_t get_offset_of_displs2_10() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___displs2_10)); }
	inline List_1_t2943684265 * get_displs2_10() const { return ___displs2_10; }
	inline List_1_t2943684265 ** get_address_of_displs2_10() { return &___displs2_10; }
	inline void set_displs2_10(List_1_t2943684265 * value)
	{
		___displs2_10 = value;
		Il2CppCodeGenWriteBarrier((&___displs2_10), value);
	}

	inline static int32_t get_offset_of_displEnds2_11() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___displEnds2_11)); }
	inline List_1_t3691557916 * get_displEnds2_11() const { return ___displEnds2_11; }
	inline List_1_t3691557916 ** get_address_of_displEnds2_11() { return &___displEnds2_11; }
	inline void set_displEnds2_11(List_1_t3691557916 * value)
	{
		___displEnds2_11 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds2_11), value);
	}

	inline static int32_t get_offset_of_voidElements_12() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___voidElements_12)); }
	inline List_1_t2665353730 * get_voidElements_12() const { return ___voidElements_12; }
	inline List_1_t2665353730 ** get_address_of_voidElements_12() { return &___voidElements_12; }
	inline void set_voidElements_12(List_1_t2665353730 * value)
	{
		___voidElements_12 = value;
		Il2CppCodeGenWriteBarrier((&___voidElements_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T2237665516_H
#ifndef LEVELCOMPONENTDISPL_T1267792563_H
#define LEVELCOMPONENTDISPL_T1267792563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponentDispl
struct  LevelComponentDispl_t1267792563  : public RuntimeObject
{
public:
	// System.Int32 LevelComponentDispl::positionX
	int32_t ___positionX_0;
	// System.Int32 LevelComponentDispl::positionY
	int32_t ___positionY_1;
	// System.Single LevelComponentDispl::directionX
	float ___directionX_2;
	// System.Single LevelComponentDispl::directionY
	float ___directionY_3;
	// System.Int32 LevelComponentDispl::displid
	int32_t ___displid_4;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(LevelComponentDispl_t1267792563, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(LevelComponentDispl_t1267792563, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(LevelComponentDispl_t1267792563, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(LevelComponentDispl_t1267792563, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_displid_4() { return static_cast<int32_t>(offsetof(LevelComponentDispl_t1267792563, ___displid_4)); }
	inline int32_t get_displid_4() const { return ___displid_4; }
	inline int32_t* get_address_of_displid_4() { return &___displid_4; }
	inline void set_displid_4(int32_t value)
	{
		___displid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENTDISPL_T1267792563_H
#ifndef MVECTOR_T2821193744_H
#define MVECTOR_T2821193744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MVector
struct  MVector_t2821193744  : public RuntimeObject
{
public:
	// System.Int32 MVector::nval
	int32_t ___nval_0;
	// System.Single[] MVector::vals
	SingleU5BU5D_t1444911251* ___vals_1;

public:
	inline static int32_t get_offset_of_nval_0() { return static_cast<int32_t>(offsetof(MVector_t2821193744, ___nval_0)); }
	inline int32_t get_nval_0() const { return ___nval_0; }
	inline int32_t* get_address_of_nval_0() { return &___nval_0; }
	inline void set_nval_0(int32_t value)
	{
		___nval_0 = value;
	}

	inline static int32_t get_offset_of_vals_1() { return static_cast<int32_t>(offsetof(MVector_t2821193744, ___vals_1)); }
	inline SingleU5BU5D_t1444911251* get_vals_1() const { return ___vals_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_vals_1() { return &___vals_1; }
	inline void set_vals_1(SingleU5BU5D_t1444911251* value)
	{
		___vals_1 = value;
		Il2CppCodeGenWriteBarrier((&___vals_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MVECTOR_T2821193744_H
#ifndef LEVELCOMPONENTDISPLEND_T436353678_H
#define LEVELCOMPONENTDISPLEND_T436353678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponentDisplEnd
struct  LevelComponentDisplEnd_t436353678  : public RuntimeObject
{
public:
	// System.Int32 LevelComponentDisplEnd::positionX
	int32_t ___positionX_0;
	// System.Int32 LevelComponentDisplEnd::positionY
	int32_t ___positionY_1;
	// System.Single LevelComponentDisplEnd::directionX
	float ___directionX_2;
	// System.Single LevelComponentDisplEnd::directionY
	float ___directionY_3;
	// System.Int32 LevelComponentDisplEnd::displid
	int32_t ___displid_4;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd_t436353678, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd_t436353678, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd_t436353678, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd_t436353678, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_displid_4() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd_t436353678, ___displid_4)); }
	inline int32_t get_displid_4() const { return ___displid_4; }
	inline int32_t* get_address_of_displid_4() { return &___displid_4; }
	inline void set_displid_4(int32_t value)
	{
		___displid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENTDISPLEND_T436353678_H
#ifndef LEVELCOMPONENTDISPLEND2_T2219483174_H
#define LEVELCOMPONENTDISPLEND2_T2219483174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponentDisplEnd2
struct  LevelComponentDisplEnd2_t2219483174  : public RuntimeObject
{
public:
	// System.Int32 LevelComponentDisplEnd2::positionX
	int32_t ___positionX_0;
	// System.Int32 LevelComponentDisplEnd2::positionY
	int32_t ___positionY_1;
	// System.Single LevelComponentDisplEnd2::directionX
	float ___directionX_2;
	// System.Single LevelComponentDisplEnd2::directionY
	float ___directionY_3;
	// System.Int32 LevelComponentDisplEnd2::displid
	int32_t ___displid_4;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd2_t2219483174, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd2_t2219483174, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd2_t2219483174, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd2_t2219483174, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_displid_4() { return static_cast<int32_t>(offsetof(LevelComponentDisplEnd2_t2219483174, ___displid_4)); }
	inline int32_t get_displid_4() const { return ___displid_4; }
	inline int32_t* get_address_of_displid_4() { return &___displid_4; }
	inline void set_displid_4(int32_t value)
	{
		___displid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENTDISPLEND2_T2219483174_H
#ifndef LEVELCOMPONENTLOAD_T253531484_H
#define LEVELCOMPONENTLOAD_T253531484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponentLoad
struct  LevelComponentLoad_t253531484  : public RuntimeObject
{
public:
	// System.Int32 LevelComponentLoad::positionX
	int32_t ___positionX_0;
	// System.Int32 LevelComponentLoad::positionY
	int32_t ___positionY_1;
	// System.Single LevelComponentLoad::directionX
	float ___directionX_2;
	// System.Single LevelComponentLoad::directionY
	float ___directionY_3;
	// System.Int32 LevelComponentLoad::loadid
	int32_t ___loadid_4;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(LevelComponentLoad_t253531484, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(LevelComponentLoad_t253531484, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(LevelComponentLoad_t253531484, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(LevelComponentLoad_t253531484, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_loadid_4() { return static_cast<int32_t>(offsetof(LevelComponentLoad_t253531484, ___loadid_4)); }
	inline int32_t get_loadid_4() const { return ___loadid_4; }
	inline int32_t* get_address_of_loadid_4() { return &___loadid_4; }
	inline void set_loadid_4(int32_t value)
	{
		___loadid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENTLOAD_T253531484_H
#ifndef LEVELCOMPONENTSUPPORT_T2849120049_H
#define LEVELCOMPONENTSUPPORT_T2849120049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelComponentSupport
struct  LevelComponentSupport_t2849120049  : public RuntimeObject
{
public:
	// System.Int32 LevelComponentSupport::positionX
	int32_t ___positionX_0;
	// System.Int32 LevelComponentSupport::positionY
	int32_t ___positionY_1;
	// System.Single LevelComponentSupport::directionX
	float ___directionX_2;
	// System.Single LevelComponentSupport::directionY
	float ___directionY_3;
	// System.Single LevelComponentSupport::rotation
	float ___rotation_4;
	// System.Boolean LevelComponentSupport::fixedSupport
	bool ___fixedSupport_5;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(LevelComponentSupport_t2849120049, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(LevelComponentSupport_t2849120049, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(LevelComponentSupport_t2849120049, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(LevelComponentSupport_t2849120049, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_rotation_4() { return static_cast<int32_t>(offsetof(LevelComponentSupport_t2849120049, ___rotation_4)); }
	inline float get_rotation_4() const { return ___rotation_4; }
	inline float* get_address_of_rotation_4() { return &___rotation_4; }
	inline void set_rotation_4(float value)
	{
		___rotation_4 = value;
	}

	inline static int32_t get_offset_of_fixedSupport_5() { return static_cast<int32_t>(offsetof(LevelComponentSupport_t2849120049, ___fixedSupport_5)); }
	inline bool get_fixedSupport_5() const { return ___fixedSupport_5; }
	inline bool* get_address_of_fixedSupport_5() { return &___fixedSupport_5; }
	inline void set_fixedSupport_5(bool value)
	{
		___fixedSupport_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCOMPONENTSUPPORT_T2849120049_H
#ifndef LEVELDB_T1000439184_H
#define LEVELDB_T1000439184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelDB
struct  LevelDB_t1000439184  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELDB_T1000439184_H
#ifndef TOPIC_T1056265799_H
#define TOPIC_T1056265799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Topic
struct  Topic_t1056265799  : public RuntimeObject
{
public:
	// System.Int64 Topic::topicId
	int64_t ___topicId_0;
	// System.String Topic::name
	String_t* ___name_1;
	// Level[] Topic::levels
	LevelU5BU5D_t979096037* ___levels_2;

public:
	inline static int32_t get_offset_of_topicId_0() { return static_cast<int32_t>(offsetof(Topic_t1056265799, ___topicId_0)); }
	inline int64_t get_topicId_0() const { return ___topicId_0; }
	inline int64_t* get_address_of_topicId_0() { return &___topicId_0; }
	inline void set_topicId_0(int64_t value)
	{
		___topicId_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Topic_t1056265799, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_levels_2() { return static_cast<int32_t>(offsetof(Topic_t1056265799, ___levels_2)); }
	inline LevelU5BU5D_t979096037* get_levels_2() const { return ___levels_2; }
	inline LevelU5BU5D_t979096037** get_address_of_levels_2() { return &___levels_2; }
	inline void set_levels_2(LevelU5BU5D_t979096037* value)
	{
		___levels_2 = value;
		Il2CppCodeGenWriteBarrier((&___levels_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPIC_T1056265799_H
#ifndef SOLVERRES_T1568196152_H
#define SOLVERRES_T1568196152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SolverRes
struct  SolverRes_t1568196152  : public RuntimeObject
{
public:
	// System.Int32 SolverRes::width
	int32_t ___width_0;
	// System.Int32 SolverRes::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(SolverRes_t1568196152, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(SolverRes_t1568196152, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERRES_T1568196152_H
#ifndef U3CENABLEMESSAGEU3EC__ITERATOR1_T2457876903_H
#define U3CENABLEMESSAGEU3EC__ITERATOR1_T2457876903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Screenshot/<EnableMessage>c__Iterator1
struct  U3CEnableMessageU3Ec__Iterator1_t2457876903  : public RuntimeObject
{
public:
	// System.Object Screenshot/<EnableMessage>c__Iterator1::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean Screenshot/<EnableMessage>c__Iterator1::$disposing
	bool ___U24disposing_1;
	// System.Int32 Screenshot/<EnableMessage>c__Iterator1::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CEnableMessageU3Ec__Iterator1_t2457876903, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CEnableMessageU3Ec__Iterator1_t2457876903, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CEnableMessageU3Ec__Iterator1_t2457876903, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENABLEMESSAGEU3EC__ITERATOR1_T2457876903_H
#ifndef U3CFLASHSCREENU3EC__ITERATOR0_T2841660896_H
#define U3CFLASHSCREENU3EC__ITERATOR0_T2841660896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Screenshot/<FlashScreen>c__Iterator0
struct  U3CFlashScreenU3Ec__Iterator0_t2841660896  : public RuntimeObject
{
public:
	// Screenshot Screenshot/<FlashScreen>c__Iterator0::$this
	Screenshot_t3680149077 * ___U24this_0;
	// System.Object Screenshot/<FlashScreen>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Screenshot/<FlashScreen>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Screenshot/<FlashScreen>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CFlashScreenU3Ec__Iterator0_t2841660896, ___U24this_0)); }
	inline Screenshot_t3680149077 * get_U24this_0() const { return ___U24this_0; }
	inline Screenshot_t3680149077 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Screenshot_t3680149077 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CFlashScreenU3Ec__Iterator0_t2841660896, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CFlashScreenU3Ec__Iterator0_t2841660896, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CFlashScreenU3Ec__Iterator0_t2841660896, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLASHSCREENU3EC__ITERATOR0_T2841660896_H
#ifndef PASSIVEBLOCK_T513448541_H
#define PASSIVEBLOCK_T513448541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PassiveBlock
struct  PassiveBlock_t513448541  : public RuntimeObject
{
public:
	// System.Int32 PassiveBlock::width
	int32_t ___width_0;
	// System.Int32 PassiveBlock::height
	int32_t ___height_1;
	// System.Int32 PassiveBlock::subdivisions
	int32_t ___subdivisions_2;
	// System.Byte[0...,0...] PassiveBlock::block
	ByteU5BU2CU5D_t4116647658* ___block_3;
	// System.Boolean PassiveBlock::blockEmpty
	bool ___blockEmpty_4;
	// System.Boolean PassiveBlock::hasPassiveVoid
	bool ___hasPassiveVoid_5;
	// System.Boolean PassiveBlock::overwriteActiveEmpty
	bool ___overwriteActiveEmpty_6;
	// System.Int32[] PassiveBlock::elementDensities
	Int32U5BU5D_t385246372* ___elementDensities_7;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_subdivisions_2() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___subdivisions_2)); }
	inline int32_t get_subdivisions_2() const { return ___subdivisions_2; }
	inline int32_t* get_address_of_subdivisions_2() { return &___subdivisions_2; }
	inline void set_subdivisions_2(int32_t value)
	{
		___subdivisions_2 = value;
	}

	inline static int32_t get_offset_of_block_3() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___block_3)); }
	inline ByteU5BU2CU5D_t4116647658* get_block_3() const { return ___block_3; }
	inline ByteU5BU2CU5D_t4116647658** get_address_of_block_3() { return &___block_3; }
	inline void set_block_3(ByteU5BU2CU5D_t4116647658* value)
	{
		___block_3 = value;
		Il2CppCodeGenWriteBarrier((&___block_3), value);
	}

	inline static int32_t get_offset_of_blockEmpty_4() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___blockEmpty_4)); }
	inline bool get_blockEmpty_4() const { return ___blockEmpty_4; }
	inline bool* get_address_of_blockEmpty_4() { return &___blockEmpty_4; }
	inline void set_blockEmpty_4(bool value)
	{
		___blockEmpty_4 = value;
	}

	inline static int32_t get_offset_of_hasPassiveVoid_5() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___hasPassiveVoid_5)); }
	inline bool get_hasPassiveVoid_5() const { return ___hasPassiveVoid_5; }
	inline bool* get_address_of_hasPassiveVoid_5() { return &___hasPassiveVoid_5; }
	inline void set_hasPassiveVoid_5(bool value)
	{
		___hasPassiveVoid_5 = value;
	}

	inline static int32_t get_offset_of_overwriteActiveEmpty_6() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___overwriteActiveEmpty_6)); }
	inline bool get_overwriteActiveEmpty_6() const { return ___overwriteActiveEmpty_6; }
	inline bool* get_address_of_overwriteActiveEmpty_6() { return &___overwriteActiveEmpty_6; }
	inline void set_overwriteActiveEmpty_6(bool value)
	{
		___overwriteActiveEmpty_6 = value;
	}

	inline static int32_t get_offset_of_elementDensities_7() { return static_cast<int32_t>(offsetof(PassiveBlock_t513448541, ___elementDensities_7)); }
	inline Int32U5BU5D_t385246372* get_elementDensities_7() const { return ___elementDensities_7; }
	inline Int32U5BU5D_t385246372** get_address_of_elementDensities_7() { return &___elementDensities_7; }
	inline void set_elementDensities_7(Int32U5BU5D_t385246372* value)
	{
		___elementDensities_7 = value;
		Il2CppCodeGenWriteBarrier((&___elementDensities_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSIVEBLOCK_T513448541_H
#ifndef MINIJSONEXTENSIONS_T1107891468_H
#define MINIJSONEXTENSIONS_T1107891468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJsonExtensions
struct  MiniJsonExtensions_t1107891468  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSONEXTENSIONS_T1107891468_H
#ifndef MINIJSON_T4198869380_H
#define MINIJSON_T4198869380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON
struct  MiniJSON_t4198869380  : public RuntimeObject
{
public:

public:
};

struct MiniJSON_t4198869380_StaticFields
{
public:
	// System.Int32 MiniJSON::lastErrorIndex
	int32_t ___lastErrorIndex_13;
	// System.String MiniJSON::lastDecode
	String_t* ___lastDecode_14;

public:
	inline static int32_t get_offset_of_lastErrorIndex_13() { return static_cast<int32_t>(offsetof(MiniJSON_t4198869380_StaticFields, ___lastErrorIndex_13)); }
	inline int32_t get_lastErrorIndex_13() const { return ___lastErrorIndex_13; }
	inline int32_t* get_address_of_lastErrorIndex_13() { return &___lastErrorIndex_13; }
	inline void set_lastErrorIndex_13(int32_t value)
	{
		___lastErrorIndex_13 = value;
	}

	inline static int32_t get_offset_of_lastDecode_14() { return static_cast<int32_t>(offsetof(MiniJSON_t4198869380_StaticFields, ___lastDecode_14)); }
	inline String_t* get_lastDecode_14() const { return ___lastDecode_14; }
	inline String_t** get_address_of_lastDecode_14() { return &___lastDecode_14; }
	inline void set_lastDecode_14(String_t* value)
	{
		___lastDecode_14 = value;
		Il2CppCodeGenWriteBarrier((&___lastDecode_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSON_T4198869380_H
#ifndef SVEC_T1806837960_H
#define SVEC_T1806837960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Svec
struct  Svec_t1806837960  : public RuntimeObject
{
public:
	// System.Single[] Svec::vals
	SingleU5BU5D_t1444911251* ___vals_0;
	// System.Int32[] Svec::indx
	Int32U5BU5D_t385246372* ___indx_1;
	// System.Int32 Svec::len
	int32_t ___len_2;

public:
	inline static int32_t get_offset_of_vals_0() { return static_cast<int32_t>(offsetof(Svec_t1806837960, ___vals_0)); }
	inline SingleU5BU5D_t1444911251* get_vals_0() const { return ___vals_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_vals_0() { return &___vals_0; }
	inline void set_vals_0(SingleU5BU5D_t1444911251* value)
	{
		___vals_0 = value;
		Il2CppCodeGenWriteBarrier((&___vals_0), value);
	}

	inline static int32_t get_offset_of_indx_1() { return static_cast<int32_t>(offsetof(Svec_t1806837960, ___indx_1)); }
	inline Int32U5BU5D_t385246372* get_indx_1() const { return ___indx_1; }
	inline Int32U5BU5D_t385246372** get_address_of_indx_1() { return &___indx_1; }
	inline void set_indx_1(Int32U5BU5D_t385246372* value)
	{
		___indx_1 = value;
		Il2CppCodeGenWriteBarrier((&___indx_1), value);
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Svec_t1806837960, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVEC_T1806837960_H
#ifndef MATHEXTRA_T4221770606_H
#define MATHEXTRA_T4221770606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MathExtra
struct  MathExtra_t4221770606  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHEXTRA_T4221770606_H
#ifndef MATRIXCCS_T3519621953_H
#define MATRIXCCS_T3519621953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatrixCCS
struct  MatrixCCS_t3519621953  : public RuntimeObject
{
public:
	// System.Int32 MatrixCCS::ncols
	int32_t ___ncols_0;
	// System.Int32 MatrixCCS::nrows
	int32_t ___nrows_1;
	// Svec[] MatrixCCS::mat
	SvecU5BU5D_t2458350361* ___mat_2;
	// System.Int32 MatrixCCS::nnz
	int32_t ___nnz_3;
	// System.Int32[] MatrixCCS::cols
	Int32U5BU5D_t385246372* ___cols_4;
	// System.Int32[] MatrixCCS::ind
	Int32U5BU5D_t385246372* ___ind_5;
	// System.Single[] MatrixCCS::val
	SingleU5BU5D_t1444911251* ___val_6;
	// System.Single[] MatrixCCS::diag
	SingleU5BU5D_t1444911251* ___diag_7;

public:
	inline static int32_t get_offset_of_ncols_0() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___ncols_0)); }
	inline int32_t get_ncols_0() const { return ___ncols_0; }
	inline int32_t* get_address_of_ncols_0() { return &___ncols_0; }
	inline void set_ncols_0(int32_t value)
	{
		___ncols_0 = value;
	}

	inline static int32_t get_offset_of_nrows_1() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___nrows_1)); }
	inline int32_t get_nrows_1() const { return ___nrows_1; }
	inline int32_t* get_address_of_nrows_1() { return &___nrows_1; }
	inline void set_nrows_1(int32_t value)
	{
		___nrows_1 = value;
	}

	inline static int32_t get_offset_of_mat_2() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___mat_2)); }
	inline SvecU5BU5D_t2458350361* get_mat_2() const { return ___mat_2; }
	inline SvecU5BU5D_t2458350361** get_address_of_mat_2() { return &___mat_2; }
	inline void set_mat_2(SvecU5BU5D_t2458350361* value)
	{
		___mat_2 = value;
		Il2CppCodeGenWriteBarrier((&___mat_2), value);
	}

	inline static int32_t get_offset_of_nnz_3() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___nnz_3)); }
	inline int32_t get_nnz_3() const { return ___nnz_3; }
	inline int32_t* get_address_of_nnz_3() { return &___nnz_3; }
	inline void set_nnz_3(int32_t value)
	{
		___nnz_3 = value;
	}

	inline static int32_t get_offset_of_cols_4() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___cols_4)); }
	inline Int32U5BU5D_t385246372* get_cols_4() const { return ___cols_4; }
	inline Int32U5BU5D_t385246372** get_address_of_cols_4() { return &___cols_4; }
	inline void set_cols_4(Int32U5BU5D_t385246372* value)
	{
		___cols_4 = value;
		Il2CppCodeGenWriteBarrier((&___cols_4), value);
	}

	inline static int32_t get_offset_of_ind_5() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___ind_5)); }
	inline Int32U5BU5D_t385246372* get_ind_5() const { return ___ind_5; }
	inline Int32U5BU5D_t385246372** get_address_of_ind_5() { return &___ind_5; }
	inline void set_ind_5(Int32U5BU5D_t385246372* value)
	{
		___ind_5 = value;
		Il2CppCodeGenWriteBarrier((&___ind_5), value);
	}

	inline static int32_t get_offset_of_val_6() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___val_6)); }
	inline SingleU5BU5D_t1444911251* get_val_6() const { return ___val_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_val_6() { return &___val_6; }
	inline void set_val_6(SingleU5BU5D_t1444911251* value)
	{
		___val_6 = value;
		Il2CppCodeGenWriteBarrier((&___val_6), value);
	}

	inline static int32_t get_offset_of_diag_7() { return static_cast<int32_t>(offsetof(MatrixCCS_t3519621953, ___diag_7)); }
	inline SingleU5BU5D_t1444911251* get_diag_7() const { return ___diag_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_diag_7() { return &___diag_7; }
	inline void set_diag_7(SingleU5BU5D_t1444911251* value)
	{
		___diag_7 = value;
		Il2CppCodeGenWriteBarrier((&___diag_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXCCS_T3519621953_H
#ifndef FEAMATRIXCOMPRESS_T1508710683_H
#define FEAMATRIXCOMPRESS_T1508710683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEAMatrixCompress
struct  FEAMatrixCompress_t1508710683  : public RuntimeObject
{
public:
	// System.Boolean[] FEAMatrixCompress::rhoPhys
	BooleanU5BU5D_t2897418192* ___rhoPhys_0;
	// System.Int32 FEAMatrixCompress::ney
	int32_t ___ney_1;
	// System.Int32 FEAMatrixCompress::nex
	int32_t ___nex_2;
	// System.Int32 FEAMatrixCompress::ny
	int32_t ___ny_3;
	// System.Int32 FEAMatrixCompress::nx
	int32_t ___nx_4;
	// System.Int32 FEAMatrixCompress::ndof
	int32_t ___ndof_5;
	// System.Int32 FEAMatrixCompress::ne
	int32_t ___ne_6;
	// System.Int32 FEAMatrixCompress::ndof_reduced
	int32_t ___ndof_reduced_7;
	// System.Int32[] FEAMatrixCompress::ActiveDof
	Int32U5BU5D_t385246372* ___ActiveDof_8;

public:
	inline static int32_t get_offset_of_rhoPhys_0() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___rhoPhys_0)); }
	inline BooleanU5BU5D_t2897418192* get_rhoPhys_0() const { return ___rhoPhys_0; }
	inline BooleanU5BU5D_t2897418192** get_address_of_rhoPhys_0() { return &___rhoPhys_0; }
	inline void set_rhoPhys_0(BooleanU5BU5D_t2897418192* value)
	{
		___rhoPhys_0 = value;
		Il2CppCodeGenWriteBarrier((&___rhoPhys_0), value);
	}

	inline static int32_t get_offset_of_ney_1() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___ney_1)); }
	inline int32_t get_ney_1() const { return ___ney_1; }
	inline int32_t* get_address_of_ney_1() { return &___ney_1; }
	inline void set_ney_1(int32_t value)
	{
		___ney_1 = value;
	}

	inline static int32_t get_offset_of_nex_2() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___nex_2)); }
	inline int32_t get_nex_2() const { return ___nex_2; }
	inline int32_t* get_address_of_nex_2() { return &___nex_2; }
	inline void set_nex_2(int32_t value)
	{
		___nex_2 = value;
	}

	inline static int32_t get_offset_of_ny_3() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___ny_3)); }
	inline int32_t get_ny_3() const { return ___ny_3; }
	inline int32_t* get_address_of_ny_3() { return &___ny_3; }
	inline void set_ny_3(int32_t value)
	{
		___ny_3 = value;
	}

	inline static int32_t get_offset_of_nx_4() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___nx_4)); }
	inline int32_t get_nx_4() const { return ___nx_4; }
	inline int32_t* get_address_of_nx_4() { return &___nx_4; }
	inline void set_nx_4(int32_t value)
	{
		___nx_4 = value;
	}

	inline static int32_t get_offset_of_ndof_5() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___ndof_5)); }
	inline int32_t get_ndof_5() const { return ___ndof_5; }
	inline int32_t* get_address_of_ndof_5() { return &___ndof_5; }
	inline void set_ndof_5(int32_t value)
	{
		___ndof_5 = value;
	}

	inline static int32_t get_offset_of_ne_6() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___ne_6)); }
	inline int32_t get_ne_6() const { return ___ne_6; }
	inline int32_t* get_address_of_ne_6() { return &___ne_6; }
	inline void set_ne_6(int32_t value)
	{
		___ne_6 = value;
	}

	inline static int32_t get_offset_of_ndof_reduced_7() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___ndof_reduced_7)); }
	inline int32_t get_ndof_reduced_7() const { return ___ndof_reduced_7; }
	inline int32_t* get_address_of_ndof_reduced_7() { return &___ndof_reduced_7; }
	inline void set_ndof_reduced_7(int32_t value)
	{
		___ndof_reduced_7 = value;
	}

	inline static int32_t get_offset_of_ActiveDof_8() { return static_cast<int32_t>(offsetof(FEAMatrixCompress_t1508710683, ___ActiveDof_8)); }
	inline Int32U5BU5D_t385246372* get_ActiveDof_8() const { return ___ActiveDof_8; }
	inline Int32U5BU5D_t385246372** get_address_of_ActiveDof_8() { return &___ActiveDof_8; }
	inline void set_ActiveDof_8(Int32U5BU5D_t385246372* value)
	{
		___ActiveDof_8 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveDof_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEAMATRIXCOMPRESS_T1508710683_H
#ifndef FEAGAMERESULT_T3745188880_H
#define FEAGAMERESULT_T3745188880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEAGameResult
struct  FEAGameResult_t3745188880  : public RuntimeObject
{
public:
	// System.Single FEAGameResult::score
	float ___score_0;
	// System.String FEAGameResult::error
	String_t* ___error_1;
	// UnityEngine.Color[] FEAGameResult::stressColor
	ColorU5BU5D_t941916413* ___stressColor_2;
	// System.Int64 FEAGameResult::time
	int64_t ___time_3;
	// System.Single FEAGameResult::minStress
	float ___minStress_4;
	// System.Single FEAGameResult::maxStress
	float ___maxStress_5;
	// System.Int64 FEAGameResult::id
	int64_t ___id_6;
	// System.Single[] FEAGameResult::u
	SingleU5BU5D_t1444911251* ___u_7;

public:
	inline static int32_t get_offset_of_score_0() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___score_0)); }
	inline float get_score_0() const { return ___score_0; }
	inline float* get_address_of_score_0() { return &___score_0; }
	inline void set_score_0(float value)
	{
		___score_0 = value;
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___error_1)); }
	inline String_t* get_error_1() const { return ___error_1; }
	inline String_t** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(String_t* value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_stressColor_2() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___stressColor_2)); }
	inline ColorU5BU5D_t941916413* get_stressColor_2() const { return ___stressColor_2; }
	inline ColorU5BU5D_t941916413** get_address_of_stressColor_2() { return &___stressColor_2; }
	inline void set_stressColor_2(ColorU5BU5D_t941916413* value)
	{
		___stressColor_2 = value;
		Il2CppCodeGenWriteBarrier((&___stressColor_2), value);
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___time_3)); }
	inline int64_t get_time_3() const { return ___time_3; }
	inline int64_t* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(int64_t value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_minStress_4() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___minStress_4)); }
	inline float get_minStress_4() const { return ___minStress_4; }
	inline float* get_address_of_minStress_4() { return &___minStress_4; }
	inline void set_minStress_4(float value)
	{
		___minStress_4 = value;
	}

	inline static int32_t get_offset_of_maxStress_5() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___maxStress_5)); }
	inline float get_maxStress_5() const { return ___maxStress_5; }
	inline float* get_address_of_maxStress_5() { return &___maxStress_5; }
	inline void set_maxStress_5(float value)
	{
		___maxStress_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___id_6)); }
	inline int64_t get_id_6() const { return ___id_6; }
	inline int64_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(int64_t value)
	{
		___id_6 = value;
	}

	inline static int32_t get_offset_of_u_7() { return static_cast<int32_t>(offsetof(FEAGameResult_t3745188880, ___u_7)); }
	inline SingleU5BU5D_t1444911251* get_u_7() const { return ___u_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_u_7() { return &___u_7; }
	inline void set_u_7(SingleU5BU5D_t1444911251* value)
	{
		___u_7 = value;
		Il2CppCodeGenWriteBarrier((&___u_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEAGAMERESULT_T3745188880_H
#ifndef SPARSELDL_T1701177157_H
#define SPARSELDL_T1701177157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SparseLDL
struct  SparseLDL_t1701177157  : public RuntimeObject
{
public:
	// System.Int32 SparseLDL::n
	int32_t ___n_0;
	// System.Int32 SparseLDL::lnz
	int32_t ___lnz_1;
	// System.Int32[] SparseLDL::Parent
	Int32U5BU5D_t385246372* ___Parent_2;
	// System.Int32[] SparseLDL::Lp
	Int32U5BU5D_t385246372* ___Lp_3;
	// System.Int32[] SparseLDL::Lnz
	Int32U5BU5D_t385246372* ___Lnz_4;
	// System.Int32[] SparseLDL::Flag
	Int32U5BU5D_t385246372* ___Flag_5;
	// System.Int32[] SparseLDL::Li
	Int32U5BU5D_t385246372* ___Li_6;
	// System.Single[] SparseLDL::Lx
	SingleU5BU5D_t1444911251* ___Lx_7;
	// System.Single[] SparseLDL::D
	SingleU5BU5D_t1444911251* ___D_8;
	// System.Single[] SparseLDL::Y
	SingleU5BU5D_t1444911251* ___Y_9;
	// System.Int32[] SparseLDL::Pattern
	Int32U5BU5D_t385246372* ___Pattern_10;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___n_0)); }
	inline int32_t get_n_0() const { return ___n_0; }
	inline int32_t* get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(int32_t value)
	{
		___n_0 = value;
	}

	inline static int32_t get_offset_of_lnz_1() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___lnz_1)); }
	inline int32_t get_lnz_1() const { return ___lnz_1; }
	inline int32_t* get_address_of_lnz_1() { return &___lnz_1; }
	inline void set_lnz_1(int32_t value)
	{
		___lnz_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Parent_2)); }
	inline Int32U5BU5D_t385246372* get_Parent_2() const { return ___Parent_2; }
	inline Int32U5BU5D_t385246372** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Int32U5BU5D_t385246372* value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_Lp_3() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Lp_3)); }
	inline Int32U5BU5D_t385246372* get_Lp_3() const { return ___Lp_3; }
	inline Int32U5BU5D_t385246372** get_address_of_Lp_3() { return &___Lp_3; }
	inline void set_Lp_3(Int32U5BU5D_t385246372* value)
	{
		___Lp_3 = value;
		Il2CppCodeGenWriteBarrier((&___Lp_3), value);
	}

	inline static int32_t get_offset_of_Lnz_4() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Lnz_4)); }
	inline Int32U5BU5D_t385246372* get_Lnz_4() const { return ___Lnz_4; }
	inline Int32U5BU5D_t385246372** get_address_of_Lnz_4() { return &___Lnz_4; }
	inline void set_Lnz_4(Int32U5BU5D_t385246372* value)
	{
		___Lnz_4 = value;
		Il2CppCodeGenWriteBarrier((&___Lnz_4), value);
	}

	inline static int32_t get_offset_of_Flag_5() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Flag_5)); }
	inline Int32U5BU5D_t385246372* get_Flag_5() const { return ___Flag_5; }
	inline Int32U5BU5D_t385246372** get_address_of_Flag_5() { return &___Flag_5; }
	inline void set_Flag_5(Int32U5BU5D_t385246372* value)
	{
		___Flag_5 = value;
		Il2CppCodeGenWriteBarrier((&___Flag_5), value);
	}

	inline static int32_t get_offset_of_Li_6() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Li_6)); }
	inline Int32U5BU5D_t385246372* get_Li_6() const { return ___Li_6; }
	inline Int32U5BU5D_t385246372** get_address_of_Li_6() { return &___Li_6; }
	inline void set_Li_6(Int32U5BU5D_t385246372* value)
	{
		___Li_6 = value;
		Il2CppCodeGenWriteBarrier((&___Li_6), value);
	}

	inline static int32_t get_offset_of_Lx_7() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Lx_7)); }
	inline SingleU5BU5D_t1444911251* get_Lx_7() const { return ___Lx_7; }
	inline SingleU5BU5D_t1444911251** get_address_of_Lx_7() { return &___Lx_7; }
	inline void set_Lx_7(SingleU5BU5D_t1444911251* value)
	{
		___Lx_7 = value;
		Il2CppCodeGenWriteBarrier((&___Lx_7), value);
	}

	inline static int32_t get_offset_of_D_8() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___D_8)); }
	inline SingleU5BU5D_t1444911251* get_D_8() const { return ___D_8; }
	inline SingleU5BU5D_t1444911251** get_address_of_D_8() { return &___D_8; }
	inline void set_D_8(SingleU5BU5D_t1444911251* value)
	{
		___D_8 = value;
		Il2CppCodeGenWriteBarrier((&___D_8), value);
	}

	inline static int32_t get_offset_of_Y_9() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Y_9)); }
	inline SingleU5BU5D_t1444911251* get_Y_9() const { return ___Y_9; }
	inline SingleU5BU5D_t1444911251** get_address_of_Y_9() { return &___Y_9; }
	inline void set_Y_9(SingleU5BU5D_t1444911251* value)
	{
		___Y_9 = value;
		Il2CppCodeGenWriteBarrier((&___Y_9), value);
	}

	inline static int32_t get_offset_of_Pattern_10() { return static_cast<int32_t>(offsetof(SparseLDL_t1701177157, ___Pattern_10)); }
	inline Int32U5BU5D_t385246372* get_Pattern_10() const { return ___Pattern_10; }
	inline Int32U5BU5D_t385246372** get_address_of_Pattern_10() { return &___Pattern_10; }
	inline void set_Pattern_10(Int32U5BU5D_t385246372* value)
	{
		___Pattern_10 = value;
		Il2CppCodeGenWriteBarrier((&___Pattern_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPARSELDL_T1701177157_H
#ifndef RECTEXT_T2723658462_H
#define RECTEXT_T2723658462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RectExt
struct  RectExt_t2723658462  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTEXT_T2723658462_H
#ifndef UUNITTESTCASE_T1875215041_H
#define UUNITTESTCASE_T1875215041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitTestCase
struct  UUnitTestCase_t1875215041  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITTESTCASE_T1875215041_H
#ifndef UUNITASSERT_T3903203386_H
#define UUNITASSERT_T3903203386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UUnitAssert
struct  UUnitAssert_t3903203386  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUNITASSERT_T3903203386_H
#ifndef THINTHICKTOOL_T2872687508_H
#define THINTHICKTOOL_T2872687508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThinThickTool
struct  ThinThickTool_t2872687508  : public RuntimeObject
{
public:
	// PainterCanvas ThinThickTool::painterCanvas
	PainterCanvas_t4160673718 * ___painterCanvas_0;
	// PassiveBlock ThinThickTool::passiveBlock
	PassiveBlock_t513448541 * ___passiveBlock_1;
	// PassiveBlock ThinThickTool::passiveBlockCopy
	PassiveBlock_t513448541 * ___passiveBlockCopy_2;
	// GameController ThinThickTool::gameController
	GameController_t2330501625 * ___gameController_3;
	// System.Boolean ThinThickTool::Thickening
	bool ___Thickening_4;
	// GUIPaintHandler ThinThickTool::guiPaintHandler
	GUIPaintHandler_t437174526 * ___guiPaintHandler_5;

public:
	inline static int32_t get_offset_of_painterCanvas_0() { return static_cast<int32_t>(offsetof(ThinThickTool_t2872687508, ___painterCanvas_0)); }
	inline PainterCanvas_t4160673718 * get_painterCanvas_0() const { return ___painterCanvas_0; }
	inline PainterCanvas_t4160673718 ** get_address_of_painterCanvas_0() { return &___painterCanvas_0; }
	inline void set_painterCanvas_0(PainterCanvas_t4160673718 * value)
	{
		___painterCanvas_0 = value;
		Il2CppCodeGenWriteBarrier((&___painterCanvas_0), value);
	}

	inline static int32_t get_offset_of_passiveBlock_1() { return static_cast<int32_t>(offsetof(ThinThickTool_t2872687508, ___passiveBlock_1)); }
	inline PassiveBlock_t513448541 * get_passiveBlock_1() const { return ___passiveBlock_1; }
	inline PassiveBlock_t513448541 ** get_address_of_passiveBlock_1() { return &___passiveBlock_1; }
	inline void set_passiveBlock_1(PassiveBlock_t513448541 * value)
	{
		___passiveBlock_1 = value;
		Il2CppCodeGenWriteBarrier((&___passiveBlock_1), value);
	}

	inline static int32_t get_offset_of_passiveBlockCopy_2() { return static_cast<int32_t>(offsetof(ThinThickTool_t2872687508, ___passiveBlockCopy_2)); }
	inline PassiveBlock_t513448541 * get_passiveBlockCopy_2() const { return ___passiveBlockCopy_2; }
	inline PassiveBlock_t513448541 ** get_address_of_passiveBlockCopy_2() { return &___passiveBlockCopy_2; }
	inline void set_passiveBlockCopy_2(PassiveBlock_t513448541 * value)
	{
		___passiveBlockCopy_2 = value;
		Il2CppCodeGenWriteBarrier((&___passiveBlockCopy_2), value);
	}

	inline static int32_t get_offset_of_gameController_3() { return static_cast<int32_t>(offsetof(ThinThickTool_t2872687508, ___gameController_3)); }
	inline GameController_t2330501625 * get_gameController_3() const { return ___gameController_3; }
	inline GameController_t2330501625 ** get_address_of_gameController_3() { return &___gameController_3; }
	inline void set_gameController_3(GameController_t2330501625 * value)
	{
		___gameController_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_3), value);
	}

	inline static int32_t get_offset_of_Thickening_4() { return static_cast<int32_t>(offsetof(ThinThickTool_t2872687508, ___Thickening_4)); }
	inline bool get_Thickening_4() const { return ___Thickening_4; }
	inline bool* get_address_of_Thickening_4() { return &___Thickening_4; }
	inline void set_Thickening_4(bool value)
	{
		___Thickening_4 = value;
	}

	inline static int32_t get_offset_of_guiPaintHandler_5() { return static_cast<int32_t>(offsetof(ThinThickTool_t2872687508, ___guiPaintHandler_5)); }
	inline GUIPaintHandler_t437174526 * get_guiPaintHandler_5() const { return ___guiPaintHandler_5; }
	inline GUIPaintHandler_t437174526 ** get_address_of_guiPaintHandler_5() { return &___guiPaintHandler_5; }
	inline void set_guiPaintHandler_5(GUIPaintHandler_t437174526 * value)
	{
		___guiPaintHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___guiPaintHandler_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THINTHICKTOOL_T2872687508_H
#ifndef ELEMENTSTIFFNESSMATRIX_T3577980630_H
#define ELEMENTSTIFFNESSMATRIX_T3577980630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ElementStiffnessMatrix
struct  ElementStiffnessMatrix_t3577980630  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSTIFFNESSMATRIX_T3577980630_H
#ifndef ELEMENTMASSMATRIX_T1408435446_H
#define ELEMENTMASSMATRIX_T1408435446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ElementMassMatrix
struct  ElementMassMatrix_t1408435446  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTMASSMATRIX_T1408435446_H
#ifndef FEAGAMEDATA_T1628373741_H
#define FEAGAMEDATA_T1628373741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEAGameData
struct  FEAGameData_t1628373741  : public RuntimeObject
{
public:
	// System.Int32[] FEAGameData::elementDensities
	Int32U5BU5D_t385246372* ___elementDensities_0;
	// System.Int64 FEAGameData::id
	int64_t ___id_1;
	// System.Int32 FEAGameData::plotChoice
	int32_t ___plotChoice_2;
	// System.Single FEAGameData::Emax
	float ___Emax_3;
	// System.Boolean FEAGameData::big_finger
	bool ___big_finger_4;
	// System.Boolean FEAGameData::ortho_move
	bool ___ortho_move_5;
	// System.Boolean FEAGameData::turnOffSolver
	bool ___turnOffSolver_6;
	// FEAComponentData[] FEAGameData::supports
	FEAComponentDataU5BU5D_t1047043642* ___supports_7;
	// FEAComponentData[] FEAGameData::displs
	FEAComponentDataU5BU5D_t1047043642* ___displs_8;
	// FEAComponentData[] FEAGameData::displEnds
	FEAComponentDataU5BU5D_t1047043642* ___displEnds_9;
	// FEAComponentData[] FEAGameData::displs2
	FEAComponentDataU5BU5D_t1047043642* ___displs2_10;
	// FEAComponentData[] FEAGameData::displEnds2
	FEAComponentDataU5BU5D_t1047043642* ___displEnds2_11;

public:
	inline static int32_t get_offset_of_elementDensities_0() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___elementDensities_0)); }
	inline Int32U5BU5D_t385246372* get_elementDensities_0() const { return ___elementDensities_0; }
	inline Int32U5BU5D_t385246372** get_address_of_elementDensities_0() { return &___elementDensities_0; }
	inline void set_elementDensities_0(Int32U5BU5D_t385246372* value)
	{
		___elementDensities_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementDensities_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___id_1)); }
	inline int64_t get_id_1() const { return ___id_1; }
	inline int64_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int64_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_plotChoice_2() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___plotChoice_2)); }
	inline int32_t get_plotChoice_2() const { return ___plotChoice_2; }
	inline int32_t* get_address_of_plotChoice_2() { return &___plotChoice_2; }
	inline void set_plotChoice_2(int32_t value)
	{
		___plotChoice_2 = value;
	}

	inline static int32_t get_offset_of_Emax_3() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___Emax_3)); }
	inline float get_Emax_3() const { return ___Emax_3; }
	inline float* get_address_of_Emax_3() { return &___Emax_3; }
	inline void set_Emax_3(float value)
	{
		___Emax_3 = value;
	}

	inline static int32_t get_offset_of_big_finger_4() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___big_finger_4)); }
	inline bool get_big_finger_4() const { return ___big_finger_4; }
	inline bool* get_address_of_big_finger_4() { return &___big_finger_4; }
	inline void set_big_finger_4(bool value)
	{
		___big_finger_4 = value;
	}

	inline static int32_t get_offset_of_ortho_move_5() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___ortho_move_5)); }
	inline bool get_ortho_move_5() const { return ___ortho_move_5; }
	inline bool* get_address_of_ortho_move_5() { return &___ortho_move_5; }
	inline void set_ortho_move_5(bool value)
	{
		___ortho_move_5 = value;
	}

	inline static int32_t get_offset_of_turnOffSolver_6() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___turnOffSolver_6)); }
	inline bool get_turnOffSolver_6() const { return ___turnOffSolver_6; }
	inline bool* get_address_of_turnOffSolver_6() { return &___turnOffSolver_6; }
	inline void set_turnOffSolver_6(bool value)
	{
		___turnOffSolver_6 = value;
	}

	inline static int32_t get_offset_of_supports_7() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___supports_7)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_supports_7() const { return ___supports_7; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_supports_7() { return &___supports_7; }
	inline void set_supports_7(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___supports_7 = value;
		Il2CppCodeGenWriteBarrier((&___supports_7), value);
	}

	inline static int32_t get_offset_of_displs_8() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___displs_8)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displs_8() const { return ___displs_8; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displs_8() { return &___displs_8; }
	inline void set_displs_8(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displs_8 = value;
		Il2CppCodeGenWriteBarrier((&___displs_8), value);
	}

	inline static int32_t get_offset_of_displEnds_9() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___displEnds_9)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displEnds_9() const { return ___displEnds_9; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displEnds_9() { return &___displEnds_9; }
	inline void set_displEnds_9(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displEnds_9 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds_9), value);
	}

	inline static int32_t get_offset_of_displs2_10() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___displs2_10)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displs2_10() const { return ___displs2_10; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displs2_10() { return &___displs2_10; }
	inline void set_displs2_10(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displs2_10 = value;
		Il2CppCodeGenWriteBarrier((&___displs2_10), value);
	}

	inline static int32_t get_offset_of_displEnds2_11() { return static_cast<int32_t>(offsetof(FEAGameData_t1628373741, ___displEnds2_11)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displEnds2_11() const { return ___displEnds2_11; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displEnds2_11() { return &___displEnds2_11; }
	inline void set_displEnds2_11(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displEnds2_11 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds2_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEAGAMEDATA_T1628373741_H
#ifndef HASHTABLEEXT_T3309791078_H
#define HASHTABLEEXT_T3309791078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HashtableExt
struct  HashtableExt_t3309791078  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLEEXT_T3309791078_H
#ifndef GUIEXT_T2334906846_H
#define GUIEXT_T2334906846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIExt
struct  GUIExt_t2334906846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIEXT_T2334906846_H
#ifndef TILEINFO_T2587709261_H
#define TILEINFO_T2587709261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TileInfo
struct  TileInfo_t2587709261  : public RuntimeObject
{
public:
	// System.Boolean TileInfo::North
	bool ___North_0;
	// System.Boolean TileInfo::NorthEast
	bool ___NorthEast_1;
	// System.Boolean TileInfo::East
	bool ___East_2;
	// System.Boolean TileInfo::SouthEast
	bool ___SouthEast_3;
	// System.Boolean TileInfo::South
	bool ___South_4;
	// System.Boolean TileInfo::SouthWest
	bool ___SouthWest_5;
	// System.Boolean TileInfo::West
	bool ___West_6;
	// System.Boolean TileInfo::NorthWest
	bool ___NorthWest_7;
	// System.Boolean TileInfo::isTile
	bool ___isTile_8;
	// PassiveBlock TileInfo::passiveBlock
	PassiveBlock_t513448541 * ___passiveBlock_9;

public:
	inline static int32_t get_offset_of_North_0() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___North_0)); }
	inline bool get_North_0() const { return ___North_0; }
	inline bool* get_address_of_North_0() { return &___North_0; }
	inline void set_North_0(bool value)
	{
		___North_0 = value;
	}

	inline static int32_t get_offset_of_NorthEast_1() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___NorthEast_1)); }
	inline bool get_NorthEast_1() const { return ___NorthEast_1; }
	inline bool* get_address_of_NorthEast_1() { return &___NorthEast_1; }
	inline void set_NorthEast_1(bool value)
	{
		___NorthEast_1 = value;
	}

	inline static int32_t get_offset_of_East_2() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___East_2)); }
	inline bool get_East_2() const { return ___East_2; }
	inline bool* get_address_of_East_2() { return &___East_2; }
	inline void set_East_2(bool value)
	{
		___East_2 = value;
	}

	inline static int32_t get_offset_of_SouthEast_3() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___SouthEast_3)); }
	inline bool get_SouthEast_3() const { return ___SouthEast_3; }
	inline bool* get_address_of_SouthEast_3() { return &___SouthEast_3; }
	inline void set_SouthEast_3(bool value)
	{
		___SouthEast_3 = value;
	}

	inline static int32_t get_offset_of_South_4() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___South_4)); }
	inline bool get_South_4() const { return ___South_4; }
	inline bool* get_address_of_South_4() { return &___South_4; }
	inline void set_South_4(bool value)
	{
		___South_4 = value;
	}

	inline static int32_t get_offset_of_SouthWest_5() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___SouthWest_5)); }
	inline bool get_SouthWest_5() const { return ___SouthWest_5; }
	inline bool* get_address_of_SouthWest_5() { return &___SouthWest_5; }
	inline void set_SouthWest_5(bool value)
	{
		___SouthWest_5 = value;
	}

	inline static int32_t get_offset_of_West_6() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___West_6)); }
	inline bool get_West_6() const { return ___West_6; }
	inline bool* get_address_of_West_6() { return &___West_6; }
	inline void set_West_6(bool value)
	{
		___West_6 = value;
	}

	inline static int32_t get_offset_of_NorthWest_7() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___NorthWest_7)); }
	inline bool get_NorthWest_7() const { return ___NorthWest_7; }
	inline bool* get_address_of_NorthWest_7() { return &___NorthWest_7; }
	inline void set_NorthWest_7(bool value)
	{
		___NorthWest_7 = value;
	}

	inline static int32_t get_offset_of_isTile_8() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___isTile_8)); }
	inline bool get_isTile_8() const { return ___isTile_8; }
	inline bool* get_address_of_isTile_8() { return &___isTile_8; }
	inline void set_isTile_8(bool value)
	{
		___isTile_8 = value;
	}

	inline static int32_t get_offset_of_passiveBlock_9() { return static_cast<int32_t>(offsetof(TileInfo_t2587709261, ___passiveBlock_9)); }
	inline PassiveBlock_t513448541 * get_passiveBlock_9() const { return ___passiveBlock_9; }
	inline PassiveBlock_t513448541 ** get_address_of_passiveBlock_9() { return &___passiveBlock_9; }
	inline void set_passiveBlock_9(PassiveBlock_t513448541 * value)
	{
		___passiveBlock_9 = value;
		Il2CppCodeGenWriteBarrier((&___passiveBlock_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEINFO_T2587709261_H
#ifndef ARRAYEXT_T2619957934_H
#define ARRAYEXT_T2619957934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrayExt
struct  ArrayExt_t2619957934  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYEXT_T2619957934_H
#ifndef SKSTATE_1_T3455126090_H
#define SKSTATE_1_T3455126090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SKState`1<TopologySelector>
struct  SKState_1_t3455126090  : public RuntimeObject
{
public:
	// SKStateMachine`1<T> SKState`1::_machine
	SKStateMachine_1_t2564681790 * ____machine_0;

public:
	inline static int32_t get_offset_of__machine_0() { return static_cast<int32_t>(offsetof(SKState_1_t3455126090, ____machine_0)); }
	inline SKStateMachine_1_t2564681790 * get__machine_0() const { return ____machine_0; }
	inline SKStateMachine_1_t2564681790 ** get_address_of__machine_0() { return &____machine_0; }
	inline void set__machine_0(SKStateMachine_1_t2564681790 * value)
	{
		____machine_0 = value;
		Il2CppCodeGenWriteBarrier((&____machine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKSTATE_1_T3455126090_H
#ifndef TESTMATRIXCOMPRESS_T925154071_H
#define TESTMATRIXCOMPRESS_T925154071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestMatrixCompress
struct  TestMatrixCompress_t925154071  : public UUnitTestCase_t1875215041
{
public:
	// System.Boolean[] TestMatrixCompress::input_ActDof
	BooleanU5BU5D_t2897418192* ___input_ActDof_0;
	// System.Int32[] TestMatrixCompress::exp_output_ActDof
	Int32U5BU5D_t385246372* ___exp_output_ActDof_1;
	// System.Int32[] TestMatrixCompress::exp_output_GenReduc
	Int32U5BU5D_t385246372* ___exp_output_GenReduc_2;
	// System.Int32[] TestMatrixCompress::exp_output_GenExpan
	Int32U5BU5D_t385246372* ___exp_output_GenExpan_3;

public:
	inline static int32_t get_offset_of_input_ActDof_0() { return static_cast<int32_t>(offsetof(TestMatrixCompress_t925154071, ___input_ActDof_0)); }
	inline BooleanU5BU5D_t2897418192* get_input_ActDof_0() const { return ___input_ActDof_0; }
	inline BooleanU5BU5D_t2897418192** get_address_of_input_ActDof_0() { return &___input_ActDof_0; }
	inline void set_input_ActDof_0(BooleanU5BU5D_t2897418192* value)
	{
		___input_ActDof_0 = value;
		Il2CppCodeGenWriteBarrier((&___input_ActDof_0), value);
	}

	inline static int32_t get_offset_of_exp_output_ActDof_1() { return static_cast<int32_t>(offsetof(TestMatrixCompress_t925154071, ___exp_output_ActDof_1)); }
	inline Int32U5BU5D_t385246372* get_exp_output_ActDof_1() const { return ___exp_output_ActDof_1; }
	inline Int32U5BU5D_t385246372** get_address_of_exp_output_ActDof_1() { return &___exp_output_ActDof_1; }
	inline void set_exp_output_ActDof_1(Int32U5BU5D_t385246372* value)
	{
		___exp_output_ActDof_1 = value;
		Il2CppCodeGenWriteBarrier((&___exp_output_ActDof_1), value);
	}

	inline static int32_t get_offset_of_exp_output_GenReduc_2() { return static_cast<int32_t>(offsetof(TestMatrixCompress_t925154071, ___exp_output_GenReduc_2)); }
	inline Int32U5BU5D_t385246372* get_exp_output_GenReduc_2() const { return ___exp_output_GenReduc_2; }
	inline Int32U5BU5D_t385246372** get_address_of_exp_output_GenReduc_2() { return &___exp_output_GenReduc_2; }
	inline void set_exp_output_GenReduc_2(Int32U5BU5D_t385246372* value)
	{
		___exp_output_GenReduc_2 = value;
		Il2CppCodeGenWriteBarrier((&___exp_output_GenReduc_2), value);
	}

	inline static int32_t get_offset_of_exp_output_GenExpan_3() { return static_cast<int32_t>(offsetof(TestMatrixCompress_t925154071, ___exp_output_GenExpan_3)); }
	inline Int32U5BU5D_t385246372* get_exp_output_GenExpan_3() const { return ___exp_output_GenExpan_3; }
	inline Int32U5BU5D_t385246372** get_address_of_exp_output_GenExpan_3() { return &___exp_output_GenExpan_3; }
	inline void set_exp_output_GenExpan_3(Int32U5BU5D_t385246372* value)
	{
		___exp_output_GenExpan_3 = value;
		Il2CppCodeGenWriteBarrier((&___exp_output_GenExpan_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTMATRIXCOMPRESS_T925154071_H
#ifndef TESTMATRIXCCS_T895833889_H
#define TESTMATRIXCCS_T895833889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestMatrixCCS
struct  TestMatrixCCS_t895833889  : public UUnitTestCase_t1875215041
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTMATRIXCCS_T895833889_H
#ifndef ASSERT_T2674897628_H
#define ASSERT_T2674897628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assert
struct  Assert_t2674897628  : public UUnitAssert_t3903203386
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERT_T2674897628_H
#ifndef FEACOMPONENTDATA_T3249130875_H
#define FEACOMPONENTDATA_T3249130875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEAComponentData
struct  FEAComponentData_t3249130875 
{
public:
	// System.Int32 FEAComponentData::positionX
	int32_t ___positionX_0;
	// System.Int32 FEAComponentData::positionY
	int32_t ___positionY_1;
	// System.Single FEAComponentData::directionX
	float ___directionX_2;
	// System.Single FEAComponentData::directionY
	float ___directionY_3;
	// System.Int32 FEAComponentData::loadId
	int32_t ___loadId_4;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(FEAComponentData_t3249130875, ___positionX_0)); }
	inline int32_t get_positionX_0() const { return ___positionX_0; }
	inline int32_t* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(int32_t value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(FEAComponentData_t3249130875, ___positionY_1)); }
	inline int32_t get_positionY_1() const { return ___positionY_1; }
	inline int32_t* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(int32_t value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_directionX_2() { return static_cast<int32_t>(offsetof(FEAComponentData_t3249130875, ___directionX_2)); }
	inline float get_directionX_2() const { return ___directionX_2; }
	inline float* get_address_of_directionX_2() { return &___directionX_2; }
	inline void set_directionX_2(float value)
	{
		___directionX_2 = value;
	}

	inline static int32_t get_offset_of_directionY_3() { return static_cast<int32_t>(offsetof(FEAComponentData_t3249130875, ___directionY_3)); }
	inline float get_directionY_3() const { return ___directionY_3; }
	inline float* get_address_of_directionY_3() { return &___directionY_3; }
	inline void set_directionY_3(float value)
	{
		___directionY_3 = value;
	}

	inline static int32_t get_offset_of_loadId_4() { return static_cast<int32_t>(offsetof(FEAComponentData_t3249130875, ___loadId_4)); }
	inline int32_t get_loadId_4() const { return ___loadId_4; }
	inline int32_t* get_address_of_loadId_4() { return &___loadId_4; }
	inline void set_loadId_4(int32_t value)
	{
		___loadId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEACOMPONENTDATA_T3249130875_H
#ifndef VEC2I_T1193278988_H
#define VEC2I_T1193278988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vec2i
struct  Vec2i_t1193278988 
{
public:
	// System.Int32 Vec2i::x
	int32_t ___x_0;
	// System.Int32 Vec2i::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2i_t1193278988, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2i_t1193278988, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T1193278988_H
#ifndef DOUBLELONGUNION_T1461655328_H
#define DOUBLELONGUNION_T1461655328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MathExtra/DoubleLongUnion
struct  DoubleLongUnion_t1461655328 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single MathExtra/DoubleLongUnion::d
			float ___d_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___d_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 MathExtra/DoubleLongUnion::tmp
			int64_t ___tmp_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___tmp_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(DoubleLongUnion_t1461655328, ___d_0)); }
	inline float get_d_0() const { return ___d_0; }
	inline float* get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(float value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_tmp_1() { return static_cast<int32_t>(offsetof(DoubleLongUnion_t1461655328, ___tmp_1)); }
	inline int64_t get_tmp_1() const { return ___tmp_1; }
	inline int64_t* get_address_of_tmp_1() { return &___tmp_1; }
	inline void set_tmp_1(int64_t value)
	{
		___tmp_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELONGUNION_T1461655328_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef FILTERTYPE_T787506034_H
#define FILTERTYPE_T787506034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologyOptimization/FilterType
struct  FilterType_t787506034 
{
public:
	// System.Int32 TopologyOptimization/FilterType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterType_t787506034, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERTYPE_T787506034_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef BRUSH_T4066421658_H
#define BRUSH_T4066421658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIPaintHandler/Brush
struct  Brush_t4066421658 
{
public:
	// System.Int32 GUIPaintHandler/Brush::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Brush_t4066421658, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSH_T4066421658_H
#ifndef TOPOLOGYCOMPONENTTYPE_T1442463493_H
#define TOPOLOGYCOMPONENTTYPE_T1442463493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologyComponent/TopologyComponentType
struct  TopologyComponentType_t1442463493 
{
public:
	// System.Int32 TopologyComponent/TopologyComponentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TopologyComponentType_t1442463493, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPOLOGYCOMPONENTTYPE_T1442463493_H
#ifndef MENU_T2559899777_H
#define MENU_T2559899777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Menu
struct  Menu_t2559899777 
{
public:
	// System.Int32 Menu::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Menu_t2559899777, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_T2559899777_H
#ifndef SELECTIONSTATEABSTRACT_T1927371580_H
#define SELECTIONSTATEABSTRACT_T1927371580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionStateAbstract
struct  SelectionStateAbstract_t1927371580  : public SKState_1_t3455126090
{
public:
	// TopologySelector SelectionStateAbstract::selector
	TopologySelector_t1922582608 * ___selector_1;

public:
	inline static int32_t get_offset_of_selector_1() { return static_cast<int32_t>(offsetof(SelectionStateAbstract_t1927371580, ___selector_1)); }
	inline TopologySelector_t1922582608 * get_selector_1() const { return ___selector_1; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_1() { return &___selector_1; }
	inline void set_selector_1(TopologySelector_t1922582608 * value)
	{
		___selector_1 = value;
		Il2CppCodeGenWriteBarrier((&___selector_1), value);
	}
};

struct SelectionStateAbstract_t1927371580_StaticFields
{
public:
	// UnityEngine.Vector3 SelectionStateAbstract::offset
	Vector3_t3722313464  ___offset_2;
	// UnityEngine.Transform SelectionStateAbstract::topComponentTransform
	Transform_t3600365921 * ___topComponentTransform_3;
	// TopologyComponent SelectionStateAbstract::topComponent
	TopologyComponent_t4181154066 * ___topComponent_4;

public:
	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(SelectionStateAbstract_t1927371580_StaticFields, ___offset_2)); }
	inline Vector3_t3722313464  get_offset_2() const { return ___offset_2; }
	inline Vector3_t3722313464 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Vector3_t3722313464  value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_topComponentTransform_3() { return static_cast<int32_t>(offsetof(SelectionStateAbstract_t1927371580_StaticFields, ___topComponentTransform_3)); }
	inline Transform_t3600365921 * get_topComponentTransform_3() const { return ___topComponentTransform_3; }
	inline Transform_t3600365921 ** get_address_of_topComponentTransform_3() { return &___topComponentTransform_3; }
	inline void set_topComponentTransform_3(Transform_t3600365921 * value)
	{
		___topComponentTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___topComponentTransform_3), value);
	}

	inline static int32_t get_offset_of_topComponent_4() { return static_cast<int32_t>(offsetof(SelectionStateAbstract_t1927371580_StaticFields, ___topComponent_4)); }
	inline TopologyComponent_t4181154066 * get_topComponent_4() const { return ___topComponent_4; }
	inline TopologyComponent_t4181154066 ** get_address_of_topComponent_4() { return &___topComponent_4; }
	inline void set_topComponent_4(TopologyComponent_t4181154066 * value)
	{
		___topComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___topComponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATEABSTRACT_T1927371580_H
#ifndef TOOL_T2235229658_H
#define TOOL_T2235229658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIPaintHandler/Tool
struct  Tool_t2235229658 
{
public:
	// System.Int32 GUIPaintHandler/Tool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Tool_t2235229658, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOL_T2235229658_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MOUSEWRAPPER_T4049244429_H
#define MOUSEWRAPPER_T4049244429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MouseWrapper
struct  MouseWrapper_t4049244429  : public RuntimeObject
{
public:

public:
};

struct MouseWrapper_t4049244429_StaticFields
{
public:
	// System.Int32 MouseWrapper::lastFrame
	int32_t ___lastFrame_0;
	// System.Boolean[] MouseWrapper::mouseButtonDown
	BooleanU5BU5D_t2897418192* ___mouseButtonDown_1;
	// System.Boolean[] MouseWrapper::mouseButtonUp
	BooleanU5BU5D_t2897418192* ___mouseButtonUp_2;
	// System.Boolean[] MouseWrapper::mouseButton
	BooleanU5BU5D_t2897418192* ___mouseButton_3;
	// UnityEngine.Vector3 MouseWrapper::mousePosition
	Vector3_t3722313464  ___mousePosition_4;

public:
	inline static int32_t get_offset_of_lastFrame_0() { return static_cast<int32_t>(offsetof(MouseWrapper_t4049244429_StaticFields, ___lastFrame_0)); }
	inline int32_t get_lastFrame_0() const { return ___lastFrame_0; }
	inline int32_t* get_address_of_lastFrame_0() { return &___lastFrame_0; }
	inline void set_lastFrame_0(int32_t value)
	{
		___lastFrame_0 = value;
	}

	inline static int32_t get_offset_of_mouseButtonDown_1() { return static_cast<int32_t>(offsetof(MouseWrapper_t4049244429_StaticFields, ___mouseButtonDown_1)); }
	inline BooleanU5BU5D_t2897418192* get_mouseButtonDown_1() const { return ___mouseButtonDown_1; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mouseButtonDown_1() { return &___mouseButtonDown_1; }
	inline void set_mouseButtonDown_1(BooleanU5BU5D_t2897418192* value)
	{
		___mouseButtonDown_1 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtonDown_1), value);
	}

	inline static int32_t get_offset_of_mouseButtonUp_2() { return static_cast<int32_t>(offsetof(MouseWrapper_t4049244429_StaticFields, ___mouseButtonUp_2)); }
	inline BooleanU5BU5D_t2897418192* get_mouseButtonUp_2() const { return ___mouseButtonUp_2; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mouseButtonUp_2() { return &___mouseButtonUp_2; }
	inline void set_mouseButtonUp_2(BooleanU5BU5D_t2897418192* value)
	{
		___mouseButtonUp_2 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButtonUp_2), value);
	}

	inline static int32_t get_offset_of_mouseButton_3() { return static_cast<int32_t>(offsetof(MouseWrapper_t4049244429_StaticFields, ___mouseButton_3)); }
	inline BooleanU5BU5D_t2897418192* get_mouseButton_3() const { return ___mouseButton_3; }
	inline BooleanU5BU5D_t2897418192** get_address_of_mouseButton_3() { return &___mouseButton_3; }
	inline void set_mouseButton_3(BooleanU5BU5D_t2897418192* value)
	{
		___mouseButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___mouseButton_3), value);
	}

	inline static int32_t get_offset_of_mousePosition_4() { return static_cast<int32_t>(offsetof(MouseWrapper_t4049244429_StaticFields, ___mousePosition_4)); }
	inline Vector3_t3722313464  get_mousePosition_4() const { return ___mousePosition_4; }
	inline Vector3_t3722313464 * get_address_of_mousePosition_4() { return &___mousePosition_4; }
	inline void set_mousePosition_4(Vector3_t3722313464  value)
	{
		___mousePosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEWRAPPER_T4049244429_H
#ifndef ELEMENTTYPE_T1217679599_H
#define ELEMENTTYPE_T1217679599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PassiveBlock/ElementType
struct  ElementType_t1217679599 
{
public:
	// System.Int32 PassiveBlock/ElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ElementType_t1217679599, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTTYPE_T1217679599_H
#ifndef COLOREXT_T86973071_H
#define COLOREXT_T86973071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorExt
struct  ColorExt_t86973071  : public RuntimeObject
{
public:

public:
};

struct ColorExt_t86973071_StaticFields
{
public:
	// UnityEngine.Color ColorExt::Material_1
	Color_t2555686324  ___Material_1_0;
	// UnityEngine.Color ColorExt::Material_2
	Color_t2555686324  ___Material_2_1;
	// UnityEngine.Color ColorExt::LightGray
	Color_t2555686324  ___LightGray_2;

public:
	inline static int32_t get_offset_of_Material_1_0() { return static_cast<int32_t>(offsetof(ColorExt_t86973071_StaticFields, ___Material_1_0)); }
	inline Color_t2555686324  get_Material_1_0() const { return ___Material_1_0; }
	inline Color_t2555686324 * get_address_of_Material_1_0() { return &___Material_1_0; }
	inline void set_Material_1_0(Color_t2555686324  value)
	{
		___Material_1_0 = value;
	}

	inline static int32_t get_offset_of_Material_2_1() { return static_cast<int32_t>(offsetof(ColorExt_t86973071_StaticFields, ___Material_2_1)); }
	inline Color_t2555686324  get_Material_2_1() const { return ___Material_2_1; }
	inline Color_t2555686324 * get_address_of_Material_2_1() { return &___Material_2_1; }
	inline void set_Material_2_1(Color_t2555686324  value)
	{
		___Material_2_1 = value;
	}

	inline static int32_t get_offset_of_LightGray_2() { return static_cast<int32_t>(offsetof(ColorExt_t86973071_StaticFields, ___LightGray_2)); }
	inline Color_t2555686324  get_LightGray_2() const { return ___LightGray_2; }
	inline Color_t2555686324 * get_address_of_LightGray_2() { return &___LightGray_2; }
	inline void set_LightGray_2(Color_t2555686324  value)
	{
		___LightGray_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOREXT_T86973071_H
#ifndef COMPUTECAPABILITIES_T3016857551_H
#define COMPUTECAPABILITIES_T3016857551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceInfo/ComputeCapabilities
struct  ComputeCapabilities_t3016857551 
{
public:
	// System.Int32 DeviceInfo/ComputeCapabilities::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ComputeCapabilities_t3016857551, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTECAPABILITIES_T3016857551_H
#ifndef DPI_T1419146022_H
#define DPI_T1419146022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceInfo/DPI
struct  DPI_t1419146022 
{
public:
	// System.Int32 DeviceInfo/DPI::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DPI_t1419146022, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DPI_T1419146022_H
#ifndef U3CANIMATEMENUCOLORU3EC__ITERATOR0_T3839011888_H
#define U3CANIMATEMENUCOLORU3EC__ITERATOR0_T3839011888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawMenuGUI/<AnimateMenuColor>c__Iterator0
struct  U3CAnimateMenuColorU3Ec__Iterator0_t3839011888  : public RuntimeObject
{
public:
	// System.Single DrawMenuGUI/<AnimateMenuColor>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// UnityEngine.Color DrawMenuGUI/<AnimateMenuColor>c__Iterator0::<white>__0
	Color_t2555686324  ___U3CwhiteU3E__0_1;
	// UnityEngine.Color DrawMenuGUI/<AnimateMenuColor>c__Iterator0::<transparent>__0
	Color_t2555686324  ___U3CtransparentU3E__0_2;
	// System.String DrawMenuGUI/<AnimateMenuColor>c__Iterator0::newValue
	String_t* ___newValue_3;
	// DrawMenuGUI DrawMenuGUI/<AnimateMenuColor>c__Iterator0::$this
	DrawMenuGUI_t2806387517 * ___U24this_4;
	// System.Object DrawMenuGUI/<AnimateMenuColor>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean DrawMenuGUI/<AnimateMenuColor>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 DrawMenuGUI/<AnimateMenuColor>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwhiteU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U3CwhiteU3E__0_1)); }
	inline Color_t2555686324  get_U3CwhiteU3E__0_1() const { return ___U3CwhiteU3E__0_1; }
	inline Color_t2555686324 * get_address_of_U3CwhiteU3E__0_1() { return &___U3CwhiteU3E__0_1; }
	inline void set_U3CwhiteU3E__0_1(Color_t2555686324  value)
	{
		___U3CwhiteU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtransparentU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U3CtransparentU3E__0_2)); }
	inline Color_t2555686324  get_U3CtransparentU3E__0_2() const { return ___U3CtransparentU3E__0_2; }
	inline Color_t2555686324 * get_address_of_U3CtransparentU3E__0_2() { return &___U3CtransparentU3E__0_2; }
	inline void set_U3CtransparentU3E__0_2(Color_t2555686324  value)
	{
		___U3CtransparentU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_newValue_3() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___newValue_3)); }
	inline String_t* get_newValue_3() const { return ___newValue_3; }
	inline String_t** get_address_of_newValue_3() { return &___newValue_3; }
	inline void set_newValue_3(String_t* value)
	{
		___newValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___newValue_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U24this_4)); }
	inline DrawMenuGUI_t2806387517 * get_U24this_4() const { return ___U24this_4; }
	inline DrawMenuGUI_t2806387517 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(DrawMenuGUI_t2806387517 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t3839011888, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEMENUCOLORU3EC__ITERATOR0_T3839011888_H
#ifndef MOBILESCREENSIZE_T1575161839_H
#define MOBILESCREENSIZE_T1575161839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceInfo/MobileScreenSize
struct  MobileScreenSize_t1575161839 
{
public:
	// System.Int32 DeviceInfo/MobileScreenSize::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MobileScreenSize_t1575161839, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILESCREENSIZE_T1575161839_H
#ifndef U3CANIMATEMENUCOLORU3EC__ITERATOR0_T889497008_H
#define U3CANIMATEMENUCOLORU3EC__ITERATOR0_T889497008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0
struct  U3CAnimateMenuColorU3Ec__Iterator0_t889497008  : public RuntimeObject
{
public:
	// System.Single BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::<time>__0
	float ___U3CtimeU3E__0_0;
	// UnityEngine.Color BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::<white>__0
	Color_t2555686324  ___U3CwhiteU3E__0_1;
	// UnityEngine.Color BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::<transparent>__0
	Color_t2555686324  ___U3CtransparentU3E__0_2;
	// System.String BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::newValue
	String_t* ___newValue_3;
	// BoundingConstraintsGUI BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::$this
	BoundingConstraintsGUI_t795470468 * ___U24this_4;
	// System.Object BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 BoundingConstraintsGUI/<AnimateMenuColor>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CwhiteU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U3CwhiteU3E__0_1)); }
	inline Color_t2555686324  get_U3CwhiteU3E__0_1() const { return ___U3CwhiteU3E__0_1; }
	inline Color_t2555686324 * get_address_of_U3CwhiteU3E__0_1() { return &___U3CwhiteU3E__0_1; }
	inline void set_U3CwhiteU3E__0_1(Color_t2555686324  value)
	{
		___U3CwhiteU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtransparentU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U3CtransparentU3E__0_2)); }
	inline Color_t2555686324  get_U3CtransparentU3E__0_2() const { return ___U3CtransparentU3E__0_2; }
	inline Color_t2555686324 * get_address_of_U3CtransparentU3E__0_2() { return &___U3CtransparentU3E__0_2; }
	inline void set_U3CtransparentU3E__0_2(Color_t2555686324  value)
	{
		___U3CtransparentU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_newValue_3() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___newValue_3)); }
	inline String_t* get_newValue_3() const { return ___newValue_3; }
	inline String_t** get_address_of_newValue_3() { return &___newValue_3; }
	inline void set_newValue_3(String_t* value)
	{
		___newValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___newValue_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U24this_4)); }
	inline BoundingConstraintsGUI_t795470468 * get_U24this_4() const { return ___U24this_4; }
	inline BoundingConstraintsGUI_t795470468 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(BoundingConstraintsGUI_t795470468 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CAnimateMenuColorU3Ec__Iterator0_t889497008, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEMENUCOLORU3EC__ITERATOR0_T889497008_H
#ifndef TOPOLOGYOPTIMIZATION_T326002099_H
#define TOPOLOGYOPTIMIZATION_T326002099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologyOptimization
struct  TopologyOptimization_t326002099  : public RuntimeObject
{
public:
	// System.Int32 TopologyOptimization::Reduced_ndof
	int32_t ___Reduced_ndof_1;
	// System.Int32[] TopologyOptimization::edof_reductor
	Int32U5BU5D_t385246372* ___edof_reductor_2;
	// TopologyOptimization/NotifyUpdateObjectiveFunction TopologyOptimization::notifyUpdateObjectiveFunction
	NotifyUpdateObjectiveFunction_t2430789460 * ___notifyUpdateObjectiveFunction_3;
	// TopologyOptimization/GetUpdatedTopologyElementDensity TopologyOptimization::getUpdatedTopologyElementDensity
	GetUpdatedTopologyElementDensity_t3009975164 * ___getUpdatedTopologyElementDensity_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) TopologyOptimization::Paused
	bool ___Paused_5;

public:
	inline static int32_t get_offset_of_Reduced_ndof_1() { return static_cast<int32_t>(offsetof(TopologyOptimization_t326002099, ___Reduced_ndof_1)); }
	inline int32_t get_Reduced_ndof_1() const { return ___Reduced_ndof_1; }
	inline int32_t* get_address_of_Reduced_ndof_1() { return &___Reduced_ndof_1; }
	inline void set_Reduced_ndof_1(int32_t value)
	{
		___Reduced_ndof_1 = value;
	}

	inline static int32_t get_offset_of_edof_reductor_2() { return static_cast<int32_t>(offsetof(TopologyOptimization_t326002099, ___edof_reductor_2)); }
	inline Int32U5BU5D_t385246372* get_edof_reductor_2() const { return ___edof_reductor_2; }
	inline Int32U5BU5D_t385246372** get_address_of_edof_reductor_2() { return &___edof_reductor_2; }
	inline void set_edof_reductor_2(Int32U5BU5D_t385246372* value)
	{
		___edof_reductor_2 = value;
		Il2CppCodeGenWriteBarrier((&___edof_reductor_2), value);
	}

	inline static int32_t get_offset_of_notifyUpdateObjectiveFunction_3() { return static_cast<int32_t>(offsetof(TopologyOptimization_t326002099, ___notifyUpdateObjectiveFunction_3)); }
	inline NotifyUpdateObjectiveFunction_t2430789460 * get_notifyUpdateObjectiveFunction_3() const { return ___notifyUpdateObjectiveFunction_3; }
	inline NotifyUpdateObjectiveFunction_t2430789460 ** get_address_of_notifyUpdateObjectiveFunction_3() { return &___notifyUpdateObjectiveFunction_3; }
	inline void set_notifyUpdateObjectiveFunction_3(NotifyUpdateObjectiveFunction_t2430789460 * value)
	{
		___notifyUpdateObjectiveFunction_3 = value;
		Il2CppCodeGenWriteBarrier((&___notifyUpdateObjectiveFunction_3), value);
	}

	inline static int32_t get_offset_of_getUpdatedTopologyElementDensity_4() { return static_cast<int32_t>(offsetof(TopologyOptimization_t326002099, ___getUpdatedTopologyElementDensity_4)); }
	inline GetUpdatedTopologyElementDensity_t3009975164 * get_getUpdatedTopologyElementDensity_4() const { return ___getUpdatedTopologyElementDensity_4; }
	inline GetUpdatedTopologyElementDensity_t3009975164 ** get_address_of_getUpdatedTopologyElementDensity_4() { return &___getUpdatedTopologyElementDensity_4; }
	inline void set_getUpdatedTopologyElementDensity_4(GetUpdatedTopologyElementDensity_t3009975164 * value)
	{
		___getUpdatedTopologyElementDensity_4 = value;
		Il2CppCodeGenWriteBarrier((&___getUpdatedTopologyElementDensity_4), value);
	}

	inline static int32_t get_offset_of_Paused_5() { return static_cast<int32_t>(offsetof(TopologyOptimization_t326002099, ___Paused_5)); }
	inline bool get_Paused_5() const { return ___Paused_5; }
	inline bool* get_address_of_Paused_5() { return &___Paused_5; }
	inline void set_Paused_5(bool value)
	{
		___Paused_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPOLOGYOPTIMIZATION_T326002099_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SELECTIONADDNEWCOMPSTATE_T2995677980_H
#define SELECTIONADDNEWCOMPSTATE_T2995677980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionAddNewCompState
struct  SelectionAddNewCompState_t2995677980  : public SelectionStateAbstract_t1927371580
{
public:
	// TopologySelector SelectionAddNewCompState::selector
	TopologySelector_t1922582608 * ___selector_5;

public:
	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(SelectionAddNewCompState_t2995677980, ___selector_5)); }
	inline TopologySelector_t1922582608 * get_selector_5() const { return ___selector_5; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(TopologySelector_t1922582608 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___selector_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONADDNEWCOMPSTATE_T2995677980_H
#ifndef SELECTIONVECTORSTATE_T889007370_H
#define SELECTIONVECTORSTATE_T889007370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionVectorState
struct  SelectionVectorState_t889007370  : public SelectionStateAbstract_t1927371580
{
public:
	// System.Single SelectionVectorState::angleOffset
	float ___angleOffset_5;
	// TopologySelector SelectionVectorState::selector
	TopologySelector_t1922582608 * ___selector_6;
	// System.Single SelectionVectorState::angle
	float ___angle_7;
	// System.Int32 SelectionVectorState::angleRounded
	int32_t ___angleRounded_8;

public:
	inline static int32_t get_offset_of_angleOffset_5() { return static_cast<int32_t>(offsetof(SelectionVectorState_t889007370, ___angleOffset_5)); }
	inline float get_angleOffset_5() const { return ___angleOffset_5; }
	inline float* get_address_of_angleOffset_5() { return &___angleOffset_5; }
	inline void set_angleOffset_5(float value)
	{
		___angleOffset_5 = value;
	}

	inline static int32_t get_offset_of_selector_6() { return static_cast<int32_t>(offsetof(SelectionVectorState_t889007370, ___selector_6)); }
	inline TopologySelector_t1922582608 * get_selector_6() const { return ___selector_6; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_6() { return &___selector_6; }
	inline void set_selector_6(TopologySelector_t1922582608 * value)
	{
		___selector_6 = value;
		Il2CppCodeGenWriteBarrier((&___selector_6), value);
	}

	inline static int32_t get_offset_of_angle_7() { return static_cast<int32_t>(offsetof(SelectionVectorState_t889007370, ___angle_7)); }
	inline float get_angle_7() const { return ___angle_7; }
	inline float* get_address_of_angle_7() { return &___angle_7; }
	inline void set_angle_7(float value)
	{
		___angle_7 = value;
	}

	inline static int32_t get_offset_of_angleRounded_8() { return static_cast<int32_t>(offsetof(SelectionVectorState_t889007370, ___angleRounded_8)); }
	inline int32_t get_angleRounded_8() const { return ___angleRounded_8; }
	inline int32_t* get_address_of_angleRounded_8() { return &___angleRounded_8; }
	inline void set_angleRounded_8(int32_t value)
	{
		___angleRounded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONVECTORSTATE_T889007370_H
#ifndef SELECTIONROTATESTATE_T2962112176_H
#define SELECTIONROTATESTATE_T2962112176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionRotateState
struct  SelectionRotateState_t2962112176  : public SelectionStateAbstract_t1927371580
{
public:
	// System.Single SelectionRotateState::angleOffset
	float ___angleOffset_5;
	// TopologySelector SelectionRotateState::selector
	TopologySelector_t1922582608 * ___selector_6;
	// System.Single SelectionRotateState::angle
	float ___angle_7;
	// System.Int32 SelectionRotateState::angleRounded
	int32_t ___angleRounded_8;

public:
	inline static int32_t get_offset_of_angleOffset_5() { return static_cast<int32_t>(offsetof(SelectionRotateState_t2962112176, ___angleOffset_5)); }
	inline float get_angleOffset_5() const { return ___angleOffset_5; }
	inline float* get_address_of_angleOffset_5() { return &___angleOffset_5; }
	inline void set_angleOffset_5(float value)
	{
		___angleOffset_5 = value;
	}

	inline static int32_t get_offset_of_selector_6() { return static_cast<int32_t>(offsetof(SelectionRotateState_t2962112176, ___selector_6)); }
	inline TopologySelector_t1922582608 * get_selector_6() const { return ___selector_6; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_6() { return &___selector_6; }
	inline void set_selector_6(TopologySelector_t1922582608 * value)
	{
		___selector_6 = value;
		Il2CppCodeGenWriteBarrier((&___selector_6), value);
	}

	inline static int32_t get_offset_of_angle_7() { return static_cast<int32_t>(offsetof(SelectionRotateState_t2962112176, ___angle_7)); }
	inline float get_angle_7() const { return ___angle_7; }
	inline float* get_address_of_angle_7() { return &___angle_7; }
	inline void set_angle_7(float value)
	{
		___angle_7 = value;
	}

	inline static int32_t get_offset_of_angleRounded_8() { return static_cast<int32_t>(offsetof(SelectionRotateState_t2962112176, ___angleRounded_8)); }
	inline int32_t get_angleRounded_8() const { return ___angleRounded_8; }
	inline int32_t* get_address_of_angleRounded_8() { return &___angleRounded_8; }
	inline void set_angleRounded_8(int32_t value)
	{
		___angleRounded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONROTATESTATE_T2962112176_H
#ifndef SELECTIONNONESTATE_T731836665_H
#define SELECTIONNONESTATE_T731836665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionNoneState
struct  SelectionNoneState_t731836665  : public SelectionStateAbstract_t1927371580
{
public:
	// TopologySelector SelectionNoneState::selector
	TopologySelector_t1922582608 * ___selector_5;

public:
	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(SelectionNoneState_t731836665, ___selector_5)); }
	inline TopologySelector_t1922582608 * get_selector_5() const { return ___selector_5; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(TopologySelector_t1922582608 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___selector_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONNONESTATE_T731836665_H
#ifndef SELECTIONMULTIDELETESTATE_T3644334706_H
#define SELECTIONMULTIDELETESTATE_T3644334706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionMultiDeleteState
struct  SelectionMultiDeleteState_t3644334706  : public SelectionStateAbstract_t1927371580
{
public:
	// TopologySelector SelectionMultiDeleteState::selector
	TopologySelector_t1922582608 * ___selector_5;

public:
	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(SelectionMultiDeleteState_t3644334706, ___selector_5)); }
	inline TopologySelector_t1922582608 * get_selector_5() const { return ___selector_5; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(TopologySelector_t1922582608 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___selector_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONMULTIDELETESTATE_T3644334706_H
#ifndef SELECTIONMOVESTATE_T4022857977_H
#define SELECTIONMOVESTATE_T4022857977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionMoveState
struct  SelectionMoveState_t4022857977  : public SelectionStateAbstract_t1927371580
{
public:
	// TopologySelector SelectionMoveState::selector
	TopologySelector_t1922582608 * ___selector_5;

public:
	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(SelectionMoveState_t4022857977, ___selector_5)); }
	inline TopologySelector_t1922582608 * get_selector_5() const { return ___selector_5; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(TopologySelector_t1922582608 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___selector_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONMOVESTATE_T4022857977_H
#ifndef SELECTIONFINGERSTATE_T629756812_H
#define SELECTIONFINGERSTATE_T629756812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionFingerState
struct  SelectionFingerState_t629756812  : public SelectionStateAbstract_t1927371580
{
public:
	// TopologySelector SelectionFingerState::selector
	TopologySelector_t1922582608 * ___selector_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,TopologyComponent> SelectionFingerState::touchDict
	Dictionary_2_t3069867397 * ___touchDict_6;

public:
	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(SelectionFingerState_t629756812, ___selector_5)); }
	inline TopologySelector_t1922582608 * get_selector_5() const { return ___selector_5; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(TopologySelector_t1922582608 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___selector_5), value);
	}

	inline static int32_t get_offset_of_touchDict_6() { return static_cast<int32_t>(offsetof(SelectionFingerState_t629756812, ___touchDict_6)); }
	inline Dictionary_2_t3069867397 * get_touchDict_6() const { return ___touchDict_6; }
	inline Dictionary_2_t3069867397 ** get_address_of_touchDict_6() { return &___touchDict_6; }
	inline void set_touchDict_6(Dictionary_2_t3069867397 * value)
	{
		___touchDict_6 = value;
		Il2CppCodeGenWriteBarrier((&___touchDict_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONFINGERSTATE_T629756812_H
#ifndef DEVICEINFO_T2768285361_H
#define DEVICEINFO_T2768285361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceInfo
struct  DeviceInfo_t2768285361  : public RuntimeObject
{
public:
	// System.Boolean DeviceInfo::_isAndroid
	bool ____isAndroid_0;
	// System.Boolean DeviceInfo::_isIOS
	bool ____isIOS_1;
	// DeviceInfo/DPI DeviceInfo::dpi
	int32_t ___dpi_2;
	// DeviceInfo/ComputeCapabilities DeviceInfo::computeCapabilities
	int32_t ___computeCapabilities_3;
	// DeviceInfo/MobileScreenSize DeviceInfo::mobileScreenSize
	int32_t ___mobileScreenSize_4;
	// System.Boolean DeviceInfo::_isWeb
	bool ____isWeb_5;

public:
	inline static int32_t get_offset_of__isAndroid_0() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361, ____isAndroid_0)); }
	inline bool get__isAndroid_0() const { return ____isAndroid_0; }
	inline bool* get_address_of__isAndroid_0() { return &____isAndroid_0; }
	inline void set__isAndroid_0(bool value)
	{
		____isAndroid_0 = value;
	}

	inline static int32_t get_offset_of__isIOS_1() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361, ____isIOS_1)); }
	inline bool get__isIOS_1() const { return ____isIOS_1; }
	inline bool* get_address_of__isIOS_1() { return &____isIOS_1; }
	inline void set__isIOS_1(bool value)
	{
		____isIOS_1 = value;
	}

	inline static int32_t get_offset_of_dpi_2() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361, ___dpi_2)); }
	inline int32_t get_dpi_2() const { return ___dpi_2; }
	inline int32_t* get_address_of_dpi_2() { return &___dpi_2; }
	inline void set_dpi_2(int32_t value)
	{
		___dpi_2 = value;
	}

	inline static int32_t get_offset_of_computeCapabilities_3() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361, ___computeCapabilities_3)); }
	inline int32_t get_computeCapabilities_3() const { return ___computeCapabilities_3; }
	inline int32_t* get_address_of_computeCapabilities_3() { return &___computeCapabilities_3; }
	inline void set_computeCapabilities_3(int32_t value)
	{
		___computeCapabilities_3 = value;
	}

	inline static int32_t get_offset_of_mobileScreenSize_4() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361, ___mobileScreenSize_4)); }
	inline int32_t get_mobileScreenSize_4() const { return ___mobileScreenSize_4; }
	inline int32_t* get_address_of_mobileScreenSize_4() { return &___mobileScreenSize_4; }
	inline void set_mobileScreenSize_4(int32_t value)
	{
		___mobileScreenSize_4 = value;
	}

	inline static int32_t get_offset_of__isWeb_5() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361, ____isWeb_5)); }
	inline bool get__isWeb_5() const { return ____isWeb_5; }
	inline bool* get_address_of__isWeb_5() { return &____isWeb_5; }
	inline void set__isWeb_5(bool value)
	{
		____isWeb_5 = value;
	}
};

struct DeviceInfo_t2768285361_StaticFields
{
public:
	// DeviceInfo DeviceInfo::deviceInfoInst
	DeviceInfo_t2768285361 * ___deviceInfoInst_6;

public:
	inline static int32_t get_offset_of_deviceInfoInst_6() { return static_cast<int32_t>(offsetof(DeviceInfo_t2768285361_StaticFields, ___deviceInfoInst_6)); }
	inline DeviceInfo_t2768285361 * get_deviceInfoInst_6() const { return ___deviceInfoInst_6; }
	inline DeviceInfo_t2768285361 ** get_address_of_deviceInfoInst_6() { return &___deviceInfoInst_6; }
	inline void set_deviceInfoInst_6(DeviceInfo_t2768285361 * value)
	{
		___deviceInfoInst_6 = value;
		Il2CppCodeGenWriteBarrier((&___deviceInfoInst_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEINFO_T2768285361_H
#ifndef FEA_T3250963177_H
#define FEA_T3250963177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEA
struct  FEA_t3250963177  : public TopologyOptimization_t326002099
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) FEA::running
	bool ___running_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) FEA::destroyed
	bool ___destroyed_7;
	// FEAComponentData[] FEA::displs
	FEAComponentDataU5BU5D_t1047043642* ___displs_8;
	// FEAComponentData[] FEA::displEnds
	FEAComponentDataU5BU5D_t1047043642* ___displEnds_9;
	// FEAComponentData[] FEA::displs2
	FEAComponentDataU5BU5D_t1047043642* ___displs2_10;
	// FEAComponentData[] FEA::displEnds2
	FEAComponentDataU5BU5D_t1047043642* ___displEnds2_11;
	// FEAComponentData[] FEA::supports
	FEAComponentDataU5BU5D_t1047043642* ___supports_12;
	// System.Int32 FEA::nex
	int32_t ___nex_13;
	// System.Int32 FEA::ney
	int32_t ___ney_14;
	// System.Int32 FEA::nx
	int32_t ___nx_15;
	// System.Int32 FEA::ny
	int32_t ___ny_16;
	// System.Int32 FEA::nnodes
	int32_t ___nnodes_17;
	// System.Int32 FEA::ndof
	int32_t ___ndof_18;
	// System.Int32 FEA::nelem
	int32_t ___nelem_19;
	// System.Int32 FEA::itercount
	int32_t ___itercount_20;
	// System.Single FEA::f
	float ___f_21;
	// System.Int32[] FEA::Element_materials
	Int32U5BU5D_t385246372* ___Element_materials_22;
	// System.Boolean[] FEA::Element_topology
	BooleanU5BU5D_t2897418192* ___Element_topology_23;
	// System.Single[] FEA::rhoPhysVal
	SingleU5BU5D_t1444911251* ___rhoPhysVal_24;
	// System.Single[] FEA::nodeVal
	SingleU5BU5D_t1444911251* ___nodeVal_25;
	// System.Int32[] FEA::nElPrNode
	Int32U5BU5D_t385246372* ___nElPrNode_26;
	// System.Single[] FEA::bMat
	SingleU5BU5D_t1444911251* ___bMat_27;
	// System.Single[] FEA::cMat
	SingleU5BU5D_t1444911251* ___cMat_28;
	// System.Single[] FEA::u
	SingleU5BU5D_t1444911251* ___u_29;
	// System.Single[] FEA::F
	SingleU5BU5D_t1444911251* ___F_30;
	// System.Single[] FEA::KE_1
	SingleU5BU5D_t1444911251* ___KE_1_31;
	// System.Single[] FEA::KE_2
	SingleU5BU5D_t1444911251* ___KE_2_32;
	// System.Boolean FEA::reducedKK
	bool ___reducedKK_33;
	// System.Boolean FEA::IsNewTopology
	bool ___IsNewTopology_34;
	// MatrixCCS FEA::KK
	MatrixCCS_t3519621953 * ___KK_35;
	// SparseLDL FEA::LDL
	SparseLDL_t1701177157 * ___LDL_36;
	// FEAMatrixCompress FEA::FEA_Compress
	FEAMatrixCompress_t1508710683 * ___FEA_Compress_37;
	// System.Int32 FEA::Reduced_ndof
	int32_t ___Reduced_ndof_38;
	// System.Int32[] FEA::edof_reductor
	Int32U5BU5D_t385246372* ___edof_reductor_39;
	// System.Int32[] FEA::edof_expander
	Int32U5BU5D_t385246372* ___edof_expander_40;
	// System.Single[] FEA::u_reduced
	SingleU5BU5D_t1444911251* ___u_reduced_41;
	// System.Single[] FEA::F_reduced
	SingleU5BU5D_t1444911251* ___F_reduced_42;
	// System.Single FEA::E1
	float ___E1_43;
	// System.Single FEA::E2
	float ___E2_44;
	// System.Single FEA::nu1
	float ___nu1_45;
	// System.Single FEA::nu2
	float ___nu2_46;
	// System.UInt32 FEA::loop
	uint32_t ___loop_47;
	// System.Int32 FEA::plotChoice
	int32_t ___plotChoice_48;
	// System.Boolean FEA::big_finger
	bool ___big_finger_49;
	// System.Boolean FEA::ortho_move
	bool ___ortho_move_50;
	// System.Boolean FEA::turnOffSolver
	bool ___turnOffSolver_51;

public:
	inline static int32_t get_offset_of_running_6() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___running_6)); }
	inline bool get_running_6() const { return ___running_6; }
	inline bool* get_address_of_running_6() { return &___running_6; }
	inline void set_running_6(bool value)
	{
		___running_6 = value;
	}

	inline static int32_t get_offset_of_destroyed_7() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___destroyed_7)); }
	inline bool get_destroyed_7() const { return ___destroyed_7; }
	inline bool* get_address_of_destroyed_7() { return &___destroyed_7; }
	inline void set_destroyed_7(bool value)
	{
		___destroyed_7 = value;
	}

	inline static int32_t get_offset_of_displs_8() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___displs_8)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displs_8() const { return ___displs_8; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displs_8() { return &___displs_8; }
	inline void set_displs_8(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displs_8 = value;
		Il2CppCodeGenWriteBarrier((&___displs_8), value);
	}

	inline static int32_t get_offset_of_displEnds_9() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___displEnds_9)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displEnds_9() const { return ___displEnds_9; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displEnds_9() { return &___displEnds_9; }
	inline void set_displEnds_9(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displEnds_9 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds_9), value);
	}

	inline static int32_t get_offset_of_displs2_10() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___displs2_10)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displs2_10() const { return ___displs2_10; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displs2_10() { return &___displs2_10; }
	inline void set_displs2_10(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displs2_10 = value;
		Il2CppCodeGenWriteBarrier((&___displs2_10), value);
	}

	inline static int32_t get_offset_of_displEnds2_11() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___displEnds2_11)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_displEnds2_11() const { return ___displEnds2_11; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_displEnds2_11() { return &___displEnds2_11; }
	inline void set_displEnds2_11(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___displEnds2_11 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds2_11), value);
	}

	inline static int32_t get_offset_of_supports_12() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___supports_12)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_supports_12() const { return ___supports_12; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_supports_12() { return &___supports_12; }
	inline void set_supports_12(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___supports_12 = value;
		Il2CppCodeGenWriteBarrier((&___supports_12), value);
	}

	inline static int32_t get_offset_of_nex_13() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nex_13)); }
	inline int32_t get_nex_13() const { return ___nex_13; }
	inline int32_t* get_address_of_nex_13() { return &___nex_13; }
	inline void set_nex_13(int32_t value)
	{
		___nex_13 = value;
	}

	inline static int32_t get_offset_of_ney_14() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___ney_14)); }
	inline int32_t get_ney_14() const { return ___ney_14; }
	inline int32_t* get_address_of_ney_14() { return &___ney_14; }
	inline void set_ney_14(int32_t value)
	{
		___ney_14 = value;
	}

	inline static int32_t get_offset_of_nx_15() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nx_15)); }
	inline int32_t get_nx_15() const { return ___nx_15; }
	inline int32_t* get_address_of_nx_15() { return &___nx_15; }
	inline void set_nx_15(int32_t value)
	{
		___nx_15 = value;
	}

	inline static int32_t get_offset_of_ny_16() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___ny_16)); }
	inline int32_t get_ny_16() const { return ___ny_16; }
	inline int32_t* get_address_of_ny_16() { return &___ny_16; }
	inline void set_ny_16(int32_t value)
	{
		___ny_16 = value;
	}

	inline static int32_t get_offset_of_nnodes_17() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nnodes_17)); }
	inline int32_t get_nnodes_17() const { return ___nnodes_17; }
	inline int32_t* get_address_of_nnodes_17() { return &___nnodes_17; }
	inline void set_nnodes_17(int32_t value)
	{
		___nnodes_17 = value;
	}

	inline static int32_t get_offset_of_ndof_18() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___ndof_18)); }
	inline int32_t get_ndof_18() const { return ___ndof_18; }
	inline int32_t* get_address_of_ndof_18() { return &___ndof_18; }
	inline void set_ndof_18(int32_t value)
	{
		___ndof_18 = value;
	}

	inline static int32_t get_offset_of_nelem_19() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nelem_19)); }
	inline int32_t get_nelem_19() const { return ___nelem_19; }
	inline int32_t* get_address_of_nelem_19() { return &___nelem_19; }
	inline void set_nelem_19(int32_t value)
	{
		___nelem_19 = value;
	}

	inline static int32_t get_offset_of_itercount_20() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___itercount_20)); }
	inline int32_t get_itercount_20() const { return ___itercount_20; }
	inline int32_t* get_address_of_itercount_20() { return &___itercount_20; }
	inline void set_itercount_20(int32_t value)
	{
		___itercount_20 = value;
	}

	inline static int32_t get_offset_of_f_21() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___f_21)); }
	inline float get_f_21() const { return ___f_21; }
	inline float* get_address_of_f_21() { return &___f_21; }
	inline void set_f_21(float value)
	{
		___f_21 = value;
	}

	inline static int32_t get_offset_of_Element_materials_22() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___Element_materials_22)); }
	inline Int32U5BU5D_t385246372* get_Element_materials_22() const { return ___Element_materials_22; }
	inline Int32U5BU5D_t385246372** get_address_of_Element_materials_22() { return &___Element_materials_22; }
	inline void set_Element_materials_22(Int32U5BU5D_t385246372* value)
	{
		___Element_materials_22 = value;
		Il2CppCodeGenWriteBarrier((&___Element_materials_22), value);
	}

	inline static int32_t get_offset_of_Element_topology_23() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___Element_topology_23)); }
	inline BooleanU5BU5D_t2897418192* get_Element_topology_23() const { return ___Element_topology_23; }
	inline BooleanU5BU5D_t2897418192** get_address_of_Element_topology_23() { return &___Element_topology_23; }
	inline void set_Element_topology_23(BooleanU5BU5D_t2897418192* value)
	{
		___Element_topology_23 = value;
		Il2CppCodeGenWriteBarrier((&___Element_topology_23), value);
	}

	inline static int32_t get_offset_of_rhoPhysVal_24() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___rhoPhysVal_24)); }
	inline SingleU5BU5D_t1444911251* get_rhoPhysVal_24() const { return ___rhoPhysVal_24; }
	inline SingleU5BU5D_t1444911251** get_address_of_rhoPhysVal_24() { return &___rhoPhysVal_24; }
	inline void set_rhoPhysVal_24(SingleU5BU5D_t1444911251* value)
	{
		___rhoPhysVal_24 = value;
		Il2CppCodeGenWriteBarrier((&___rhoPhysVal_24), value);
	}

	inline static int32_t get_offset_of_nodeVal_25() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nodeVal_25)); }
	inline SingleU5BU5D_t1444911251* get_nodeVal_25() const { return ___nodeVal_25; }
	inline SingleU5BU5D_t1444911251** get_address_of_nodeVal_25() { return &___nodeVal_25; }
	inline void set_nodeVal_25(SingleU5BU5D_t1444911251* value)
	{
		___nodeVal_25 = value;
		Il2CppCodeGenWriteBarrier((&___nodeVal_25), value);
	}

	inline static int32_t get_offset_of_nElPrNode_26() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nElPrNode_26)); }
	inline Int32U5BU5D_t385246372* get_nElPrNode_26() const { return ___nElPrNode_26; }
	inline Int32U5BU5D_t385246372** get_address_of_nElPrNode_26() { return &___nElPrNode_26; }
	inline void set_nElPrNode_26(Int32U5BU5D_t385246372* value)
	{
		___nElPrNode_26 = value;
		Il2CppCodeGenWriteBarrier((&___nElPrNode_26), value);
	}

	inline static int32_t get_offset_of_bMat_27() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___bMat_27)); }
	inline SingleU5BU5D_t1444911251* get_bMat_27() const { return ___bMat_27; }
	inline SingleU5BU5D_t1444911251** get_address_of_bMat_27() { return &___bMat_27; }
	inline void set_bMat_27(SingleU5BU5D_t1444911251* value)
	{
		___bMat_27 = value;
		Il2CppCodeGenWriteBarrier((&___bMat_27), value);
	}

	inline static int32_t get_offset_of_cMat_28() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___cMat_28)); }
	inline SingleU5BU5D_t1444911251* get_cMat_28() const { return ___cMat_28; }
	inline SingleU5BU5D_t1444911251** get_address_of_cMat_28() { return &___cMat_28; }
	inline void set_cMat_28(SingleU5BU5D_t1444911251* value)
	{
		___cMat_28 = value;
		Il2CppCodeGenWriteBarrier((&___cMat_28), value);
	}

	inline static int32_t get_offset_of_u_29() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___u_29)); }
	inline SingleU5BU5D_t1444911251* get_u_29() const { return ___u_29; }
	inline SingleU5BU5D_t1444911251** get_address_of_u_29() { return &___u_29; }
	inline void set_u_29(SingleU5BU5D_t1444911251* value)
	{
		___u_29 = value;
		Il2CppCodeGenWriteBarrier((&___u_29), value);
	}

	inline static int32_t get_offset_of_F_30() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___F_30)); }
	inline SingleU5BU5D_t1444911251* get_F_30() const { return ___F_30; }
	inline SingleU5BU5D_t1444911251** get_address_of_F_30() { return &___F_30; }
	inline void set_F_30(SingleU5BU5D_t1444911251* value)
	{
		___F_30 = value;
		Il2CppCodeGenWriteBarrier((&___F_30), value);
	}

	inline static int32_t get_offset_of_KE_1_31() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___KE_1_31)); }
	inline SingleU5BU5D_t1444911251* get_KE_1_31() const { return ___KE_1_31; }
	inline SingleU5BU5D_t1444911251** get_address_of_KE_1_31() { return &___KE_1_31; }
	inline void set_KE_1_31(SingleU5BU5D_t1444911251* value)
	{
		___KE_1_31 = value;
		Il2CppCodeGenWriteBarrier((&___KE_1_31), value);
	}

	inline static int32_t get_offset_of_KE_2_32() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___KE_2_32)); }
	inline SingleU5BU5D_t1444911251* get_KE_2_32() const { return ___KE_2_32; }
	inline SingleU5BU5D_t1444911251** get_address_of_KE_2_32() { return &___KE_2_32; }
	inline void set_KE_2_32(SingleU5BU5D_t1444911251* value)
	{
		___KE_2_32 = value;
		Il2CppCodeGenWriteBarrier((&___KE_2_32), value);
	}

	inline static int32_t get_offset_of_reducedKK_33() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___reducedKK_33)); }
	inline bool get_reducedKK_33() const { return ___reducedKK_33; }
	inline bool* get_address_of_reducedKK_33() { return &___reducedKK_33; }
	inline void set_reducedKK_33(bool value)
	{
		___reducedKK_33 = value;
	}

	inline static int32_t get_offset_of_IsNewTopology_34() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___IsNewTopology_34)); }
	inline bool get_IsNewTopology_34() const { return ___IsNewTopology_34; }
	inline bool* get_address_of_IsNewTopology_34() { return &___IsNewTopology_34; }
	inline void set_IsNewTopology_34(bool value)
	{
		___IsNewTopology_34 = value;
	}

	inline static int32_t get_offset_of_KK_35() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___KK_35)); }
	inline MatrixCCS_t3519621953 * get_KK_35() const { return ___KK_35; }
	inline MatrixCCS_t3519621953 ** get_address_of_KK_35() { return &___KK_35; }
	inline void set_KK_35(MatrixCCS_t3519621953 * value)
	{
		___KK_35 = value;
		Il2CppCodeGenWriteBarrier((&___KK_35), value);
	}

	inline static int32_t get_offset_of_LDL_36() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___LDL_36)); }
	inline SparseLDL_t1701177157 * get_LDL_36() const { return ___LDL_36; }
	inline SparseLDL_t1701177157 ** get_address_of_LDL_36() { return &___LDL_36; }
	inline void set_LDL_36(SparseLDL_t1701177157 * value)
	{
		___LDL_36 = value;
		Il2CppCodeGenWriteBarrier((&___LDL_36), value);
	}

	inline static int32_t get_offset_of_FEA_Compress_37() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___FEA_Compress_37)); }
	inline FEAMatrixCompress_t1508710683 * get_FEA_Compress_37() const { return ___FEA_Compress_37; }
	inline FEAMatrixCompress_t1508710683 ** get_address_of_FEA_Compress_37() { return &___FEA_Compress_37; }
	inline void set_FEA_Compress_37(FEAMatrixCompress_t1508710683 * value)
	{
		___FEA_Compress_37 = value;
		Il2CppCodeGenWriteBarrier((&___FEA_Compress_37), value);
	}

	inline static int32_t get_offset_of_Reduced_ndof_38() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___Reduced_ndof_38)); }
	inline int32_t get_Reduced_ndof_38() const { return ___Reduced_ndof_38; }
	inline int32_t* get_address_of_Reduced_ndof_38() { return &___Reduced_ndof_38; }
	inline void set_Reduced_ndof_38(int32_t value)
	{
		___Reduced_ndof_38 = value;
	}

	inline static int32_t get_offset_of_edof_reductor_39() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___edof_reductor_39)); }
	inline Int32U5BU5D_t385246372* get_edof_reductor_39() const { return ___edof_reductor_39; }
	inline Int32U5BU5D_t385246372** get_address_of_edof_reductor_39() { return &___edof_reductor_39; }
	inline void set_edof_reductor_39(Int32U5BU5D_t385246372* value)
	{
		___edof_reductor_39 = value;
		Il2CppCodeGenWriteBarrier((&___edof_reductor_39), value);
	}

	inline static int32_t get_offset_of_edof_expander_40() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___edof_expander_40)); }
	inline Int32U5BU5D_t385246372* get_edof_expander_40() const { return ___edof_expander_40; }
	inline Int32U5BU5D_t385246372** get_address_of_edof_expander_40() { return &___edof_expander_40; }
	inline void set_edof_expander_40(Int32U5BU5D_t385246372* value)
	{
		___edof_expander_40 = value;
		Il2CppCodeGenWriteBarrier((&___edof_expander_40), value);
	}

	inline static int32_t get_offset_of_u_reduced_41() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___u_reduced_41)); }
	inline SingleU5BU5D_t1444911251* get_u_reduced_41() const { return ___u_reduced_41; }
	inline SingleU5BU5D_t1444911251** get_address_of_u_reduced_41() { return &___u_reduced_41; }
	inline void set_u_reduced_41(SingleU5BU5D_t1444911251* value)
	{
		___u_reduced_41 = value;
		Il2CppCodeGenWriteBarrier((&___u_reduced_41), value);
	}

	inline static int32_t get_offset_of_F_reduced_42() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___F_reduced_42)); }
	inline SingleU5BU5D_t1444911251* get_F_reduced_42() const { return ___F_reduced_42; }
	inline SingleU5BU5D_t1444911251** get_address_of_F_reduced_42() { return &___F_reduced_42; }
	inline void set_F_reduced_42(SingleU5BU5D_t1444911251* value)
	{
		___F_reduced_42 = value;
		Il2CppCodeGenWriteBarrier((&___F_reduced_42), value);
	}

	inline static int32_t get_offset_of_E1_43() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___E1_43)); }
	inline float get_E1_43() const { return ___E1_43; }
	inline float* get_address_of_E1_43() { return &___E1_43; }
	inline void set_E1_43(float value)
	{
		___E1_43 = value;
	}

	inline static int32_t get_offset_of_E2_44() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___E2_44)); }
	inline float get_E2_44() const { return ___E2_44; }
	inline float* get_address_of_E2_44() { return &___E2_44; }
	inline void set_E2_44(float value)
	{
		___E2_44 = value;
	}

	inline static int32_t get_offset_of_nu1_45() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nu1_45)); }
	inline float get_nu1_45() const { return ___nu1_45; }
	inline float* get_address_of_nu1_45() { return &___nu1_45; }
	inline void set_nu1_45(float value)
	{
		___nu1_45 = value;
	}

	inline static int32_t get_offset_of_nu2_46() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___nu2_46)); }
	inline float get_nu2_46() const { return ___nu2_46; }
	inline float* get_address_of_nu2_46() { return &___nu2_46; }
	inline void set_nu2_46(float value)
	{
		___nu2_46 = value;
	}

	inline static int32_t get_offset_of_loop_47() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___loop_47)); }
	inline uint32_t get_loop_47() const { return ___loop_47; }
	inline uint32_t* get_address_of_loop_47() { return &___loop_47; }
	inline void set_loop_47(uint32_t value)
	{
		___loop_47 = value;
	}

	inline static int32_t get_offset_of_plotChoice_48() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___plotChoice_48)); }
	inline int32_t get_plotChoice_48() const { return ___plotChoice_48; }
	inline int32_t* get_address_of_plotChoice_48() { return &___plotChoice_48; }
	inline void set_plotChoice_48(int32_t value)
	{
		___plotChoice_48 = value;
	}

	inline static int32_t get_offset_of_big_finger_49() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___big_finger_49)); }
	inline bool get_big_finger_49() const { return ___big_finger_49; }
	inline bool* get_address_of_big_finger_49() { return &___big_finger_49; }
	inline void set_big_finger_49(bool value)
	{
		___big_finger_49 = value;
	}

	inline static int32_t get_offset_of_ortho_move_50() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___ortho_move_50)); }
	inline bool get_ortho_move_50() const { return ___ortho_move_50; }
	inline bool* get_address_of_ortho_move_50() { return &___ortho_move_50; }
	inline void set_ortho_move_50(bool value)
	{
		___ortho_move_50 = value;
	}

	inline static int32_t get_offset_of_turnOffSolver_51() { return static_cast<int32_t>(offsetof(FEA_t3250963177, ___turnOffSolver_51)); }
	inline bool get_turnOffSolver_51() const { return ___turnOffSolver_51; }
	inline bool* get_address_of_turnOffSolver_51() { return &___turnOffSolver_51; }
	inline void set_turnOffSolver_51(bool value)
	{
		___turnOffSolver_51 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEA_T3250963177_H
#ifndef SELECTIONDISTIBUTESTATE_T399974760_H
#define SELECTIONDISTIBUTESTATE_T399974760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectionDistibuteState
struct  SelectionDistibuteState_t399974760  : public SelectionStateAbstract_t1927371580
{
public:
	// TopologySelector SelectionDistibuteState::selector
	TopologySelector_t1922582608 * ___selector_5;

public:
	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(SelectionDistibuteState_t399974760, ___selector_5)); }
	inline TopologySelector_t1922582608 * get_selector_5() const { return ___selector_5; }
	inline TopologySelector_t1922582608 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(TopologySelector_t1922582608 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((&___selector_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONDISTIBUTESTATE_T399974760_H
#ifndef UPDATETOOL_T905929733_H
#define UPDATETOOL_T905929733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIPaintHandler/UpdateTool
struct  UpdateTool_t905929733  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETOOL_T905929733_H
#ifndef NOTIFYUPDATEOBJECTIVEFUNCTION_T2430789460_H
#define NOTIFYUPDATEOBJECTIVEFUNCTION_T2430789460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologyOptimization/NotifyUpdateObjectiveFunction
struct  NotifyUpdateObjectiveFunction_t2430789460  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYUPDATEOBJECTIVEFUNCTION_T2430789460_H
#ifndef GETUPDATEDTOPOLOGYELEMENTDENSITY_T3009975164_H
#define GETUPDATEDTOPOLOGYELEMENTDENSITY_T3009975164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologyOptimization/GetUpdatedTopologyElementDensity
struct  GetUpdatedTopologyElementDensity_t3009975164  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETUPDATEDTOPOLOGYELEMENTDENSITY_T3009975164_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef NOTIFYEVALUATIONUPDATE_T2480692004_H
#define NOTIFYEVALUATIONUPDATE_T2480692004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/NotifyEvaluationUpdate
struct  NotifyEvaluationUpdate_t2480692004  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYEVALUATIONUPDATE_T2480692004_H
#ifndef SENDNEXTTIMESTEP_T1962693451_H
#define SENDNEXTTIMESTEP_T1962693451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FEAVibrate/SendNextTimestep
struct  SendNextTimestep_t1962693451  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDNEXTTIMESTEP_T1962693451_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef FONTCHANGE_T2675305062_H
#define FONTCHANGE_T2675305062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FontChange
struct  FontChange_t2675305062  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin[] FontChange::guiSkins
	GUISkinU5BU5D_t4017239199* ___guiSkins_2;
	// UnityEngine.Font FontChange::fontSmall
	Font_t1956802104 * ___fontSmall_3;
	// UnityEngine.Font FontChange::fontMedium
	Font_t1956802104 * ___fontMedium_4;
	// UnityEngine.Font FontChange::fontLarge
	Font_t1956802104 * ___fontLarge_5;

public:
	inline static int32_t get_offset_of_guiSkins_2() { return static_cast<int32_t>(offsetof(FontChange_t2675305062, ___guiSkins_2)); }
	inline GUISkinU5BU5D_t4017239199* get_guiSkins_2() const { return ___guiSkins_2; }
	inline GUISkinU5BU5D_t4017239199** get_address_of_guiSkins_2() { return &___guiSkins_2; }
	inline void set_guiSkins_2(GUISkinU5BU5D_t4017239199* value)
	{
		___guiSkins_2 = value;
		Il2CppCodeGenWriteBarrier((&___guiSkins_2), value);
	}

	inline static int32_t get_offset_of_fontSmall_3() { return static_cast<int32_t>(offsetof(FontChange_t2675305062, ___fontSmall_3)); }
	inline Font_t1956802104 * get_fontSmall_3() const { return ___fontSmall_3; }
	inline Font_t1956802104 ** get_address_of_fontSmall_3() { return &___fontSmall_3; }
	inline void set_fontSmall_3(Font_t1956802104 * value)
	{
		___fontSmall_3 = value;
		Il2CppCodeGenWriteBarrier((&___fontSmall_3), value);
	}

	inline static int32_t get_offset_of_fontMedium_4() { return static_cast<int32_t>(offsetof(FontChange_t2675305062, ___fontMedium_4)); }
	inline Font_t1956802104 * get_fontMedium_4() const { return ___fontMedium_4; }
	inline Font_t1956802104 ** get_address_of_fontMedium_4() { return &___fontMedium_4; }
	inline void set_fontMedium_4(Font_t1956802104 * value)
	{
		___fontMedium_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontMedium_4), value);
	}

	inline static int32_t get_offset_of_fontLarge_5() { return static_cast<int32_t>(offsetof(FontChange_t2675305062, ___fontLarge_5)); }
	inline Font_t1956802104 * get_fontLarge_5() const { return ___fontLarge_5; }
	inline Font_t1956802104 ** get_address_of_fontLarge_5() { return &___fontLarge_5; }
	inline void set_fontLarge_5(Font_t1956802104 * value)
	{
		___fontLarge_5 = value;
		Il2CppCodeGenWriteBarrier((&___fontLarge_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTCHANGE_T2675305062_H
#ifndef TOPOLOGYCOMPONENT_T4181154066_H
#define TOPOLOGYCOMPONENT_T4181154066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologyComponent
struct  TopologyComponent_t4181154066  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material TopologyComponent::material
	Material_t340375123 * ___material_2;
	// UnityEngine.Color TopologyComponent::initialColor
	Color_t2555686324  ___initialColor_3;
	// UnityEngine.Color TopologyComponent::defaultColor
	Color_t2555686324  ___defaultColor_4;
	// TopologyComponent/TopologyComponentType TopologyComponent::topologyComponentType
	int32_t ___topologyComponentType_6;
	// System.Int32 TopologyComponent::loadId
	int32_t ___loadId_7;
	// System.Int32 TopologyComponent::displNo
	int32_t ___displNo_8;
	// LevelComponentSupport TopologyComponent::support
	LevelComponentSupport_t2849120049 * ___support_9;
	// LevelComponentDispl TopologyComponent::displ
	LevelComponentDispl_t1267792563 * ___displ_10;
	// LevelComponentDisplEnd TopologyComponent::displEnd
	LevelComponentDisplEnd_t436353678 * ___displEnd_11;
	// LevelComponentDispl2 TopologyComponent::displ2
	LevelComponentDispl2_t1471609523 * ___displ2_12;
	// LevelComponentDisplEnd2 TopologyComponent::displEnd2
	LevelComponentDisplEnd2_t2219483174 * ___displEnd2_13;
	// System.Single TopologyComponent::hatchScale
	float ___hatchScale_14;
	// UnityEngine.Mesh TopologyComponent::hatchingMesh
	Mesh_t3648964284 * ___hatchingMesh_15;
	// UnityEngine.GameObject TopologyComponent::hatchingObject
	GameObject_t1113636619 * ___hatchingObject_16;
	// UnityEngine.Material TopologyComponent::hatchingMaterial
	Material_t340375123 * ___hatchingMaterial_17;
	// UnityEngine.Mesh TopologyComponent::lineObject
	Mesh_t3648964284 * ___lineObject_18;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TopologyComponent::lineGameObjects
	List_1_t2585711361 * ___lineGameObjects_19;
	// UnityEngine.Mesh TopologyComponent::selectorComponent
	Mesh_t3648964284 * ___selectorComponent_20;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TopologyComponent::selectorComponents
	List_1_t2585711361 * ___selectorComponents_21;
	// PainterCanvas TopologyComponent::textureUpdater
	PainterCanvas_t4160673718 * ___textureUpdater_22;
	// System.Single TopologyComponent::componentSizeValue
	float ___componentSizeValue_23;
	// UnityEngine.Vector2 TopologyComponent::scaleValue
	Vector2_t2156229523  ___scaleValue_24;
	// System.Single TopologyComponent::forceMagnitude
	float ___forceMagnitude_25;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_initialColor_3() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___initialColor_3)); }
	inline Color_t2555686324  get_initialColor_3() const { return ___initialColor_3; }
	inline Color_t2555686324 * get_address_of_initialColor_3() { return &___initialColor_3; }
	inline void set_initialColor_3(Color_t2555686324  value)
	{
		___initialColor_3 = value;
	}

	inline static int32_t get_offset_of_defaultColor_4() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___defaultColor_4)); }
	inline Color_t2555686324  get_defaultColor_4() const { return ___defaultColor_4; }
	inline Color_t2555686324 * get_address_of_defaultColor_4() { return &___defaultColor_4; }
	inline void set_defaultColor_4(Color_t2555686324  value)
	{
		___defaultColor_4 = value;
	}

	inline static int32_t get_offset_of_topologyComponentType_6() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___topologyComponentType_6)); }
	inline int32_t get_topologyComponentType_6() const { return ___topologyComponentType_6; }
	inline int32_t* get_address_of_topologyComponentType_6() { return &___topologyComponentType_6; }
	inline void set_topologyComponentType_6(int32_t value)
	{
		___topologyComponentType_6 = value;
	}

	inline static int32_t get_offset_of_loadId_7() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___loadId_7)); }
	inline int32_t get_loadId_7() const { return ___loadId_7; }
	inline int32_t* get_address_of_loadId_7() { return &___loadId_7; }
	inline void set_loadId_7(int32_t value)
	{
		___loadId_7 = value;
	}

	inline static int32_t get_offset_of_displNo_8() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___displNo_8)); }
	inline int32_t get_displNo_8() const { return ___displNo_8; }
	inline int32_t* get_address_of_displNo_8() { return &___displNo_8; }
	inline void set_displNo_8(int32_t value)
	{
		___displNo_8 = value;
	}

	inline static int32_t get_offset_of_support_9() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___support_9)); }
	inline LevelComponentSupport_t2849120049 * get_support_9() const { return ___support_9; }
	inline LevelComponentSupport_t2849120049 ** get_address_of_support_9() { return &___support_9; }
	inline void set_support_9(LevelComponentSupport_t2849120049 * value)
	{
		___support_9 = value;
		Il2CppCodeGenWriteBarrier((&___support_9), value);
	}

	inline static int32_t get_offset_of_displ_10() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___displ_10)); }
	inline LevelComponentDispl_t1267792563 * get_displ_10() const { return ___displ_10; }
	inline LevelComponentDispl_t1267792563 ** get_address_of_displ_10() { return &___displ_10; }
	inline void set_displ_10(LevelComponentDispl_t1267792563 * value)
	{
		___displ_10 = value;
		Il2CppCodeGenWriteBarrier((&___displ_10), value);
	}

	inline static int32_t get_offset_of_displEnd_11() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___displEnd_11)); }
	inline LevelComponentDisplEnd_t436353678 * get_displEnd_11() const { return ___displEnd_11; }
	inline LevelComponentDisplEnd_t436353678 ** get_address_of_displEnd_11() { return &___displEnd_11; }
	inline void set_displEnd_11(LevelComponentDisplEnd_t436353678 * value)
	{
		___displEnd_11 = value;
		Il2CppCodeGenWriteBarrier((&___displEnd_11), value);
	}

	inline static int32_t get_offset_of_displ2_12() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___displ2_12)); }
	inline LevelComponentDispl2_t1471609523 * get_displ2_12() const { return ___displ2_12; }
	inline LevelComponentDispl2_t1471609523 ** get_address_of_displ2_12() { return &___displ2_12; }
	inline void set_displ2_12(LevelComponentDispl2_t1471609523 * value)
	{
		___displ2_12 = value;
		Il2CppCodeGenWriteBarrier((&___displ2_12), value);
	}

	inline static int32_t get_offset_of_displEnd2_13() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___displEnd2_13)); }
	inline LevelComponentDisplEnd2_t2219483174 * get_displEnd2_13() const { return ___displEnd2_13; }
	inline LevelComponentDisplEnd2_t2219483174 ** get_address_of_displEnd2_13() { return &___displEnd2_13; }
	inline void set_displEnd2_13(LevelComponentDisplEnd2_t2219483174 * value)
	{
		___displEnd2_13 = value;
		Il2CppCodeGenWriteBarrier((&___displEnd2_13), value);
	}

	inline static int32_t get_offset_of_hatchScale_14() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___hatchScale_14)); }
	inline float get_hatchScale_14() const { return ___hatchScale_14; }
	inline float* get_address_of_hatchScale_14() { return &___hatchScale_14; }
	inline void set_hatchScale_14(float value)
	{
		___hatchScale_14 = value;
	}

	inline static int32_t get_offset_of_hatchingMesh_15() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___hatchingMesh_15)); }
	inline Mesh_t3648964284 * get_hatchingMesh_15() const { return ___hatchingMesh_15; }
	inline Mesh_t3648964284 ** get_address_of_hatchingMesh_15() { return &___hatchingMesh_15; }
	inline void set_hatchingMesh_15(Mesh_t3648964284 * value)
	{
		___hatchingMesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___hatchingMesh_15), value);
	}

	inline static int32_t get_offset_of_hatchingObject_16() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___hatchingObject_16)); }
	inline GameObject_t1113636619 * get_hatchingObject_16() const { return ___hatchingObject_16; }
	inline GameObject_t1113636619 ** get_address_of_hatchingObject_16() { return &___hatchingObject_16; }
	inline void set_hatchingObject_16(GameObject_t1113636619 * value)
	{
		___hatchingObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___hatchingObject_16), value);
	}

	inline static int32_t get_offset_of_hatchingMaterial_17() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___hatchingMaterial_17)); }
	inline Material_t340375123 * get_hatchingMaterial_17() const { return ___hatchingMaterial_17; }
	inline Material_t340375123 ** get_address_of_hatchingMaterial_17() { return &___hatchingMaterial_17; }
	inline void set_hatchingMaterial_17(Material_t340375123 * value)
	{
		___hatchingMaterial_17 = value;
		Il2CppCodeGenWriteBarrier((&___hatchingMaterial_17), value);
	}

	inline static int32_t get_offset_of_lineObject_18() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___lineObject_18)); }
	inline Mesh_t3648964284 * get_lineObject_18() const { return ___lineObject_18; }
	inline Mesh_t3648964284 ** get_address_of_lineObject_18() { return &___lineObject_18; }
	inline void set_lineObject_18(Mesh_t3648964284 * value)
	{
		___lineObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___lineObject_18), value);
	}

	inline static int32_t get_offset_of_lineGameObjects_19() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___lineGameObjects_19)); }
	inline List_1_t2585711361 * get_lineGameObjects_19() const { return ___lineGameObjects_19; }
	inline List_1_t2585711361 ** get_address_of_lineGameObjects_19() { return &___lineGameObjects_19; }
	inline void set_lineGameObjects_19(List_1_t2585711361 * value)
	{
		___lineGameObjects_19 = value;
		Il2CppCodeGenWriteBarrier((&___lineGameObjects_19), value);
	}

	inline static int32_t get_offset_of_selectorComponent_20() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___selectorComponent_20)); }
	inline Mesh_t3648964284 * get_selectorComponent_20() const { return ___selectorComponent_20; }
	inline Mesh_t3648964284 ** get_address_of_selectorComponent_20() { return &___selectorComponent_20; }
	inline void set_selectorComponent_20(Mesh_t3648964284 * value)
	{
		___selectorComponent_20 = value;
		Il2CppCodeGenWriteBarrier((&___selectorComponent_20), value);
	}

	inline static int32_t get_offset_of_selectorComponents_21() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___selectorComponents_21)); }
	inline List_1_t2585711361 * get_selectorComponents_21() const { return ___selectorComponents_21; }
	inline List_1_t2585711361 ** get_address_of_selectorComponents_21() { return &___selectorComponents_21; }
	inline void set_selectorComponents_21(List_1_t2585711361 * value)
	{
		___selectorComponents_21 = value;
		Il2CppCodeGenWriteBarrier((&___selectorComponents_21), value);
	}

	inline static int32_t get_offset_of_textureUpdater_22() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___textureUpdater_22)); }
	inline PainterCanvas_t4160673718 * get_textureUpdater_22() const { return ___textureUpdater_22; }
	inline PainterCanvas_t4160673718 ** get_address_of_textureUpdater_22() { return &___textureUpdater_22; }
	inline void set_textureUpdater_22(PainterCanvas_t4160673718 * value)
	{
		___textureUpdater_22 = value;
		Il2CppCodeGenWriteBarrier((&___textureUpdater_22), value);
	}

	inline static int32_t get_offset_of_componentSizeValue_23() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___componentSizeValue_23)); }
	inline float get_componentSizeValue_23() const { return ___componentSizeValue_23; }
	inline float* get_address_of_componentSizeValue_23() { return &___componentSizeValue_23; }
	inline void set_componentSizeValue_23(float value)
	{
		___componentSizeValue_23 = value;
	}

	inline static int32_t get_offset_of_scaleValue_24() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___scaleValue_24)); }
	inline Vector2_t2156229523  get_scaleValue_24() const { return ___scaleValue_24; }
	inline Vector2_t2156229523 * get_address_of_scaleValue_24() { return &___scaleValue_24; }
	inline void set_scaleValue_24(Vector2_t2156229523  value)
	{
		___scaleValue_24 = value;
	}

	inline static int32_t get_offset_of_forceMagnitude_25() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066, ___forceMagnitude_25)); }
	inline float get_forceMagnitude_25() const { return ___forceMagnitude_25; }
	inline float* get_address_of_forceMagnitude_25() { return &___forceMagnitude_25; }
	inline void set_forceMagnitude_25(float value)
	{
		___forceMagnitude_25 = value;
	}
};

struct TopologyComponent_t4181154066_StaticFields
{
public:
	// UnityEngine.Color TopologyComponent::selectedColor
	Color_t2555686324  ___selectedColor_5;

public:
	inline static int32_t get_offset_of_selectedColor_5() { return static_cast<int32_t>(offsetof(TopologyComponent_t4181154066_StaticFields, ___selectedColor_5)); }
	inline Color_t2555686324  get_selectedColor_5() const { return ___selectedColor_5; }
	inline Color_t2555686324 * get_address_of_selectedColor_5() { return &___selectedColor_5; }
	inline void set_selectedColor_5(Color_t2555686324  value)
	{
		___selectedColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPOLOGYCOMPONENT_T4181154066_H
#ifndef PAINTERCANVAS_T4160673718_H
#define PAINTERCANVAS_T4160673718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PainterCanvas
struct  PainterCanvas_t4160673718  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PainterCanvas::width
	int32_t ___width_2;
	// System.Int32 PainterCanvas::height
	int32_t ___height_3;
	// UnityEngine.Texture2D PainterCanvas::texture
	Texture2D_t3840446185 * ___texture_4;
	// UnityEngine.Texture2D PainterCanvas::textureStress
	Texture2D_t3840446185 * ___textureStress_5;
	// PassiveBlock PainterCanvas::passiveBlock
	PassiveBlock_t513448541 * ___passiveBlock_6;
	// System.Int32 PainterCanvas::textureWidth
	int32_t ___textureWidth_7;
	// System.Int32 PainterCanvas::textureHeight
	int32_t ___textureHeight_8;
	// GameController PainterCanvas::gameController
	GameController_t2330501625 * ___gameController_9;
	// System.Single PainterCanvas::updateTime
	float ___updateTime_10;
	// System.Int32 PainterCanvas::updates
	int32_t ___updates_11;
	// System.Single PainterCanvas::updateTimeTotal
	float ___updateTimeTotal_12;
	// System.Int32 PainterCanvas::ndopt
	int32_t ___ndopt_13;
	// System.Int32 PainterCanvas::ndvis
	int32_t ___ndvis_14;
	// LinePainter`1<PassiveBlock/ElementType> PainterCanvas::linePainter
	LinePainter_1_t2650701617 * ___linePainter_15;

public:
	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___texture_4)); }
	inline Texture2D_t3840446185 * get_texture_4() const { return ___texture_4; }
	inline Texture2D_t3840446185 ** get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(Texture2D_t3840446185 * value)
	{
		___texture_4 = value;
		Il2CppCodeGenWriteBarrier((&___texture_4), value);
	}

	inline static int32_t get_offset_of_textureStress_5() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___textureStress_5)); }
	inline Texture2D_t3840446185 * get_textureStress_5() const { return ___textureStress_5; }
	inline Texture2D_t3840446185 ** get_address_of_textureStress_5() { return &___textureStress_5; }
	inline void set_textureStress_5(Texture2D_t3840446185 * value)
	{
		___textureStress_5 = value;
		Il2CppCodeGenWriteBarrier((&___textureStress_5), value);
	}

	inline static int32_t get_offset_of_passiveBlock_6() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___passiveBlock_6)); }
	inline PassiveBlock_t513448541 * get_passiveBlock_6() const { return ___passiveBlock_6; }
	inline PassiveBlock_t513448541 ** get_address_of_passiveBlock_6() { return &___passiveBlock_6; }
	inline void set_passiveBlock_6(PassiveBlock_t513448541 * value)
	{
		___passiveBlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___passiveBlock_6), value);
	}

	inline static int32_t get_offset_of_textureWidth_7() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___textureWidth_7)); }
	inline int32_t get_textureWidth_7() const { return ___textureWidth_7; }
	inline int32_t* get_address_of_textureWidth_7() { return &___textureWidth_7; }
	inline void set_textureWidth_7(int32_t value)
	{
		___textureWidth_7 = value;
	}

	inline static int32_t get_offset_of_textureHeight_8() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___textureHeight_8)); }
	inline int32_t get_textureHeight_8() const { return ___textureHeight_8; }
	inline int32_t* get_address_of_textureHeight_8() { return &___textureHeight_8; }
	inline void set_textureHeight_8(int32_t value)
	{
		___textureHeight_8 = value;
	}

	inline static int32_t get_offset_of_gameController_9() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___gameController_9)); }
	inline GameController_t2330501625 * get_gameController_9() const { return ___gameController_9; }
	inline GameController_t2330501625 ** get_address_of_gameController_9() { return &___gameController_9; }
	inline void set_gameController_9(GameController_t2330501625 * value)
	{
		___gameController_9 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_9), value);
	}

	inline static int32_t get_offset_of_updateTime_10() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___updateTime_10)); }
	inline float get_updateTime_10() const { return ___updateTime_10; }
	inline float* get_address_of_updateTime_10() { return &___updateTime_10; }
	inline void set_updateTime_10(float value)
	{
		___updateTime_10 = value;
	}

	inline static int32_t get_offset_of_updates_11() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___updates_11)); }
	inline int32_t get_updates_11() const { return ___updates_11; }
	inline int32_t* get_address_of_updates_11() { return &___updates_11; }
	inline void set_updates_11(int32_t value)
	{
		___updates_11 = value;
	}

	inline static int32_t get_offset_of_updateTimeTotal_12() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___updateTimeTotal_12)); }
	inline float get_updateTimeTotal_12() const { return ___updateTimeTotal_12; }
	inline float* get_address_of_updateTimeTotal_12() { return &___updateTimeTotal_12; }
	inline void set_updateTimeTotal_12(float value)
	{
		___updateTimeTotal_12 = value;
	}

	inline static int32_t get_offset_of_ndopt_13() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___ndopt_13)); }
	inline int32_t get_ndopt_13() const { return ___ndopt_13; }
	inline int32_t* get_address_of_ndopt_13() { return &___ndopt_13; }
	inline void set_ndopt_13(int32_t value)
	{
		___ndopt_13 = value;
	}

	inline static int32_t get_offset_of_ndvis_14() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___ndvis_14)); }
	inline int32_t get_ndvis_14() const { return ___ndvis_14; }
	inline int32_t* get_address_of_ndvis_14() { return &___ndvis_14; }
	inline void set_ndvis_14(int32_t value)
	{
		___ndvis_14 = value;
	}

	inline static int32_t get_offset_of_linePainter_15() { return static_cast<int32_t>(offsetof(PainterCanvas_t4160673718, ___linePainter_15)); }
	inline LinePainter_1_t2650701617 * get_linePainter_15() const { return ___linePainter_15; }
	inline LinePainter_1_t2650701617 ** get_address_of_linePainter_15() { return &___linePainter_15; }
	inline void set_linePainter_15(LinePainter_1_t2650701617 * value)
	{
		___linePainter_15 = value;
		Il2CppCodeGenWriteBarrier((&___linePainter_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAINTERCANVAS_T4160673718_H
#ifndef GUIPAINTHANDLER_T437174526_H
#define GUIPAINTHANDLER_T437174526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIPaintHandler
struct  GUIPaintHandler_t437174526  : public MonoBehaviour_t3962482529
{
public:
	// System.EventHandler GUIPaintHandler::NewTopology
	EventHandler_t1348719766 * ___NewTopology_2;
	// System.Int32 GUIPaintHandler::PenWidth
	int32_t ___PenWidth_3;
	// GUIPaintHandler/UpdateTool GUIPaintHandler::updateTool
	UpdateTool_t905929733 * ___updateTool_4;
	// PainterCanvas GUIPaintHandler::painterCanvas
	PainterCanvas_t4160673718 * ___painterCanvas_5;
	// GameController GUIPaintHandler::gameController
	GameController_t2330501625 * ___gameController_6;
	// MiniGUI GUIPaintHandler::miniGui
	MiniGUI_t213183967 * ___miniGui_7;
	// Vec2i GUIPaintHandler::lastPoint
	Vec2i_t1193278988  ___lastPoint_8;
	// System.Boolean GUIPaintHandler::isPainting
	bool ___isPainting_9;
	// System.Single GUIPaintHandler::material
	float ___material_10;
	// PassiveBlock/ElementType GUIPaintHandler::materialType
	int32_t ___materialType_11;
	// GUIPaintHandler/Tool GUIPaintHandler::selectedTool
	int32_t ___selectedTool_12;
	// GUIPaintHandler/Brush GUIPaintHandler::selectedPainter
	int32_t ___selectedPainter_13;
	// ThinThickTool GUIPaintHandler::thinThickTool
	ThinThickTool_t2872687508 * ___thinThickTool_14;

public:
	inline static int32_t get_offset_of_NewTopology_2() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___NewTopology_2)); }
	inline EventHandler_t1348719766 * get_NewTopology_2() const { return ___NewTopology_2; }
	inline EventHandler_t1348719766 ** get_address_of_NewTopology_2() { return &___NewTopology_2; }
	inline void set_NewTopology_2(EventHandler_t1348719766 * value)
	{
		___NewTopology_2 = value;
		Il2CppCodeGenWriteBarrier((&___NewTopology_2), value);
	}

	inline static int32_t get_offset_of_PenWidth_3() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___PenWidth_3)); }
	inline int32_t get_PenWidth_3() const { return ___PenWidth_3; }
	inline int32_t* get_address_of_PenWidth_3() { return &___PenWidth_3; }
	inline void set_PenWidth_3(int32_t value)
	{
		___PenWidth_3 = value;
	}

	inline static int32_t get_offset_of_updateTool_4() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___updateTool_4)); }
	inline UpdateTool_t905929733 * get_updateTool_4() const { return ___updateTool_4; }
	inline UpdateTool_t905929733 ** get_address_of_updateTool_4() { return &___updateTool_4; }
	inline void set_updateTool_4(UpdateTool_t905929733 * value)
	{
		___updateTool_4 = value;
		Il2CppCodeGenWriteBarrier((&___updateTool_4), value);
	}

	inline static int32_t get_offset_of_painterCanvas_5() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___painterCanvas_5)); }
	inline PainterCanvas_t4160673718 * get_painterCanvas_5() const { return ___painterCanvas_5; }
	inline PainterCanvas_t4160673718 ** get_address_of_painterCanvas_5() { return &___painterCanvas_5; }
	inline void set_painterCanvas_5(PainterCanvas_t4160673718 * value)
	{
		___painterCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___painterCanvas_5), value);
	}

	inline static int32_t get_offset_of_gameController_6() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___gameController_6)); }
	inline GameController_t2330501625 * get_gameController_6() const { return ___gameController_6; }
	inline GameController_t2330501625 ** get_address_of_gameController_6() { return &___gameController_6; }
	inline void set_gameController_6(GameController_t2330501625 * value)
	{
		___gameController_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_6), value);
	}

	inline static int32_t get_offset_of_miniGui_7() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___miniGui_7)); }
	inline MiniGUI_t213183967 * get_miniGui_7() const { return ___miniGui_7; }
	inline MiniGUI_t213183967 ** get_address_of_miniGui_7() { return &___miniGui_7; }
	inline void set_miniGui_7(MiniGUI_t213183967 * value)
	{
		___miniGui_7 = value;
		Il2CppCodeGenWriteBarrier((&___miniGui_7), value);
	}

	inline static int32_t get_offset_of_lastPoint_8() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___lastPoint_8)); }
	inline Vec2i_t1193278988  get_lastPoint_8() const { return ___lastPoint_8; }
	inline Vec2i_t1193278988 * get_address_of_lastPoint_8() { return &___lastPoint_8; }
	inline void set_lastPoint_8(Vec2i_t1193278988  value)
	{
		___lastPoint_8 = value;
	}

	inline static int32_t get_offset_of_isPainting_9() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___isPainting_9)); }
	inline bool get_isPainting_9() const { return ___isPainting_9; }
	inline bool* get_address_of_isPainting_9() { return &___isPainting_9; }
	inline void set_isPainting_9(bool value)
	{
		___isPainting_9 = value;
	}

	inline static int32_t get_offset_of_material_10() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___material_10)); }
	inline float get_material_10() const { return ___material_10; }
	inline float* get_address_of_material_10() { return &___material_10; }
	inline void set_material_10(float value)
	{
		___material_10 = value;
	}

	inline static int32_t get_offset_of_materialType_11() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___materialType_11)); }
	inline int32_t get_materialType_11() const { return ___materialType_11; }
	inline int32_t* get_address_of_materialType_11() { return &___materialType_11; }
	inline void set_materialType_11(int32_t value)
	{
		___materialType_11 = value;
	}

	inline static int32_t get_offset_of_selectedTool_12() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___selectedTool_12)); }
	inline int32_t get_selectedTool_12() const { return ___selectedTool_12; }
	inline int32_t* get_address_of_selectedTool_12() { return &___selectedTool_12; }
	inline void set_selectedTool_12(int32_t value)
	{
		___selectedTool_12 = value;
	}

	inline static int32_t get_offset_of_selectedPainter_13() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___selectedPainter_13)); }
	inline int32_t get_selectedPainter_13() const { return ___selectedPainter_13; }
	inline int32_t* get_address_of_selectedPainter_13() { return &___selectedPainter_13; }
	inline void set_selectedPainter_13(int32_t value)
	{
		___selectedPainter_13 = value;
	}

	inline static int32_t get_offset_of_thinThickTool_14() { return static_cast<int32_t>(offsetof(GUIPaintHandler_t437174526, ___thinThickTool_14)); }
	inline ThinThickTool_t2872687508 * get_thinThickTool_14() const { return ___thinThickTool_14; }
	inline ThinThickTool_t2872687508 ** get_address_of_thinThickTool_14() { return &___thinThickTool_14; }
	inline void set_thinThickTool_14(ThinThickTool_t2872687508 * value)
	{
		___thinThickTool_14 = value;
		Il2CppCodeGenWriteBarrier((&___thinThickTool_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIPAINTHANDLER_T437174526_H
#ifndef CANVASMESH_T1528759999_H
#define CANVASMESH_T1528759999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanvasMesh
struct  CanvasMesh_t1528759999  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CanvasMesh::sharedVertices
	bool ___sharedVertices_2;
	// System.Boolean CanvasMesh::isDefShown
	bool ___isDefShown_3;
	// System.Boolean CanvasMesh::showDef
	bool ___showDef_4;
	// System.Single CanvasMesh::sizeX
	float ___sizeX_5;
	// System.Single CanvasMesh::sizeY
	float ___sizeY_6;
	// System.Int32 CanvasMesh::nDofX
	int32_t ___nDofX_7;
	// System.Int32 CanvasMesh::nDofY
	int32_t ___nDofY_8;
	// System.Single[] CanvasMesh::u
	SingleU5BU5D_t1444911251* ___u_9;
	// UnityEngine.Vector2[] CanvasMesh::uvcoordinates
	Vector2U5BU5D_t1457185986* ___uvcoordinates_10;
	// UnityEngine.Vector2[] CanvasMesh::uvcoordinates2
	Vector2U5BU5D_t1457185986* ___uvcoordinates2_11;
	// UnityEngine.Vector3[] CanvasMesh::coordinates
	Vector3U5BU5D_t1718750761* ___coordinates_12;
	// System.Int32[] CanvasMesh::triangles
	Int32U5BU5D_t385246372* ___triangles_13;
	// GameController CanvasMesh::gc
	GameController_t2330501625 * ___gc_14;
	// System.Boolean CanvasMesh::isMeshInit
	bool ___isMeshInit_15;

public:
	inline static int32_t get_offset_of_sharedVertices_2() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___sharedVertices_2)); }
	inline bool get_sharedVertices_2() const { return ___sharedVertices_2; }
	inline bool* get_address_of_sharedVertices_2() { return &___sharedVertices_2; }
	inline void set_sharedVertices_2(bool value)
	{
		___sharedVertices_2 = value;
	}

	inline static int32_t get_offset_of_isDefShown_3() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___isDefShown_3)); }
	inline bool get_isDefShown_3() const { return ___isDefShown_3; }
	inline bool* get_address_of_isDefShown_3() { return &___isDefShown_3; }
	inline void set_isDefShown_3(bool value)
	{
		___isDefShown_3 = value;
	}

	inline static int32_t get_offset_of_showDef_4() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___showDef_4)); }
	inline bool get_showDef_4() const { return ___showDef_4; }
	inline bool* get_address_of_showDef_4() { return &___showDef_4; }
	inline void set_showDef_4(bool value)
	{
		___showDef_4 = value;
	}

	inline static int32_t get_offset_of_sizeX_5() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___sizeX_5)); }
	inline float get_sizeX_5() const { return ___sizeX_5; }
	inline float* get_address_of_sizeX_5() { return &___sizeX_5; }
	inline void set_sizeX_5(float value)
	{
		___sizeX_5 = value;
	}

	inline static int32_t get_offset_of_sizeY_6() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___sizeY_6)); }
	inline float get_sizeY_6() const { return ___sizeY_6; }
	inline float* get_address_of_sizeY_6() { return &___sizeY_6; }
	inline void set_sizeY_6(float value)
	{
		___sizeY_6 = value;
	}

	inline static int32_t get_offset_of_nDofX_7() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___nDofX_7)); }
	inline int32_t get_nDofX_7() const { return ___nDofX_7; }
	inline int32_t* get_address_of_nDofX_7() { return &___nDofX_7; }
	inline void set_nDofX_7(int32_t value)
	{
		___nDofX_7 = value;
	}

	inline static int32_t get_offset_of_nDofY_8() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___nDofY_8)); }
	inline int32_t get_nDofY_8() const { return ___nDofY_8; }
	inline int32_t* get_address_of_nDofY_8() { return &___nDofY_8; }
	inline void set_nDofY_8(int32_t value)
	{
		___nDofY_8 = value;
	}

	inline static int32_t get_offset_of_u_9() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___u_9)); }
	inline SingleU5BU5D_t1444911251* get_u_9() const { return ___u_9; }
	inline SingleU5BU5D_t1444911251** get_address_of_u_9() { return &___u_9; }
	inline void set_u_9(SingleU5BU5D_t1444911251* value)
	{
		___u_9 = value;
		Il2CppCodeGenWriteBarrier((&___u_9), value);
	}

	inline static int32_t get_offset_of_uvcoordinates_10() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___uvcoordinates_10)); }
	inline Vector2U5BU5D_t1457185986* get_uvcoordinates_10() const { return ___uvcoordinates_10; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvcoordinates_10() { return &___uvcoordinates_10; }
	inline void set_uvcoordinates_10(Vector2U5BU5D_t1457185986* value)
	{
		___uvcoordinates_10 = value;
		Il2CppCodeGenWriteBarrier((&___uvcoordinates_10), value);
	}

	inline static int32_t get_offset_of_uvcoordinates2_11() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___uvcoordinates2_11)); }
	inline Vector2U5BU5D_t1457185986* get_uvcoordinates2_11() const { return ___uvcoordinates2_11; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvcoordinates2_11() { return &___uvcoordinates2_11; }
	inline void set_uvcoordinates2_11(Vector2U5BU5D_t1457185986* value)
	{
		___uvcoordinates2_11 = value;
		Il2CppCodeGenWriteBarrier((&___uvcoordinates2_11), value);
	}

	inline static int32_t get_offset_of_coordinates_12() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___coordinates_12)); }
	inline Vector3U5BU5D_t1718750761* get_coordinates_12() const { return ___coordinates_12; }
	inline Vector3U5BU5D_t1718750761** get_address_of_coordinates_12() { return &___coordinates_12; }
	inline void set_coordinates_12(Vector3U5BU5D_t1718750761* value)
	{
		___coordinates_12 = value;
		Il2CppCodeGenWriteBarrier((&___coordinates_12), value);
	}

	inline static int32_t get_offset_of_triangles_13() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___triangles_13)); }
	inline Int32U5BU5D_t385246372* get_triangles_13() const { return ___triangles_13; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_13() { return &___triangles_13; }
	inline void set_triangles_13(Int32U5BU5D_t385246372* value)
	{
		___triangles_13 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_13), value);
	}

	inline static int32_t get_offset_of_gc_14() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___gc_14)); }
	inline GameController_t2330501625 * get_gc_14() const { return ___gc_14; }
	inline GameController_t2330501625 ** get_address_of_gc_14() { return &___gc_14; }
	inline void set_gc_14(GameController_t2330501625 * value)
	{
		___gc_14 = value;
		Il2CppCodeGenWriteBarrier((&___gc_14), value);
	}

	inline static int32_t get_offset_of_isMeshInit_15() { return static_cast<int32_t>(offsetof(CanvasMesh_t1528759999, ___isMeshInit_15)); }
	inline bool get_isMeshInit_15() const { return ___isMeshInit_15; }
	inline bool* get_address_of_isMeshInit_15() { return &___isMeshInit_15; }
	inline void set_isMeshInit_15(bool value)
	{
		___isMeshInit_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASMESH_T1528759999_H
#ifndef DEBUGOND_T2473991057_H
#define DEBUGOND_T2473991057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugOnD
struct  DebugOnD_t2473991057  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGOND_T2473991057_H
#ifndef SELECTEDLEVEL_T4079635930_H
#define SELECTEDLEVEL_T4079635930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectedLevel
struct  SelectedLevel_t4079635930  : public MonoBehaviour_t3962482529
{
public:
	// Level SelectedLevel::level
	Level_t2237665516 * ___level_2;

public:
	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(SelectedLevel_t4079635930, ___level_2)); }
	inline Level_t2237665516 * get_level_2() const { return ___level_2; }
	inline Level_t2237665516 ** get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(Level_t2237665516 * value)
	{
		___level_2 = value;
		Il2CppCodeGenWriteBarrier((&___level_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTEDLEVEL_T4079635930_H
#ifndef COLORBARGUI_T1806409552_H
#define COLORBARGUI_T1806409552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorBarGUI
struct  ColorBarGUI_t1806409552  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 ColorBarGUI::textureWidth
	int32_t ___textureWidth_2;
	// System.Int32 ColorBarGUI::textureHeight
	int32_t ___textureHeight_3;
	// UnityEngine.Texture2D ColorBarGUI::texture
	Texture2D_t3840446185 * ___texture_4;
	// UnityEngine.Rect ColorBarGUI::positionBar
	Rect_t2360479859  ___positionBar_5;
	// UnityEngine.Rect ColorBarGUI::positionMin
	Rect_t2360479859  ___positionMin_6;
	// UnityEngine.Rect ColorBarGUI::positionMax
	Rect_t2360479859  ___positionMax_7;
	// System.Single ColorBarGUI::maxVal
	float ___maxVal_8;
	// System.Single ColorBarGUI::minVal
	float ___minVal_9;
	// System.Single ColorBarGUI::norm
	float ___norm_10;
	// System.Single ColorBarGUI::colVal
	float ___colVal_11;
	// System.String ColorBarGUI::unit
	String_t* ___unit_12;
	// System.String[] ColorBarGUI::plotUnits
	StringU5BU5D_t1281789340* ___plotUnits_13;
	// GameController ColorBarGUI::gc
	GameController_t2330501625 * ___gc_14;
	// UnityEngine.GUIStyle ColorBarGUI::colorbarTextStyle
	GUIStyle_t3956901511 * ___colorbarTextStyle_15;

public:
	inline static int32_t get_offset_of_textureWidth_2() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___textureWidth_2)); }
	inline int32_t get_textureWidth_2() const { return ___textureWidth_2; }
	inline int32_t* get_address_of_textureWidth_2() { return &___textureWidth_2; }
	inline void set_textureWidth_2(int32_t value)
	{
		___textureWidth_2 = value;
	}

	inline static int32_t get_offset_of_textureHeight_3() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___textureHeight_3)); }
	inline int32_t get_textureHeight_3() const { return ___textureHeight_3; }
	inline int32_t* get_address_of_textureHeight_3() { return &___textureHeight_3; }
	inline void set_textureHeight_3(int32_t value)
	{
		___textureHeight_3 = value;
	}

	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___texture_4)); }
	inline Texture2D_t3840446185 * get_texture_4() const { return ___texture_4; }
	inline Texture2D_t3840446185 ** get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(Texture2D_t3840446185 * value)
	{
		___texture_4 = value;
		Il2CppCodeGenWriteBarrier((&___texture_4), value);
	}

	inline static int32_t get_offset_of_positionBar_5() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___positionBar_5)); }
	inline Rect_t2360479859  get_positionBar_5() const { return ___positionBar_5; }
	inline Rect_t2360479859 * get_address_of_positionBar_5() { return &___positionBar_5; }
	inline void set_positionBar_5(Rect_t2360479859  value)
	{
		___positionBar_5 = value;
	}

	inline static int32_t get_offset_of_positionMin_6() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___positionMin_6)); }
	inline Rect_t2360479859  get_positionMin_6() const { return ___positionMin_6; }
	inline Rect_t2360479859 * get_address_of_positionMin_6() { return &___positionMin_6; }
	inline void set_positionMin_6(Rect_t2360479859  value)
	{
		___positionMin_6 = value;
	}

	inline static int32_t get_offset_of_positionMax_7() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___positionMax_7)); }
	inline Rect_t2360479859  get_positionMax_7() const { return ___positionMax_7; }
	inline Rect_t2360479859 * get_address_of_positionMax_7() { return &___positionMax_7; }
	inline void set_positionMax_7(Rect_t2360479859  value)
	{
		___positionMax_7 = value;
	}

	inline static int32_t get_offset_of_maxVal_8() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___maxVal_8)); }
	inline float get_maxVal_8() const { return ___maxVal_8; }
	inline float* get_address_of_maxVal_8() { return &___maxVal_8; }
	inline void set_maxVal_8(float value)
	{
		___maxVal_8 = value;
	}

	inline static int32_t get_offset_of_minVal_9() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___minVal_9)); }
	inline float get_minVal_9() const { return ___minVal_9; }
	inline float* get_address_of_minVal_9() { return &___minVal_9; }
	inline void set_minVal_9(float value)
	{
		___minVal_9 = value;
	}

	inline static int32_t get_offset_of_norm_10() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___norm_10)); }
	inline float get_norm_10() const { return ___norm_10; }
	inline float* get_address_of_norm_10() { return &___norm_10; }
	inline void set_norm_10(float value)
	{
		___norm_10 = value;
	}

	inline static int32_t get_offset_of_colVal_11() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___colVal_11)); }
	inline float get_colVal_11() const { return ___colVal_11; }
	inline float* get_address_of_colVal_11() { return &___colVal_11; }
	inline void set_colVal_11(float value)
	{
		___colVal_11 = value;
	}

	inline static int32_t get_offset_of_unit_12() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___unit_12)); }
	inline String_t* get_unit_12() const { return ___unit_12; }
	inline String_t** get_address_of_unit_12() { return &___unit_12; }
	inline void set_unit_12(String_t* value)
	{
		___unit_12 = value;
		Il2CppCodeGenWriteBarrier((&___unit_12), value);
	}

	inline static int32_t get_offset_of_plotUnits_13() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___plotUnits_13)); }
	inline StringU5BU5D_t1281789340* get_plotUnits_13() const { return ___plotUnits_13; }
	inline StringU5BU5D_t1281789340** get_address_of_plotUnits_13() { return &___plotUnits_13; }
	inline void set_plotUnits_13(StringU5BU5D_t1281789340* value)
	{
		___plotUnits_13 = value;
		Il2CppCodeGenWriteBarrier((&___plotUnits_13), value);
	}

	inline static int32_t get_offset_of_gc_14() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___gc_14)); }
	inline GameController_t2330501625 * get_gc_14() const { return ___gc_14; }
	inline GameController_t2330501625 ** get_address_of_gc_14() { return &___gc_14; }
	inline void set_gc_14(GameController_t2330501625 * value)
	{
		___gc_14 = value;
		Il2CppCodeGenWriteBarrier((&___gc_14), value);
	}

	inline static int32_t get_offset_of_colorbarTextStyle_15() { return static_cast<int32_t>(offsetof(ColorBarGUI_t1806409552, ___colorbarTextStyle_15)); }
	inline GUIStyle_t3956901511 * get_colorbarTextStyle_15() const { return ___colorbarTextStyle_15; }
	inline GUIStyle_t3956901511 ** get_address_of_colorbarTextStyle_15() { return &___colorbarTextStyle_15; }
	inline void set_colorbarTextStyle_15(GUIStyle_t3956901511 * value)
	{
		___colorbarTextStyle_15 = value;
		Il2CppCodeGenWriteBarrier((&___colorbarTextStyle_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBARGUI_T1806409552_H
#ifndef GRAVITY_T379910146_H
#define GRAVITY_T379910146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gravity
struct  Gravity_t379910146  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Quaternion Gravity::rotation
	Quaternion_t2301928331  ___rotation_2;
	// System.Single Gravity::magnitude
	float ___magnitude_3;
	// UnityEngine.Vector3 Gravity::eulerRotation
	Vector3_t3722313464  ___eulerRotation_4;
	// System.Boolean Gravity::hasGyroscope
	bool ___hasGyroscope_6;
	// System.Boolean Gravity::hasAccelerometer
	bool ___hasAccelerometer_7;

public:
	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(Gravity_t379910146, ___rotation_2)); }
	inline Quaternion_t2301928331  get_rotation_2() const { return ___rotation_2; }
	inline Quaternion_t2301928331 * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(Quaternion_t2301928331  value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_magnitude_3() { return static_cast<int32_t>(offsetof(Gravity_t379910146, ___magnitude_3)); }
	inline float get_magnitude_3() const { return ___magnitude_3; }
	inline float* get_address_of_magnitude_3() { return &___magnitude_3; }
	inline void set_magnitude_3(float value)
	{
		___magnitude_3 = value;
	}

	inline static int32_t get_offset_of_eulerRotation_4() { return static_cast<int32_t>(offsetof(Gravity_t379910146, ___eulerRotation_4)); }
	inline Vector3_t3722313464  get_eulerRotation_4() const { return ___eulerRotation_4; }
	inline Vector3_t3722313464 * get_address_of_eulerRotation_4() { return &___eulerRotation_4; }
	inline void set_eulerRotation_4(Vector3_t3722313464  value)
	{
		___eulerRotation_4 = value;
	}

	inline static int32_t get_offset_of_hasGyroscope_6() { return static_cast<int32_t>(offsetof(Gravity_t379910146, ___hasGyroscope_6)); }
	inline bool get_hasGyroscope_6() const { return ___hasGyroscope_6; }
	inline bool* get_address_of_hasGyroscope_6() { return &___hasGyroscope_6; }
	inline void set_hasGyroscope_6(bool value)
	{
		___hasGyroscope_6 = value;
	}

	inline static int32_t get_offset_of_hasAccelerometer_7() { return static_cast<int32_t>(offsetof(Gravity_t379910146, ___hasAccelerometer_7)); }
	inline bool get_hasAccelerometer_7() const { return ___hasAccelerometer_7; }
	inline bool* get_address_of_hasAccelerometer_7() { return &___hasAccelerometer_7; }
	inline void set_hasAccelerometer_7(bool value)
	{
		___hasAccelerometer_7 = value;
	}
};

struct Gravity_t379910146_StaticFields
{
public:
	// Gravity Gravity::gravity
	Gravity_t379910146 * ___gravity_5;

public:
	inline static int32_t get_offset_of_gravity_5() { return static_cast<int32_t>(offsetof(Gravity_t379910146_StaticFields, ___gravity_5)); }
	inline Gravity_t379910146 * get_gravity_5() const { return ___gravity_5; }
	inline Gravity_t379910146 ** get_address_of_gravity_5() { return &___gravity_5; }
	inline void set_gravity_5(Gravity_t379910146 * value)
	{
		___gravity_5 = value;
		Il2CppCodeGenWriteBarrier((&___gravity_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAVITY_T379910146_H
#ifndef BOUNDINGCONSTRAINTSGUI_T795470468_H
#define BOUNDINGCONSTRAINTSGUI_T795470468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundingConstraintsGUI
struct  BoundingConstraintsGUI_t795470468  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin BoundingConstraintsGUI::guiSkin
	GUISkin_t1244372282 * ___guiSkin_2;
	// System.String BoundingConstraintsGUI::selectedMenu
	String_t* ___selectedMenu_3;
	// System.String BoundingConstraintsGUI::selectedMenuVisible
	String_t* ___selectedMenuVisible_4;
	// UnityEngine.Texture2D[] BoundingConstraintsGUI::textures
	Texture2DU5BU5D_t149664596* ___textures_5;
	// UnityEngine.Texture2D[] BoundingConstraintsGUI::selected_textures
	Texture2DU5BU5D_t149664596* ___selected_textures_6;
	// System.String[] BoundingConstraintsGUI::tools
	StringU5BU5D_t1281789340* ___tools_14;
	// Menu[] BoundingConstraintsGUI::toolsMenu
	MenuU5BU5D_t2527869020* ___toolsMenu_15;
	// UnityEngine.Rect BoundingConstraintsGUI::penButtonPosition
	Rect_t2360479859  ___penButtonPosition_17;
	// UnityEngine.Color BoundingConstraintsGUI::selectedMenuColor
	Color_t2555686324  ___selectedMenuColor_18;
	// UnityEngine.Vector3 BoundingConstraintsGUI::scale
	Vector3_t3722313464  ___scale_19;
	// UnityEngine.Vector4 BoundingConstraintsGUI::offset
	Vector4_t3319028937  ___offset_20;

public:
	inline static int32_t get_offset_of_guiSkin_2() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___guiSkin_2)); }
	inline GUISkin_t1244372282 * get_guiSkin_2() const { return ___guiSkin_2; }
	inline GUISkin_t1244372282 ** get_address_of_guiSkin_2() { return &___guiSkin_2; }
	inline void set_guiSkin_2(GUISkin_t1244372282 * value)
	{
		___guiSkin_2 = value;
		Il2CppCodeGenWriteBarrier((&___guiSkin_2), value);
	}

	inline static int32_t get_offset_of_selectedMenu_3() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___selectedMenu_3)); }
	inline String_t* get_selectedMenu_3() const { return ___selectedMenu_3; }
	inline String_t** get_address_of_selectedMenu_3() { return &___selectedMenu_3; }
	inline void set_selectedMenu_3(String_t* value)
	{
		___selectedMenu_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMenu_3), value);
	}

	inline static int32_t get_offset_of_selectedMenuVisible_4() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___selectedMenuVisible_4)); }
	inline String_t* get_selectedMenuVisible_4() const { return ___selectedMenuVisible_4; }
	inline String_t** get_address_of_selectedMenuVisible_4() { return &___selectedMenuVisible_4; }
	inline void set_selectedMenuVisible_4(String_t* value)
	{
		___selectedMenuVisible_4 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMenuVisible_4), value);
	}

	inline static int32_t get_offset_of_textures_5() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___textures_5)); }
	inline Texture2DU5BU5D_t149664596* get_textures_5() const { return ___textures_5; }
	inline Texture2DU5BU5D_t149664596** get_address_of_textures_5() { return &___textures_5; }
	inline void set_textures_5(Texture2DU5BU5D_t149664596* value)
	{
		___textures_5 = value;
		Il2CppCodeGenWriteBarrier((&___textures_5), value);
	}

	inline static int32_t get_offset_of_selected_textures_6() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___selected_textures_6)); }
	inline Texture2DU5BU5D_t149664596* get_selected_textures_6() const { return ___selected_textures_6; }
	inline Texture2DU5BU5D_t149664596** get_address_of_selected_textures_6() { return &___selected_textures_6; }
	inline void set_selected_textures_6(Texture2DU5BU5D_t149664596* value)
	{
		___selected_textures_6 = value;
		Il2CppCodeGenWriteBarrier((&___selected_textures_6), value);
	}

	inline static int32_t get_offset_of_tools_14() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___tools_14)); }
	inline StringU5BU5D_t1281789340* get_tools_14() const { return ___tools_14; }
	inline StringU5BU5D_t1281789340** get_address_of_tools_14() { return &___tools_14; }
	inline void set_tools_14(StringU5BU5D_t1281789340* value)
	{
		___tools_14 = value;
		Il2CppCodeGenWriteBarrier((&___tools_14), value);
	}

	inline static int32_t get_offset_of_toolsMenu_15() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___toolsMenu_15)); }
	inline MenuU5BU5D_t2527869020* get_toolsMenu_15() const { return ___toolsMenu_15; }
	inline MenuU5BU5D_t2527869020** get_address_of_toolsMenu_15() { return &___toolsMenu_15; }
	inline void set_toolsMenu_15(MenuU5BU5D_t2527869020* value)
	{
		___toolsMenu_15 = value;
		Il2CppCodeGenWriteBarrier((&___toolsMenu_15), value);
	}

	inline static int32_t get_offset_of_penButtonPosition_17() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___penButtonPosition_17)); }
	inline Rect_t2360479859  get_penButtonPosition_17() const { return ___penButtonPosition_17; }
	inline Rect_t2360479859 * get_address_of_penButtonPosition_17() { return &___penButtonPosition_17; }
	inline void set_penButtonPosition_17(Rect_t2360479859  value)
	{
		___penButtonPosition_17 = value;
	}

	inline static int32_t get_offset_of_selectedMenuColor_18() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___selectedMenuColor_18)); }
	inline Color_t2555686324  get_selectedMenuColor_18() const { return ___selectedMenuColor_18; }
	inline Color_t2555686324 * get_address_of_selectedMenuColor_18() { return &___selectedMenuColor_18; }
	inline void set_selectedMenuColor_18(Color_t2555686324  value)
	{
		___selectedMenuColor_18 = value;
	}

	inline static int32_t get_offset_of_scale_19() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___scale_19)); }
	inline Vector3_t3722313464  get_scale_19() const { return ___scale_19; }
	inline Vector3_t3722313464 * get_address_of_scale_19() { return &___scale_19; }
	inline void set_scale_19(Vector3_t3722313464  value)
	{
		___scale_19 = value;
	}

	inline static int32_t get_offset_of_offset_20() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468, ___offset_20)); }
	inline Vector4_t3319028937  get_offset_20() const { return ___offset_20; }
	inline Vector4_t3319028937 * get_address_of_offset_20() { return &___offset_20; }
	inline void set_offset_20(Vector4_t3319028937  value)
	{
		___offset_20 = value;
	}
};

struct BoundingConstraintsGUI_t795470468_StaticFields
{
public:
	// Menu BoundingConstraintsGUI::selectedTool
	int32_t ___selectedTool_16;

public:
	inline static int32_t get_offset_of_selectedTool_16() { return static_cast<int32_t>(offsetof(BoundingConstraintsGUI_t795470468_StaticFields, ___selectedTool_16)); }
	inline int32_t get_selectedTool_16() const { return ___selectedTool_16; }
	inline int32_t* get_address_of_selectedTool_16() { return &___selectedTool_16; }
	inline void set_selectedTool_16(int32_t value)
	{
		___selectedTool_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGCONSTRAINTSGUI_T795470468_H
#ifndef TOPOLOGYSELECTOR_T1922582608_H
#define TOPOLOGYSELECTOR_T1922582608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TopologySelector
struct  TopologySelector_t1922582608  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera TopologySelector::mainCamera
	Camera_t4157153871 * ___mainCamera_2;
	// TopologyComponent TopologySelector::displEndComp
	TopologyComponent_t4181154066 * ___displEndComp_3;
	// PainterCanvas TopologySelector::textureUpdater
	PainterCanvas_t4160673718 * ___textureUpdater_4;
	// GameController TopologySelector::topologyApplication
	GameController_t2330501625 * ___topologyApplication_5;
	// DrawMenuGUI TopologySelector::gameMenuGUI
	DrawMenuGUI_t2806387517 * ___gameMenuGUI_6;
	// CanvasMesh TopologySelector::cm
	CanvasMesh_t1528759999 * ___cm_7;
	// SKStateMachine`1<TopologySelector> TopologySelector::_state
	SKStateMachine_1_t2564681790 * ____state_8;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ___mainCamera_2)); }
	inline Camera_t4157153871 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Camera_t4157153871 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}

	inline static int32_t get_offset_of_displEndComp_3() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ___displEndComp_3)); }
	inline TopologyComponent_t4181154066 * get_displEndComp_3() const { return ___displEndComp_3; }
	inline TopologyComponent_t4181154066 ** get_address_of_displEndComp_3() { return &___displEndComp_3; }
	inline void set_displEndComp_3(TopologyComponent_t4181154066 * value)
	{
		___displEndComp_3 = value;
		Il2CppCodeGenWriteBarrier((&___displEndComp_3), value);
	}

	inline static int32_t get_offset_of_textureUpdater_4() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ___textureUpdater_4)); }
	inline PainterCanvas_t4160673718 * get_textureUpdater_4() const { return ___textureUpdater_4; }
	inline PainterCanvas_t4160673718 ** get_address_of_textureUpdater_4() { return &___textureUpdater_4; }
	inline void set_textureUpdater_4(PainterCanvas_t4160673718 * value)
	{
		___textureUpdater_4 = value;
		Il2CppCodeGenWriteBarrier((&___textureUpdater_4), value);
	}

	inline static int32_t get_offset_of_topologyApplication_5() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ___topologyApplication_5)); }
	inline GameController_t2330501625 * get_topologyApplication_5() const { return ___topologyApplication_5; }
	inline GameController_t2330501625 ** get_address_of_topologyApplication_5() { return &___topologyApplication_5; }
	inline void set_topologyApplication_5(GameController_t2330501625 * value)
	{
		___topologyApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___topologyApplication_5), value);
	}

	inline static int32_t get_offset_of_gameMenuGUI_6() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ___gameMenuGUI_6)); }
	inline DrawMenuGUI_t2806387517 * get_gameMenuGUI_6() const { return ___gameMenuGUI_6; }
	inline DrawMenuGUI_t2806387517 ** get_address_of_gameMenuGUI_6() { return &___gameMenuGUI_6; }
	inline void set_gameMenuGUI_6(DrawMenuGUI_t2806387517 * value)
	{
		___gameMenuGUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameMenuGUI_6), value);
	}

	inline static int32_t get_offset_of_cm_7() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ___cm_7)); }
	inline CanvasMesh_t1528759999 * get_cm_7() const { return ___cm_7; }
	inline CanvasMesh_t1528759999 ** get_address_of_cm_7() { return &___cm_7; }
	inline void set_cm_7(CanvasMesh_t1528759999 * value)
	{
		___cm_7 = value;
		Il2CppCodeGenWriteBarrier((&___cm_7), value);
	}

	inline static int32_t get_offset_of__state_8() { return static_cast<int32_t>(offsetof(TopologySelector_t1922582608, ____state_8)); }
	inline SKStateMachine_1_t2564681790 * get__state_8() const { return ____state_8; }
	inline SKStateMachine_1_t2564681790 ** get_address_of__state_8() { return &____state_8; }
	inline void set__state_8(SKStateMachine_1_t2564681790 * value)
	{
		____state_8 = value;
		Il2CppCodeGenWriteBarrier((&____state_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPOLOGYSELECTOR_T1922582608_H
#ifndef TESTALL_T3182214224_H
#define TESTALL_T3182214224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestAll
struct  TestAll_t3182214224  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTALL_T3182214224_H
#ifndef DRAWMENUGUI_T2806387517_H
#define DRAWMENUGUI_T2806387517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawMenuGUI
struct  DrawMenuGUI_t2806387517  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect DrawMenuGUI::labelPosition
	Rect_t2360479859  ___labelPosition_2;
	// UnityEngine.Rect DrawMenuGUI::roundButtonPosition
	Rect_t2360479859  ___roundButtonPosition_3;
	// UnityEngine.Rect DrawMenuGUI::penButtonPosition
	Rect_t2360479859  ___penButtonPosition_4;
	// System.Single DrawMenuGUI::densityValue
	float ___densityValue_5;
	// GUIPaintHandler DrawMenuGUI::guiHandler
	GUIPaintHandler_t437174526 * ___guiHandler_6;
	// System.Int32 DrawMenuGUI::selectedTool
	int32_t ___selectedTool_7;
	// System.Int32 DrawMenuGUI::selectedBrush
	int32_t ___selectedBrush_8;
	// UnityEngine.GUISkin DrawMenuGUI::guiSkin
	GUISkin_t1244372282 * ___guiSkin_9;
	// UnityEngine.Texture2D[] DrawMenuGUI::textures
	Texture2DU5BU5D_t149664596* ___textures_10;
	// UnityEngine.Texture2D[] DrawMenuGUI::selected_textures
	Texture2DU5BU5D_t149664596* ___selected_textures_11;
	// System.String[] DrawMenuGUI::tools
	StringU5BU5D_t1281789340* ___tools_12;
	// System.String DrawMenuGUI::selectedMenu
	String_t* ___selectedMenu_13;
	// System.String DrawMenuGUI::selectedMenuVisible
	String_t* ___selectedMenuVisible_14;
	// UnityEngine.Color DrawMenuGUI::selectedMenuColor
	Color_t2555686324  ___selectedMenuColor_15;
	// UnityEngine.Vector3 DrawMenuGUI::scale
	Vector3_t3722313464  ___scale_16;
	// UnityEngine.Vector4 DrawMenuGUI::offset
	Vector4_t3319028937  ___offset_17;

public:
	inline static int32_t get_offset_of_labelPosition_2() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___labelPosition_2)); }
	inline Rect_t2360479859  get_labelPosition_2() const { return ___labelPosition_2; }
	inline Rect_t2360479859 * get_address_of_labelPosition_2() { return &___labelPosition_2; }
	inline void set_labelPosition_2(Rect_t2360479859  value)
	{
		___labelPosition_2 = value;
	}

	inline static int32_t get_offset_of_roundButtonPosition_3() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___roundButtonPosition_3)); }
	inline Rect_t2360479859  get_roundButtonPosition_3() const { return ___roundButtonPosition_3; }
	inline Rect_t2360479859 * get_address_of_roundButtonPosition_3() { return &___roundButtonPosition_3; }
	inline void set_roundButtonPosition_3(Rect_t2360479859  value)
	{
		___roundButtonPosition_3 = value;
	}

	inline static int32_t get_offset_of_penButtonPosition_4() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___penButtonPosition_4)); }
	inline Rect_t2360479859  get_penButtonPosition_4() const { return ___penButtonPosition_4; }
	inline Rect_t2360479859 * get_address_of_penButtonPosition_4() { return &___penButtonPosition_4; }
	inline void set_penButtonPosition_4(Rect_t2360479859  value)
	{
		___penButtonPosition_4 = value;
	}

	inline static int32_t get_offset_of_densityValue_5() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___densityValue_5)); }
	inline float get_densityValue_5() const { return ___densityValue_5; }
	inline float* get_address_of_densityValue_5() { return &___densityValue_5; }
	inline void set_densityValue_5(float value)
	{
		___densityValue_5 = value;
	}

	inline static int32_t get_offset_of_guiHandler_6() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___guiHandler_6)); }
	inline GUIPaintHandler_t437174526 * get_guiHandler_6() const { return ___guiHandler_6; }
	inline GUIPaintHandler_t437174526 ** get_address_of_guiHandler_6() { return &___guiHandler_6; }
	inline void set_guiHandler_6(GUIPaintHandler_t437174526 * value)
	{
		___guiHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___guiHandler_6), value);
	}

	inline static int32_t get_offset_of_selectedTool_7() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___selectedTool_7)); }
	inline int32_t get_selectedTool_7() const { return ___selectedTool_7; }
	inline int32_t* get_address_of_selectedTool_7() { return &___selectedTool_7; }
	inline void set_selectedTool_7(int32_t value)
	{
		___selectedTool_7 = value;
	}

	inline static int32_t get_offset_of_selectedBrush_8() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___selectedBrush_8)); }
	inline int32_t get_selectedBrush_8() const { return ___selectedBrush_8; }
	inline int32_t* get_address_of_selectedBrush_8() { return &___selectedBrush_8; }
	inline void set_selectedBrush_8(int32_t value)
	{
		___selectedBrush_8 = value;
	}

	inline static int32_t get_offset_of_guiSkin_9() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___guiSkin_9)); }
	inline GUISkin_t1244372282 * get_guiSkin_9() const { return ___guiSkin_9; }
	inline GUISkin_t1244372282 ** get_address_of_guiSkin_9() { return &___guiSkin_9; }
	inline void set_guiSkin_9(GUISkin_t1244372282 * value)
	{
		___guiSkin_9 = value;
		Il2CppCodeGenWriteBarrier((&___guiSkin_9), value);
	}

	inline static int32_t get_offset_of_textures_10() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___textures_10)); }
	inline Texture2DU5BU5D_t149664596* get_textures_10() const { return ___textures_10; }
	inline Texture2DU5BU5D_t149664596** get_address_of_textures_10() { return &___textures_10; }
	inline void set_textures_10(Texture2DU5BU5D_t149664596* value)
	{
		___textures_10 = value;
		Il2CppCodeGenWriteBarrier((&___textures_10), value);
	}

	inline static int32_t get_offset_of_selected_textures_11() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___selected_textures_11)); }
	inline Texture2DU5BU5D_t149664596* get_selected_textures_11() const { return ___selected_textures_11; }
	inline Texture2DU5BU5D_t149664596** get_address_of_selected_textures_11() { return &___selected_textures_11; }
	inline void set_selected_textures_11(Texture2DU5BU5D_t149664596* value)
	{
		___selected_textures_11 = value;
		Il2CppCodeGenWriteBarrier((&___selected_textures_11), value);
	}

	inline static int32_t get_offset_of_tools_12() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___tools_12)); }
	inline StringU5BU5D_t1281789340* get_tools_12() const { return ___tools_12; }
	inline StringU5BU5D_t1281789340** get_address_of_tools_12() { return &___tools_12; }
	inline void set_tools_12(StringU5BU5D_t1281789340* value)
	{
		___tools_12 = value;
		Il2CppCodeGenWriteBarrier((&___tools_12), value);
	}

	inline static int32_t get_offset_of_selectedMenu_13() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___selectedMenu_13)); }
	inline String_t* get_selectedMenu_13() const { return ___selectedMenu_13; }
	inline String_t** get_address_of_selectedMenu_13() { return &___selectedMenu_13; }
	inline void set_selectedMenu_13(String_t* value)
	{
		___selectedMenu_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMenu_13), value);
	}

	inline static int32_t get_offset_of_selectedMenuVisible_14() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___selectedMenuVisible_14)); }
	inline String_t* get_selectedMenuVisible_14() const { return ___selectedMenuVisible_14; }
	inline String_t** get_address_of_selectedMenuVisible_14() { return &___selectedMenuVisible_14; }
	inline void set_selectedMenuVisible_14(String_t* value)
	{
		___selectedMenuVisible_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMenuVisible_14), value);
	}

	inline static int32_t get_offset_of_selectedMenuColor_15() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___selectedMenuColor_15)); }
	inline Color_t2555686324  get_selectedMenuColor_15() const { return ___selectedMenuColor_15; }
	inline Color_t2555686324 * get_address_of_selectedMenuColor_15() { return &___selectedMenuColor_15; }
	inline void set_selectedMenuColor_15(Color_t2555686324  value)
	{
		___selectedMenuColor_15 = value;
	}

	inline static int32_t get_offset_of_scale_16() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___scale_16)); }
	inline Vector3_t3722313464  get_scale_16() const { return ___scale_16; }
	inline Vector3_t3722313464 * get_address_of_scale_16() { return &___scale_16; }
	inline void set_scale_16(Vector3_t3722313464  value)
	{
		___scale_16 = value;
	}

	inline static int32_t get_offset_of_offset_17() { return static_cast<int32_t>(offsetof(DrawMenuGUI_t2806387517, ___offset_17)); }
	inline Vector4_t3319028937  get_offset_17() const { return ___offset_17; }
	inline Vector4_t3319028937 * get_address_of_offset_17() { return &___offset_17; }
	inline void set_offset_17(Vector4_t3319028937  value)
	{
		___offset_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWMENUGUI_T2806387517_H
#ifndef GAMEGUI_T2956616616_H
#define GAMEGUI_T2956616616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameGUI
struct  GameGUI_t2956616616  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUISkin GameGUI::gui
	GUISkin_t1244372282 * ___gui_2;
	// UnityEngine.Rect GameGUI::scoreRect
	Rect_t2360479859  ___scoreRect_3;
	// Level GameGUI::level
	Level_t2237665516 * ___level_4;
	// GameController GameGUI::gameController
	GameController_t2330501625 * ___gameController_5;
	// System.Single GameGUI::score
	float ___score_6;
	// GUIPaintHandler GameGUI::paintHandler
	GUIPaintHandler_t437174526 * ___paintHandler_7;
	// System.Boolean GameGUI::debugRects
	bool ___debugRects_9;
	// DrawMenuGUI GameGUI::gameMenuGUI
	DrawMenuGUI_t2806387517 * ___gameMenuGUI_10;
	// BoundingConstraintsGUI GameGUI::boundingConstraintsGUI
	BoundingConstraintsGUI_t795470468 * ___boundingConstraintsGUI_11;
	// UnityEngine.MeshRenderer GameGUI::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_13;
	// System.Globalization.NumberFormatInfo GameGUI::numberFormatInfo
	NumberFormatInfo_t435877138 * ___numberFormatInfo_14;

public:
	inline static int32_t get_offset_of_gui_2() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___gui_2)); }
	inline GUISkin_t1244372282 * get_gui_2() const { return ___gui_2; }
	inline GUISkin_t1244372282 ** get_address_of_gui_2() { return &___gui_2; }
	inline void set_gui_2(GUISkin_t1244372282 * value)
	{
		___gui_2 = value;
		Il2CppCodeGenWriteBarrier((&___gui_2), value);
	}

	inline static int32_t get_offset_of_scoreRect_3() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___scoreRect_3)); }
	inline Rect_t2360479859  get_scoreRect_3() const { return ___scoreRect_3; }
	inline Rect_t2360479859 * get_address_of_scoreRect_3() { return &___scoreRect_3; }
	inline void set_scoreRect_3(Rect_t2360479859  value)
	{
		___scoreRect_3 = value;
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___level_4)); }
	inline Level_t2237665516 * get_level_4() const { return ___level_4; }
	inline Level_t2237665516 ** get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(Level_t2237665516 * value)
	{
		___level_4 = value;
		Il2CppCodeGenWriteBarrier((&___level_4), value);
	}

	inline static int32_t get_offset_of_gameController_5() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___gameController_5)); }
	inline GameController_t2330501625 * get_gameController_5() const { return ___gameController_5; }
	inline GameController_t2330501625 ** get_address_of_gameController_5() { return &___gameController_5; }
	inline void set_gameController_5(GameController_t2330501625 * value)
	{
		___gameController_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_5), value);
	}

	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___score_6)); }
	inline float get_score_6() const { return ___score_6; }
	inline float* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(float value)
	{
		___score_6 = value;
	}

	inline static int32_t get_offset_of_paintHandler_7() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___paintHandler_7)); }
	inline GUIPaintHandler_t437174526 * get_paintHandler_7() const { return ___paintHandler_7; }
	inline GUIPaintHandler_t437174526 ** get_address_of_paintHandler_7() { return &___paintHandler_7; }
	inline void set_paintHandler_7(GUIPaintHandler_t437174526 * value)
	{
		___paintHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___paintHandler_7), value);
	}

	inline static int32_t get_offset_of_debugRects_9() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___debugRects_9)); }
	inline bool get_debugRects_9() const { return ___debugRects_9; }
	inline bool* get_address_of_debugRects_9() { return &___debugRects_9; }
	inline void set_debugRects_9(bool value)
	{
		___debugRects_9 = value;
	}

	inline static int32_t get_offset_of_gameMenuGUI_10() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___gameMenuGUI_10)); }
	inline DrawMenuGUI_t2806387517 * get_gameMenuGUI_10() const { return ___gameMenuGUI_10; }
	inline DrawMenuGUI_t2806387517 ** get_address_of_gameMenuGUI_10() { return &___gameMenuGUI_10; }
	inline void set_gameMenuGUI_10(DrawMenuGUI_t2806387517 * value)
	{
		___gameMenuGUI_10 = value;
		Il2CppCodeGenWriteBarrier((&___gameMenuGUI_10), value);
	}

	inline static int32_t get_offset_of_boundingConstraintsGUI_11() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___boundingConstraintsGUI_11)); }
	inline BoundingConstraintsGUI_t795470468 * get_boundingConstraintsGUI_11() const { return ___boundingConstraintsGUI_11; }
	inline BoundingConstraintsGUI_t795470468 ** get_address_of_boundingConstraintsGUI_11() { return &___boundingConstraintsGUI_11; }
	inline void set_boundingConstraintsGUI_11(BoundingConstraintsGUI_t795470468 * value)
	{
		___boundingConstraintsGUI_11 = value;
		Il2CppCodeGenWriteBarrier((&___boundingConstraintsGUI_11), value);
	}

	inline static int32_t get_offset_of_meshRenderer_13() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___meshRenderer_13)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_13() const { return ___meshRenderer_13; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_13() { return &___meshRenderer_13; }
	inline void set_meshRenderer_13(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_13), value);
	}

	inline static int32_t get_offset_of_numberFormatInfo_14() { return static_cast<int32_t>(offsetof(GameGUI_t2956616616, ___numberFormatInfo_14)); }
	inline NumberFormatInfo_t435877138 * get_numberFormatInfo_14() const { return ___numberFormatInfo_14; }
	inline NumberFormatInfo_t435877138 ** get_address_of_numberFormatInfo_14() { return &___numberFormatInfo_14; }
	inline void set_numberFormatInfo_14(NumberFormatInfo_t435877138 * value)
	{
		___numberFormatInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numberFormatInfo_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEGUI_T2956616616_H
#ifndef GAMEMAINMENUGUI_T1155257793_H
#define GAMEMAINMENUGUI_T1155257793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameMainMenuGUI
struct  GameMainMenuGUI_t1155257793  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D GameMainMenuGUI::texture
	Texture2D_t3840446185 * ___texture_2;
	// System.String[] GameMainMenuGUI::plotChoices
	StringU5BU5D_t1281789340* ___plotChoices_3;
	// System.Single GameMainMenuGUI::uScale
	float ___uScale_4;

public:
	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(GameMainMenuGUI_t1155257793, ___texture_2)); }
	inline Texture2D_t3840446185 * get_texture_2() const { return ___texture_2; }
	inline Texture2D_t3840446185 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(Texture2D_t3840446185 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}

	inline static int32_t get_offset_of_plotChoices_3() { return static_cast<int32_t>(offsetof(GameMainMenuGUI_t1155257793, ___plotChoices_3)); }
	inline StringU5BU5D_t1281789340* get_plotChoices_3() const { return ___plotChoices_3; }
	inline StringU5BU5D_t1281789340** get_address_of_plotChoices_3() { return &___plotChoices_3; }
	inline void set_plotChoices_3(StringU5BU5D_t1281789340* value)
	{
		___plotChoices_3 = value;
		Il2CppCodeGenWriteBarrier((&___plotChoices_3), value);
	}

	inline static int32_t get_offset_of_uScale_4() { return static_cast<int32_t>(offsetof(GameMainMenuGUI_t1155257793, ___uScale_4)); }
	inline float get_uScale_4() const { return ___uScale_4; }
	inline float* get_address_of_uScale_4() { return &___uScale_4; }
	inline void set_uScale_4(float value)
	{
		___uScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMAINMENUGUI_T1155257793_H
#ifndef TEXTUREDEBUG_T1588131437_H
#define TEXTUREDEBUG_T1588131437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextureDebug
struct  TextureDebug_t1588131437  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREDEBUG_T1588131437_H
#ifndef MATERIALMENUGUI_T4221244765_H
#define MATERIALMENUGUI_T4221244765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialMenuGUI
struct  MaterialMenuGUI_t4221244765  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MaterialMenuGUI::EmaxMat1
	float ___EmaxMat1_2;
	// System.Single MaterialMenuGUI::EmaxMat2
	float ___EmaxMat2_3;

public:
	inline static int32_t get_offset_of_EmaxMat1_2() { return static_cast<int32_t>(offsetof(MaterialMenuGUI_t4221244765, ___EmaxMat1_2)); }
	inline float get_EmaxMat1_2() const { return ___EmaxMat1_2; }
	inline float* get_address_of_EmaxMat1_2() { return &___EmaxMat1_2; }
	inline void set_EmaxMat1_2(float value)
	{
		___EmaxMat1_2 = value;
	}

	inline static int32_t get_offset_of_EmaxMat2_3() { return static_cast<int32_t>(offsetof(MaterialMenuGUI_t4221244765, ___EmaxMat2_3)); }
	inline float get_EmaxMat2_3() const { return ___EmaxMat2_3; }
	inline float* get_address_of_EmaxMat2_3() { return &___EmaxMat2_3; }
	inline void set_EmaxMat2_3(float value)
	{
		___EmaxMat2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALMENUGUI_T4221244765_H
#ifndef MINIGUI_T213183967_H
#define MINIGUI_T213183967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniGUI
struct  MiniGUI_t213183967  : public MonoBehaviour_t3962482529
{
public:
	// GUIPaintHandler MiniGUI::gph
	GUIPaintHandler_t437174526 * ___gph_2;
	// TopologySelector MiniGUI::sel
	TopologySelector_t1922582608 * ___sel_3;
	// GameController MiniGUI::gc
	GameController_t2330501625 * ___gc_4;
	// CanvasMesh MiniGUI::cm
	CanvasMesh_t1528759999 * ___cm_5;
	// ColorBarGUI MiniGUI::cbg
	ColorBarGUI_t1806409552 * ___cbg_6;
	// UnityEngine.Camera MiniGUI::camera
	Camera_t4157153871 * ___camera_7;
	// UnityEngine.Canvas MiniGUI::Background_canvas
	Canvas_t3310196443 * ___Background_canvas_8;
	// UnityEngine.UI.Image MiniGUI::Background_graph
	Image_t2670269651 * ___Background_graph_9;
	// Screenshot MiniGUI::screenshot
	Screenshot_t3680149077 * ___screenshot_10;
	// System.String MiniGUI::selectedMenu
	String_t* ___selectedMenu_11;
	// System.String MiniGUI::selectedTool
	String_t* ___selectedTool_12;
	// System.String MiniGUI::selectedMaterial
	String_t* ___selectedMaterial_13;
	// System.String MiniGUI::selectedBackground
	String_t* ___selectedBackground_14;
	// System.String MiniGUI::plotType
	String_t* ___plotType_15;
	// System.String MiniGUI::meshLevel
	String_t* ___meshLevel_16;
	// System.Int32 MiniGUI::oldPlotChoice
	int32_t ___oldPlotChoice_17;
	// System.Int32 MiniGUI::selectedPenSize
	int32_t ___selectedPenSize_18;
	// System.Boolean MiniGUI::showDef
	bool ___showDef_19;
	// System.Single MiniGUI::nBottons
	float ___nBottons_20;
	// System.Int32 MiniGUI::nGUIChanges
	int32_t ___nGUIChanges_21;
	// System.Boolean MiniGUI::grayBox
	bool ___grayBox_22;
	// System.Boolean MiniGUI::is_gravity
	bool ___is_gravity_23;
	// System.Boolean MiniGUI::DisplayScreenshotMessage
	bool ___DisplayScreenshotMessage_24;
	// UnityEngine.Color MiniGUI::MenuBlue
	Color_t2555686324  ___MenuBlue_25;
	// UnityEngine.Rect MiniGUI::topButton
	Rect_t2360479859  ___topButton_26;
	// UnityEngine.Rect MiniGUI::menuButton
	Rect_t2360479859  ___menuButton_27;
	// UnityEngine.Rect MiniGUI::subMenuButton
	Rect_t2360479859  ___subMenuButton_28;
	// UnityEngine.Rect MiniGUI::showTool
	Rect_t2360479859  ___showTool_29;
	// UnityEngine.Rect MiniGUI::showLogo
	Rect_t2360479859  ___showLogo_30;
	// UnityEngine.Rect MiniGUI::colorBarPos
	Rect_t2360479859  ___colorBarPos_31;
	// UnityEngine.Rect MiniGUI::messagePos
	Rect_t2360479859  ___messagePos_32;
	// UnityEngine.Rect MiniGUI::warningPos
	Rect_t2360479859  ___warningPos_33;
	// System.Int32 MiniGUI::fac
	int32_t ___fac_34;
	// System.EventHandler MiniGUI::NewTopology
	EventHandler_t1348719766 * ___NewTopology_35;
	// UnityEngine.GUIStyle MiniGUI::TooltipStyle
	GUIStyle_t3956901511 * ___TooltipStyle_36;
	// UnityEngine.GUIStyle MiniGUI::buttonStyle
	GUIStyle_t3956901511 * ___buttonStyle_37;
	// UnityEngine.GUIStyle MiniGUI::labelStyle
	GUIStyle_t3956901511 * ___labelStyle_38;
	// UnityEngine.GUIStyle MiniGUI::messageStyle
	GUIStyle_t3956901511 * ___messageStyle_39;
	// UnityEngine.GUIStyle MiniGUI::DrawTooltipStyle
	GUIStyle_t3956901511 * ___DrawTooltipStyle_40;
	// UnityEngine.GUIStyle MiniGUI::aboutTextStyle
	GUIStyle_t3956901511 * ___aboutTextStyle_41;
	// UnityEngine.GUIStyle MiniGUI::linkStyle
	GUIStyle_t3956901511 * ___linkStyle_42;
	// Menu[] MiniGUI::bcToolsMenu
	MenuU5BU5D_t2527869020* ___bcToolsMenu_53;
	// Menu[] MiniGUI::fingerToolsMenu
	MenuU5BU5D_t2527869020* ___fingerToolsMenu_54;
	// UnityEngine.Texture2D[] MiniGUI::mainMenuTextures
	Texture2DU5BU5D_t149664596* ___mainMenuTextures_55;
	// UnityEngine.Texture2D[] MiniGUI::drawTextures
	Texture2DU5BU5D_t149664596* ___drawTextures_56;
	// UnityEngine.Texture2D[] MiniGUI::bcTextures
	Texture2DU5BU5D_t149664596* ___bcTextures_57;
	// UnityEngine.Texture2D[] MiniGUI::fingerTextures
	Texture2DU5BU5D_t149664596* ___fingerTextures_58;
	// UnityEngine.Texture2D[] MiniGUI::helpTextures
	Texture2DU5BU5D_t149664596* ___helpTextures_59;
	// UnityEngine.Texture2D MiniGUI::topLogoTexture
	Texture2D_t3840446185 * ___topLogoTexture_60;
	// UnityEngine.Rect MiniGUI::aboutPanelRect
	Rect_t2360479859  ___aboutPanelRect_61;
	// UnityEngine.Rect MiniGUI::aboutPanelRectGroup
	Rect_t2360479859  ___aboutPanelRectGroup_62;
	// UnityEngine.Rect MiniGUI::helpPanelRectGroup
	Rect_t2360479859  ___helpPanelRectGroup_63;
	// UnityEngine.Rect MiniGUI::helpTitleRect
	Rect_t2360479859  ___helpTitleRect_64;
	// UnityEngine.Rect MiniGUI::helpTextRect
	Rect_t2360479859  ___helpTextRect_65;
	// UnityEngine.Rect MiniGUI::helpTextureRect
	Rect_t2360479859  ___helpTextureRect_66;
	// UnityEngine.Rect MiniGUI::grayBoxRect
	Rect_t2360479859  ___grayBoxRect_67;
	// System.String MiniGUI::aboutText
	String_t* ___aboutText_68;
	// System.String MiniGUI::helpTitle
	String_t* ___helpTitle_69;
	// System.String[] MiniGUI::helpText
	StringU5BU5D_t1281789340* ___helpText_70;
	// UnityEngine.Vector2 MiniGUI::scrollPosition
	Vector2_t2156229523  ___scrollPosition_71;
	// UnityEngine.Vector2 MiniGUI::helpscrollPosition
	Vector2_t2156229523  ___helpscrollPosition_72;
	// System.Int32 MiniGUI::FingerID
	int32_t ___FingerID_73;

public:
	inline static int32_t get_offset_of_gph_2() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___gph_2)); }
	inline GUIPaintHandler_t437174526 * get_gph_2() const { return ___gph_2; }
	inline GUIPaintHandler_t437174526 ** get_address_of_gph_2() { return &___gph_2; }
	inline void set_gph_2(GUIPaintHandler_t437174526 * value)
	{
		___gph_2 = value;
		Il2CppCodeGenWriteBarrier((&___gph_2), value);
	}

	inline static int32_t get_offset_of_sel_3() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___sel_3)); }
	inline TopologySelector_t1922582608 * get_sel_3() const { return ___sel_3; }
	inline TopologySelector_t1922582608 ** get_address_of_sel_3() { return &___sel_3; }
	inline void set_sel_3(TopologySelector_t1922582608 * value)
	{
		___sel_3 = value;
		Il2CppCodeGenWriteBarrier((&___sel_3), value);
	}

	inline static int32_t get_offset_of_gc_4() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___gc_4)); }
	inline GameController_t2330501625 * get_gc_4() const { return ___gc_4; }
	inline GameController_t2330501625 ** get_address_of_gc_4() { return &___gc_4; }
	inline void set_gc_4(GameController_t2330501625 * value)
	{
		___gc_4 = value;
		Il2CppCodeGenWriteBarrier((&___gc_4), value);
	}

	inline static int32_t get_offset_of_cm_5() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___cm_5)); }
	inline CanvasMesh_t1528759999 * get_cm_5() const { return ___cm_5; }
	inline CanvasMesh_t1528759999 ** get_address_of_cm_5() { return &___cm_5; }
	inline void set_cm_5(CanvasMesh_t1528759999 * value)
	{
		___cm_5 = value;
		Il2CppCodeGenWriteBarrier((&___cm_5), value);
	}

	inline static int32_t get_offset_of_cbg_6() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___cbg_6)); }
	inline ColorBarGUI_t1806409552 * get_cbg_6() const { return ___cbg_6; }
	inline ColorBarGUI_t1806409552 ** get_address_of_cbg_6() { return &___cbg_6; }
	inline void set_cbg_6(ColorBarGUI_t1806409552 * value)
	{
		___cbg_6 = value;
		Il2CppCodeGenWriteBarrier((&___cbg_6), value);
	}

	inline static int32_t get_offset_of_camera_7() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___camera_7)); }
	inline Camera_t4157153871 * get_camera_7() const { return ___camera_7; }
	inline Camera_t4157153871 ** get_address_of_camera_7() { return &___camera_7; }
	inline void set_camera_7(Camera_t4157153871 * value)
	{
		___camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___camera_7), value);
	}

	inline static int32_t get_offset_of_Background_canvas_8() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___Background_canvas_8)); }
	inline Canvas_t3310196443 * get_Background_canvas_8() const { return ___Background_canvas_8; }
	inline Canvas_t3310196443 ** get_address_of_Background_canvas_8() { return &___Background_canvas_8; }
	inline void set_Background_canvas_8(Canvas_t3310196443 * value)
	{
		___Background_canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___Background_canvas_8), value);
	}

	inline static int32_t get_offset_of_Background_graph_9() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___Background_graph_9)); }
	inline Image_t2670269651 * get_Background_graph_9() const { return ___Background_graph_9; }
	inline Image_t2670269651 ** get_address_of_Background_graph_9() { return &___Background_graph_9; }
	inline void set_Background_graph_9(Image_t2670269651 * value)
	{
		___Background_graph_9 = value;
		Il2CppCodeGenWriteBarrier((&___Background_graph_9), value);
	}

	inline static int32_t get_offset_of_screenshot_10() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___screenshot_10)); }
	inline Screenshot_t3680149077 * get_screenshot_10() const { return ___screenshot_10; }
	inline Screenshot_t3680149077 ** get_address_of_screenshot_10() { return &___screenshot_10; }
	inline void set_screenshot_10(Screenshot_t3680149077 * value)
	{
		___screenshot_10 = value;
		Il2CppCodeGenWriteBarrier((&___screenshot_10), value);
	}

	inline static int32_t get_offset_of_selectedMenu_11() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___selectedMenu_11)); }
	inline String_t* get_selectedMenu_11() const { return ___selectedMenu_11; }
	inline String_t** get_address_of_selectedMenu_11() { return &___selectedMenu_11; }
	inline void set_selectedMenu_11(String_t* value)
	{
		___selectedMenu_11 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMenu_11), value);
	}

	inline static int32_t get_offset_of_selectedTool_12() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___selectedTool_12)); }
	inline String_t* get_selectedTool_12() const { return ___selectedTool_12; }
	inline String_t** get_address_of_selectedTool_12() { return &___selectedTool_12; }
	inline void set_selectedTool_12(String_t* value)
	{
		___selectedTool_12 = value;
		Il2CppCodeGenWriteBarrier((&___selectedTool_12), value);
	}

	inline static int32_t get_offset_of_selectedMaterial_13() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___selectedMaterial_13)); }
	inline String_t* get_selectedMaterial_13() const { return ___selectedMaterial_13; }
	inline String_t** get_address_of_selectedMaterial_13() { return &___selectedMaterial_13; }
	inline void set_selectedMaterial_13(String_t* value)
	{
		___selectedMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___selectedMaterial_13), value);
	}

	inline static int32_t get_offset_of_selectedBackground_14() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___selectedBackground_14)); }
	inline String_t* get_selectedBackground_14() const { return ___selectedBackground_14; }
	inline String_t** get_address_of_selectedBackground_14() { return &___selectedBackground_14; }
	inline void set_selectedBackground_14(String_t* value)
	{
		___selectedBackground_14 = value;
		Il2CppCodeGenWriteBarrier((&___selectedBackground_14), value);
	}

	inline static int32_t get_offset_of_plotType_15() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___plotType_15)); }
	inline String_t* get_plotType_15() const { return ___plotType_15; }
	inline String_t** get_address_of_plotType_15() { return &___plotType_15; }
	inline void set_plotType_15(String_t* value)
	{
		___plotType_15 = value;
		Il2CppCodeGenWriteBarrier((&___plotType_15), value);
	}

	inline static int32_t get_offset_of_meshLevel_16() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___meshLevel_16)); }
	inline String_t* get_meshLevel_16() const { return ___meshLevel_16; }
	inline String_t** get_address_of_meshLevel_16() { return &___meshLevel_16; }
	inline void set_meshLevel_16(String_t* value)
	{
		___meshLevel_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshLevel_16), value);
	}

	inline static int32_t get_offset_of_oldPlotChoice_17() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___oldPlotChoice_17)); }
	inline int32_t get_oldPlotChoice_17() const { return ___oldPlotChoice_17; }
	inline int32_t* get_address_of_oldPlotChoice_17() { return &___oldPlotChoice_17; }
	inline void set_oldPlotChoice_17(int32_t value)
	{
		___oldPlotChoice_17 = value;
	}

	inline static int32_t get_offset_of_selectedPenSize_18() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___selectedPenSize_18)); }
	inline int32_t get_selectedPenSize_18() const { return ___selectedPenSize_18; }
	inline int32_t* get_address_of_selectedPenSize_18() { return &___selectedPenSize_18; }
	inline void set_selectedPenSize_18(int32_t value)
	{
		___selectedPenSize_18 = value;
	}

	inline static int32_t get_offset_of_showDef_19() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___showDef_19)); }
	inline bool get_showDef_19() const { return ___showDef_19; }
	inline bool* get_address_of_showDef_19() { return &___showDef_19; }
	inline void set_showDef_19(bool value)
	{
		___showDef_19 = value;
	}

	inline static int32_t get_offset_of_nBottons_20() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___nBottons_20)); }
	inline float get_nBottons_20() const { return ___nBottons_20; }
	inline float* get_address_of_nBottons_20() { return &___nBottons_20; }
	inline void set_nBottons_20(float value)
	{
		___nBottons_20 = value;
	}

	inline static int32_t get_offset_of_nGUIChanges_21() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___nGUIChanges_21)); }
	inline int32_t get_nGUIChanges_21() const { return ___nGUIChanges_21; }
	inline int32_t* get_address_of_nGUIChanges_21() { return &___nGUIChanges_21; }
	inline void set_nGUIChanges_21(int32_t value)
	{
		___nGUIChanges_21 = value;
	}

	inline static int32_t get_offset_of_grayBox_22() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___grayBox_22)); }
	inline bool get_grayBox_22() const { return ___grayBox_22; }
	inline bool* get_address_of_grayBox_22() { return &___grayBox_22; }
	inline void set_grayBox_22(bool value)
	{
		___grayBox_22 = value;
	}

	inline static int32_t get_offset_of_is_gravity_23() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___is_gravity_23)); }
	inline bool get_is_gravity_23() const { return ___is_gravity_23; }
	inline bool* get_address_of_is_gravity_23() { return &___is_gravity_23; }
	inline void set_is_gravity_23(bool value)
	{
		___is_gravity_23 = value;
	}

	inline static int32_t get_offset_of_DisplayScreenshotMessage_24() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___DisplayScreenshotMessage_24)); }
	inline bool get_DisplayScreenshotMessage_24() const { return ___DisplayScreenshotMessage_24; }
	inline bool* get_address_of_DisplayScreenshotMessage_24() { return &___DisplayScreenshotMessage_24; }
	inline void set_DisplayScreenshotMessage_24(bool value)
	{
		___DisplayScreenshotMessage_24 = value;
	}

	inline static int32_t get_offset_of_MenuBlue_25() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___MenuBlue_25)); }
	inline Color_t2555686324  get_MenuBlue_25() const { return ___MenuBlue_25; }
	inline Color_t2555686324 * get_address_of_MenuBlue_25() { return &___MenuBlue_25; }
	inline void set_MenuBlue_25(Color_t2555686324  value)
	{
		___MenuBlue_25 = value;
	}

	inline static int32_t get_offset_of_topButton_26() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___topButton_26)); }
	inline Rect_t2360479859  get_topButton_26() const { return ___topButton_26; }
	inline Rect_t2360479859 * get_address_of_topButton_26() { return &___topButton_26; }
	inline void set_topButton_26(Rect_t2360479859  value)
	{
		___topButton_26 = value;
	}

	inline static int32_t get_offset_of_menuButton_27() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___menuButton_27)); }
	inline Rect_t2360479859  get_menuButton_27() const { return ___menuButton_27; }
	inline Rect_t2360479859 * get_address_of_menuButton_27() { return &___menuButton_27; }
	inline void set_menuButton_27(Rect_t2360479859  value)
	{
		___menuButton_27 = value;
	}

	inline static int32_t get_offset_of_subMenuButton_28() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___subMenuButton_28)); }
	inline Rect_t2360479859  get_subMenuButton_28() const { return ___subMenuButton_28; }
	inline Rect_t2360479859 * get_address_of_subMenuButton_28() { return &___subMenuButton_28; }
	inline void set_subMenuButton_28(Rect_t2360479859  value)
	{
		___subMenuButton_28 = value;
	}

	inline static int32_t get_offset_of_showTool_29() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___showTool_29)); }
	inline Rect_t2360479859  get_showTool_29() const { return ___showTool_29; }
	inline Rect_t2360479859 * get_address_of_showTool_29() { return &___showTool_29; }
	inline void set_showTool_29(Rect_t2360479859  value)
	{
		___showTool_29 = value;
	}

	inline static int32_t get_offset_of_showLogo_30() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___showLogo_30)); }
	inline Rect_t2360479859  get_showLogo_30() const { return ___showLogo_30; }
	inline Rect_t2360479859 * get_address_of_showLogo_30() { return &___showLogo_30; }
	inline void set_showLogo_30(Rect_t2360479859  value)
	{
		___showLogo_30 = value;
	}

	inline static int32_t get_offset_of_colorBarPos_31() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___colorBarPos_31)); }
	inline Rect_t2360479859  get_colorBarPos_31() const { return ___colorBarPos_31; }
	inline Rect_t2360479859 * get_address_of_colorBarPos_31() { return &___colorBarPos_31; }
	inline void set_colorBarPos_31(Rect_t2360479859  value)
	{
		___colorBarPos_31 = value;
	}

	inline static int32_t get_offset_of_messagePos_32() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___messagePos_32)); }
	inline Rect_t2360479859  get_messagePos_32() const { return ___messagePos_32; }
	inline Rect_t2360479859 * get_address_of_messagePos_32() { return &___messagePos_32; }
	inline void set_messagePos_32(Rect_t2360479859  value)
	{
		___messagePos_32 = value;
	}

	inline static int32_t get_offset_of_warningPos_33() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___warningPos_33)); }
	inline Rect_t2360479859  get_warningPos_33() const { return ___warningPos_33; }
	inline Rect_t2360479859 * get_address_of_warningPos_33() { return &___warningPos_33; }
	inline void set_warningPos_33(Rect_t2360479859  value)
	{
		___warningPos_33 = value;
	}

	inline static int32_t get_offset_of_fac_34() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___fac_34)); }
	inline int32_t get_fac_34() const { return ___fac_34; }
	inline int32_t* get_address_of_fac_34() { return &___fac_34; }
	inline void set_fac_34(int32_t value)
	{
		___fac_34 = value;
	}

	inline static int32_t get_offset_of_NewTopology_35() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___NewTopology_35)); }
	inline EventHandler_t1348719766 * get_NewTopology_35() const { return ___NewTopology_35; }
	inline EventHandler_t1348719766 ** get_address_of_NewTopology_35() { return &___NewTopology_35; }
	inline void set_NewTopology_35(EventHandler_t1348719766 * value)
	{
		___NewTopology_35 = value;
		Il2CppCodeGenWriteBarrier((&___NewTopology_35), value);
	}

	inline static int32_t get_offset_of_TooltipStyle_36() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___TooltipStyle_36)); }
	inline GUIStyle_t3956901511 * get_TooltipStyle_36() const { return ___TooltipStyle_36; }
	inline GUIStyle_t3956901511 ** get_address_of_TooltipStyle_36() { return &___TooltipStyle_36; }
	inline void set_TooltipStyle_36(GUIStyle_t3956901511 * value)
	{
		___TooltipStyle_36 = value;
		Il2CppCodeGenWriteBarrier((&___TooltipStyle_36), value);
	}

	inline static int32_t get_offset_of_buttonStyle_37() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___buttonStyle_37)); }
	inline GUIStyle_t3956901511 * get_buttonStyle_37() const { return ___buttonStyle_37; }
	inline GUIStyle_t3956901511 ** get_address_of_buttonStyle_37() { return &___buttonStyle_37; }
	inline void set_buttonStyle_37(GUIStyle_t3956901511 * value)
	{
		___buttonStyle_37 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStyle_37), value);
	}

	inline static int32_t get_offset_of_labelStyle_38() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___labelStyle_38)); }
	inline GUIStyle_t3956901511 * get_labelStyle_38() const { return ___labelStyle_38; }
	inline GUIStyle_t3956901511 ** get_address_of_labelStyle_38() { return &___labelStyle_38; }
	inline void set_labelStyle_38(GUIStyle_t3956901511 * value)
	{
		___labelStyle_38 = value;
		Il2CppCodeGenWriteBarrier((&___labelStyle_38), value);
	}

	inline static int32_t get_offset_of_messageStyle_39() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___messageStyle_39)); }
	inline GUIStyle_t3956901511 * get_messageStyle_39() const { return ___messageStyle_39; }
	inline GUIStyle_t3956901511 ** get_address_of_messageStyle_39() { return &___messageStyle_39; }
	inline void set_messageStyle_39(GUIStyle_t3956901511 * value)
	{
		___messageStyle_39 = value;
		Il2CppCodeGenWriteBarrier((&___messageStyle_39), value);
	}

	inline static int32_t get_offset_of_DrawTooltipStyle_40() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___DrawTooltipStyle_40)); }
	inline GUIStyle_t3956901511 * get_DrawTooltipStyle_40() const { return ___DrawTooltipStyle_40; }
	inline GUIStyle_t3956901511 ** get_address_of_DrawTooltipStyle_40() { return &___DrawTooltipStyle_40; }
	inline void set_DrawTooltipStyle_40(GUIStyle_t3956901511 * value)
	{
		___DrawTooltipStyle_40 = value;
		Il2CppCodeGenWriteBarrier((&___DrawTooltipStyle_40), value);
	}

	inline static int32_t get_offset_of_aboutTextStyle_41() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___aboutTextStyle_41)); }
	inline GUIStyle_t3956901511 * get_aboutTextStyle_41() const { return ___aboutTextStyle_41; }
	inline GUIStyle_t3956901511 ** get_address_of_aboutTextStyle_41() { return &___aboutTextStyle_41; }
	inline void set_aboutTextStyle_41(GUIStyle_t3956901511 * value)
	{
		___aboutTextStyle_41 = value;
		Il2CppCodeGenWriteBarrier((&___aboutTextStyle_41), value);
	}

	inline static int32_t get_offset_of_linkStyle_42() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___linkStyle_42)); }
	inline GUIStyle_t3956901511 * get_linkStyle_42() const { return ___linkStyle_42; }
	inline GUIStyle_t3956901511 ** get_address_of_linkStyle_42() { return &___linkStyle_42; }
	inline void set_linkStyle_42(GUIStyle_t3956901511 * value)
	{
		___linkStyle_42 = value;
		Il2CppCodeGenWriteBarrier((&___linkStyle_42), value);
	}

	inline static int32_t get_offset_of_bcToolsMenu_53() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___bcToolsMenu_53)); }
	inline MenuU5BU5D_t2527869020* get_bcToolsMenu_53() const { return ___bcToolsMenu_53; }
	inline MenuU5BU5D_t2527869020** get_address_of_bcToolsMenu_53() { return &___bcToolsMenu_53; }
	inline void set_bcToolsMenu_53(MenuU5BU5D_t2527869020* value)
	{
		___bcToolsMenu_53 = value;
		Il2CppCodeGenWriteBarrier((&___bcToolsMenu_53), value);
	}

	inline static int32_t get_offset_of_fingerToolsMenu_54() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___fingerToolsMenu_54)); }
	inline MenuU5BU5D_t2527869020* get_fingerToolsMenu_54() const { return ___fingerToolsMenu_54; }
	inline MenuU5BU5D_t2527869020** get_address_of_fingerToolsMenu_54() { return &___fingerToolsMenu_54; }
	inline void set_fingerToolsMenu_54(MenuU5BU5D_t2527869020* value)
	{
		___fingerToolsMenu_54 = value;
		Il2CppCodeGenWriteBarrier((&___fingerToolsMenu_54), value);
	}

	inline static int32_t get_offset_of_mainMenuTextures_55() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___mainMenuTextures_55)); }
	inline Texture2DU5BU5D_t149664596* get_mainMenuTextures_55() const { return ___mainMenuTextures_55; }
	inline Texture2DU5BU5D_t149664596** get_address_of_mainMenuTextures_55() { return &___mainMenuTextures_55; }
	inline void set_mainMenuTextures_55(Texture2DU5BU5D_t149664596* value)
	{
		___mainMenuTextures_55 = value;
		Il2CppCodeGenWriteBarrier((&___mainMenuTextures_55), value);
	}

	inline static int32_t get_offset_of_drawTextures_56() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___drawTextures_56)); }
	inline Texture2DU5BU5D_t149664596* get_drawTextures_56() const { return ___drawTextures_56; }
	inline Texture2DU5BU5D_t149664596** get_address_of_drawTextures_56() { return &___drawTextures_56; }
	inline void set_drawTextures_56(Texture2DU5BU5D_t149664596* value)
	{
		___drawTextures_56 = value;
		Il2CppCodeGenWriteBarrier((&___drawTextures_56), value);
	}

	inline static int32_t get_offset_of_bcTextures_57() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___bcTextures_57)); }
	inline Texture2DU5BU5D_t149664596* get_bcTextures_57() const { return ___bcTextures_57; }
	inline Texture2DU5BU5D_t149664596** get_address_of_bcTextures_57() { return &___bcTextures_57; }
	inline void set_bcTextures_57(Texture2DU5BU5D_t149664596* value)
	{
		___bcTextures_57 = value;
		Il2CppCodeGenWriteBarrier((&___bcTextures_57), value);
	}

	inline static int32_t get_offset_of_fingerTextures_58() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___fingerTextures_58)); }
	inline Texture2DU5BU5D_t149664596* get_fingerTextures_58() const { return ___fingerTextures_58; }
	inline Texture2DU5BU5D_t149664596** get_address_of_fingerTextures_58() { return &___fingerTextures_58; }
	inline void set_fingerTextures_58(Texture2DU5BU5D_t149664596* value)
	{
		___fingerTextures_58 = value;
		Il2CppCodeGenWriteBarrier((&___fingerTextures_58), value);
	}

	inline static int32_t get_offset_of_helpTextures_59() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpTextures_59)); }
	inline Texture2DU5BU5D_t149664596* get_helpTextures_59() const { return ___helpTextures_59; }
	inline Texture2DU5BU5D_t149664596** get_address_of_helpTextures_59() { return &___helpTextures_59; }
	inline void set_helpTextures_59(Texture2DU5BU5D_t149664596* value)
	{
		___helpTextures_59 = value;
		Il2CppCodeGenWriteBarrier((&___helpTextures_59), value);
	}

	inline static int32_t get_offset_of_topLogoTexture_60() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___topLogoTexture_60)); }
	inline Texture2D_t3840446185 * get_topLogoTexture_60() const { return ___topLogoTexture_60; }
	inline Texture2D_t3840446185 ** get_address_of_topLogoTexture_60() { return &___topLogoTexture_60; }
	inline void set_topLogoTexture_60(Texture2D_t3840446185 * value)
	{
		___topLogoTexture_60 = value;
		Il2CppCodeGenWriteBarrier((&___topLogoTexture_60), value);
	}

	inline static int32_t get_offset_of_aboutPanelRect_61() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___aboutPanelRect_61)); }
	inline Rect_t2360479859  get_aboutPanelRect_61() const { return ___aboutPanelRect_61; }
	inline Rect_t2360479859 * get_address_of_aboutPanelRect_61() { return &___aboutPanelRect_61; }
	inline void set_aboutPanelRect_61(Rect_t2360479859  value)
	{
		___aboutPanelRect_61 = value;
	}

	inline static int32_t get_offset_of_aboutPanelRectGroup_62() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___aboutPanelRectGroup_62)); }
	inline Rect_t2360479859  get_aboutPanelRectGroup_62() const { return ___aboutPanelRectGroup_62; }
	inline Rect_t2360479859 * get_address_of_aboutPanelRectGroup_62() { return &___aboutPanelRectGroup_62; }
	inline void set_aboutPanelRectGroup_62(Rect_t2360479859  value)
	{
		___aboutPanelRectGroup_62 = value;
	}

	inline static int32_t get_offset_of_helpPanelRectGroup_63() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpPanelRectGroup_63)); }
	inline Rect_t2360479859  get_helpPanelRectGroup_63() const { return ___helpPanelRectGroup_63; }
	inline Rect_t2360479859 * get_address_of_helpPanelRectGroup_63() { return &___helpPanelRectGroup_63; }
	inline void set_helpPanelRectGroup_63(Rect_t2360479859  value)
	{
		___helpPanelRectGroup_63 = value;
	}

	inline static int32_t get_offset_of_helpTitleRect_64() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpTitleRect_64)); }
	inline Rect_t2360479859  get_helpTitleRect_64() const { return ___helpTitleRect_64; }
	inline Rect_t2360479859 * get_address_of_helpTitleRect_64() { return &___helpTitleRect_64; }
	inline void set_helpTitleRect_64(Rect_t2360479859  value)
	{
		___helpTitleRect_64 = value;
	}

	inline static int32_t get_offset_of_helpTextRect_65() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpTextRect_65)); }
	inline Rect_t2360479859  get_helpTextRect_65() const { return ___helpTextRect_65; }
	inline Rect_t2360479859 * get_address_of_helpTextRect_65() { return &___helpTextRect_65; }
	inline void set_helpTextRect_65(Rect_t2360479859  value)
	{
		___helpTextRect_65 = value;
	}

	inline static int32_t get_offset_of_helpTextureRect_66() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpTextureRect_66)); }
	inline Rect_t2360479859  get_helpTextureRect_66() const { return ___helpTextureRect_66; }
	inline Rect_t2360479859 * get_address_of_helpTextureRect_66() { return &___helpTextureRect_66; }
	inline void set_helpTextureRect_66(Rect_t2360479859  value)
	{
		___helpTextureRect_66 = value;
	}

	inline static int32_t get_offset_of_grayBoxRect_67() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___grayBoxRect_67)); }
	inline Rect_t2360479859  get_grayBoxRect_67() const { return ___grayBoxRect_67; }
	inline Rect_t2360479859 * get_address_of_grayBoxRect_67() { return &___grayBoxRect_67; }
	inline void set_grayBoxRect_67(Rect_t2360479859  value)
	{
		___grayBoxRect_67 = value;
	}

	inline static int32_t get_offset_of_aboutText_68() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___aboutText_68)); }
	inline String_t* get_aboutText_68() const { return ___aboutText_68; }
	inline String_t** get_address_of_aboutText_68() { return &___aboutText_68; }
	inline void set_aboutText_68(String_t* value)
	{
		___aboutText_68 = value;
		Il2CppCodeGenWriteBarrier((&___aboutText_68), value);
	}

	inline static int32_t get_offset_of_helpTitle_69() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpTitle_69)); }
	inline String_t* get_helpTitle_69() const { return ___helpTitle_69; }
	inline String_t** get_address_of_helpTitle_69() { return &___helpTitle_69; }
	inline void set_helpTitle_69(String_t* value)
	{
		___helpTitle_69 = value;
		Il2CppCodeGenWriteBarrier((&___helpTitle_69), value);
	}

	inline static int32_t get_offset_of_helpText_70() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpText_70)); }
	inline StringU5BU5D_t1281789340* get_helpText_70() const { return ___helpText_70; }
	inline StringU5BU5D_t1281789340** get_address_of_helpText_70() { return &___helpText_70; }
	inline void set_helpText_70(StringU5BU5D_t1281789340* value)
	{
		___helpText_70 = value;
		Il2CppCodeGenWriteBarrier((&___helpText_70), value);
	}

	inline static int32_t get_offset_of_scrollPosition_71() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___scrollPosition_71)); }
	inline Vector2_t2156229523  get_scrollPosition_71() const { return ___scrollPosition_71; }
	inline Vector2_t2156229523 * get_address_of_scrollPosition_71() { return &___scrollPosition_71; }
	inline void set_scrollPosition_71(Vector2_t2156229523  value)
	{
		___scrollPosition_71 = value;
	}

	inline static int32_t get_offset_of_helpscrollPosition_72() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___helpscrollPosition_72)); }
	inline Vector2_t2156229523  get_helpscrollPosition_72() const { return ___helpscrollPosition_72; }
	inline Vector2_t2156229523 * get_address_of_helpscrollPosition_72() { return &___helpscrollPosition_72; }
	inline void set_helpscrollPosition_72(Vector2_t2156229523  value)
	{
		___helpscrollPosition_72 = value;
	}

	inline static int32_t get_offset_of_FingerID_73() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967, ___FingerID_73)); }
	inline int32_t get_FingerID_73() const { return ___FingerID_73; }
	inline int32_t* get_address_of_FingerID_73() { return &___FingerID_73; }
	inline void set_FingerID_73(int32_t value)
	{
		___FingerID_73 = value;
	}
};

struct MiniGUI_t213183967_StaticFields
{
public:
	// Menu MiniGUI::selectedBcTool
	int32_t ___selectedBcTool_51;
	// Menu MiniGUI::selectedBcMenu
	int32_t ___selectedBcMenu_52;

public:
	inline static int32_t get_offset_of_selectedBcTool_51() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967_StaticFields, ___selectedBcTool_51)); }
	inline int32_t get_selectedBcTool_51() const { return ___selectedBcTool_51; }
	inline int32_t* get_address_of_selectedBcTool_51() { return &___selectedBcTool_51; }
	inline void set_selectedBcTool_51(int32_t value)
	{
		___selectedBcTool_51 = value;
	}

	inline static int32_t get_offset_of_selectedBcMenu_52() { return static_cast<int32_t>(offsetof(MiniGUI_t213183967_StaticFields, ___selectedBcMenu_52)); }
	inline int32_t get_selectedBcMenu_52() const { return ___selectedBcMenu_52; }
	inline int32_t* get_address_of_selectedBcMenu_52() { return &___selectedBcMenu_52; }
	inline void set_selectedBcMenu_52(int32_t value)
	{
		___selectedBcMenu_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIGUI_T213183967_H
#ifndef SCREENSHOT_T3680149077_H
#define SCREENSHOT_T3680149077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Screenshot
struct  Screenshot_t3680149077  : public MonoBehaviour_t3962482529
{
public:
	// MiniGUI Screenshot::mgui
	MiniGUI_t213183967 * ___mgui_2;
	// UnityEngine.UI.Image Screenshot::Flash_image
	Image_t2670269651 * ___Flash_image_3;
	// System.Boolean Screenshot::isFlash
	bool ___isFlash_4;
	// UnityEngine.Color Screenshot::FlashCol
	Color_t2555686324  ___FlashCol_5;

public:
	inline static int32_t get_offset_of_mgui_2() { return static_cast<int32_t>(offsetof(Screenshot_t3680149077, ___mgui_2)); }
	inline MiniGUI_t213183967 * get_mgui_2() const { return ___mgui_2; }
	inline MiniGUI_t213183967 ** get_address_of_mgui_2() { return &___mgui_2; }
	inline void set_mgui_2(MiniGUI_t213183967 * value)
	{
		___mgui_2 = value;
		Il2CppCodeGenWriteBarrier((&___mgui_2), value);
	}

	inline static int32_t get_offset_of_Flash_image_3() { return static_cast<int32_t>(offsetof(Screenshot_t3680149077, ___Flash_image_3)); }
	inline Image_t2670269651 * get_Flash_image_3() const { return ___Flash_image_3; }
	inline Image_t2670269651 ** get_address_of_Flash_image_3() { return &___Flash_image_3; }
	inline void set_Flash_image_3(Image_t2670269651 * value)
	{
		___Flash_image_3 = value;
		Il2CppCodeGenWriteBarrier((&___Flash_image_3), value);
	}

	inline static int32_t get_offset_of_isFlash_4() { return static_cast<int32_t>(offsetof(Screenshot_t3680149077, ___isFlash_4)); }
	inline bool get_isFlash_4() const { return ___isFlash_4; }
	inline bool* get_address_of_isFlash_4() { return &___isFlash_4; }
	inline void set_isFlash_4(bool value)
	{
		___isFlash_4 = value;
	}

	inline static int32_t get_offset_of_FlashCol_5() { return static_cast<int32_t>(offsetof(Screenshot_t3680149077, ___FlashCol_5)); }
	inline Color_t2555686324  get_FlashCol_5() const { return ___FlashCol_5; }
	inline Color_t2555686324 * get_address_of_FlashCol_5() { return &___FlashCol_5; }
	inline void set_FlashCol_5(Color_t2555686324  value)
	{
		___FlashCol_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOT_T3680149077_H
#ifndef SCREENROTATION_T1262145093_H
#define SCREENROTATION_T1262145093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenRotation
struct  ScreenRotation_t1262145093  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ScreenRotation::autoRotate
	bool ___autoRotate_2;

public:
	inline static int32_t get_offset_of_autoRotate_2() { return static_cast<int32_t>(offsetof(ScreenRotation_t1262145093, ___autoRotate_2)); }
	inline bool get_autoRotate_2() const { return ___autoRotate_2; }
	inline bool* get_address_of_autoRotate_2() { return &___autoRotate_2; }
	inline void set_autoRotate_2(bool value)
	{
		___autoRotate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENROTATION_T1262145093_H
#ifndef GAMECONTROLLER_T2330501625_H
#define GAMECONTROLLER_T2330501625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t2330501625  : public MonoBehaviour_t3962482529
{
public:
	// Level GameController::level
	Level_t2237665516 * ___level_2;
	// PainterCanvas GameController::paintCanvas
	PainterCanvas_t4160673718 * ___paintCanvas_3;
	// GameGUI GameController::gameGUI
	GameGUI_t2956616616 * ___gameGUI_4;
	// ThreadRunner`2<System.Int32,FEAGameResult> GameController::VIB_thread
	ThreadRunner_2_t2510904282 * ___VIB_thread_5;
	// ThreadRunner`2<FEAGameData,FEAGameResult> GameController::threadRunner
	ThreadRunner_2_t3453383158 * ___threadRunner_6;
	// FEA GameController::FiniteElementAnalysis
	FEA_t3250963177 * ___FiniteElementAnalysis_7;
	// FEAVibrate GameController::FEA_VIB
	FEAVibrate_t3015876168 * ___FEA_VIB_8;
	// GameController/NotifyEvaluationUpdate GameController::GameResultUpdates
	NotifyEvaluationUpdate_t2480692004 * ___GameResultUpdates_9;
	// UnityEngine.GameObject GameController::prefabSlidingSupport
	GameObject_t1113636619 * ___prefabSlidingSupport_10;
	// UnityEngine.GameObject GameController::prefabFixedSupport
	GameObject_t1113636619 * ___prefabFixedSupport_11;
	// UnityEngine.GameObject GameController::prefabDispl
	GameObject_t1113636619 * ___prefabDispl_12;
	// UnityEngine.GameObject GameController::prefabDisplEnd
	GameObject_t1113636619 * ___prefabDisplEnd_13;
	// UnityEngine.GameObject GameController::prefabDispl2
	GameObject_t1113636619 * ___prefabDispl2_14;
	// UnityEngine.GameObject GameController::prefabDisplEnd2
	GameObject_t1113636619 * ___prefabDisplEnd2_15;
	// System.Int32 GameController::plotChoice
	int32_t ___plotChoice_16;
	// System.Single GameController::Emax
	float ___Emax_17;
	// System.Int32 GameController::noOfActiveBC
	int32_t ___noOfActiveBC_18;
	// System.Int32 GameController::noOfActiveDispl
	int32_t ___noOfActiveDispl_19;
	// System.Int32 GameController::noOfActiveDispl2
	int32_t ___noOfActiveDispl2_20;
	// System.Int32 GameController::noOfDispls2
	int32_t ___noOfDispls2_23;
	// System.Int32 GameController::noOfDisplEnds2
	int32_t ___noOfDisplEnds2_24;
	// System.Int32 GameController::noOfDispls
	int32_t ___noOfDispls_25;
	// System.Int32 GameController::noOfDisplEnds
	int32_t ___noOfDisplEnds_26;
	// FEAGameResult GameController::VIBres
	FEAGameResult_t3745188880 * ___VIBres_27;
	// FEAGameData GameController::VIBinp
	FEAGameData_t1628373741 * ___VIBinp_28;
	// System.Int32 GameController::VIB_count
	int32_t ___VIB_count_29;
	// System.Boolean GameController::VIB_running
	bool ___VIB_running_30;
	// System.Boolean GameController::IsVIB
	bool ___IsVIB_31;
	// System.Boolean GameController::VIB_enabled
	bool ___VIB_enabled_32;
	// System.Boolean GameController::VIB_gravity
	bool ___VIB_gravity_33;
	// System.Boolean GameController::big_finger
	bool ___big_finger_34;
	// System.Boolean GameController::ortho_move
	bool ___ortho_move_35;
	// System.Boolean GameController::tos
	bool ___tos_36;
	// System.Boolean GameController::NewVIBData
	bool ___NewVIBData_37;
	// System.Boolean GameController::HasSupports
	bool ___HasSupports_38;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameController::supports
	List_1_t2585711361 * ___supports_39;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameController::displs
	List_1_t2585711361 * ___displs_40;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameController::displEnds
	List_1_t2585711361 * ___displEnds_41;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameController::displs2
	List_1_t2585711361 * ___displs2_42;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameController::displEnds2
	List_1_t2585711361 * ___displEnds2_43;
	// UnityEngine.Vector2[] GameController::realSupPos
	Vector2U5BU5D_t1457185986* ___realSupPos_44;
	// System.Int64 GameController::requestId
	int64_t ___requestId_45;
	// System.Int32 GameController::timedShowDisp
	int32_t ___timedShowDisp_46;
	// FEAComponentData[] GameController::VIBsup
	FEAComponentDataU5BU5D_t1047043642* ___VIBsup_47;
	// WorkingAnimation GameController::workingAnimation
	WorkingAnimation_t2928320557 * ___workingAnimation_48;
	// CanvasMesh GameController::cm
	CanvasMesh_t1528759999 * ___cm_49;
	// GUIPaintHandler GameController::gph
	GUIPaintHandler_t437174526 * ___gph_50;
	// TopologySelector GameController::ts
	TopologySelector_t1922582608 * ___ts_51;
	// ColorBarGUI GameController::gbg
	ColorBarGUI_t1806409552 * ___gbg_52;
	// MiniGUI GameController::miniGUI
	MiniGUI_t213183967 * ___miniGUI_53;
	// System.Int32 GameController::debug
	int32_t ___debug_54;

public:
	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___level_2)); }
	inline Level_t2237665516 * get_level_2() const { return ___level_2; }
	inline Level_t2237665516 ** get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(Level_t2237665516 * value)
	{
		___level_2 = value;
		Il2CppCodeGenWriteBarrier((&___level_2), value);
	}

	inline static int32_t get_offset_of_paintCanvas_3() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___paintCanvas_3)); }
	inline PainterCanvas_t4160673718 * get_paintCanvas_3() const { return ___paintCanvas_3; }
	inline PainterCanvas_t4160673718 ** get_address_of_paintCanvas_3() { return &___paintCanvas_3; }
	inline void set_paintCanvas_3(PainterCanvas_t4160673718 * value)
	{
		___paintCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___paintCanvas_3), value);
	}

	inline static int32_t get_offset_of_gameGUI_4() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___gameGUI_4)); }
	inline GameGUI_t2956616616 * get_gameGUI_4() const { return ___gameGUI_4; }
	inline GameGUI_t2956616616 ** get_address_of_gameGUI_4() { return &___gameGUI_4; }
	inline void set_gameGUI_4(GameGUI_t2956616616 * value)
	{
		___gameGUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameGUI_4), value);
	}

	inline static int32_t get_offset_of_VIB_thread_5() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIB_thread_5)); }
	inline ThreadRunner_2_t2510904282 * get_VIB_thread_5() const { return ___VIB_thread_5; }
	inline ThreadRunner_2_t2510904282 ** get_address_of_VIB_thread_5() { return &___VIB_thread_5; }
	inline void set_VIB_thread_5(ThreadRunner_2_t2510904282 * value)
	{
		___VIB_thread_5 = value;
		Il2CppCodeGenWriteBarrier((&___VIB_thread_5), value);
	}

	inline static int32_t get_offset_of_threadRunner_6() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___threadRunner_6)); }
	inline ThreadRunner_2_t3453383158 * get_threadRunner_6() const { return ___threadRunner_6; }
	inline ThreadRunner_2_t3453383158 ** get_address_of_threadRunner_6() { return &___threadRunner_6; }
	inline void set_threadRunner_6(ThreadRunner_2_t3453383158 * value)
	{
		___threadRunner_6 = value;
		Il2CppCodeGenWriteBarrier((&___threadRunner_6), value);
	}

	inline static int32_t get_offset_of_FiniteElementAnalysis_7() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___FiniteElementAnalysis_7)); }
	inline FEA_t3250963177 * get_FiniteElementAnalysis_7() const { return ___FiniteElementAnalysis_7; }
	inline FEA_t3250963177 ** get_address_of_FiniteElementAnalysis_7() { return &___FiniteElementAnalysis_7; }
	inline void set_FiniteElementAnalysis_7(FEA_t3250963177 * value)
	{
		___FiniteElementAnalysis_7 = value;
		Il2CppCodeGenWriteBarrier((&___FiniteElementAnalysis_7), value);
	}

	inline static int32_t get_offset_of_FEA_VIB_8() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___FEA_VIB_8)); }
	inline FEAVibrate_t3015876168 * get_FEA_VIB_8() const { return ___FEA_VIB_8; }
	inline FEAVibrate_t3015876168 ** get_address_of_FEA_VIB_8() { return &___FEA_VIB_8; }
	inline void set_FEA_VIB_8(FEAVibrate_t3015876168 * value)
	{
		___FEA_VIB_8 = value;
		Il2CppCodeGenWriteBarrier((&___FEA_VIB_8), value);
	}

	inline static int32_t get_offset_of_GameResultUpdates_9() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___GameResultUpdates_9)); }
	inline NotifyEvaluationUpdate_t2480692004 * get_GameResultUpdates_9() const { return ___GameResultUpdates_9; }
	inline NotifyEvaluationUpdate_t2480692004 ** get_address_of_GameResultUpdates_9() { return &___GameResultUpdates_9; }
	inline void set_GameResultUpdates_9(NotifyEvaluationUpdate_t2480692004 * value)
	{
		___GameResultUpdates_9 = value;
		Il2CppCodeGenWriteBarrier((&___GameResultUpdates_9), value);
	}

	inline static int32_t get_offset_of_prefabSlidingSupport_10() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___prefabSlidingSupport_10)); }
	inline GameObject_t1113636619 * get_prefabSlidingSupport_10() const { return ___prefabSlidingSupport_10; }
	inline GameObject_t1113636619 ** get_address_of_prefabSlidingSupport_10() { return &___prefabSlidingSupport_10; }
	inline void set_prefabSlidingSupport_10(GameObject_t1113636619 * value)
	{
		___prefabSlidingSupport_10 = value;
		Il2CppCodeGenWriteBarrier((&___prefabSlidingSupport_10), value);
	}

	inline static int32_t get_offset_of_prefabFixedSupport_11() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___prefabFixedSupport_11)); }
	inline GameObject_t1113636619 * get_prefabFixedSupport_11() const { return ___prefabFixedSupport_11; }
	inline GameObject_t1113636619 ** get_address_of_prefabFixedSupport_11() { return &___prefabFixedSupport_11; }
	inline void set_prefabFixedSupport_11(GameObject_t1113636619 * value)
	{
		___prefabFixedSupport_11 = value;
		Il2CppCodeGenWriteBarrier((&___prefabFixedSupport_11), value);
	}

	inline static int32_t get_offset_of_prefabDispl_12() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___prefabDispl_12)); }
	inline GameObject_t1113636619 * get_prefabDispl_12() const { return ___prefabDispl_12; }
	inline GameObject_t1113636619 ** get_address_of_prefabDispl_12() { return &___prefabDispl_12; }
	inline void set_prefabDispl_12(GameObject_t1113636619 * value)
	{
		___prefabDispl_12 = value;
		Il2CppCodeGenWriteBarrier((&___prefabDispl_12), value);
	}

	inline static int32_t get_offset_of_prefabDisplEnd_13() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___prefabDisplEnd_13)); }
	inline GameObject_t1113636619 * get_prefabDisplEnd_13() const { return ___prefabDisplEnd_13; }
	inline GameObject_t1113636619 ** get_address_of_prefabDisplEnd_13() { return &___prefabDisplEnd_13; }
	inline void set_prefabDisplEnd_13(GameObject_t1113636619 * value)
	{
		___prefabDisplEnd_13 = value;
		Il2CppCodeGenWriteBarrier((&___prefabDisplEnd_13), value);
	}

	inline static int32_t get_offset_of_prefabDispl2_14() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___prefabDispl2_14)); }
	inline GameObject_t1113636619 * get_prefabDispl2_14() const { return ___prefabDispl2_14; }
	inline GameObject_t1113636619 ** get_address_of_prefabDispl2_14() { return &___prefabDispl2_14; }
	inline void set_prefabDispl2_14(GameObject_t1113636619 * value)
	{
		___prefabDispl2_14 = value;
		Il2CppCodeGenWriteBarrier((&___prefabDispl2_14), value);
	}

	inline static int32_t get_offset_of_prefabDisplEnd2_15() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___prefabDisplEnd2_15)); }
	inline GameObject_t1113636619 * get_prefabDisplEnd2_15() const { return ___prefabDisplEnd2_15; }
	inline GameObject_t1113636619 ** get_address_of_prefabDisplEnd2_15() { return &___prefabDisplEnd2_15; }
	inline void set_prefabDisplEnd2_15(GameObject_t1113636619 * value)
	{
		___prefabDisplEnd2_15 = value;
		Il2CppCodeGenWriteBarrier((&___prefabDisplEnd2_15), value);
	}

	inline static int32_t get_offset_of_plotChoice_16() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___plotChoice_16)); }
	inline int32_t get_plotChoice_16() const { return ___plotChoice_16; }
	inline int32_t* get_address_of_plotChoice_16() { return &___plotChoice_16; }
	inline void set_plotChoice_16(int32_t value)
	{
		___plotChoice_16 = value;
	}

	inline static int32_t get_offset_of_Emax_17() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___Emax_17)); }
	inline float get_Emax_17() const { return ___Emax_17; }
	inline float* get_address_of_Emax_17() { return &___Emax_17; }
	inline void set_Emax_17(float value)
	{
		___Emax_17 = value;
	}

	inline static int32_t get_offset_of_noOfActiveBC_18() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfActiveBC_18)); }
	inline int32_t get_noOfActiveBC_18() const { return ___noOfActiveBC_18; }
	inline int32_t* get_address_of_noOfActiveBC_18() { return &___noOfActiveBC_18; }
	inline void set_noOfActiveBC_18(int32_t value)
	{
		___noOfActiveBC_18 = value;
	}

	inline static int32_t get_offset_of_noOfActiveDispl_19() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfActiveDispl_19)); }
	inline int32_t get_noOfActiveDispl_19() const { return ___noOfActiveDispl_19; }
	inline int32_t* get_address_of_noOfActiveDispl_19() { return &___noOfActiveDispl_19; }
	inline void set_noOfActiveDispl_19(int32_t value)
	{
		___noOfActiveDispl_19 = value;
	}

	inline static int32_t get_offset_of_noOfActiveDispl2_20() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfActiveDispl2_20)); }
	inline int32_t get_noOfActiveDispl2_20() const { return ___noOfActiveDispl2_20; }
	inline int32_t* get_address_of_noOfActiveDispl2_20() { return &___noOfActiveDispl2_20; }
	inline void set_noOfActiveDispl2_20(int32_t value)
	{
		___noOfActiveDispl2_20 = value;
	}

	inline static int32_t get_offset_of_noOfDispls2_23() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfDispls2_23)); }
	inline int32_t get_noOfDispls2_23() const { return ___noOfDispls2_23; }
	inline int32_t* get_address_of_noOfDispls2_23() { return &___noOfDispls2_23; }
	inline void set_noOfDispls2_23(int32_t value)
	{
		___noOfDispls2_23 = value;
	}

	inline static int32_t get_offset_of_noOfDisplEnds2_24() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfDisplEnds2_24)); }
	inline int32_t get_noOfDisplEnds2_24() const { return ___noOfDisplEnds2_24; }
	inline int32_t* get_address_of_noOfDisplEnds2_24() { return &___noOfDisplEnds2_24; }
	inline void set_noOfDisplEnds2_24(int32_t value)
	{
		___noOfDisplEnds2_24 = value;
	}

	inline static int32_t get_offset_of_noOfDispls_25() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfDispls_25)); }
	inline int32_t get_noOfDispls_25() const { return ___noOfDispls_25; }
	inline int32_t* get_address_of_noOfDispls_25() { return &___noOfDispls_25; }
	inline void set_noOfDispls_25(int32_t value)
	{
		___noOfDispls_25 = value;
	}

	inline static int32_t get_offset_of_noOfDisplEnds_26() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___noOfDisplEnds_26)); }
	inline int32_t get_noOfDisplEnds_26() const { return ___noOfDisplEnds_26; }
	inline int32_t* get_address_of_noOfDisplEnds_26() { return &___noOfDisplEnds_26; }
	inline void set_noOfDisplEnds_26(int32_t value)
	{
		___noOfDisplEnds_26 = value;
	}

	inline static int32_t get_offset_of_VIBres_27() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIBres_27)); }
	inline FEAGameResult_t3745188880 * get_VIBres_27() const { return ___VIBres_27; }
	inline FEAGameResult_t3745188880 ** get_address_of_VIBres_27() { return &___VIBres_27; }
	inline void set_VIBres_27(FEAGameResult_t3745188880 * value)
	{
		___VIBres_27 = value;
		Il2CppCodeGenWriteBarrier((&___VIBres_27), value);
	}

	inline static int32_t get_offset_of_VIBinp_28() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIBinp_28)); }
	inline FEAGameData_t1628373741 * get_VIBinp_28() const { return ___VIBinp_28; }
	inline FEAGameData_t1628373741 ** get_address_of_VIBinp_28() { return &___VIBinp_28; }
	inline void set_VIBinp_28(FEAGameData_t1628373741 * value)
	{
		___VIBinp_28 = value;
		Il2CppCodeGenWriteBarrier((&___VIBinp_28), value);
	}

	inline static int32_t get_offset_of_VIB_count_29() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIB_count_29)); }
	inline int32_t get_VIB_count_29() const { return ___VIB_count_29; }
	inline int32_t* get_address_of_VIB_count_29() { return &___VIB_count_29; }
	inline void set_VIB_count_29(int32_t value)
	{
		___VIB_count_29 = value;
	}

	inline static int32_t get_offset_of_VIB_running_30() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIB_running_30)); }
	inline bool get_VIB_running_30() const { return ___VIB_running_30; }
	inline bool* get_address_of_VIB_running_30() { return &___VIB_running_30; }
	inline void set_VIB_running_30(bool value)
	{
		___VIB_running_30 = value;
	}

	inline static int32_t get_offset_of_IsVIB_31() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___IsVIB_31)); }
	inline bool get_IsVIB_31() const { return ___IsVIB_31; }
	inline bool* get_address_of_IsVIB_31() { return &___IsVIB_31; }
	inline void set_IsVIB_31(bool value)
	{
		___IsVIB_31 = value;
	}

	inline static int32_t get_offset_of_VIB_enabled_32() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIB_enabled_32)); }
	inline bool get_VIB_enabled_32() const { return ___VIB_enabled_32; }
	inline bool* get_address_of_VIB_enabled_32() { return &___VIB_enabled_32; }
	inline void set_VIB_enabled_32(bool value)
	{
		___VIB_enabled_32 = value;
	}

	inline static int32_t get_offset_of_VIB_gravity_33() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIB_gravity_33)); }
	inline bool get_VIB_gravity_33() const { return ___VIB_gravity_33; }
	inline bool* get_address_of_VIB_gravity_33() { return &___VIB_gravity_33; }
	inline void set_VIB_gravity_33(bool value)
	{
		___VIB_gravity_33 = value;
	}

	inline static int32_t get_offset_of_big_finger_34() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___big_finger_34)); }
	inline bool get_big_finger_34() const { return ___big_finger_34; }
	inline bool* get_address_of_big_finger_34() { return &___big_finger_34; }
	inline void set_big_finger_34(bool value)
	{
		___big_finger_34 = value;
	}

	inline static int32_t get_offset_of_ortho_move_35() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___ortho_move_35)); }
	inline bool get_ortho_move_35() const { return ___ortho_move_35; }
	inline bool* get_address_of_ortho_move_35() { return &___ortho_move_35; }
	inline void set_ortho_move_35(bool value)
	{
		___ortho_move_35 = value;
	}

	inline static int32_t get_offset_of_tos_36() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___tos_36)); }
	inline bool get_tos_36() const { return ___tos_36; }
	inline bool* get_address_of_tos_36() { return &___tos_36; }
	inline void set_tos_36(bool value)
	{
		___tos_36 = value;
	}

	inline static int32_t get_offset_of_NewVIBData_37() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___NewVIBData_37)); }
	inline bool get_NewVIBData_37() const { return ___NewVIBData_37; }
	inline bool* get_address_of_NewVIBData_37() { return &___NewVIBData_37; }
	inline void set_NewVIBData_37(bool value)
	{
		___NewVIBData_37 = value;
	}

	inline static int32_t get_offset_of_HasSupports_38() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___HasSupports_38)); }
	inline bool get_HasSupports_38() const { return ___HasSupports_38; }
	inline bool* get_address_of_HasSupports_38() { return &___HasSupports_38; }
	inline void set_HasSupports_38(bool value)
	{
		___HasSupports_38 = value;
	}

	inline static int32_t get_offset_of_supports_39() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___supports_39)); }
	inline List_1_t2585711361 * get_supports_39() const { return ___supports_39; }
	inline List_1_t2585711361 ** get_address_of_supports_39() { return &___supports_39; }
	inline void set_supports_39(List_1_t2585711361 * value)
	{
		___supports_39 = value;
		Il2CppCodeGenWriteBarrier((&___supports_39), value);
	}

	inline static int32_t get_offset_of_displs_40() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___displs_40)); }
	inline List_1_t2585711361 * get_displs_40() const { return ___displs_40; }
	inline List_1_t2585711361 ** get_address_of_displs_40() { return &___displs_40; }
	inline void set_displs_40(List_1_t2585711361 * value)
	{
		___displs_40 = value;
		Il2CppCodeGenWriteBarrier((&___displs_40), value);
	}

	inline static int32_t get_offset_of_displEnds_41() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___displEnds_41)); }
	inline List_1_t2585711361 * get_displEnds_41() const { return ___displEnds_41; }
	inline List_1_t2585711361 ** get_address_of_displEnds_41() { return &___displEnds_41; }
	inline void set_displEnds_41(List_1_t2585711361 * value)
	{
		___displEnds_41 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds_41), value);
	}

	inline static int32_t get_offset_of_displs2_42() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___displs2_42)); }
	inline List_1_t2585711361 * get_displs2_42() const { return ___displs2_42; }
	inline List_1_t2585711361 ** get_address_of_displs2_42() { return &___displs2_42; }
	inline void set_displs2_42(List_1_t2585711361 * value)
	{
		___displs2_42 = value;
		Il2CppCodeGenWriteBarrier((&___displs2_42), value);
	}

	inline static int32_t get_offset_of_displEnds2_43() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___displEnds2_43)); }
	inline List_1_t2585711361 * get_displEnds2_43() const { return ___displEnds2_43; }
	inline List_1_t2585711361 ** get_address_of_displEnds2_43() { return &___displEnds2_43; }
	inline void set_displEnds2_43(List_1_t2585711361 * value)
	{
		___displEnds2_43 = value;
		Il2CppCodeGenWriteBarrier((&___displEnds2_43), value);
	}

	inline static int32_t get_offset_of_realSupPos_44() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___realSupPos_44)); }
	inline Vector2U5BU5D_t1457185986* get_realSupPos_44() const { return ___realSupPos_44; }
	inline Vector2U5BU5D_t1457185986** get_address_of_realSupPos_44() { return &___realSupPos_44; }
	inline void set_realSupPos_44(Vector2U5BU5D_t1457185986* value)
	{
		___realSupPos_44 = value;
		Il2CppCodeGenWriteBarrier((&___realSupPos_44), value);
	}

	inline static int32_t get_offset_of_requestId_45() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___requestId_45)); }
	inline int64_t get_requestId_45() const { return ___requestId_45; }
	inline int64_t* get_address_of_requestId_45() { return &___requestId_45; }
	inline void set_requestId_45(int64_t value)
	{
		___requestId_45 = value;
	}

	inline static int32_t get_offset_of_timedShowDisp_46() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___timedShowDisp_46)); }
	inline int32_t get_timedShowDisp_46() const { return ___timedShowDisp_46; }
	inline int32_t* get_address_of_timedShowDisp_46() { return &___timedShowDisp_46; }
	inline void set_timedShowDisp_46(int32_t value)
	{
		___timedShowDisp_46 = value;
	}

	inline static int32_t get_offset_of_VIBsup_47() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___VIBsup_47)); }
	inline FEAComponentDataU5BU5D_t1047043642* get_VIBsup_47() const { return ___VIBsup_47; }
	inline FEAComponentDataU5BU5D_t1047043642** get_address_of_VIBsup_47() { return &___VIBsup_47; }
	inline void set_VIBsup_47(FEAComponentDataU5BU5D_t1047043642* value)
	{
		___VIBsup_47 = value;
		Il2CppCodeGenWriteBarrier((&___VIBsup_47), value);
	}

	inline static int32_t get_offset_of_workingAnimation_48() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___workingAnimation_48)); }
	inline WorkingAnimation_t2928320557 * get_workingAnimation_48() const { return ___workingAnimation_48; }
	inline WorkingAnimation_t2928320557 ** get_address_of_workingAnimation_48() { return &___workingAnimation_48; }
	inline void set_workingAnimation_48(WorkingAnimation_t2928320557 * value)
	{
		___workingAnimation_48 = value;
		Il2CppCodeGenWriteBarrier((&___workingAnimation_48), value);
	}

	inline static int32_t get_offset_of_cm_49() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___cm_49)); }
	inline CanvasMesh_t1528759999 * get_cm_49() const { return ___cm_49; }
	inline CanvasMesh_t1528759999 ** get_address_of_cm_49() { return &___cm_49; }
	inline void set_cm_49(CanvasMesh_t1528759999 * value)
	{
		___cm_49 = value;
		Il2CppCodeGenWriteBarrier((&___cm_49), value);
	}

	inline static int32_t get_offset_of_gph_50() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___gph_50)); }
	inline GUIPaintHandler_t437174526 * get_gph_50() const { return ___gph_50; }
	inline GUIPaintHandler_t437174526 ** get_address_of_gph_50() { return &___gph_50; }
	inline void set_gph_50(GUIPaintHandler_t437174526 * value)
	{
		___gph_50 = value;
		Il2CppCodeGenWriteBarrier((&___gph_50), value);
	}

	inline static int32_t get_offset_of_ts_51() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___ts_51)); }
	inline TopologySelector_t1922582608 * get_ts_51() const { return ___ts_51; }
	inline TopologySelector_t1922582608 ** get_address_of_ts_51() { return &___ts_51; }
	inline void set_ts_51(TopologySelector_t1922582608 * value)
	{
		___ts_51 = value;
		Il2CppCodeGenWriteBarrier((&___ts_51), value);
	}

	inline static int32_t get_offset_of_gbg_52() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___gbg_52)); }
	inline ColorBarGUI_t1806409552 * get_gbg_52() const { return ___gbg_52; }
	inline ColorBarGUI_t1806409552 ** get_address_of_gbg_52() { return &___gbg_52; }
	inline void set_gbg_52(ColorBarGUI_t1806409552 * value)
	{
		___gbg_52 = value;
		Il2CppCodeGenWriteBarrier((&___gbg_52), value);
	}

	inline static int32_t get_offset_of_miniGUI_53() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___miniGUI_53)); }
	inline MiniGUI_t213183967 * get_miniGUI_53() const { return ___miniGUI_53; }
	inline MiniGUI_t213183967 ** get_address_of_miniGUI_53() { return &___miniGUI_53; }
	inline void set_miniGUI_53(MiniGUI_t213183967 * value)
	{
		___miniGUI_53 = value;
		Il2CppCodeGenWriteBarrier((&___miniGUI_53), value);
	}

	inline static int32_t get_offset_of_debug_54() { return static_cast<int32_t>(offsetof(GameController_t2330501625, ___debug_54)); }
	inline int32_t get_debug_54() const { return ___debug_54; }
	inline int32_t* get_address_of_debug_54() { return &___debug_54; }
	inline void set_debug_54(int32_t value)
	{
		___debug_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROLLER_T2330501625_H
#ifndef CANVASBORDER_T2952345418_H
#define CANVASBORDER_T2952345418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanvasBorder
struct  CanvasBorder_t2952345418  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material CanvasBorder::BorderMaterial
	Material_t340375123 * ___BorderMaterial_2;
	// PassiveBlock CanvasBorder::passiveBlock
	PassiveBlock_t513448541 * ___passiveBlock_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CanvasBorder::vertices
	List_1_t899420910 * ___vertices_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> CanvasBorder::uv
	List_1_t3628304265 * ___uv_5;
	// System.Collections.Generic.List`1<System.Int32> CanvasBorder::indices
	List_1_t128053199 * ___indices_6;

public:
	inline static int32_t get_offset_of_BorderMaterial_2() { return static_cast<int32_t>(offsetof(CanvasBorder_t2952345418, ___BorderMaterial_2)); }
	inline Material_t340375123 * get_BorderMaterial_2() const { return ___BorderMaterial_2; }
	inline Material_t340375123 ** get_address_of_BorderMaterial_2() { return &___BorderMaterial_2; }
	inline void set_BorderMaterial_2(Material_t340375123 * value)
	{
		___BorderMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___BorderMaterial_2), value);
	}

	inline static int32_t get_offset_of_passiveBlock_3() { return static_cast<int32_t>(offsetof(CanvasBorder_t2952345418, ___passiveBlock_3)); }
	inline PassiveBlock_t513448541 * get_passiveBlock_3() const { return ___passiveBlock_3; }
	inline PassiveBlock_t513448541 ** get_address_of_passiveBlock_3() { return &___passiveBlock_3; }
	inline void set_passiveBlock_3(PassiveBlock_t513448541 * value)
	{
		___passiveBlock_3 = value;
		Il2CppCodeGenWriteBarrier((&___passiveBlock_3), value);
	}

	inline static int32_t get_offset_of_vertices_4() { return static_cast<int32_t>(offsetof(CanvasBorder_t2952345418, ___vertices_4)); }
	inline List_1_t899420910 * get_vertices_4() const { return ___vertices_4; }
	inline List_1_t899420910 ** get_address_of_vertices_4() { return &___vertices_4; }
	inline void set_vertices_4(List_1_t899420910 * value)
	{
		___vertices_4 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_4), value);
	}

	inline static int32_t get_offset_of_uv_5() { return static_cast<int32_t>(offsetof(CanvasBorder_t2952345418, ___uv_5)); }
	inline List_1_t3628304265 * get_uv_5() const { return ___uv_5; }
	inline List_1_t3628304265 ** get_address_of_uv_5() { return &___uv_5; }
	inline void set_uv_5(List_1_t3628304265 * value)
	{
		___uv_5 = value;
		Il2CppCodeGenWriteBarrier((&___uv_5), value);
	}

	inline static int32_t get_offset_of_indices_6() { return static_cast<int32_t>(offsetof(CanvasBorder_t2952345418, ___indices_6)); }
	inline List_1_t128053199 * get_indices_6() const { return ___indices_6; }
	inline List_1_t128053199 ** get_address_of_indices_6() { return &___indices_6; }
	inline void set_indices_6(List_1_t128053199 * value)
	{
		___indices_6 = value;
		Il2CppCodeGenWriteBarrier((&___indices_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASBORDER_T2952345418_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (CanvasBorder_t2952345418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[5] = 
{
	CanvasBorder_t2952345418::get_offset_of_BorderMaterial_2(),
	CanvasBorder_t2952345418::get_offset_of_passiveBlock_3(),
	CanvasBorder_t2952345418::get_offset_of_vertices_4(),
	CanvasBorder_t2952345418::get_offset_of_uv_5(),
	CanvasBorder_t2952345418::get_offset_of_indices_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (CanvasMesh_t1528759999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[14] = 
{
	CanvasMesh_t1528759999::get_offset_of_sharedVertices_2(),
	CanvasMesh_t1528759999::get_offset_of_isDefShown_3(),
	CanvasMesh_t1528759999::get_offset_of_showDef_4(),
	CanvasMesh_t1528759999::get_offset_of_sizeX_5(),
	CanvasMesh_t1528759999::get_offset_of_sizeY_6(),
	CanvasMesh_t1528759999::get_offset_of_nDofX_7(),
	CanvasMesh_t1528759999::get_offset_of_nDofY_8(),
	CanvasMesh_t1528759999::get_offset_of_u_9(),
	CanvasMesh_t1528759999::get_offset_of_uvcoordinates_10(),
	CanvasMesh_t1528759999::get_offset_of_uvcoordinates2_11(),
	CanvasMesh_t1528759999::get_offset_of_coordinates_12(),
	CanvasMesh_t1528759999::get_offset_of_triangles_13(),
	CanvasMesh_t1528759999::get_offset_of_gc_14(),
	CanvasMesh_t1528759999::get_offset_of_isMeshInit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (GUIPaintHandler_t437174526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[13] = 
{
	GUIPaintHandler_t437174526::get_offset_of_NewTopology_2(),
	GUIPaintHandler_t437174526::get_offset_of_PenWidth_3(),
	GUIPaintHandler_t437174526::get_offset_of_updateTool_4(),
	GUIPaintHandler_t437174526::get_offset_of_painterCanvas_5(),
	GUIPaintHandler_t437174526::get_offset_of_gameController_6(),
	GUIPaintHandler_t437174526::get_offset_of_miniGui_7(),
	GUIPaintHandler_t437174526::get_offset_of_lastPoint_8(),
	GUIPaintHandler_t437174526::get_offset_of_isPainting_9(),
	GUIPaintHandler_t437174526::get_offset_of_material_10(),
	GUIPaintHandler_t437174526::get_offset_of_materialType_11(),
	GUIPaintHandler_t437174526::get_offset_of_selectedTool_12(),
	GUIPaintHandler_t437174526::get_offset_of_selectedPainter_13(),
	GUIPaintHandler_t437174526::get_offset_of_thinThickTool_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (Brush_t4066421658)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[3] = 
{
	Brush_t4066421658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (Tool_t2235229658)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[6] = 
{
	Tool_t2235229658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (UpdateTool_t905929733), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (PainterCanvas_t4160673718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[14] = 
{
	PainterCanvas_t4160673718::get_offset_of_width_2(),
	PainterCanvas_t4160673718::get_offset_of_height_3(),
	PainterCanvas_t4160673718::get_offset_of_texture_4(),
	PainterCanvas_t4160673718::get_offset_of_textureStress_5(),
	PainterCanvas_t4160673718::get_offset_of_passiveBlock_6(),
	PainterCanvas_t4160673718::get_offset_of_textureWidth_7(),
	PainterCanvas_t4160673718::get_offset_of_textureHeight_8(),
	PainterCanvas_t4160673718::get_offset_of_gameController_9(),
	PainterCanvas_t4160673718::get_offset_of_updateTime_10(),
	PainterCanvas_t4160673718::get_offset_of_updates_11(),
	PainterCanvas_t4160673718::get_offset_of_updateTimeTotal_12(),
	PainterCanvas_t4160673718::get_offset_of_ndopt_13(),
	PainterCanvas_t4160673718::get_offset_of_ndvis_14(),
	PainterCanvas_t4160673718::get_offset_of_linePainter_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (ThinThickTool_t2872687508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[6] = 
{
	ThinThickTool_t2872687508::get_offset_of_painterCanvas_0(),
	ThinThickTool_t2872687508::get_offset_of_passiveBlock_1(),
	ThinThickTool_t2872687508::get_offset_of_passiveBlockCopy_2(),
	ThinThickTool_t2872687508::get_offset_of_gameController_3(),
	ThinThickTool_t2872687508::get_offset_of_Thickening_4(),
	ThinThickTool_t2872687508::get_offset_of_guiPaintHandler_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (TileInfo_t2587709261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[10] = 
{
	TileInfo_t2587709261::get_offset_of_North_0(),
	TileInfo_t2587709261::get_offset_of_NorthEast_1(),
	TileInfo_t2587709261::get_offset_of_East_2(),
	TileInfo_t2587709261::get_offset_of_SouthEast_3(),
	TileInfo_t2587709261::get_offset_of_South_4(),
	TileInfo_t2587709261::get_offset_of_SouthWest_5(),
	TileInfo_t2587709261::get_offset_of_West_6(),
	TileInfo_t2587709261::get_offset_of_NorthWest_7(),
	TileInfo_t2587709261::get_offset_of_isTile_8(),
	TileInfo_t2587709261::get_offset_of_passiveBlock_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (DebugOnD_t2473991057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (DeviceInfo_t2768285361), -1, sizeof(DeviceInfo_t2768285361_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1917[7] = 
{
	DeviceInfo_t2768285361::get_offset_of__isAndroid_0(),
	DeviceInfo_t2768285361::get_offset_of__isIOS_1(),
	DeviceInfo_t2768285361::get_offset_of_dpi_2(),
	DeviceInfo_t2768285361::get_offset_of_computeCapabilities_3(),
	DeviceInfo_t2768285361::get_offset_of_mobileScreenSize_4(),
	DeviceInfo_t2768285361::get_offset_of__isWeb_5(),
	DeviceInfo_t2768285361_StaticFields::get_offset_of_deviceInfoInst_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (MobileScreenSize_t1575161839)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1918[4] = 
{
	MobileScreenSize_t1575161839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (DPI_t1419146022)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1919[3] = 
{
	DPI_t1419146022::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (ComputeCapabilities_t3016857551)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[4] = 
{
	ComputeCapabilities_t3016857551::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (ArrayExt_t2619957934), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (ColorExt_t86973071), -1, sizeof(ColorExt_t86973071_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1922[3] = 
{
	ColorExt_t86973071_StaticFields::get_offset_of_Material_1_0(),
	ColorExt_t86973071_StaticFields::get_offset_of_Material_2_1(),
	ColorExt_t86973071_StaticFields::get_offset_of_LightGray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (GUIExt_t2334906846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (HashtableExt_t3309791078), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (RectExt_t2723658462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (ElementMassMatrix_t1408435446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (ElementStiffnessMatrix_t3577980630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (FEA_t3250963177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[46] = 
{
	FEA_t3250963177::get_offset_of_running_6(),
	FEA_t3250963177::get_offset_of_destroyed_7(),
	FEA_t3250963177::get_offset_of_displs_8(),
	FEA_t3250963177::get_offset_of_displEnds_9(),
	FEA_t3250963177::get_offset_of_displs2_10(),
	FEA_t3250963177::get_offset_of_displEnds2_11(),
	FEA_t3250963177::get_offset_of_supports_12(),
	FEA_t3250963177::get_offset_of_nex_13(),
	FEA_t3250963177::get_offset_of_ney_14(),
	FEA_t3250963177::get_offset_of_nx_15(),
	FEA_t3250963177::get_offset_of_ny_16(),
	FEA_t3250963177::get_offset_of_nnodes_17(),
	FEA_t3250963177::get_offset_of_ndof_18(),
	FEA_t3250963177::get_offset_of_nelem_19(),
	FEA_t3250963177::get_offset_of_itercount_20(),
	FEA_t3250963177::get_offset_of_f_21(),
	FEA_t3250963177::get_offset_of_Element_materials_22(),
	FEA_t3250963177::get_offset_of_Element_topology_23(),
	FEA_t3250963177::get_offset_of_rhoPhysVal_24(),
	FEA_t3250963177::get_offset_of_nodeVal_25(),
	FEA_t3250963177::get_offset_of_nElPrNode_26(),
	FEA_t3250963177::get_offset_of_bMat_27(),
	FEA_t3250963177::get_offset_of_cMat_28(),
	FEA_t3250963177::get_offset_of_u_29(),
	FEA_t3250963177::get_offset_of_F_30(),
	FEA_t3250963177::get_offset_of_KE_1_31(),
	FEA_t3250963177::get_offset_of_KE_2_32(),
	FEA_t3250963177::get_offset_of_reducedKK_33(),
	FEA_t3250963177::get_offset_of_IsNewTopology_34(),
	FEA_t3250963177::get_offset_of_KK_35(),
	FEA_t3250963177::get_offset_of_LDL_36(),
	FEA_t3250963177::get_offset_of_FEA_Compress_37(),
	FEA_t3250963177::get_offset_of_Reduced_ndof_38(),
	FEA_t3250963177::get_offset_of_edof_reductor_39(),
	FEA_t3250963177::get_offset_of_edof_expander_40(),
	FEA_t3250963177::get_offset_of_u_reduced_41(),
	FEA_t3250963177::get_offset_of_F_reduced_42(),
	FEA_t3250963177::get_offset_of_E1_43(),
	FEA_t3250963177::get_offset_of_E2_44(),
	FEA_t3250963177::get_offset_of_nu1_45(),
	FEA_t3250963177::get_offset_of_nu2_46(),
	FEA_t3250963177::get_offset_of_loop_47(),
	FEA_t3250963177::get_offset_of_plotChoice_48(),
	FEA_t3250963177::get_offset_of_big_finger_49(),
	FEA_t3250963177::get_offset_of_ortho_move_50(),
	FEA_t3250963177::get_offset_of_turnOffSolver_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (FEAComponentData_t3249130875)+ sizeof (RuntimeObject), sizeof(FEAComponentData_t3249130875 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1929[5] = 
{
	FEAComponentData_t3249130875::get_offset_of_positionX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FEAComponentData_t3249130875::get_offset_of_positionY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FEAComponentData_t3249130875::get_offset_of_directionX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FEAComponentData_t3249130875::get_offset_of_directionY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FEAComponentData_t3249130875::get_offset_of_loadId_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (FEAGameData_t1628373741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[12] = 
{
	FEAGameData_t1628373741::get_offset_of_elementDensities_0(),
	FEAGameData_t1628373741::get_offset_of_id_1(),
	FEAGameData_t1628373741::get_offset_of_plotChoice_2(),
	FEAGameData_t1628373741::get_offset_of_Emax_3(),
	FEAGameData_t1628373741::get_offset_of_big_finger_4(),
	FEAGameData_t1628373741::get_offset_of_ortho_move_5(),
	FEAGameData_t1628373741::get_offset_of_turnOffSolver_6(),
	FEAGameData_t1628373741::get_offset_of_supports_7(),
	FEAGameData_t1628373741::get_offset_of_displs_8(),
	FEAGameData_t1628373741::get_offset_of_displEnds_9(),
	FEAGameData_t1628373741::get_offset_of_displs2_10(),
	FEAGameData_t1628373741::get_offset_of_displEnds2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (FEAGameResult_t3745188880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[8] = 
{
	FEAGameResult_t3745188880::get_offset_of_score_0(),
	FEAGameResult_t3745188880::get_offset_of_error_1(),
	FEAGameResult_t3745188880::get_offset_of_stressColor_2(),
	FEAGameResult_t3745188880::get_offset_of_time_3(),
	FEAGameResult_t3745188880::get_offset_of_minStress_4(),
	FEAGameResult_t3745188880::get_offset_of_maxStress_5(),
	FEAGameResult_t3745188880::get_offset_of_id_6(),
	FEAGameResult_t3745188880::get_offset_of_u_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (FEAMatrixCompress_t1508710683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[9] = 
{
	FEAMatrixCompress_t1508710683::get_offset_of_rhoPhys_0(),
	FEAMatrixCompress_t1508710683::get_offset_of_ney_1(),
	FEAMatrixCompress_t1508710683::get_offset_of_nex_2(),
	FEAMatrixCompress_t1508710683::get_offset_of_ny_3(),
	FEAMatrixCompress_t1508710683::get_offset_of_nx_4(),
	FEAMatrixCompress_t1508710683::get_offset_of_ndof_5(),
	FEAMatrixCompress_t1508710683::get_offset_of_ne_6(),
	FEAMatrixCompress_t1508710683::get_offset_of_ndof_reduced_7(),
	FEAMatrixCompress_t1508710683::get_offset_of_ActiveDof_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (FEAVibrate_t3015876168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[39] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FEAVibrate_t3015876168::get_offset_of_dt_12(),
	FEAVibrate_t3015876168::get_offset_of_max_norm_13(),
	FEAVibrate_t3015876168::get_offset_of_M_14(),
	FEAVibrate_t3015876168::get_offset_of_D_15(),
	FEAVibrate_t3015876168::get_offset_of_D_old_16(),
	FEAVibrate_t3015876168::get_offset_of_D_new_17(),
	FEAVibrate_t3015876168::get_offset_of_LHS_inv_18(),
	FEAVibrate_t3015876168::get_offset_of_RHS_Dold_19(),
	FEAVibrate_t3015876168::get_offset_of_RHS_D_20(),
	FEAVibrate_t3015876168::get_offset_of_R_int_21(),
	FEAVibrate_t3015876168::get_offset_of_ElementMaterials_22(),
	FEAVibrate_t3015876168::get_offset_of_edof_expander_23(),
	FEAVibrate_t3015876168::get_offset_of_edof_reductor_24(),
	FEAVibrate_t3015876168::get_offset_of_ndof_25(),
	FEAVibrate_t3015876168::get_offset_of_ney_26(),
	FEAVibrate_t3015876168::get_offset_of_nex_27(),
	FEAVibrate_t3015876168::get_offset_of_nelem_28(),
	FEAVibrate_t3015876168::get_offset_of_ndof_full_29(),
	FEAVibrate_t3015876168::get_offset_of_KK_30(),
	FEAVibrate_t3015876168::get_offset_of_CD_31(),
	FEAVibrate_t3015876168::get_offset_of_CD_old_32(),
	FEAVibrate_t3015876168::get_offset_of_displs2_dof_33(),
	FEAVibrate_t3015876168::get_offset_of_displs2_val_34(),
	FEAVibrate_t3015876168::get_offset_of_fdof_35(),
	FEAVibrate_t3015876168::get_offset_of_PlotChoice_36(),
	FEAVibrate_t3015876168::get_offset_of_destroyed_37(),
	FEAVibrate_t3015876168::get_offset_of_sendnexttimestep_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (SendNextTimestep_t1962693451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (TopologyOptimization_t326002099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[6] = 
{
	0,
	TopologyOptimization_t326002099::get_offset_of_Reduced_ndof_1(),
	TopologyOptimization_t326002099::get_offset_of_edof_reductor_2(),
	TopologyOptimization_t326002099::get_offset_of_notifyUpdateObjectiveFunction_3(),
	TopologyOptimization_t326002099::get_offset_of_getUpdatedTopologyElementDensity_4(),
	TopologyOptimization_t326002099::get_offset_of_Paused_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (FilterType_t787506034)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1936[3] = 
{
	FilterType_t787506034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (NotifyUpdateObjectiveFunction_t2430789460), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (GetUpdatedTopologyElementDensity_t3009975164), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (FontChange_t2675305062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[4] = 
{
	FontChange_t2675305062::get_offset_of_guiSkins_2(),
	FontChange_t2675305062::get_offset_of_fontSmall_3(),
	FontChange_t2675305062::get_offset_of_fontMedium_4(),
	FontChange_t2675305062::get_offset_of_fontLarge_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (Level_t2237665516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[13] = 
{
	Level_t2237665516::get_offset_of_name_0(),
	Level_t2237665516::get_offset_of_maxTime_1(),
	Level_t2237665516::get_offset_of_volumeFraction_2(),
	Level_t2237665516::get_offset_of_resolutionX_3(),
	Level_t2237665516::get_offset_of_resolutionY_4(),
	Level_t2237665516::get_offset_of_numberOfLoadCases_5(),
	Level_t2237665516::get_offset_of_loads_6(),
	Level_t2237665516::get_offset_of_supports_7(),
	Level_t2237665516::get_offset_of_displs_8(),
	Level_t2237665516::get_offset_of_displEnds_9(),
	Level_t2237665516::get_offset_of_displs2_10(),
	Level_t2237665516::get_offset_of_displEnds2_11(),
	Level_t2237665516::get_offset_of_voidElements_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (LevelComponentDispl_t1267792563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[5] = 
{
	LevelComponentDispl_t1267792563::get_offset_of_positionX_0(),
	LevelComponentDispl_t1267792563::get_offset_of_positionY_1(),
	LevelComponentDispl_t1267792563::get_offset_of_directionX_2(),
	LevelComponentDispl_t1267792563::get_offset_of_directionY_3(),
	LevelComponentDispl_t1267792563::get_offset_of_displid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (LevelComponentDispl2_t1471609523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[5] = 
{
	LevelComponentDispl2_t1471609523::get_offset_of_positionX_0(),
	LevelComponentDispl2_t1471609523::get_offset_of_positionY_1(),
	LevelComponentDispl2_t1471609523::get_offset_of_directionX_2(),
	LevelComponentDispl2_t1471609523::get_offset_of_directionY_3(),
	LevelComponentDispl2_t1471609523::get_offset_of_displid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (LevelComponentDisplEnd_t436353678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[5] = 
{
	LevelComponentDisplEnd_t436353678::get_offset_of_positionX_0(),
	LevelComponentDisplEnd_t436353678::get_offset_of_positionY_1(),
	LevelComponentDisplEnd_t436353678::get_offset_of_directionX_2(),
	LevelComponentDisplEnd_t436353678::get_offset_of_directionY_3(),
	LevelComponentDisplEnd_t436353678::get_offset_of_displid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (LevelComponentDisplEnd2_t2219483174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[5] = 
{
	LevelComponentDisplEnd2_t2219483174::get_offset_of_positionX_0(),
	LevelComponentDisplEnd2_t2219483174::get_offset_of_positionY_1(),
	LevelComponentDisplEnd2_t2219483174::get_offset_of_directionX_2(),
	LevelComponentDisplEnd2_t2219483174::get_offset_of_directionY_3(),
	LevelComponentDisplEnd2_t2219483174::get_offset_of_displid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (LevelComponentLoad_t253531484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[5] = 
{
	LevelComponentLoad_t253531484::get_offset_of_positionX_0(),
	LevelComponentLoad_t253531484::get_offset_of_positionY_1(),
	LevelComponentLoad_t253531484::get_offset_of_directionX_2(),
	LevelComponentLoad_t253531484::get_offset_of_directionY_3(),
	LevelComponentLoad_t253531484::get_offset_of_loadid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (LevelComponentSupport_t2849120049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[6] = 
{
	LevelComponentSupport_t2849120049::get_offset_of_positionX_0(),
	LevelComponentSupport_t2849120049::get_offset_of_positionY_1(),
	LevelComponentSupport_t2849120049::get_offset_of_directionX_2(),
	LevelComponentSupport_t2849120049::get_offset_of_directionY_3(),
	LevelComponentSupport_t2849120049::get_offset_of_rotation_4(),
	LevelComponentSupport_t2849120049::get_offset_of_fixedSupport_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (LevelDB_t1000439184), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (SelectedLevel_t4079635930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[1] = 
{
	SelectedLevel_t4079635930::get_offset_of_level_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (Topic_t1056265799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[3] = 
{
	Topic_t1056265799::get_offset_of_topicId_0(),
	Topic_t1056265799::get_offset_of_name_1(),
	Topic_t1056265799::get_offset_of_levels_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (GameController_t2330501625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[53] = 
{
	GameController_t2330501625::get_offset_of_level_2(),
	GameController_t2330501625::get_offset_of_paintCanvas_3(),
	GameController_t2330501625::get_offset_of_gameGUI_4(),
	GameController_t2330501625::get_offset_of_VIB_thread_5(),
	GameController_t2330501625::get_offset_of_threadRunner_6(),
	GameController_t2330501625::get_offset_of_FiniteElementAnalysis_7(),
	GameController_t2330501625::get_offset_of_FEA_VIB_8(),
	GameController_t2330501625::get_offset_of_GameResultUpdates_9(),
	GameController_t2330501625::get_offset_of_prefabSlidingSupport_10(),
	GameController_t2330501625::get_offset_of_prefabFixedSupport_11(),
	GameController_t2330501625::get_offset_of_prefabDispl_12(),
	GameController_t2330501625::get_offset_of_prefabDisplEnd_13(),
	GameController_t2330501625::get_offset_of_prefabDispl2_14(),
	GameController_t2330501625::get_offset_of_prefabDisplEnd2_15(),
	GameController_t2330501625::get_offset_of_plotChoice_16(),
	GameController_t2330501625::get_offset_of_Emax_17(),
	GameController_t2330501625::get_offset_of_noOfActiveBC_18(),
	GameController_t2330501625::get_offset_of_noOfActiveDispl_19(),
	GameController_t2330501625::get_offset_of_noOfActiveDispl2_20(),
	0,
	0,
	GameController_t2330501625::get_offset_of_noOfDispls2_23(),
	GameController_t2330501625::get_offset_of_noOfDisplEnds2_24(),
	GameController_t2330501625::get_offset_of_noOfDispls_25(),
	GameController_t2330501625::get_offset_of_noOfDisplEnds_26(),
	GameController_t2330501625::get_offset_of_VIBres_27(),
	GameController_t2330501625::get_offset_of_VIBinp_28(),
	GameController_t2330501625::get_offset_of_VIB_count_29(),
	GameController_t2330501625::get_offset_of_VIB_running_30(),
	GameController_t2330501625::get_offset_of_IsVIB_31(),
	GameController_t2330501625::get_offset_of_VIB_enabled_32(),
	GameController_t2330501625::get_offset_of_VIB_gravity_33(),
	GameController_t2330501625::get_offset_of_big_finger_34(),
	GameController_t2330501625::get_offset_of_ortho_move_35(),
	GameController_t2330501625::get_offset_of_tos_36(),
	GameController_t2330501625::get_offset_of_NewVIBData_37(),
	GameController_t2330501625::get_offset_of_HasSupports_38(),
	GameController_t2330501625::get_offset_of_supports_39(),
	GameController_t2330501625::get_offset_of_displs_40(),
	GameController_t2330501625::get_offset_of_displEnds_41(),
	GameController_t2330501625::get_offset_of_displs2_42(),
	GameController_t2330501625::get_offset_of_displEnds2_43(),
	GameController_t2330501625::get_offset_of_realSupPos_44(),
	GameController_t2330501625::get_offset_of_requestId_45(),
	GameController_t2330501625::get_offset_of_timedShowDisp_46(),
	GameController_t2330501625::get_offset_of_VIBsup_47(),
	GameController_t2330501625::get_offset_of_workingAnimation_48(),
	GameController_t2330501625::get_offset_of_cm_49(),
	GameController_t2330501625::get_offset_of_gph_50(),
	GameController_t2330501625::get_offset_of_ts_51(),
	GameController_t2330501625::get_offset_of_gbg_52(),
	GameController_t2330501625::get_offset_of_miniGUI_53(),
	GameController_t2330501625::get_offset_of_debug_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (NotifyEvaluationUpdate_t2480692004), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (Gravity_t379910146), -1, sizeof(Gravity_t379910146_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1952[6] = 
{
	Gravity_t379910146::get_offset_of_rotation_2(),
	Gravity_t379910146::get_offset_of_magnitude_3(),
	Gravity_t379910146::get_offset_of_eulerRotation_4(),
	Gravity_t379910146_StaticFields::get_offset_of_gravity_5(),
	Gravity_t379910146::get_offset_of_hasGyroscope_6(),
	Gravity_t379910146::get_offset_of_hasAccelerometer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (BoundingConstraintsGUI_t795470468), -1, sizeof(BoundingConstraintsGUI_t795470468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1953[19] = 
{
	BoundingConstraintsGUI_t795470468::get_offset_of_guiSkin_2(),
	BoundingConstraintsGUI_t795470468::get_offset_of_selectedMenu_3(),
	BoundingConstraintsGUI_t795470468::get_offset_of_selectedMenuVisible_4(),
	BoundingConstraintsGUI_t795470468::get_offset_of_textures_5(),
	BoundingConstraintsGUI_t795470468::get_offset_of_selected_textures_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	BoundingConstraintsGUI_t795470468::get_offset_of_tools_14(),
	BoundingConstraintsGUI_t795470468::get_offset_of_toolsMenu_15(),
	BoundingConstraintsGUI_t795470468_StaticFields::get_offset_of_selectedTool_16(),
	BoundingConstraintsGUI_t795470468::get_offset_of_penButtonPosition_17(),
	BoundingConstraintsGUI_t795470468::get_offset_of_selectedMenuColor_18(),
	BoundingConstraintsGUI_t795470468::get_offset_of_scale_19(),
	BoundingConstraintsGUI_t795470468::get_offset_of_offset_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (U3CAnimateMenuColorU3Ec__Iterator0_t889497008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[8] = 
{
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U3CtimeU3E__0_0(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U3CwhiteU3E__0_1(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U3CtransparentU3E__0_2(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_newValue_3(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U24this_4(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U24current_5(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U24disposing_6(),
	U3CAnimateMenuColorU3Ec__Iterator0_t889497008::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (ColorBarGUI_t1806409552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[14] = 
{
	ColorBarGUI_t1806409552::get_offset_of_textureWidth_2(),
	ColorBarGUI_t1806409552::get_offset_of_textureHeight_3(),
	ColorBarGUI_t1806409552::get_offset_of_texture_4(),
	ColorBarGUI_t1806409552::get_offset_of_positionBar_5(),
	ColorBarGUI_t1806409552::get_offset_of_positionMin_6(),
	ColorBarGUI_t1806409552::get_offset_of_positionMax_7(),
	ColorBarGUI_t1806409552::get_offset_of_maxVal_8(),
	ColorBarGUI_t1806409552::get_offset_of_minVal_9(),
	ColorBarGUI_t1806409552::get_offset_of_norm_10(),
	ColorBarGUI_t1806409552::get_offset_of_colVal_11(),
	ColorBarGUI_t1806409552::get_offset_of_unit_12(),
	ColorBarGUI_t1806409552::get_offset_of_plotUnits_13(),
	ColorBarGUI_t1806409552::get_offset_of_gc_14(),
	ColorBarGUI_t1806409552::get_offset_of_colorbarTextStyle_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (DrawMenuGUI_t2806387517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[16] = 
{
	DrawMenuGUI_t2806387517::get_offset_of_labelPosition_2(),
	DrawMenuGUI_t2806387517::get_offset_of_roundButtonPosition_3(),
	DrawMenuGUI_t2806387517::get_offset_of_penButtonPosition_4(),
	DrawMenuGUI_t2806387517::get_offset_of_densityValue_5(),
	DrawMenuGUI_t2806387517::get_offset_of_guiHandler_6(),
	DrawMenuGUI_t2806387517::get_offset_of_selectedTool_7(),
	DrawMenuGUI_t2806387517::get_offset_of_selectedBrush_8(),
	DrawMenuGUI_t2806387517::get_offset_of_guiSkin_9(),
	DrawMenuGUI_t2806387517::get_offset_of_textures_10(),
	DrawMenuGUI_t2806387517::get_offset_of_selected_textures_11(),
	DrawMenuGUI_t2806387517::get_offset_of_tools_12(),
	DrawMenuGUI_t2806387517::get_offset_of_selectedMenu_13(),
	DrawMenuGUI_t2806387517::get_offset_of_selectedMenuVisible_14(),
	DrawMenuGUI_t2806387517::get_offset_of_selectedMenuColor_15(),
	DrawMenuGUI_t2806387517::get_offset_of_scale_16(),
	DrawMenuGUI_t2806387517::get_offset_of_offset_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (U3CAnimateMenuColorU3Ec__Iterator0_t3839011888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[8] = 
{
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U3CtimeU3E__0_0(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U3CwhiteU3E__0_1(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U3CtransparentU3E__0_2(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_newValue_3(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U24this_4(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U24current_5(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U24disposing_6(),
	U3CAnimateMenuColorU3Ec__Iterator0_t3839011888::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (GameGUI_t2956616616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[13] = 
{
	GameGUI_t2956616616::get_offset_of_gui_2(),
	GameGUI_t2956616616::get_offset_of_scoreRect_3(),
	GameGUI_t2956616616::get_offset_of_level_4(),
	GameGUI_t2956616616::get_offset_of_gameController_5(),
	GameGUI_t2956616616::get_offset_of_score_6(),
	GameGUI_t2956616616::get_offset_of_paintHandler_7(),
	0,
	GameGUI_t2956616616::get_offset_of_debugRects_9(),
	GameGUI_t2956616616::get_offset_of_gameMenuGUI_10(),
	GameGUI_t2956616616::get_offset_of_boundingConstraintsGUI_11(),
	0,
	GameGUI_t2956616616::get_offset_of_meshRenderer_13(),
	GameGUI_t2956616616::get_offset_of_numberFormatInfo_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (GameMainMenuGUI_t1155257793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[3] = 
{
	GameMainMenuGUI_t1155257793::get_offset_of_texture_2(),
	GameMainMenuGUI_t1155257793::get_offset_of_plotChoices_3(),
	GameMainMenuGUI_t1155257793::get_offset_of_uScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (MaterialMenuGUI_t4221244765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[2] = 
{
	MaterialMenuGUI_t4221244765::get_offset_of_EmaxMat1_2(),
	MaterialMenuGUI_t4221244765::get_offset_of_EmaxMat2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (MiniGUI_t213183967), -1, sizeof(MiniGUI_t213183967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1961[72] = 
{
	MiniGUI_t213183967::get_offset_of_gph_2(),
	MiniGUI_t213183967::get_offset_of_sel_3(),
	MiniGUI_t213183967::get_offset_of_gc_4(),
	MiniGUI_t213183967::get_offset_of_cm_5(),
	MiniGUI_t213183967::get_offset_of_cbg_6(),
	MiniGUI_t213183967::get_offset_of_camera_7(),
	MiniGUI_t213183967::get_offset_of_Background_canvas_8(),
	MiniGUI_t213183967::get_offset_of_Background_graph_9(),
	MiniGUI_t213183967::get_offset_of_screenshot_10(),
	MiniGUI_t213183967::get_offset_of_selectedMenu_11(),
	MiniGUI_t213183967::get_offset_of_selectedTool_12(),
	MiniGUI_t213183967::get_offset_of_selectedMaterial_13(),
	MiniGUI_t213183967::get_offset_of_selectedBackground_14(),
	MiniGUI_t213183967::get_offset_of_plotType_15(),
	MiniGUI_t213183967::get_offset_of_meshLevel_16(),
	MiniGUI_t213183967::get_offset_of_oldPlotChoice_17(),
	MiniGUI_t213183967::get_offset_of_selectedPenSize_18(),
	MiniGUI_t213183967::get_offset_of_showDef_19(),
	MiniGUI_t213183967::get_offset_of_nBottons_20(),
	MiniGUI_t213183967::get_offset_of_nGUIChanges_21(),
	MiniGUI_t213183967::get_offset_of_grayBox_22(),
	MiniGUI_t213183967::get_offset_of_is_gravity_23(),
	MiniGUI_t213183967::get_offset_of_DisplayScreenshotMessage_24(),
	MiniGUI_t213183967::get_offset_of_MenuBlue_25(),
	MiniGUI_t213183967::get_offset_of_topButton_26(),
	MiniGUI_t213183967::get_offset_of_menuButton_27(),
	MiniGUI_t213183967::get_offset_of_subMenuButton_28(),
	MiniGUI_t213183967::get_offset_of_showTool_29(),
	MiniGUI_t213183967::get_offset_of_showLogo_30(),
	MiniGUI_t213183967::get_offset_of_colorBarPos_31(),
	MiniGUI_t213183967::get_offset_of_messagePos_32(),
	MiniGUI_t213183967::get_offset_of_warningPos_33(),
	MiniGUI_t213183967::get_offset_of_fac_34(),
	MiniGUI_t213183967::get_offset_of_NewTopology_35(),
	MiniGUI_t213183967::get_offset_of_TooltipStyle_36(),
	MiniGUI_t213183967::get_offset_of_buttonStyle_37(),
	MiniGUI_t213183967::get_offset_of_labelStyle_38(),
	MiniGUI_t213183967::get_offset_of_messageStyle_39(),
	MiniGUI_t213183967::get_offset_of_DrawTooltipStyle_40(),
	MiniGUI_t213183967::get_offset_of_aboutTextStyle_41(),
	MiniGUI_t213183967::get_offset_of_linkStyle_42(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MiniGUI_t213183967_StaticFields::get_offset_of_selectedBcTool_51(),
	MiniGUI_t213183967_StaticFields::get_offset_of_selectedBcMenu_52(),
	MiniGUI_t213183967::get_offset_of_bcToolsMenu_53(),
	MiniGUI_t213183967::get_offset_of_fingerToolsMenu_54(),
	MiniGUI_t213183967::get_offset_of_mainMenuTextures_55(),
	MiniGUI_t213183967::get_offset_of_drawTextures_56(),
	MiniGUI_t213183967::get_offset_of_bcTextures_57(),
	MiniGUI_t213183967::get_offset_of_fingerTextures_58(),
	MiniGUI_t213183967::get_offset_of_helpTextures_59(),
	MiniGUI_t213183967::get_offset_of_topLogoTexture_60(),
	MiniGUI_t213183967::get_offset_of_aboutPanelRect_61(),
	MiniGUI_t213183967::get_offset_of_aboutPanelRectGroup_62(),
	MiniGUI_t213183967::get_offset_of_helpPanelRectGroup_63(),
	MiniGUI_t213183967::get_offset_of_helpTitleRect_64(),
	MiniGUI_t213183967::get_offset_of_helpTextRect_65(),
	MiniGUI_t213183967::get_offset_of_helpTextureRect_66(),
	MiniGUI_t213183967::get_offset_of_grayBoxRect_67(),
	MiniGUI_t213183967::get_offset_of_aboutText_68(),
	MiniGUI_t213183967::get_offset_of_helpTitle_69(),
	MiniGUI_t213183967::get_offset_of_helpText_70(),
	MiniGUI_t213183967::get_offset_of_scrollPosition_71(),
	MiniGUI_t213183967::get_offset_of_helpscrollPosition_72(),
	MiniGUI_t213183967::get_offset_of_FingerID_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (SelectionAddNewCompState_t2995677980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[1] = 
{
	SelectionAddNewCompState_t2995677980::get_offset_of_selector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (SelectionDistibuteState_t399974760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[1] = 
{
	SelectionDistibuteState_t399974760::get_offset_of_selector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (SelectionFingerState_t629756812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[2] = 
{
	SelectionFingerState_t629756812::get_offset_of_selector_5(),
	SelectionFingerState_t629756812::get_offset_of_touchDict_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (SelectionMoveState_t4022857977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[1] = 
{
	SelectionMoveState_t4022857977::get_offset_of_selector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (SelectionMultiDeleteState_t3644334706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[1] = 
{
	SelectionMultiDeleteState_t3644334706::get_offset_of_selector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (SelectionNoneState_t731836665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[1] = 
{
	SelectionNoneState_t731836665::get_offset_of_selector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (SelectionRotateState_t2962112176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[4] = 
{
	SelectionRotateState_t2962112176::get_offset_of_angleOffset_5(),
	SelectionRotateState_t2962112176::get_offset_of_selector_6(),
	SelectionRotateState_t2962112176::get_offset_of_angle_7(),
	SelectionRotateState_t2962112176::get_offset_of_angleRounded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (SelectionStateAbstract_t1927371580), -1, sizeof(SelectionStateAbstract_t1927371580_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1969[4] = 
{
	SelectionStateAbstract_t1927371580::get_offset_of_selector_1(),
	SelectionStateAbstract_t1927371580_StaticFields::get_offset_of_offset_2(),
	SelectionStateAbstract_t1927371580_StaticFields::get_offset_of_topComponentTransform_3(),
	SelectionStateAbstract_t1927371580_StaticFields::get_offset_of_topComponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (SelectionVectorState_t889007370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[4] = 
{
	SelectionVectorState_t889007370::get_offset_of_angleOffset_5(),
	SelectionVectorState_t889007370::get_offset_of_selector_6(),
	SelectionVectorState_t889007370::get_offset_of_angle_7(),
	SelectionVectorState_t889007370::get_offset_of_angleRounded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (TopologyComponent_t4181154066), -1, sizeof(TopologyComponent_t4181154066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1971[24] = 
{
	TopologyComponent_t4181154066::get_offset_of_material_2(),
	TopologyComponent_t4181154066::get_offset_of_initialColor_3(),
	TopologyComponent_t4181154066::get_offset_of_defaultColor_4(),
	TopologyComponent_t4181154066_StaticFields::get_offset_of_selectedColor_5(),
	TopologyComponent_t4181154066::get_offset_of_topologyComponentType_6(),
	TopologyComponent_t4181154066::get_offset_of_loadId_7(),
	TopologyComponent_t4181154066::get_offset_of_displNo_8(),
	TopologyComponent_t4181154066::get_offset_of_support_9(),
	TopologyComponent_t4181154066::get_offset_of_displ_10(),
	TopologyComponent_t4181154066::get_offset_of_displEnd_11(),
	TopologyComponent_t4181154066::get_offset_of_displ2_12(),
	TopologyComponent_t4181154066::get_offset_of_displEnd2_13(),
	TopologyComponent_t4181154066::get_offset_of_hatchScale_14(),
	TopologyComponent_t4181154066::get_offset_of_hatchingMesh_15(),
	TopologyComponent_t4181154066::get_offset_of_hatchingObject_16(),
	TopologyComponent_t4181154066::get_offset_of_hatchingMaterial_17(),
	TopologyComponent_t4181154066::get_offset_of_lineObject_18(),
	TopologyComponent_t4181154066::get_offset_of_lineGameObjects_19(),
	TopologyComponent_t4181154066::get_offset_of_selectorComponent_20(),
	TopologyComponent_t4181154066::get_offset_of_selectorComponents_21(),
	TopologyComponent_t4181154066::get_offset_of_textureUpdater_22(),
	TopologyComponent_t4181154066::get_offset_of_componentSizeValue_23(),
	TopologyComponent_t4181154066::get_offset_of_scaleValue_24(),
	TopologyComponent_t4181154066::get_offset_of_forceMagnitude_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (TopologyComponentType_t1442463493)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[8] = 
{
	TopologyComponentType_t1442463493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (TopologySelector_t1922582608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[7] = 
{
	TopologySelector_t1922582608::get_offset_of_mainCamera_2(),
	TopologySelector_t1922582608::get_offset_of_displEndComp_3(),
	TopologySelector_t1922582608::get_offset_of_textureUpdater_4(),
	TopologySelector_t1922582608::get_offset_of_topologyApplication_5(),
	TopologySelector_t1922582608::get_offset_of_gameMenuGUI_6(),
	TopologySelector_t1922582608::get_offset_of_cm_7(),
	TopologySelector_t1922582608::get_offset_of__state_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (MathExtra_t4221770606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (DoubleLongUnion_t1461655328)+ sizeof (RuntimeObject), sizeof(DoubleLongUnion_t1461655328 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[2] = 
{
	DoubleLongUnion_t1461655328::get_offset_of_d_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleLongUnion_t1461655328::get_offset_of_tmp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (MatrixCCS_t3519621953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[8] = 
{
	MatrixCCS_t3519621953::get_offset_of_ncols_0(),
	MatrixCCS_t3519621953::get_offset_of_nrows_1(),
	MatrixCCS_t3519621953::get_offset_of_mat_2(),
	MatrixCCS_t3519621953::get_offset_of_nnz_3(),
	MatrixCCS_t3519621953::get_offset_of_cols_4(),
	MatrixCCS_t3519621953::get_offset_of_ind_5(),
	MatrixCCS_t3519621953::get_offset_of_val_6(),
	MatrixCCS_t3519621953::get_offset_of_diag_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (MVector_t2821193744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[2] = 
{
	MVector_t2821193744::get_offset_of_nval_0(),
	MVector_t2821193744::get_offset_of_vals_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (SparseLDL_t1701177157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[11] = 
{
	SparseLDL_t1701177157::get_offset_of_n_0(),
	SparseLDL_t1701177157::get_offset_of_lnz_1(),
	SparseLDL_t1701177157::get_offset_of_Parent_2(),
	SparseLDL_t1701177157::get_offset_of_Lp_3(),
	SparseLDL_t1701177157::get_offset_of_Lnz_4(),
	SparseLDL_t1701177157::get_offset_of_Flag_5(),
	SparseLDL_t1701177157::get_offset_of_Li_6(),
	SparseLDL_t1701177157::get_offset_of_Lx_7(),
	SparseLDL_t1701177157::get_offset_of_D_8(),
	SparseLDL_t1701177157::get_offset_of_Y_9(),
	SparseLDL_t1701177157::get_offset_of_Pattern_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (Svec_t1806837960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[3] = 
{
	Svec_t1806837960::get_offset_of_vals_0(),
	Svec_t1806837960::get_offset_of_indx_1(),
	Svec_t1806837960::get_offset_of_len_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (Vec2i_t1193278988)+ sizeof (RuntimeObject), sizeof(Vec2i_t1193278988 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1980[2] = 
{
	Vec2i_t1193278988::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2i_t1193278988::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (Menu_t2559899777)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[23] = 
{
	Menu_t2559899777::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (MiniJSON_t4198869380), -1, sizeof(MiniJSON_t4198869380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1982[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MiniJSON_t4198869380_StaticFields::get_offset_of_lastErrorIndex_13(),
	MiniJSON_t4198869380_StaticFields::get_offset_of_lastDecode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (MiniJsonExtensions_t1107891468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (MouseWrapper_t4049244429), -1, sizeof(MouseWrapper_t4049244429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1984[5] = 
{
	MouseWrapper_t4049244429_StaticFields::get_offset_of_lastFrame_0(),
	MouseWrapper_t4049244429_StaticFields::get_offset_of_mouseButtonDown_1(),
	MouseWrapper_t4049244429_StaticFields::get_offset_of_mouseButtonUp_2(),
	MouseWrapper_t4049244429_StaticFields::get_offset_of_mouseButton_3(),
	MouseWrapper_t4049244429_StaticFields::get_offset_of_mousePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (PassiveBlock_t513448541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[8] = 
{
	PassiveBlock_t513448541::get_offset_of_width_0(),
	PassiveBlock_t513448541::get_offset_of_height_1(),
	PassiveBlock_t513448541::get_offset_of_subdivisions_2(),
	PassiveBlock_t513448541::get_offset_of_block_3(),
	PassiveBlock_t513448541::get_offset_of_blockEmpty_4(),
	PassiveBlock_t513448541::get_offset_of_hasPassiveVoid_5(),
	PassiveBlock_t513448541::get_offset_of_overwriteActiveEmpty_6(),
	PassiveBlock_t513448541::get_offset_of_elementDensities_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (ElementType_t1217679599)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[6] = 
{
	ElementType_t1217679599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (ScreenRotation_t1262145093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[1] = 
{
	ScreenRotation_t1262145093::get_offset_of_autoRotate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (Screenshot_t3680149077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[4] = 
{
	Screenshot_t3680149077::get_offset_of_mgui_2(),
	Screenshot_t3680149077::get_offset_of_Flash_image_3(),
	Screenshot_t3680149077::get_offset_of_isFlash_4(),
	Screenshot_t3680149077::get_offset_of_FlashCol_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U3CFlashScreenU3Ec__Iterator0_t2841660896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[4] = 
{
	U3CFlashScreenU3Ec__Iterator0_t2841660896::get_offset_of_U24this_0(),
	U3CFlashScreenU3Ec__Iterator0_t2841660896::get_offset_of_U24current_1(),
	U3CFlashScreenU3Ec__Iterator0_t2841660896::get_offset_of_U24disposing_2(),
	U3CFlashScreenU3Ec__Iterator0_t2841660896::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (U3CEnableMessageU3Ec__Iterator1_t2457876903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[3] = 
{
	U3CEnableMessageU3Ec__Iterator1_t2457876903::get_offset_of_U24current_0(),
	U3CEnableMessageU3Ec__Iterator1_t2457876903::get_offset_of_U24disposing_1(),
	U3CEnableMessageU3Ec__Iterator1_t2457876903::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (SolverRes_t1568196152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[2] = 
{
	SolverRes_t1568196152::get_offset_of_width_0(),
	SolverRes_t1568196152::get_offset_of_height_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (TextureDebug_t1588131437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (Assert_t2674897628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (TestMatrixCCS_t895833889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (TestMatrixCompress_t925154071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[4] = 
{
	TestMatrixCompress_t925154071::get_offset_of_input_ActDof_0(),
	TestMatrixCompress_t925154071::get_offset_of_exp_output_ActDof_1(),
	TestMatrixCompress_t925154071::get_offset_of_exp_output_GenReduc_2(),
	TestMatrixCompress_t925154071::get_offset_of_exp_output_GenExpan_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (TestAll_t3182214224), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
